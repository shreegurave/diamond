$(document).ready(function() {
	countKachoNumber();
	countSarin();
	polishCount();
	countPolishRepairing();
	countKapanJama();
});

/**
 * 
 */
$(document).on("change", ".kacho_number_n", function() { countKachoNumber(); });

$(document).on("change", ".kacho_number_v", function() { countKachoNumber(); });

function countKachoNumber()
{
	$("#kn_total_n").val( calculation("kacho_number_n") );
    $("#kn_total_v").val( calculation("kacho_number_v") );
    
    var saij = ( $("#kn_total_n").val() / $("#kn_total_v").val() );
    
    if( saij == 'Infinity' ) { $("#kn_saij").val( 0 ); }
    else if( !isNaN( saij ) ) { $("#kn_saij").val( saij.toFixed(2) ); }
    else { $("#kn_saij").val( 0 ); }
    
    var takavari = ( $("#kn_total_v").val() / $("#kWeight").text() ) * 100;
    if( takavari == 'Infinity' ) { $("#kn_takavari").val( 0 ); }
    else if( !isNaN( takavari ) ) { $("#kn_takavari").val( takavari.toFixed(2) ); }
    else { $("#kn_takavari").val( 0 ); }
}

/**
 * 
 */
$(document).on("change", ".s_nung", function() { countSarin(); });

$(document).on("change", ".s_weight", function() { countSarin(); });

function countSarin()
{
	$("#sc_nung").val( calculation("s_nung") );
    $("#sc_weight").val( calculation("s_weight") );
    
    var saij = ( $("#sc_nung").val() / $("#sc_weight").val() );
    if( saij == 'Infinity' ) { $("#sc_saij").val( 0 ); }
    else if( !isNaN( saij ) ) { $("#sc_saij").val( saij.toFixed(2) ); }
    else { $("#sc_saij").val( 0 ); }
    
    var takavari = ( $("#sc_weight").val() / $("#kWeight").text() ) * 100;
    if( takavari == 'Infinity' ) { $("#sc_takavari").val( 0 ); }
    else if( !isNaN( takavari ) ) { $("#sc_takavari").val( takavari.toFixed(2) ); }
    else { $("#sc_takavari").val( 0 ); }
}

/**
 * 
 */
$(document).on("change", ".p_nung", function() { polishCount(); });

$(document).on("change", ".p_weight", function() { polishCount(); });

$(document).on("change", ".p_4p_ghat_nung", function() { polishCount(); });

$(document).on("change", ".p_4p_weight", function() { polishCount(); });

$(document).on("change", ".p_4p_t_k", function() { polishCount(); });

$(document).on("change", ".p_4p_ghat_variation", function() { polishCount(); });

$(document).on("change", ".p_tbl_nung", function() { polishCount(); });

$(document).on("change", ".p_tbl_weight", function() { polishCount(); });

$(document).on("change", ".p_tly_nung", function() { polishCount(); });

$(document).on("change", ".p_tly_weight", function() { polishCount(); });

$(document).on("change", ".p_m_nung", function() { polishCount(); });

$(document).on("change", ".p_m_weight", function() { polishCount(); });

$(document).on("change", ".p_tk_nung", function() { polishCount(); });

$(document).on("change", ".p_tk_weight", function() { polishCount(); });


function polishCount()
{
	$("#pc_nung").val( calculation("p_nung") );
	$("#pc_weight").val( calculation("p_weight") );
	$("#pc_4p_g_nung").val( calculation("p_4p_ghat_nung") );
	$("#pc_4p_weight").val( calculation("p_4p_weight") );
	$("#pc_4p_tk").val( calculation("p_4p_t_k") );
	$("#pc_4p_variation").val( calculation("p_4p_ghat_variation") );
	$("#pc_tbl_nung").val( calculation("p_tbl_nung") );
	$("#pc_tbl_weight").val( calculation("p_tbl_weight") );
	$("#pc_tbl_tk").val( calculation("p_tbl_tk") );
	$("#pc_tly_nung").val( calculation("p_tly_nung") );
	$("#pc_tly_weight").val( calculation("p_tly_weight") );
	$("#pc_tly_tk").val( calculation("p_tly_tk") );
	$("#pc_m_nung").val( calculation("p_m_nung") );
	$("#pc_m_weight").val( calculation("p_m_weight") );
	$("#pc_m_tk").val( calculation("p_m_tk") );
	$("#pc_tk_nung").val( calculation("p_tk_nung") );
	$("#pc_tk_weight").val( calculation("p_tk_weight") );
	$("#pc_tk_tk").val( calculation("p_tk_tk") );
	$("#pc_nung").val( calculation("p_nung") );

	var pc_4p_takavari = ( $("#pc_4p_weight").val() / $("#pc_weight").val() ) * 100;
    if( pc_4p_takavari == 'Infinity' ) { $("#pc_4p_takavari").val( 0 ); }
    else if( !isNaN( pc_4p_takavari ) ) { $("#pc_4p_takavari").val( pc_4p_takavari.toFixed(2) ); }
    else { $("#pc_4p_takavari").val( 0 ); }
    
    var pc_tbl_takavari = ( $("#pc_tbl_weight").val() / $("#pc_4p_weight").val() ) * 100;
    if( pc_tbl_takavari == 'Infinity' ) { $("#pc_tbl_takavari").val( 0 ); }
    else if( !isNaN( pc_tbl_takavari ) ) { $("#pc_tbl_takavari").val( pc_tbl_takavari.toFixed(2) ); }
    else { $("#pc_tbl_takavari").val( 0 ); }
    
    var pc_tly_takavari = ( $("#pc_tly_weight").val() / $("#pc_tbl_weight").val() ) * 100;
    if( pc_tly_takavari == 'Infinity' ) { $("#pc_tly_takavari").val( 0 ); }
    else if( !isNaN( pc_tly_takavari ) ) { $("#pc_tly_takavari").val( pc_tly_takavari.toFixed(2) ); }
    else { $("#pc_tly_takavari").val( 0 ); }
    
    var pc_m_takavari = ( $("#pc_m_weight").val() / $("#pc_tly_weight").val() ) * 100;
    if( pc_m_takavari == 'Infinity' ) { $("#pc_m_takavari").val( 0 ); }
    else if( !isNaN( pc_m_takavari ) ) { $("#pc_m_takavari").val( pc_m_takavari.toFixed(2) ); }
    else { $("#pc_m_takavari").val( 0 ); }
    
    var pc_tk_takavari = ( $("#pc_tk_weight").val() / $("#pc_m_weight").val() ) * 100;
    if( pc_tk_takavari == 'Infinity' ) { $("#pc_tk_takavari").val( 0 ); }
    else if( !isNaN( pc_tk_takavari ) ) { $("#pc_tk_takavari").val( pc_tk_takavari.toFixed(2) ); }
    else { $("#pc_tk_takavari").val( 0 ); }
}

/**
 * 
 */
$(document).on("change", ".kj_kapan", function() { countKapanJama(); });

$(document).on("change", ".kj_nung", function() { countKapanJama(); });

$(document).on("change", ".kj_gol", function() { countKapanJama(); });

$(document).on("change", ".kj_singal_1", function() { countKapanJama(); });

$(document).on("change", ".kj_singal_2", function() { countKapanJama(); });

$(document).on("change", ".kj_selection", function() { countKapanJama(); });

$(document).on("change", ".kj_choki_1", function() { countKapanJama(); });

$(document).on("change", ".kj_choki_2", function() { countKapanJama(); });

$(document).on("change", ".kj_palchu", function() { countKapanJama(); });

$(document).on("change", ".kj_uchu_gol", function() { countKapanJama(); });

$(document).on("change", ".kj_color_out", function() { countKapanJama(); });

$(document).on("change", ".kj_total_out", function() { countKapanJama(); });

$(document).on("change", ".kj_out", function() { countKapanJama(); });

$(document).on("change", ".kj_chur", function() { countKapanJama(); });

function countKapanJama()
{
	$("#kjc_kapan").val( calculation("kj_kapan") );
    $("#kjc_nung").val( calculation("kj_nung") );
    $("#kjc_gol").val( calculation("kj_gol") );
    $("#kjc_singal_1").val( calculation("kj_singal_1") );
    $("#kjc_singal_2").val( calculation("kj_singal_2") );
    $("#kjc_selection").val( calculation("kj_selection") );
    $("#kjc_choki_1").val( calculation("kj_choki_1") );
    $("#kjc_choki_2").val( calculation("kj_choki_2") );
    $("#kjc_palchu").val( calculation("kj_palchu") );
    $("#kjc_uchu_gol").val( calculation("kj_uchu_gol") );
    $("#kjc_color_out").val( calculation("kj_color_out") );
    $("#kjc_total_out").val( calculation("kj_total_out") );
    $("#kjc_out").val( calculation("kj_out") );
    $("#kjc_chur").val( calculation("kj_chur") );
}

/**
 * 
 */
$(document).on("change", ".apl_than", function() { countPolishRepairing(); });

$(document).on("change", ".apl_weight", function() { countPolishRepairing(); });

$(document).on("change", ".avl_than", function() { countPolishRepairing(); });

$(document).on("change", ".avl_weight", function() { countPolishRepairing(); });

function countPolishRepairing()
{
	$("#apl_than").val( calculation("apl_than") );
    $("#apl_weight").val( calculation("apl_weight") );
    $("#avl_than").val( calculation("avl_than") );
    $("#avl_weight").val( calculation("avl_weight") );
}

/**
 * 
 */
$(document).on("change", ".lagath_count", function() { countLagath(); });

function countLagath()
{
	$("#lagath_count").val( calculation("lagath_count") );
}

/**
 * 
 */
$(document).on("change", ".l_charni_total", function() { countCharniTotal(); });

function countCharniTotal()
{
	$("#l_charni_total").val( calculation("l_charni_total") );
}


/**
 * 
 */
$(document).on("change", ".soing_nung", function() { countSoing(); });

$(document).on("change", ".soing_weight", function() { countSoing(); });

$(document).on("change", ".soing_avl_weight", function() { countSoing(); });

$(document).on("change", ".soing_ghat", function() { countSoing(); });

function countSoing()
{
	$("#soing_nung").val( calculation("soing_nung") );
    $("#soing_weight").val( calculation("soing_weight") );
    $("#soing_avl_weight").val( calculation("soing_avl_weight") );
    $("#soing_ghat").val( calculation("soing_ghat") );
    
    var takavari = ( $("#soing_ghat").val() / $("#soing_weight").val() ) * 100;
    if( takavari == 'Infinity' ) { $("#soing_takavari").val( 0 ); }
    else if( !isNaN( takavari ) ) { $("#soing_takavari").val( takavari.toFixed(2) ); }
    else { $("#soing_takavari").val( 0 ); }
}

/**
 * 
 */
$(document).on("change", ".sabka_nung", function() { countSabka(); });

$(document).on("change", ".sabka_weight", function() { countSabka(); });

$(document).on("change", ".sabka_avl_weight", function() { countSabka(); });

$(document).on("change", ".sabka_ghat", function() { countSabka(); });

function countSabka()
{
	$("#sabka_nung").val( calculation("sabka_nung") );
    $("#sabka_weight").val( calculation("sabka_weight") );
    $("#sabka_avl_weight").val( calculation("sabka_avl_weight") );
    $("#sabka_ghat").val( calculation("sabka_ghat") );
    
    var takavari = ( $("#sabka_ghat").val() / $("#sabka_weight").val() ) * 100;
    if( takavari == 'Infinity' ) { $("#sabka_takavari").val( 0 ); }
    else if( !isNaN( takavari ) ) { $("#sabka_takavari").val( takavari.toFixed(2) ); }
    else { $("#sabka_takavari").val( 0 ); }
}

function calculation( cal_class )
{
	var total = 0; 
	$("."+cal_class).each(function(){
		if ( $(this).val().indexOf('-') > -1 )
		{
			total -= -$(this).val();
		}
		else
		{
			total += +$(this).val();
		}
    });
	
	return total;
}
