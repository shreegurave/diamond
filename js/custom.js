//varibale specifies is to hide call me box
var is_hide_call_me = false; 

//maintain slideup events of livehelp it is required because livehelp are connected with callme back and other this kind of events
var is_hide_livehelp = false;

$(document).ready(function(){	
	//roll over text input on call me back
	var is_box_called = false;
	
	$(document).on({
		mouseenter: function () {
			//below variable set to false so that hideCallMe function set on Timeout will know that hide is not required			
			is_hide_call_me = false;
			
			//below variable set to false so that hideCallMe function set on Timeout will know that hide livehelp is not required			
			is_hide_livehelp = false;
			
			if( is_box_called == false && $( this ).hasClass( 'callMeBack' ) )
			{
				var loc = base_url + 'home/callMeBack';
				form_data = { x:$( '.callMeBack' ).position().left , y:$( '.callMeBack' ).position().top };
				$.get(loc, form_data, function(data){
					$(document.body).append( data );
					is_box_called = true;
				});
			}
			else
			{
				if( $('#callmebox').length > 0 )
				{ $('#callmebox').show(); }	
			}
		},
		mouseleave: function () {
			if( $('#callmebox').length > 0 )
			{
				is_hide_call_me = true;
				
				if( !$( this ).hasClass( 'callMeBack' ) )
				{ is_hide_livehelp = true; }
				
				setTimeout( function() { hideCallMe(); }, 500);	
			}
		}
	}, '.callMeBack, #callmebox');
	//////////////////////////////////////
	
	//skype ID on skype hoverr
	var is_skype_id = false;
	$(document).on({
		mouseenter: function () {
			
			if( is_skype_id == false )
			{
				var loc = base_url + 'home/skype';
				form_data = { x:$( '.skype' ).position().left , y:$( '.skype' ).position().top };
				$.get(loc, form_data, function(data){
					$(document.body).append( data );
					is_skype_id = true;
				});
			}
			else
			{
				if( $('#skype_id').length > 0 )
				{ $('#skype_id').show(); }	
			}
		},
		mouseleave: function () {
			if( $('#skype_id').length > 0 )
			{
				$('#skype_id').hide();
			}
		}
	}, '.skype');
	
	//Close button:		
	$(".close").live('click',function () 
	{
		$(this).parent().slideUp(400);
			return false;
	});
	
	/* Compare Size Popup */
	$('.popup_compare').live('click',function(){
		$('#popup_compare_size').show();
	});
	
	//// navigation motions ////
	$("#navi li").has("ul").hover(function(){
			$(this).addClass("current").children("ul").fadeIn('3000');
		}, function() {
			$(this).removeClass("current").children("ul").stop(true, true).fadeOut('3000');
	});
	
	//new navigation menu changes on 24042014
	$(".siteInspirationItem").has("ul").hover(function(){
			$(this).addClass("current").children("div").show();
		}, function() {
			$(this).removeClass("current").children("div").fadeOut('2000');
	});
	
	

	$("<select />").appendTo("#navi");

	$("<option />", {
	   "selected": "selected",
	   "value"   : "",
	   "text"    : "Navigation Menu"
	}).appendTo("#navi select");

	$("#navi ul li a").each(function() {
		var el = $(this);
		$("<option />", {
			"value"   : el.attr("href"),
			"text"    : el.text()
		}).appendTo("#navi select");
	});
		
	$("#navi select").change(function() {
		window.location = $(this).find("option:selected").val();
	});
	//// End navigation motions ////
	
	// header live help //
	 $(".liLiveHelp").hover(function () {
		$(".divSocialIcon", this).stop().slideDown();
  	 }, function () {
		 
		 //below variable set to true so that hideCallMe function set on Timeout will know that hide livehelp is required
		 is_hide_livehelp = true;
		 
		 if( $('#callmebox').length > 0 && !$('#callmebox').is(':visible') )
		 {
			$(".divSocialIcon", this).slideUp();
		 }
 	});
	
	// onclick open live chat popup
	$('.live-chat').live('click',function(){
		$('.perrian_livechat').trigger("click");
	});
	$('.live-chat-au').live('click',function(){
		$zopim.livechat.window.toggle();
	});
	
	// mouse over div box for fetching product detail on listing pages
	$(document).on({
		
		mouseenter: function () {
			//deny hover event if user agent is from mobile like device
			if(is_mobile) { return false };

			var obj = $('#'+$(this).attr('data-'));
			var val = $(this).attr('val');
			
			if($(obj).hasClass('animated'))
			{
				$(obj).css('display', 'block');
				return true;
			}
			
			var speed = 1000;
			if($(obj).hasClass('boxcaptionright') || $(obj).hasClass('boxcaptionrightrts'))
			{
				if($(obj).hasClass('boxcaptionright'))
				{
					animateRight(obj, speed, '350');
				}
				else
				{
					animateRight(obj, speed, '307');
				}
			}
			else if($(obj).hasClass('boxcaptionleft') || $(obj).hasClass('boxcaptionleftrts'))
			{
				if($(obj).hasClass('boxcaptionleft'))
				{
					animateLeft(obj, speed, '357', '-381');
				}
				else
				{
					animateLeft(obj, speed, '307', '-335.5');
				}
			}
	
			form_data = {val : val};
			var loc = base_url+'jewellery/hoverDetail';
			
			fetchDisAjax(form_data, loc, 'get', false, false, $('#span_'+val), false, false, null, '');
			
			$(obj).addClass('animated')
		},
		mouseleave: function () {
			//deny hover event if user agent is from mobile like device
			if(is_mobile) { return false };
			
			var obj = $('#'+$(this).attr('data-'));
			$(obj).css('display', 'none');
		}
	},'.hovanimate');
	
	// mouse hover over compare check box event
	/*$(document).on({
		mouseenter: function () {
			if($( this ).hasClass('comp_unchecked'))
			{
				$(this).css('height', 134);
			}
		},
		mouseleave: function () {
			$(this).css('height', 134);
		}
	},'.comp_unchecked, .comp_checked');*/
	
	//// grey scaling function ////
	$(".bti").fadeTo(500, 0.4).hover(function () {
			$(this).fadeTo(500, 1);
		}, function () {
			$(this).fadeTo(500, 0.4);
	});
	
	$(".latestthree").hover(function () {
			$(this).find(".title").fadeTo(500, 0.4);
			$(this).find(".latestthreeimage").fadeTo(500, 0.4);
		}, function () {
			$(this).find(".title").fadeTo(500, 1);
			$(this).find(".latestthreeimage").fadeTo(500, 1);
	});
	//// End grey scaling function ////
	
	//// Start Scroll Top Function //// 
	$(window).bind('scroll', function(){
		if($(this).scrollTop() > 200) {
		$("#scrolltab").fadeIn('3000');
		}
		if($(this).scrollTop() < 199){
			$("#scrolltab").fadeOut('3000');
		}
	});
	
	$('#scrolltab').click(function(){
		$("html, body").animate({scrollTop:0}, 'slow');
	});
	//// End Scroll Top Function ////
	
	// Initialise Facebox Modal window:
	$('a[rel*=modal]').each(function(){
		$(this).facebox();
	});
		
	/* Resize popup box*/		
	$(window).resize(function(){
		setOverlayPos('#facebox');
	});
	
	//// Start Tabs Function ////
	$('.tab').click(function () {
		$('.tabs_container > .tabs > li.active').removeClass('active');
		$(this).parent().slideDown('slow').addClass('active');
		$('.tabs_container > .tab_contents_container > div.tab_contents_active').slideUp('slow').removeClass('tab_contents_active');
		
		$(this.rel).slideDown('slow').addClass('tab_contents_active');
	});
	//// End Tabs Function ////
	
	//// Start Toggle Function ////
		$(".togglewrap .togglecontent").hide();
		$('.togglewrap .toggletitle.active').addClass('active').next().show();
	
		$(".togglewrap .toggletitle").click(function(){
			$(this).toggleClass("active").next().slideToggle("fast");
			return false;
		});
	//// End Toggle Function ////
	
	//// Start Accordian Function ////
	$('.accordionwrap .accordioncontent').hide();
	$('.accordionwrap .accordiontitle:first-child').addClass('active').next().show();
	
	$('.accordionwrap .accordiontitle').click(function() {
		if($(this).next().is(':hidden')) {
			$(this).parent().find(".accordiontitle").removeClass('active').next().slideUp('fast');
			$(this).toggleClass('active').next().slideDown('fast');
		}
		return false;
	});
	//// End Accordian Function ////
	
	//// Start Newsletter Subscriber ////
	$('#subscribe_btn').live('click',function(){
		
		$("#subscribe_btn").addClass("hide");
		$("#preloading").removeClass("hide");
		
		$('#subscribe_msg').show();
		form_data = $('#subscribe_form :input'); 
		var loc = (base_url+'login/newsletterSubscribe');
		
		$.post(loc, form_data, function (json) {
			var resp = ($.parseJSON(json));

			$("#subscribe_btn").removeClass("hide");
			$("#preloading").addClass("hide");
			//$('#subscribe_msg').css({'font-size': '14px'});

			if(resp['error'])
			{
				$('#subscribe_msg').css({'color':'#F00'});
				$('#subscribe_msg').html(resp['error']['newsletter_email']);
			}
			else if(resp['success'])
			{
				$('#subscribe_msg').css({'color':'#FFF'});
				$('#subscribe_msg').html(resp['success']);
			}
			
		});
	});
	//// End Newsletter Subscriber ////
	
	
//commented out by hiren donda
/*	//// Start search  ////
	$('#search_btn').live('click',function(){
		$('#search_btn').show();
		form_data = $('#search :input'); 
		var loc = (base_url+'jewellery/saveSearchTermForm');
		
		$.post(loc, form_data, function (json) {
			var resp = ($.parseJSON(json));
			if(resp['error'])
			{
				$('#error_msg').css({'color':'#F00'});
				$('#error_msg').text(resp['error']['search_terms_keywords']).show();
			}
			
		});
	});
	//// End  search ////
*/	

	//related product hide/show
	$('#toggle_tags').live('click', function(event) {        
		 $('#content-tags').toggle('show');
	});
	//product tag links hide/show
	$('#toggle_tags_links').live('click', function(event) {        
		 $('#content-tags-links').toggle('show');
	});
	
});

function onclickModal() {
  $('a[rel*=modal]').facebox() ;
}

/*
+---------------------------------------------+
	first charachet lower
+---------------------------------------------+
*/ 
function lcFirst(str)
{
	str+= '';
	var f = str.charAt(0).toLowerCase();
	return f+ str.substr(1);
}
/*
+--------------------------------------------------+
	Function will display products according to pagination
	#params : perpage : fetch perpage value
+--------------------------------------------------+
*/
function perPageManage()
{
	$('#form_search').submit();
}


/*
save feed back form 
*/
function saveNewslatterForm(obj)
{
	$.post(base_url+'home/newsletter',$(obj).serialize(),function(response){ 
		var cts = $.parseJSON(response);
		if(typeof(cts.success) == 'undefined')
			displayErrors(cts);
		else
		{
			$('.input-notification',obj).hide();
			$(obj).find('.notification_area_feedback').html(getNotificationHtml('success','Your email id is subscribed.'));
			$(obj).find('.lightbox_container_textbox').val('');
		}
		
	});
	return false;
}
/*
save email to friend
*/
function saveEmailToFriendForm(obj)
{
	$('.popup_loader_div img').css('display', 'block');
	$.post(base_url+'jewellery/emailToFriend',$(obj).serialize(),function(response){ 
		var cts = $.parseJSON(response);
		if(typeof(cts.success) == 'undefined')
			displayErrors(cts);
		else
		{
			$('.input-notification',obj).hide();
			$(obj).find('.notification_area_feedback').html(getNotificationHtml('success','Your message has been send to your friends.'));
			$(obj).find('.c-form').val('');
		}
		$('.popup_loader_div img').css('display', 'none');
		
	});
	return false;
}
/*
save request for ring sizer
*/
function saveOrderRingSizer(obj)
{
	btnVal = $(obj).attr('value');
	if(btnVal == 'Yes')
		form_data = $(obj).serialize() + "&YesBtn=Yes";
	else
		form_data = $(obj).serialize();
		
	$('.popup_loader_div img').css('display', 'block');
	$.post(base_url+'home/orderRingSizerPopup',form_data,function(response){ 
		var cts = $.parseJSON(response);
		if(typeof(cts.success) == 'undefined')
			displayErrors(cts);
		else
		{
			if(btnVal == 'Yes')
				$('.notification_area_if_customer_login').html(getNotificationHtml('success','Your request is successfully saved.'));
			else
			{
				$('.input-notification',obj).hide();
				$(obj).find('.notification_area_feedback').html(getNotificationHtml('success','Your request is successfully saved.'));
				$(obj).find('.c-form').val('');
			}
		}
		$('.popup_loader_div img').css('display', 'none');
		
	});
	return false;
}

function setOverlayPos(selector){//update by hitesh 30 - 08 - 2012 (xp ie7 bug fixed).
   
   if(typeof(selector) == 'undefined')
		selector = '.blockPage';
		
   // Get window sizes
   var winhgt = parseInt($(window).height());
   var winwth = parseInt($(window).width());
   
   
   var blockstyle = $(selector).css("display");
   if(blockstyle == "block"){
		   // Get Block sizes
		   var overlayhgt = parseInt($(selector).height());
		   var overlaywth = parseInt($(selector).width());
		   
				   if(overlayhgt != null && overlayhgt!="" && overlaywth != null && overlaywth!=""){
						   // Get Document sizes
						   var dochgt = ($(document).height()>900)?$(document).height():900; //$(document).height(); //900;
						   var docwth = $(document).width();
						   var mytop = '', myleft='';
				   
						   if(winhgt > dochgt){
								   mytop = (dochgt - overlayhgt) / 2;
								   myleft = (docwth - overlaywth) / 2;
						   }
						   else{
								   mytop = (winhgt - overlayhgt) / 2;
								   myleft = (winwth - overlaywth) / 2;
						   }
						   mytop += parseInt($(window).scrollTop());
						   
						   if(winhgt<overlayhgt){
								   mytop = 0;
						   }
						   if(winwth<overlaywth){
								   myleft = 0;
						   }
						   var tp = (mytop >0) ? mytop-140 : mytop;
						   $(selector).css({
								   top: ((tp < 0) ? 5 : tp)+'px',
								   left: myleft+'px'
						   });
				   }
		   
   }
}
 
 /*
+-------------------------------------------------------------+
	@author Hiren Donda
	function  will submit filter form
+-------------------------------------------------------------+
*/	
	function submitSearch(act,isappend)
	{
		if(isappend && act != '')
		{
			act = act + '.html';
		}
		
		var loc ='';
		if(isappend)
		{
			loc = (base_url+'jewellery/search/'+act);
		}
		else
		{
			loc = act;
		}
		
		var html='';
		var search_terms_keywords = $('input[name="search_terms_keywords"]').val();
		if($('#searchf').length>0)
		{
			$('#searchf').get(0).setAttribute('action', loc);

			if($('#sort_by').length>0)
			{
				var sort_by = $('#sort_by').val();
				html += '<select name="sort_by" style="display:none;">';
				html += '<option value="'+sort_by+'" >x</option>';
				html += '</select>';
			}
			html += '<input type="hidden" name="search_terms_keywords" value="'+search_terms_keywords+'" />';
			
			$('#searchf').append(html);
		}
		else
		{
			html += '<form method="post" name="searchf" id="searchf" action="'+loc+'">';		
			if($('#sort_by').length>0)
			{
				var sort_by = $('#sort_by').val();
				html += '<select name="sort_by" style="display:none;">';
				html += '<option value="'+sort_by+'" >x</option>';
				html += '</select>';
			}
			html += '<input type="hidden" name="search_terms_keywords" value="'+search_terms_keywords+'" />';
			html += '</form>';	
			 $('body').append(html);	
		}
		$('#searchf').submit(); 
	}

/*
 *	function will load area as per city selected
 */
function loadArea(city_name,sta_id,class_name,con_url)
{
	$("."+class_name).html('Loading...');
	form_data={city_name : city_name,sta_id : sta_id};
	var loc = (base_url+con_url);
	$.post(loc, form_data, function (data)
	{
		$("."+class_name).html(data);
	});
}

/*
 *	function will load area as per city selected
 */
function loadPincode(area_name,city_name,sta_id,name,con_url)
{
	$('input[name="'+name+'"]').val('Loading...');
	form_data={area_name : area_name,city_name : city_name,sta_id : sta_id};
	var loc = (base_url+con_url);
	$.post(loc, form_data, function (data)
	{
		var arr = $.parseJSON(data);
		$('input[name="'+name+'"]').val(arr['pincode']);
		$('input[name="'+name+'_hidd"]').val(arr['pincode_id']);
	});
}

/*
 *  removes loader when image loads
 */
function loadImage(obj)
{
	$(obj).removeClass('loader_img');
}

$(window).load(function()
{
	/*var html = '<span class="header_email_cycle">info@'.baseDomain().'</span>';
	$('.contact').append(html);
	
	$('.cycle_tag').cycle({
		fx:'fade',
		speed:300,
		timeout:8000,
		easing:'linear',
		pause:1 
	});

	$('.prod_rev').show();
	
	//check if user agent is from mobile
	if(isMobile.any())
	{
		// Mobile!
		is_mobile = true;
	}
	else
	{
		// Not mobile
		is_mobile = false; 
	}*/
	
	setInterval(function() { homeInitImage('home_flash_image1', 1500); }, 4500);
	setInterval(function() { homeInitImage('home_flash_image2', 1500); }, 4500);
});

function homeInitImage(class_name, speed)
{
	var is_dis = false;	//variable specifies if displayed image is found or not
	var is_swaped = false;	//variable defines
	
	$('.'+class_name).each(function(index, el)
	{
		if( $(this).css('display') == 'block' )
		{
			$(this).fadeOut(speed, function()
			{
				$(this).css({'display':'none'});

				var obj;
				if( $('.'+class_name).eq(index + 1).length > 0 )	//check if has next element
				{
					obj =$('.'+class_name).eq(index + 1);
					$(obj).fadeIn(speed, function()
					{
						$(obj).css({'display':'block'});
					});
				}
				else							  //otherwise start from begining
				{
					obj = $('.'+class_name).first();

					$(obj).fadeIn(speed, function()
					{
						$(obj).css({'display':'block'});
					});
				}
			});
			return false;
		}
	});
}

/** 
 * @author Hiren Donda
 * @abstract animate objects towards right
 */
function animateRight(obj, speed, width)
{
	
	$(obj).css('display', 'block');
	$(obj).css('opacity', '0.9');
	$(obj).animate({width: width}, speed);
}

/** 
 * @author Hiren Donda
 * @abstract animate objects towards left
 */
function animateLeft(obj, speed, width, marginLeft)
{
	
	$(obj).css('display', 'block');
	$(obj).css('opacity', '0.9');
	$(obj).animate({marginLeft:  marginLeft, width: width}, speed);
}

/** 
 * @author Hiren Donda
 * @abstract common function to fetch content using ajax and dispplay
 */
function fetchDisAjax(form_data, loc, method, is_parse, is_validate, res_obj, is_append, is_dis_err, msg_obj, before_after)
{
	
	if(method=='get')
	{
		$.get(loc, form_data, function(data){
			
			var arr = null;
			if(is_parse)
			{
				arr = $.parseJSON(data);
			}
			else
			{
				arr = data;
			}
			
			if(!is_validate || arr['type']=='success')
			{
				if(is_append)
				{
					$(res_obj).append(arr);	
				}
				else
				{
					$(res_obj).html(arr);	
				}
			}
			else if(is_dis_err)
			{
				if(before_after=='before')	
				{
					$(msg_obj).before( getNotificationHtml( arr['type'], arr['msg'] ) );
				}
			}
			
		});
			
	}
}

///variable that specfies if compare button is displayed or not
var is_compare_but_displyed = false;

/**
 * @author Hiren donda
 * @abstract used in page scroll pagination
 */
function processCompare(obj)
{	
	//change css class for checkbox background image
	var label = $(obj).next("label");
	if( $(obj).is(':checked') )
	{
		$( label ).removeClass('comp_unchecked').addClass('comp_checked');
	}
	else
	{
		$( label ).removeClass('comp_checked').addClass('comp_unchecked');
	}
	
	var cnt = 0;
	$('.compare').each(function(){
		if($(this).is(':checked')){
			cnt++;
		}
		
		if(cnt>3)
		{
			$( label ).removeClass('comp_checked').addClass('comp_unchecked');
			$(obj).removeAttr('checked');
			$('.contain').prepend( getNotificationHtml( 'warning', 'You can compare 3 products at a time.' ) );		
			$('html, body').animate({ scrollTop: 0 }, 'slow');
			return false;
		}
	});
	
	if(!is_compare_but_displyed && cnt>=2)
	{
		$('body').append('<a id="comp_butt" style="cursor:pointer;" class="compare_button" onclick="$(\'#comparef\').submit();"><img border="0" src="'+asset_url+'images/compare.png"></a>');		
		is_compare_but_displyed = true;
	}
	else if(is_compare_but_displyed && cnt<=1)
	{
		$('#comp_butt').remove();		
		is_compare_but_displyed = false;
	}
		   
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

/**
 * @author Hiren donda
 * @abstract function will insert call me entry
 */
function callMe( input_id, load_id )
{
	$('#'+load_id).show();
	$('#'+input_id+'_error').hide();
	
	var loc = base_url + 'home/callMeBack';
	form_data = { ci_customer_phone_number : $('#'+input_id).val() };
	$.post(loc, form_data, function(data){ 

		var arr = ($.parseJSON(data));
		
		if(arr['type']=='success')
		{
			$('#'+input_id+'_error').css({'color':'green'});
			$('#'+input_id+'_error').text(arr['msg']).show();
		}
		else if(arr['error'])
		{
			$('#'+input_id+'_error').css({'color':'red'});
			$('#'+input_id+'_error').text(arr['error']['ci_customer_phone_number']).show();
		}
		$('#'+load_id).hide();
	});
}

/**
 * @author Hiren donda
 * @abstract function will hide call me box
 */
	function hideCallMe()
	{
		if( is_hide_call_me )
		{
			is_hide_call_me = false; $('#callmebox').hide(); 
			if( is_hide_livehelp )
			{
				is_hide_livehelp=false; $(".divSocialIcon", $(".liLiveHelp")).slideUp();
			}
		}
	}