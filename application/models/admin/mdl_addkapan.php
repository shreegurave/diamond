<?php
class mdl_addkapan extends CI_Model
{
	var $cTableName = '';
	var $cAutoId = '';
	var $cPrimaryId = '';
	var $cCategory = '';
	
	function getData()
	{
		if($this->cPrimaryId == "")
		{
			$f = $this->input->get('f');
			$s = $this->input->get('s');
			
			$k_name= $this->input->get('k_name');
			$k_add_name= $this->input->get('k_add_name');
			$k_total= $this->input->get('k_total');
			$k_weight= $this->input->get('k_weight');
			$k_date= $this->input->get('k_date');
			$k_day= $this->input->get('k_day');
			
			if(isset( $k_name ) && $k_name != "")
				$this->db->where('k_name LIKE \'%'.$k_name.'%\' ');
				
			if(isset( $k_add_name ) && $k_add_name != "")
				$this->db->where('k_add_name LIKE \'%'.$k_add_name.'%\' ');
				
			if(isset( $k_total ) && $k_total != "")
				$this->db->where('k_total', $k_total );	
				
			if(isset( $k_weight ) && $k_weight != "")
				$this->db->where('k_weight', $k_weight );
				
			if(isset( $k_date ) && $k_date != "")
				$this->db->where('k_date', $k_date );
				
			if(isset( $k_day ) && $k_day != "")
				$this->db->where('k_day', $k_day );
			
			if($f !='' && $s != '')
				$this->db->order_by($f,$s);				
			else
				$this->db->order_by($this->cAutoId,'ASC');
				
// 			if( MANUFACTURER_ID != 7 )
// 				$this->db->where('manufacturer_id', MANUFACTURER_ID );
				
		}
		else if($this->cPrimaryId != '')
			$this->db->where($this->cAutoId,$this->cPrimaryId);
		
// 		$this->db->join('admin_user_group', 'admin_user_group.admin_user_group_id = admin_user.admin_user_group_id');
		$res = $this->db->get($this->cTableName);
		//echo $this->db->last_query();
		return $res;
		
	}
	
	function getKapanJamaData()
	{
		$f = $this->input->get('f');
		$s = $this->input->get('s');
		
		$k_name= $this->input->get('k_name');
		$k_add_name= $this->input->get('k_add_name');
		$k_total= $this->input->get('k_total');
		$k_weight= $this->input->get('k_weight');
		$k_date= $this->input->get('k_date');
		$k_day= $this->input->get('k_day');
		
		if(isset( $k_name ) && $k_name != "")
			$this->db->where('k_name LIKE \'%'.$k_name.'%\' ');
			
		if(isset( $k_add_name ) && $k_add_name != "")
			$this->db->where('k_add_name LIKE \'%'.$k_add_name.'%\' ');
			
		if(isset( $k_total ) && $k_total != "")
			$this->db->where('k_total', $k_total );
			
		if(isset( $k_weight ) && $k_weight != "")
			$this->db->where('k_weight', $k_weight );
			
		if(isset( $k_date ) && $k_date != "")
			$this->db->where('k_date', $k_date );
			
		if(isset( $k_day ) && $k_day != "")
			$this->db->where('k_day', $k_day );
			
		if($f !='' && $s != '')
			$this->db->order_by($f,$s);
		else
			$this->db->order_by($this->cTableName.'.'.$this->cAutoId,'ASC');
		
		$this->db->join( 'kapan_jama kj', 'kj.kapan_id = '.$this->cTableName.'.kapan_id' );
		$res = $this->db->get( $this->cTableName );
		//echo $this->db->last_query();
		return $res;
	}
	
	
	function saveData()
	{
		$data = $this->input->post();
		
		unset($data['item_id']);

		//if primary id set then we have to make update query
		if($this->cPrimaryId != '')
		{
			$this->db->set('k_modified_date', 'NOW()', FALSE);
			$this->db->where($this->cAutoId,$this->cPrimaryId)->update($this->cTableName,$data);			
			$last_id = $this->cPrimaryId;
			$logType = 'E';			
		}		
		else // insert new row
		{
			$this->db->insert($this->cTableName,$data);
			$last_id = $this->db->insert_id();
			$logType = 'A';
		}
		
		//kapan jama entry
		$kj['kapan_id'] = $last_id;
		
		if( getField( "kapan_jama_id" , "kapan_jama", "kapan_id", $last_id) )
		{
			$this->db->set( 'kj_modified_date', 'NOW()', FALSE );
			$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "kapan_jama", $kj );
		}
		else
		{
			$this->db->insert( 'kapan_jama', $kj );
		}
		
		
		saveAdminLog($this->router->class, @$data['k_name'], $this->cTableName, $this->cAutoId, $last_id, $logType); 
		setFlashMessage('success','Kapan has been '.(($this->cPrimaryId != '') ? 'updated': 'inserted').' successfully.');
		
	}
/*
+----------------------------------------------------------+
	Deleting item. hadle both request get and post.
	with single delete and multiple delete.
	@prams : $ids -> integer or array
+----------------------------------------------------------+
*/	
	function deleteData($ids)
	{
		$returnArr = array();
		if($ids)
		{
			foreach($ids as $id)
			{
				$getName = getField('k_name', $this->cTableName, $this->cAutoId, $id);
				saveAdminLog($this->router->class, @$getName, $this->cTableName, $this->cAutoId, $id, 'D');
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kapan_jama' );
				$this->db->where_in( $this->cAutoId, $id )->delete( $this->cTableName );
			}
			$returnArr['type'] ='success';
			$returnArr['msg'] = count($ids)." records has been deleted successfully.";
		}
		else{
			$returnArr['type'] ='error';
			$returnArr['msg'] = "Please select at least 1 item.";
		}
		echo json_encode($returnArr);
	}
/*
+-----------------------------------------+
	Update status for enabled/disabled
	@params : post array of ids, status
+-----------------------------------------+
*/	
	function updateStatus()
	{
		$status = $this->input->post('status');
		$cat_id = $this->input->post('cat_id');
		$data['admin_user_status'] = $status;
		
		$this->db->where($this->cAutoId,$cat_id);
		$this->db->update($this->cTable,$data);
		//echo $this->db->last_query();
		
	}
	/*
+------------------------------------------------------+
	uploads product image folder
+------------------------------------------------------+
*/	
	function resizeUploadImage()
	{
		/*$file_size = str_replace('M','',ini_get('upload_max_filesize'));
		$object_size = convertToMb($_FILES['article_image']['size']);
		
		if($file_size < $object_size)
			setFlashMessage('error','Upload limit exceed.');
		else*/ 
		{
			$image = uploadFile('admin_profile_image','image','admin_profile'); //input file, type, folder
			if(@$image['error'])
			{
				setFlashMessage('error',$image['error']);
				redirect('admin/'.$this->router->class);
				
			}
			/*$width = getField('image_size_width','image_size','image_size_id',$this->input->post('image_size_id'));
			$height = getField('image_size_height','image_size','image_size_id',$this->input->post('image_size_id'));
			$path = $image['path'];*/
			//$sizeArr = $this->db->where('image_size_id',$this->input->post('image_size_id'))->where('image_size_status','0')->get('image_size')->row_array();
			$path = $image['path'];
			$dest = getResizeFileNameByPath($path,'m',''); //image path, type(s,m), folder
			$returnFlag = resize_image($path, $dest, 60, 60); //source, destination, width, height
			@unlink($path); //delete old image
			return $dest;
		}
	}


}