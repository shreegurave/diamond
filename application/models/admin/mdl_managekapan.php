<?php
class mdl_managekapan extends CI_Model
{
	var $cTableName = '';
	var $cAutoId = '';
	var $cPrimaryId = '';
	var $cCategory = '';
	
	function getData()
	{
		if($this->cPrimaryId == "")
		{
			$f = $this->input->get('f');
			$s = $this->input->get('s');
			
			$k_name= $this->input->get('k_name');
			$k_add_name= $this->input->get('k_add_name');
			$k_total= $this->input->get('k_total');
			$k_weight= $this->input->get('k_weight');
			$k_date= $this->input->get('k_date');
			$k_day= $this->input->get('k_day');
			
			if(isset( $k_name ) && $k_name != "")
				$this->db->where('k_name LIKE \'%'.$k_name.'%\' ');
				
			if(isset( $k_add_name ) && $k_add_name != "")
				$this->db->where('k_add_name LIKE \'%'.$k_add_name.'%\' ');
				
			if(isset( $k_total ) && $k_total != "")
				$this->db->where('k_total', $k_total );	
				
			if(isset( $k_weight ) && $k_weight != "")
				$this->db->where('k_weight', $k_weight );
				
			if(isset( $k_date ) && $k_date != "")
				$this->db->where('k_date LIKE \'%'.formatDate( 'Y-m-d', $k_date ).'%\' ');
				
			if(isset( $k_day ) && $k_day != "")
				$this->db->where('k_day', $k_day );
			
			if($f !='' && $s != '')
				$this->db->order_by($f,$s);				
			else
				$this->db->order_by($this->cAutoId,'ASC');
				
		}
		else if($this->cPrimaryId != '')
		{
			$this->db->where( $this->cTableName.'.'.$this->cAutoId, $this->cPrimaryId );
			$this->db->join( 'kacho_number kn', 'kn.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'sarin_count sc', 'sc.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'polish_count pc', 'pc.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'polish_repairing pr', 'pr.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'kapan_jama_count kjc', 'kjc.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'soing_count soc', 'soc.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'sabka_count sac', 'sac.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
		}
		
		$res = $this->db->get($this->cTableName);
// 		echo $this->db->last_query();
		return $res;
		
	}
	
	function getSarinData()
	{
		return executeQuery( "SELECT * FROM sarin WHERE kapan_id = ".$this->cPrimaryId );
	}
	
	function getPolishData()
	{
		return executeQuery( "SELECT * FROM polish WHERE kapan_id = ".$this->cPrimaryId );
	}
	
	function getKapanJamaData()
	{
		return exeQuery( "SELECT * FROM kapan_jama WHERE kapan_id = ".$this->cPrimaryId );
	}
	
	function getSoingData()
	{
		return executeQuery( "SELECT * FROM soing WHERE kapan_id = ".$this->cPrimaryId );
	}
	
	function getSabkaData()
	{
		return executeQuery( "SELECT * FROM sabka WHERE kapan_id = ".$this->cPrimaryId );
	}
	
	function saveData()
	{
		$data = $this->input->post();
		$kapan_id = _de( $data['item_id']);
		
		unset($data['item_id']);
		
		//Kacho Number
		
			$kn['kapan_id'] = $kapan_id;
			$kn['kn_sd_n'] = $data['kn_sd_n'];
			$kn['kn_sd_v'] = $data['kn_sd_v'];
			$kn['kn_sd1_n'] = $data['kn_sd1_n'];
			$kn['kn_sd1_v'] = $data['kn_sd1_v'];
			$kn['kn_g1_n'] = $data['kn_g1_n'];
			$kn['kn_g1_v'] = $data['kn_g1_v'];
			$kn['kn_g2_n'] = $data['kn_g2_n'];
			$kn['kn_g2_v'] = $data['kn_g2_v'];
			$kn['kn_u1_n'] = $data['kn_u1_n'];
			$kn['kn_u1_v'] = $data['kn_u1_v'];
			$kn['kn_u2_n'] = $data['kn_u2_n'];
			$kn['kn_u2_v'] = $data['kn_u2_v'];
			$kn['kn_u3_n'] = $data['kn_u3_n'];
			$kn['kn_u3_v'] = $data['kn_u3_v'];
			$kn['kn_palchu_n'] = $data['kn_palchu_n'];
			$kn['kn_palchu_v'] = $data['kn_palchu_v'];
			$kn['kn_ta_n'] = $data['kn_ta_n'];
			$kn['kn_ta_v'] = $data['kn_ta_v'];
			$kn['kn_mix_n'] = $data['kn_mix_n'];
			$kn['kn_mix_v'] = $data['kn_mix_v'];
			$kn['kn_extra1_n'] = $data['kn_extra1_n'];
			$kn['kn_extra1_v'] = $data['kn_extra1_v'];
			$kn['kn_extra2_n'] = $data['kn_extra2_n'];
			$kn['kn_extra2_v'] = $data['kn_extra2_v'];
			$kn['kn_extra3_n'] = $data['kn_extra3_n'];
			$kn['kn_extra3_v'] = $data['kn_extra3_v'];
			$kn['kn_total_n'] = $data['kn_total_n'];
			$kn['kn_total_v'] = $data['kn_total_v'];
			$kn['kn_saij'] = $data['kn_saij'];
			$kn['kn_takavari'] = $data['kn_takavari'];
			$kn['kn_extra_1'] = $data['kn_extra_1'];
			$kn['kn_extra_2'] = $data['kn_extra_2'];
			$kn['kn_extra_3'] = $data['kn_extra_3'];
			
			if( getField( "kacho_number_id" , "kacho_number", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'kn_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "kacho_number", $kn );
			}
			else 
			{
				$this->db->insert( 'kacho_number', $kn);
			}
			
			//flush array
			$kn = array();
		
		//Sarin
			$sarinRecord = $data['sarinCount'];//count( $data['s_date'] );
			$sc['kapan_id'] = $sarin['kapan_id'] = $kapan_id;
			
			//delete all old record
			query( "DELETE FROM sarin WHERE kapan_id = ".$kapan_id );
			
			for ( $row=0;$row<$sarinRecord;$row++ )
			{
// 				if( !empty( $data['s_date'][$row]) )
				{
					if( $data['s_date'][$row] == "1970-01-01")
					{
						$data['s_date'][$row] = "01-01-2000";
					}
					
					$sarin['s_date'] = formatDate( 'Y-m-d', $data['s_date'][$row] );
					$sarin['s_nung'] = $data['s_nung'][$row];
					$sarin['s_weight'] = $data['s_weight'][$row];
					$sarin['s_pyority'] = $data['s_pyority'][$row];
					$sarin['s_charni'] = $data['s_charni'][$row];
					$sarin['s_takavari'] = $data['s_takavari'][$row];
					$sarin['s_name'] = $data['s_name'][$row];
					$sarin['s_4p'] = $data['s_4p'][$row];
					
					//insert sarin record
					$this->db->insert( 'sarin', $sarin);
				}
			}
			
			//sarin count insert/update record
			$sc['sc_saij'] = $data['sc_saij'];
			$sc['sc_takavari'] = $data['sc_takavari'];
			$sc['sc_nung'] = $data['sc_nung'];
			$sc['sc_weight'] = $data['sc_weight'];
			
			if( getField( "sarin_count_id" , "sarin_count", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'sc_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "sarin_count", $sc );
			}
			else
			{
				$this->db->insert( 'sarin_count', $sc);
			}
			
			//flush array
			$sarin = array();
			$sc = array();
			
		
		//Polish
			$polishRecord = $data['polishCount'];//count( $data['p_lot_start_date'] );
			$pc['kapan_id'] = $polish['kapan_id'] = $kapan_id;
// 			pr($polishRecord);
// 			pr($data);die;
			//delete all old record
			query( "DELETE FROM polish WHERE kapan_id = ".$kapan_id );
			
			for ( $row=0;$row<$polishRecord;$row++ )
			{
// 				if( strpos( $data['p_lot_start_date'][$row], "1970-01-01" ) || strpos( $data['p_lot_last_date'][$row], "1970-01-01" ))//!empty( $data['p_lot_start_date'][$row]) || 
				{
					if( $data['p_lot_start_date'][$row] == "1970-01-01")
					{
						$data['p_lot_start_date'][$row] = "01-01-2000";
					}
					
					if( $data['p_lot_last_date'][$row] == "1970-01-01")
					{
						$data['p_lot_last_date'][$row] = "01-01-2000";
					}
					
					$polish['p_lot_start_date'] = formatDate( 'Y-m-d', ( $data['p_lot_start_date'][$row] ) ? $data['p_lot_start_date'][$row] : "01-01-2000");
					$polish['p_nung'] = $data['p_nung'][$row];
					$polish['p_weight'] = $data['p_weight'][$row];
					$polish['p_pyority'] = $data['p_pyority'][$row];
					$polish['p_charni'] = $data['p_charni'][$row];
					$polish['p_4p_ghat_nung'] = $data['p_4p_ghat_nung'][$row];
					$polish['p_4p_weight'] = $data['p_4p_weight'][$row];
					$polish['p_4p_t_k'] = $data['p_4p_t_k'][$row];
					$polish['p_4p_takavari'] = $data['p_4p_takavari'][$row];
					$polish['p_4p_avl_takavari'] = $data['p_4p_avl_takavari'][$row];
					$polish['p_4p_ghat_variation'] = $data['p_4p_ghat_variation'][$row];
					$polish['p_4p_name'] = $data['p_4p_name'][$row];
					$polish['p_tbl_nung'] = $data['p_tbl_nung'][$row];
					$polish['p_tbl_weight'] = $data['p_tbl_weight'][$row];
					$polish['p_tbl_t_k'] = $data['p_tbl_t_k'][$row];
					$polish['p_tbl_takavri'] = $data['p_tbl_takavri'][$row];
					$polish['p_tly_nung'] = $data['p_tly_nung'][$row];
					$polish['p_tly_weight'] = $data['p_tly_weight'][$row];
					$polish['p_tly_t_k'] = $data['p_tly_t_k'][$row];
					$polish['p_tly_takavari'] = $data['p_tly_takavari'][$row];
					$polish['p_tly_sarni_1'] = $data['p_tly_sarni_1'][$row];
					$polish['p_tly_sarni_2'] = $data['p_tly_sarni_2'][$row];	
					$polish['p_m_nung'] = $data['p_m_nung'][$row];
					$polish['p_m_weight'] = $data['p_m_weight'][$row];
					$polish['p_m_t_k'] = $data['p_m_t_k'][$row];
					$polish['p_m_takavari'] = $data['p_m_takavari'][$row];
					$polish['p_tk_nung'] = $data['p_tk_nung'][$row];
					$polish['p_tk_weight'] = $data['p_tk_weight'][$row];
					$polish['p_tk_t_k'] = $data['p_tk_t_k'][$row];
					$polish['p_tk_takavari'] = $data['p_tk_takavari'][$row];
					$polish['p_lot_last_date'] = formatDate( 'Y-m-d', ( $data['p_lot_last_date'][$row] ) ? $data['p_lot_last_date'][$row] : "01-01-2000");
					$polish['p_variation'] = $data['p_variation'][$row];
					
					//insert polish record
					$this->db->insert( 'polish', $polish);
				}
				
			}
				
			$pc['pc_4p_name'] = @$data['pc_4p_name'];
			$pc['pc_4p_takavari'] = @$data['pc_4p_takavari'];
			$pc['pc_tbl_name'] = @$data['pc_tbl_name'];
			$pc['pc_tbl_takavari'] = @$data['pc_tbl_takavari'];
			$pc['pc_tly_name'] = @$data['pc_tly_name'];
			$pc['pc_tly_takavari'] = @$data['pc_tly_takavari'];
			$pc['pc_m_takavari'] = @$data['pc_m_takavari'];
			$pc['pc_tk_takavari'] = @$data['pc_tk_takavari'];
			$pc['pc_nung'] = @$data['pc_nung'];
			$pc['pc_weight'] = @$data['pc_weight'];
			$pc['pc_4p_g_nung'] = @$data['pc_4p_g_nung'];
			$pc['pc_4p_weight'] = @$data['pc_4p_weight'];
			$pc['pc_4p_tk'] = @$data['pc_4p_tk'];
			$pc['pc_4p_variation'] = @$data['pc_4p_variation'];
			$pc['pc_tbl_nung'] = @$data['pc_tbl_nung'];
			$pc['pc_tbl_weight'] = @$data['pc_tbl_weight'];
			$pc['pc_tly_nung'] = @$data['pc_tly_nung'];
			$pc['pc_tly_weight'] = @$data['pc_tly_weight'];
			$pc['pc_m_nung'] = @$data['pc_m_nung'];
			$pc['pc_m_weight'] = @$data['pc_m_weight'];
			$pc['pc_tk_nung'] = @$data['pc_tk_nung'];
			$pc['pc_tk_weight'] = @$data['pc_tk_weight'];
			
			if( getField( "polish_count_id" , "polish_count", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'pc_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "polish_count", $pc );
			}
			else
			{
				$this->db->insert( 'polish_count', $pc);
			}
			
			//flush array
			$polish = array();
			$pc = array();
			
		//Polish Repairing
			$pr['kapan_id'] = $kapan_id;
			$pr['pr_dhar_apl_than'] = @$data['pr_dhar_apl_than'];
			$pr['pr_dhar_apl_weight'] = @$data['pr_dhar_apl_weight'];
			$pr['pr_dhar_avl_than'] = @$data['pr_dhar_avl_than'];
			$pr['pr_dhar_avl_weight'] = @$data['pr_dhar_avl_weight'];
			$pr['pr_tly_apl_than'] = @$data['pr_tly_apl_than'];
			$pr['pr_tly_apl_weight'] = @$data['pr_tly_apl_weight'];
			$pr['pr_tly_avl_than'] = @$data['pr_tly_avl_than'];
			$pr['pr_tly_avl_weight'] = @$data['pr_tly_avl_weight'];
			$pr['pr_tbl_apl_than'] = @$data['pr_tbl_apl_than'];
			$pr['pr_tbl_apl_weight'] = @$data['pr_tbl_apl_weight'];
			$pr['pr_tbl_avl_than'] = @$data['pr_tbl_avl_than'];
			$pr['pr_tbl_avl_weight'] = @$data['pr_tbl_avl_weight'];
			$pr['pr_stly_apl_than'] = @$data['pr_stly_apl_than'];
			$pr['pr_stly_apl_weight'] = @$data['pr_stly_apl_weight'];
			$pr['pr_stly_avl_than'] = @$data['pr_stly_avl_than'];
			$pr['pr_stly_avl_weight'] = @$data['pr_stly_avl_weight'];
			$pr['pr_m_apl_than'] = @$data['pr_m_apl_than'];
			$pr['pr_m_apl_weight'] = @$data['pr_m_apl_weight'];
			$pr['pr_m_avl_than'] = @$data['pr_m_avl_than'];
			$pr['pr_m_avl_weight'] = @$data['pr_m_avl_weight'];
			$pr['pr_hiro_apl_than'] = @$data['pr_hiro_apl_than'];
			$pr['pr_hiro_apl_weight'] = @$data['pr_hiro_apl_weight'];
			$pr['pr_hiro_avl_than'] = @$data['pr_hiro_avl_than'];
			$pr['pr_hiro_avl_weight'] = @$data['pr_hiro_avl_weight'];
			$pr['pr_tik_apl_than'] = @$data['pr_tik_apl_than'];
			$pr['pr_tik_apl_weight'] = @$data['pr_tik_apl_weight'];
			$pr['pr_tik_avl_than'] = @$data['pr_tik_avl_than'];
			$pr['pr_tik_avl_weight'] = @$data['pr_tik_avl_weight'];
			$pr['pr_e1_apl_than'] = @$data['pr_e1_apl_than'];
			$pr['pr_e1_apl_weight'] = @$data['pr_e1_apl_weight'];
			$pr['pr_e1_avl_than'] = @$data['pr_e1_avl_than'];
			$pr['pr_e1_avl_weight'] = @$data['pr_e1_avl_weight'];
			$pr['pr_e2_apl_than'] = @$data['pr_e2_apl_than'];
			$pr['pr_e2_apl_weight'] = @$data['pr_e2_apl_weight'];
			$pr['pr_e2_avl_than'] = @$data['pr_e2_avl_than'];
			$pr['pr_e2_avl_weight'] = @$data['pr_e2_avl_weight'];
			$pr['pr_e3_apl_than'] = @$data['pr_e3_apl_than'];
			$pr['pr_e3_apl_weight'] = @$data['pr_e3_apl_weight'];
			$pr['pr_e3_avl_than'] = @$data['pr_e3_avl_than'];
			$pr['pr_e3_avl_weight'] = @$data['pr_e3_avl_weight'];
			$pr['pr_total_apl_than'] = @$data['pr_total_apl_than'];
			$pr['pr_total_apl_weight'] = @$data['pr_total_apl_weight'];
			$pr['pr_total_avl_than'] = @$data['pr_total_avl_than'];
			$pr['pr_total_avl_weight'] = @$data['pr_total_avl_weight'];
			$pr['pr_tbld_apl_than'] = @$data['pr_tbld_apl_than'];
			$pr['pr_tbld_apl_weight'] = @$data['pr_tbld_apl_weight'];
			$pr['pr_tbld_avl_than'] = @$data['pr_tbld_avl_than'];
			$pr['pr_tbld_avl_weight'] = @$data['pr_tbld_avl_weight'];
			$pr['pr_dori_apl_than'] = @$data['pr_dori_apl_than'];
			$pr['pr_dori_apl_weight'] = @$data['pr_dori_apl_weight'];
			$pr['pr_dori_avl_than'] = @$data['pr_dori_avl_than'];
			$pr['pr_dori_avl_weight'] = @$data['pr_dori_avl_weight'];
			$pr['pr_e1_extra_1'] = @$data['pr_e1_extra_1'];
			$pr['pr_e1_extra_2'] = @$data['pr_e1_extra_2'];
			$pr['pr_e1_extra_3'] = @$data['pr_e1_extra_3'];
			
			if( getField( "polish_repairing_id" , "polish_repairing", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'pr_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "polish_repairing", $pr );
			}
			else
			{
				$this->db->insert( 'polish_repairing', $pr);
			}
			
			//flush array
			$pr = array();
			
		//takavari
		
			$takavari['kapan_id'] = $kapan_id;
			$takavari['t_kachi_takavari_1'] = @$data['t_kachi_takavari_1'];
			$takavari['t_kachi_takavari_2'] = @$data['t_kachi_takavari_2'];
			$takavari['t_kachi_takavari_3'] = @$data['t_kachi_takavari_3'];
			$takavari['t_kachi_saij_1'] = @$data['t_kachi_saij_1'];
			$takavari['t_kachi_saij_2'] = @$data['t_kachi_saij_2'];
			$takavari['t_kachi_saij_3'] = @$data['t_kachi_saij_3'];
			$takavari['t_taiyar_taiytakavari_1'] = @$data['t_taiyar_taiytakavari_1'];
			$takavari['t_taiyar_taiytakavari_2'] = @$data['t_taiyar_taiytakavari_2'];
			$takavari['t_taiyar_taiytakavari_3'] = @$data['t_taiyar_taiytakavari_3'];
			$takavari['t_taiyar_saij_1'] = @$data['t_taiyar_saij_1'];
			$takavari['t_taiyar_saij_2'] = @$data['t_taiyar_saij_2'];
			$takavari['t_taiyar_saij_3'] = @$data['t_taiyar_saij_3'];
			
			if( getField( "takavari_id" , "takavari", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 't_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "takavari", $takavari);
			}
			else
			{
				$this->db->insert( 'takavari', $takavari);
			}
			
			//flush array
			$takavari = array();
			
		//Kapan Jama
		
// 			$kapanJama = count( $data['kj_start_date'] );
			$kjc['kapan_id'] = $kc['kapan_id'] = $kj['kapan_id'] = $kapan_id;
			
			//delete all old record
			/* query( "DELETE FROM kapan_jama WHERE kapan_id = ".$kapan_id );
			
			for ( $row=0;$row<$kapanJama;$row++ )
			{
				if( !empty( $data['kj_start_date'][$row]) )
				{
					$kj['kj_start_date'] = $data['kj_start_date'][$row];
					$kj['kj_raph'] = $data['kj_raph'][$row];
					$kj['kj_total_kapan'] = $data['kj_total_kapan'][$row];
					$kj['kj_kapan'] = $data['kj_kapan'][$row];
					$kj['kj_nung'] = $data['kj_nung'][$row];
					$kj['kj_gol'] = $data['kj_gol'][$row];
					$kj['kj_saij'] = $data['kj_saij'][$row];
					$kj['kj_takavari'] = $data['kj_takavari'][$row];
					$kj['kj_singal_1'] = $data['kj_singal_1'][$row];
					$kj['kj_singal_2'] = $data['kj_singal_2'][$row];
					$kj['kj_selection'] = $data['kj_selection'][$row];
					$kj['kj_choki_1'] = $data['kj_choki_1'][$row];
					$kj['kj_choki_2'] = $data['kj_choki_2'][$row];
					$kj['kj_palchu'] = $data['kj_palchu'][$row];
					$kj['kj_uchu_gol'] = $data['kj_uchu_gol'][$row];
					$kj['kj_color_out'] = $data['kj_color_out'][$row];
					$kj['kj_total_out'] = $data['kj_total_out'][$row];
					$kj['kj_out'] = $data['kj_out'][$row];
					$kj['kj_chur'] = $data['kj_chur'][$row];
					$kj['kj_ghat'] = $data['kj_ghat'][$row];
					$kj['kj_end_date'] = $data['kj_end_date'][$row];
					
					$this->db->insert( 'kapan_jama', $kj );
				}
			} */
			
			$kj['kj_start_date'] = @$data['kj_start_date'];
			$kj['kj_raph'] = @$data['kj_raph'];
			$kj['kj_total_kapan'] = @$data['kj_total_kapan'];
			$kj['kj_kapan'] = @$data['kj_kapan'];
			$kj['kj_nung'] = @$data['kj_nung'];
			$kj['kj_gol'] = @$data['kj_gol'];
			$kj['kj_saij'] = @$data['kj_saij'];
			$kj['kj_takavari'] = @$data['kj_takavari'];
			$kj['kj_singal_1'] = @$data['kj_singal_1'];
			$kj['kj_singal_2'] = @$data['kj_singal_2'];
			$kj['kj_selection'] = @$data['kj_selection'];
			$kj['kj_choki_1'] = @$data['kj_choki_1'];
			$kj['kj_choki_2'] = @$data['kj_choki_2'];
			$kj['kj_palchu'] = @$data['kj_palchu'];
			$kj['kj_uchu_gol'] = @$data['kj_uchu_gol'];
			$kj['kj_color_out'] = @$data['kj_color_out'];
			$kj['kj_total_out'] = @$data['kj_total_out'];
			$kj['kj_out'] = @$data['kj_out'];
			$kj['kj_chur'] = @$data['kj_chur'];
			$kj['kj_ghat'] = @$data['kj_ghat'];
			$kj['kj_end_date'] = @$data['kj_end_date'];
			
			if( getField( "kapan_jama_id" , "kapan_jama", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'kj_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "kapan_jama", $kj );
			}
			else
			{
				$this->db->insert( 'kapan_jama', $kj );
			}
			
			
// 			$kjc['kjc_kapan'] = $data['kjc_kapan'];
// 			$kjc['kjc_nung'] = $data['kjc_nung'];
// 			$kjc['kjc_gol'] = $data['kjc_gol'];
// 			$kjc['kjc_singal_1'] = $data['kjc_singal_1'];
// 			$kjc['kjc_singal_2'] = $data['kjc_singal_2'];
// 			$kjc['kjc_selection'] = $data['kjc_selection'];
// 			$kjc['kjc_choki_1'] = $data['kjc_choki_1'];
// 			$kjc['kjc_choki_2'] = $data['kjc_choki_2'];
// 			$kjc['kjc_palchu'] = $data['kjc_palchu'];
// 			$kjc['kjc_uchu_gol'] = $data['kjc_uchu_gol'];
// 			$kjc['kjc_color_out'] = $data['kjc_color_out'];
// 			$kjc['kjc_total_out'] = $data['kjc_total_out'];
// 			$kjc['kjc_out'] = $data['kjc_out'];
// 			$kjc['kjc_chur'] = $data['kjc_chur'];
			
// 			if( getField( "kapan_jama_count_id" , "kapan_jama_count", "kapan_id", $kapan_id ) )
// 			{
// 				$this->db->set( 'kjc_modified_date', 'NOW()', FALSE );
// 				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "kapan_jama_count", $kjc);
// 			}
// 			else
// 			{
// 				$this->db->insert( 'kapan_jama_count', $kjc);
// 			}
		
			//flush array
			$kjc = array();
			$kc = array();
			
		//Soing
			$soingRecord = $data['soingCount'];//count( $data['soing_date'] );
			$soc['kapan_id'] = $soing['kapan_id'] = $kapan_id;
			//delete all old record
			query( "DELETE FROM soing WHERE kapan_id = ".$kapan_id );
			
			for ( $row=0;$row<$soingRecord;$row++ )
			{
// 				if( !empty( $data['soing_date'][$row]) )
				{
					if( $data['soing_date'][$row] == "1970-01-01")
					{
						$data['soing_date'][$row] = "01-01-2000";
					}
					
					$soing['soing_date'] = formatDate( 'Y-m-d', $data['soing_date'][$row] );
					$soing['soing_nung'] = $data['soing_nung'][$row];
					$soing['soing_weight'] = $data['soing_weight'][$row];
					$soing['soing_avl_weight'] = $data['soing_avl_weight'][$row];
					$soing['soing_ghat'] = $data['soing_ghat'][$row];
					$soing['soing_takavari'] = $data['soing_takavari'][$row];
					$soing['soing_name'] = $data['soing_name'][$row];
					
					//insert sarin record
					$this->db->insert( 'soing', $soing);
				}
			}
			
			$soc['soing_nung_total'] = $data['soing_nung_total'];
			$soc['soing_weight_total'] = $data['soing_weight_total'];
			$soc['soing_avl_weight_total'] = $data['soing_avl_weight_total'];
			$soc['soing_ghat_total'] = $data['soing_ghat_total'];
			$soc['soing_takavari'] = $data['soing_takavari_total'];
			
			if( getField( "soing_count_id" , "soing_count", "kapan_id", $kapan_id ) )
			{
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "soing_count", $soc);
			}
			else
			{
				$this->db->insert( 'soing_count', $soc);
			}
			//flush array
			$soing = array();
			$soc = array();
			
		//Soing
			$sabkaRecord = $data['sabkaCount'];//count( $data['sabka_date'] );
			$sac['kapan_id'] = $sabka['kapan_id'] = $kapan_id;
			//delete all old record
			query( "DELETE FROM sabka WHERE kapan_id = ".$kapan_id );
			
			for ( $row=0;$row<$sabkaRecord;$row++ )
			{
// 				if( !empty( $data['sabka_date'][$row]) )
				{
					if( $data['sabka_date'][$row] == "1970-01-01")
					{
						$data['sabka_date'][$row] = "01-01-2000";
					}
					
					$sabka['sabka_date'] = formatDate( 'Y-m-d', $data['sabka_date'][$row] );
					$sabka['sabka_nung'] = $data['sabka_nung'][$row];
					$sabka['sabka_weight'] = $data['sabka_weight'][$row];
					$sabka['sabka_avl_weight'] = $data['sabka_avl_weight'][$row];
					$sabka['sabka_ghat'] = $data['sabka_ghat'][$row];
					$sabka['sabka_takavari'] = $data['sabka_takavari'][$row];
					$sabka['sabka_name'] = $data['sabka_name'][$row];
					
					//insert sarin record
					$this->db->insert( 'sabka', $sabka);
				}
			}
			
			$sac['sabka_nung_total'] = $data['sabka_nung_total'];
			$sac['sabka_weight_total'] = $data['sabka_weight_total'];
			$sac['sabka_avl_weight_total'] = $data['sabka_avl_weight_total'];
			$sac['sabka_ghat_total'] = $data['sabka_ghat_total'];
			$sac['sabka_takavari'] = $data['sabka_takavari_total'];
			
			if( getField( "sabka_count_id" , "sabka_count", "kapan_id", $kapan_id ) )
			{
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "sabka_count", $sac);
			}
			else
			{
				$this->db->insert( 'sabka_count', $sac);
			}
			//flush array
			$sabka = array();
			$sac = array();
			
// 		saveAdminLog( $this->router->class, @$data['k_name'], $this->cTableName, $this->cAutoId, $last_id, $logType ); 
		setFlashMessage( 'success', 'Kapan has been updated successfully.');
		
	}
/*
+----------------------------------------------------------+
	Deleting item. hadle both request get and post.
	with single delete and multiple delete.
	@prams : $ids -> integer or array
+----------------------------------------------------------+
*/	
	function deleteData($ids)
	{
		$returnArr = array();
		if($ids)
		{
			foreach($ids as $id)
			{
				$getName = getField('k_name', $this->cTableName, $this->cAutoId, $id);
				saveAdminLog($this->router->class, @$getName, $this->cTableName, $this->cAutoId, $id, 'D');
				
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kacho_number' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kapan_jama' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kapan_jama_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish_repairing' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'sarin' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'sarin_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'takavari' );
			}
			$returnArr['type'] ='success';
			$returnArr['msg'] = count($ids)." records has been deleted successfully.";
		}
		else{
			$returnArr['type'] ='error';
			$returnArr['msg'] = "Please select at least 1 item.";
		}
		echo json_encode($returnArr);
	}
/*
+-----------------------------------------+
	Update status for enabled/disabled
	@params : post array of ids, status
+-----------------------------------------+
*/	
	function updateStatus()
	{
		$status = $this->input->post('status');
		$cat_id = $this->input->post('cat_id');
		$data['admin_user_status'] = $status;
		
		$this->db->where($this->cAutoId,$cat_id);
		$this->db->update($this->cTable,$data);
		//echo $this->db->last_query();
		
	}
	/*
+------------------------------------------------------+
	uploads product image folder
+------------------------------------------------------+
*/	
	function resizeUploadImage()
	{
		/*$file_size = str_replace('M','',ini_get('upload_max_filesize'));
		$object_size = convertToMb($_FILES['article_image']['size']);
		
		if($file_size < $object_size)
			setFlashMessage('error','Upload limit exceed.');
		else*/ 
		{
			$image = uploadFile('admin_profile_image','image','admin_profile'); //input file, type, folder
			if(@$image['error'])
			{
				setFlashMessage('error',$image['error']);
				redirect('admin/'.$this->router->class);
				
			}
			/*$width = getField('image_size_width','image_size','image_size_id',$this->input->post('image_size_id'));
			$height = getField('image_size_height','image_size','image_size_id',$this->input->post('image_size_id'));
			$path = $image['path'];*/
			//$sizeArr = $this->db->where('image_size_id',$this->input->post('image_size_id'))->where('image_size_status','0')->get('image_size')->row_array();
			$path = $image['path'];
			$dest = getResizeFileNameByPath($path,'m',''); //image path, type(s,m), folder
			$returnFlag = resize_image($path, $dest, 60, 60); //source, destination, width, height
			@unlink($path); //delete old image
			return $dest;
		}
	}


}