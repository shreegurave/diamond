
<?php
class mdl_lagadh extends CI_Model
{
	var $cTableName = '';
	var $cAutoId = '';
	var $cPrimaryId = '';
	var $cCategory = '';
	
	function getData()
	{
		if($this->cPrimaryId == "")
		{
			$f = $this->input->get('f');
			$s = $this->input->get('s');
			
			$k_name= $this->input->get('k_name');
			$k_add_name= $this->input->get('k_add_name');
			$k_total= $this->input->get('k_total');
			$k_weight= $this->input->get('k_weight');
			$k_date= $this->input->get('k_date');
			$k_day= $this->input->get('k_day');
			
			if(isset( $k_name ) && $k_name != "")
				$this->db->where('k_name LIKE \'%'.$k_name.'%\' ');
				
			if(isset( $k_add_name ) && $k_add_name != "")
				$this->db->where('k_add_name LIKE \'%'.$k_add_name.'%\' ');
				
			if(isset( $k_total ) && $k_total != "")
				$this->db->where('k_total', $k_total );	
				
			if(isset( $k_weight ) && $k_weight != "")
				$this->db->where('k_weight', $k_weight );
				
			if(isset( $k_date ) && $k_date != "")
				$this->db->where('k_date LIKE \'%'.formatDate( 'Y-m-d', $k_date ).'%\' ');
				
			if(isset( $k_day ) && $k_day != "")
				$this->db->where('k_day', $k_day );
			
			if($f !='' && $s != '')
				$this->db->order_by($f,$s);				
			else
				$this->db->order_by($this->cAutoId,'ASC');
				
		}
		else if($this->cPrimaryId != '')
		{
			$this->db->where( $this->cTableName.'.'.$this->cAutoId, $this->cPrimaryId );
			$this->db->join( 'lagadh l', 'l.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
			$this->db->join( 'takavari t', 't.kapan_id = '.$this->cTableName.'.kapan_id', 'LEFT' );
		}
		
		$res = $this->db->get($this->cTableName);
// 		echo $this->db->last_query();
		return $res;
		
	}
	
	function saveData()
	{
		$data = $this->input->post();
		$kapan_id = _de( $data['item_id']);
		unset($data['item_id']);
		
		//Takavari
		
			$t['kapan_id'] = $kapan_id;
			$t['kn_sd_n'] = $data['t_kachi_takavari_1'];
			$t['kn_sd_v'] = $data['t_kachi_takavari_2'];
			$t['kn_sd1_n'] = $data['t_kachi_takavari_3'];
			$t['kn_sd1_v'] = $data['t_kachi_saij_1'];
			$t['kn_g1_n'] = $data['t_kachi_saij_2'];
			$t['kn_g1_v'] = $data['t_kachi_saij_3'];
			
// 			if( getField( "takavari_id" , "takavari", "kapan_id", $kapan_id ) )
// 			{
// 				$this->db->set( 't_modified_date', 'NOW()', FALSE );
// 				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "takavari", $t );
// 			}
// 			else 
// 			{
// 				$this->db->insert( 'takavari', $t );
// 			}
			
			//flush array
			$t = array();
		
		//Lagath
			$l['kapan_id'] = $kapan_id;
			$l['l_bannavvu'] = $data['l_bannavvu'];
			$l['l_singal_1'] = $data['l_singal_1'];
			$l['l_singal_2'] = $data['l_singal_2'];
			$l['l_bot'] = $data['l_bot'];
			$l['l_choki_1'] = $data['l_choki_1'];
			$l['l_choki_2'] = $data['l_choki_2'];
			$l['l_palchu'] = $data['l_palchu'];
			$l['l_unchu_gol'] = $data['l_unchu_gol'];
			$l['l_color_out'] = $data['l_color_out'];
			$l['l_aankhu_out'] = $data['l_aankhu_out'];
			$l['l_chur'] = $data['l_chur'];
			$l['l_soing_ghat'] = $data['l_soing_ghat'];
			$l['l_chapka_ghat'] = $data['l_chapka_ghat'];
			$l['l_vadharani_ghat'] = $data['l_vadharani_ghat'];
			$l['l_extra_txt_1'] = $data['l_extra_txt_1'];
			$l['l_extra_val_1'] = $data['l_extra_val_1'];
			$l['l_extra_txt_2'] = $data['l_extra_txt_2'];
			$l['l_extra_val_2'] = $data['l_extra_val_2'];
			$l['l_extra_txt_3'] = $data['l_extra_txt_3'];
			$l['l_extra_val_3'] = $data['l_extra_val_3'];
			$l['l_total'] = $data['l_total'];
			$l['l_aavel_vajan'] = $data['l_aavel_vajan'];
			$l['l_mangel_vajan'] = $data['l_mangel_vajan'];
			$l['l_veriation'] = $data['l_veriation'];
			$l['l_extra_txt_4'] = $data['l_extra_txt_4'];
			$l['l_extra_val_4'] = $data['l_extra_val_4'];
			$l['l_extra_txt_5'] = $data['l_extra_txt_5'];
			$l['l_extra_val_5'] = $data['l_extra_val_5'];
			$l['l_extra_txt_6'] = $data['l_extra_txt_6'];
			$l['l_extra_val_6'] = $data['l_extra_val_6'];
			$l['l_plus_ele'] = $data['l_plus_ele'];
			$l['l_plus_eight'] = $data['l_plus_eight'];
			$l['l_plus_six'] = $data['l_plus_six'];
			$l['l_plus_four'] = $data['l_plus_four'];
			$l['l_plus_two'] = $data['l_plus_two'];
			$l['l_plus_ziro'] = $data['l_plus_ziro'];
			$l['l_min_ziro'] = $data['l_min_ziro'];
			$l['l_veriation'] = $data['l_veriation'];
			$l['l_t_extra_val_4'] = $data['l_t_extra_val_4'];
			$l['l_t_extra_val_5'] = $data['l_t_extra_val_5'];
			$l['l_t_extra_val_6'] = $data['l_t_extra_val_6'];
			$l['l_t_plus_ele'] = $data['l_t_plus_ele'];
			$l['l_t_plus_eight'] = $data['l_t_plus_eight'];
			$l['l_t_plus_six'] = $data['l_t_plus_six'];
			$l['l_t_plus_four'] = $data['l_t_plus_four'];
			$l['l_t_plus_two'] = $data['l_t_plus_two'];
			$l['l_t_plus_ziro'] = $data['l_t_plus_ziro'];
			$l['l_t_min_ziro'] = $data['l_t_min_ziro'];
			$l['l_charni_total'] = $data['l_charni_total'];
			$l['l_note'] = $data['l_note'];
			
			if( getField( "lagadh_id" , "lagadh", "kapan_id", $kapan_id ) )
			{
				$this->db->set( 'l_modified_date', 'NOW()', FALSE );
				$this->db->where( $this->cAutoId, $this->cPrimaryId )->update( "lagadh", $l );
			}
			else
			{
				$this->db->insert( 'lagadh', $l );
			}
			
			//flush array
			$l = array();
			
			setFlashMessage('success','Lagadh has been '.( ($this->cPrimaryId != '' ) ? 'updated': 'inserted').' successfully.');
		
	}
/*
+----------------------------------------------------------+
	Deleting item. hadle both request get and post.
	with single delete and multiple delete.
	@prams : $ids -> integer or array
+----------------------------------------------------------+
*/	
	function deleteData($ids)
	{
		$returnArr = array();
		if($ids)
		{
			foreach($ids as $id)
			{
				$getName = getField('k_name', $this->cTableName, $this->cAutoId, $id);
				saveAdminLog($this->router->class, @$getName, $this->cTableName, $this->cAutoId, $id, 'D');
				
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kacho_number' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kapan_jama' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'kapan_jama_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'polish_repairing' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'sarin' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'sarin_count' );
				$this->db->where_in( $this->cAutoId, $id )->delete( 'takavari' );
			}
			$returnArr['type'] ='success';
			$returnArr['msg'] = count($ids)." records has been deleted successfully.";
		}
		else{
			$returnArr['type'] ='error';
			$returnArr['msg'] = "Please select at least 1 item.";
		}
		echo json_encode($returnArr);
	}
/*
+-----------------------------------------+
	Update status for enabled/disabled
	@params : post array of ids, status
+-----------------------------------------+
*/	
	function updateStatus()
	{
		$status = $this->input->post('status');
		$cat_id = $this->input->post('cat_id');
		$data['admin_user_status'] = $status;
		
		$this->db->where($this->cAutoId,$cat_id);
		$this->db->update($this->cTable,$data);
		//echo $this->db->last_query();
		
	}
	/*
+------------------------------------------------------+
	uploads product image folder
+------------------------------------------------------+
*/	
	function resizeUploadImage()
	{
		/*$file_size = str_replace('M','',ini_get('upload_max_filesize'));
		$object_size = convertToMb($_FILES['article_image']['size']);
		
		if($file_size < $object_size)
			setFlashMessage('error','Upload limit exceed.');
		else*/ 
		{
			$image = uploadFile('admin_profile_image','image','admin_profile'); //input file, type, folder
			if(@$image['error'])
			{
				setFlashMessage('error',$image['error']);
				redirect('admin/'.$this->router->class);
				
			}
			/*$width = getField('image_size_width','image_size','image_size_id',$this->input->post('image_size_id'));
			$height = getField('image_size_height','image_size','image_size_id',$this->input->post('image_size_id'));
			$path = $image['path'];*/
			//$sizeArr = $this->db->where('image_size_id',$this->input->post('image_size_id'))->where('image_size_status','0')->get('image_size')->row_array();
			$path = $image['path'];
			$dest = getResizeFileNameByPath($path,'m',''); //image path, type(s,m), folder
			$returnFlag = resize_image($path, $dest, 60, 60); //source, destination, width, height
			@unlink($path); //delete old image
			return $dest;
		}
	}


}