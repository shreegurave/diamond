	
    <?php 
    $this->load->view('elements/footer-menu');  
//  if( $this->session->userdata('is_download_app') )
// 	{
// 		$this->session->set_userdata('is_entersite_loaded',1);
// 	}
    ?>
	<div class="clearfix hide">
		<div class="pull-left"></div>
		<div class="pull-right">
			<a class="mobile_app cursor" id="mobile_app" data-toggle="modal" data-target="#hsquareModal"></a>
		</div>
	</div>
	<?php $this->load->view('elements/download_notification-popup')?>
    </div>
    <!-- //PAGE -->
</div>

<!-- TOVAR MODAL CONTENT -->
<div id="modal-body" class="clearfix">
	<div id="tovar_content"></div>
	<div class="close_block"></div>
</div>
<!-- TOVAR MODAL CONTENT -->

	<!-- SCRIPTS -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if IE]><html class="ie" lang="en"> <![endif]-->
	
	<!--<script src="<?php echo asset_url('js/jquery.min.js')?>" type="text/javascript"></script>-->
	<script src="<?php echo asset_url('js/bootstrap.min.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/jquery.sticky.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/parallax.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/jquery.flexslider-min.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/jquery.jcarousel.js')?>" type="text/javascript"></script>
    <script src="<?php echo asset_url('js/jqueryui.custom.min.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/fancySelect.js')?>"></script>
	<script src="<?php echo asset_url('js/animate.js')?>" type="text/javascript"></script>
    <script src="<?php echo asset_url('js/myscript.js')?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/common.js');?>" type="text/javascript"></script>
	<script src="<?php echo asset_url('js/custom.js');?>" type="text/javascript"></script>
    
	<script>
		if (top != self) top.location.replace(self.location.href);		
	</script>
</body>
</html>