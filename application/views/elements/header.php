<!DOCTYPE html>
<html lang="en">

<head>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- START META -->
	<?php
		/**
		 * global meta in case of local is missing
		 */ 
		$scArr;
		if( IS_CACHE )
		{
			$cache_key = cacheKey( 'site_config' );
			
			if ( ! $header_menu = get_cache( $cache_key ) )
			{
				$scArr = $this->db->where('manufacturer_id',MANUFACTURER_ID)->get('site_config')->row_array();
			
				saveCacheKey( $cache_key, 'site_config');
			
				// Save into the cache for infinite time
				save_cache( $cache_key, $header_menu, 0 );
			}
		}
		else
		{
			$scArr = $this->db->where('manufacturer_id',MANUFACTURER_ID)->get('site_config')->row_array();
		}
	?>

	<title><?php echo ( !empty($custom_page_title) ) ? $custom_page_title : $scArr['custom_page_title']; ?></title>
	<base href="<?php echo site_url();?>" />
	<link rel="shortcut icon" href="<?php echo asset_url('images/fevicon.png')?>">
	
	<meta name="description" content="<?php echo ( !empty($meta_description) ) ? $meta_description : $scArr['meta_description'];?>" />
	<meta name="keywords" content="<?php echo ( !empty($meta_keyword) ) ? $meta_keyword : $scArr['meta_keyword'];?>" />
	<meta name="robots" content="<?php echo ( !empty($robots) ) ? getField('robots_name','seo_robots','robots_id', $robots) : getField('robots_name','seo_robots','robots_id', $scArr['robots']); ?>" />
	<meta name="author" content="<?php echo ( !empty($author) ) ? $author : $scArr['author']; ?>" />
	<meta name="copyright" content="Copyright (c) <?php echo date('Y') ?>" />
	<meta name="generator" content="<?php echo getField('config_value','configuration','config_key','SEO_GENERATOR') ?>" />
	
	<?php if( !empty($canonical) ):?>
	<link rel="canonical" href="<?php echo $canonical; ?>" />
	<?php endif;?>
	
	<!-- END META -->

	<meta http-equiv="vary" content="User-Agent">
	
	<!-- CSS -->
	<link href="<?php echo asset_url('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset_url('css/flexslider.css')?>" rel="stylesheet" type="text/css" />
	<!--<link href="<?php //echo asset_url('css/fancySelect.css')?>" rel="stylesheet" media="screen, projection" />-->
	<link href="<?php echo asset_url('css/animate.css')?>" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo asset_url('css/style.css?v=1.8')?>" rel="stylesheet" type="text/css" />
    
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href="<?php echo asset_url('css/font-awesome.css')?>" rel="stylesheet">
    
    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
    
    <?php $this->load->view('elements/js-variables');  ?>
    

    <noscript> <!-- Show a notification if the user has disabled javascript -->
	<div class="notification error png_bg">
	    <div>
	        Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser" rel="nofollow,noindex">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser" rel="nofollow,noindex">enable</a> Javascript to navigate the interface properly.
	    </div>
	</div>
	</noscript>    
	
</head>
<body>

<!-- PRELOADER -->
<div id="preloader" class="hide"><img src="<?php echo asset_url('images/preloader.gif')?>" alt="" /></div>
<!-- //PRELOADER -->
<div class="preloader_hide1">

	<!-- PAGE -->
	<div id="page">
    
    <?php 
		/**
		 * header-menu cache layer had been oved within "header-menu" view since cache is applicable now only to header-menu-sub
		 */
		$this->load->view('elements/header-menu');
	?>
