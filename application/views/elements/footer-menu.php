		<?php
		$tollFree = getField('config_value','configuration','config_key','TOLL_FREE_NO');
		$infoEmail = getField('config_value','configuration','config_key','ADMIN_EMAIL');
		?>
        <footer>
			
			<!-- CONTAINER -->
			<div class="container" data-animated='fadeInUp'>
				
				<!-- ROW -->
				<div class="row">
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
						<h4><?php echo getLangMsg("contact")?></h4>
						<div class="foot_address"><?php echo getLangMsg("add");?></div>
						<div class="foot_phone"><a href="tel:<?php echo $tollFree;?>" ><?php echo $tollFree; ?></a></div>
						<div class="foot_mail"><a href="mailto:<?php echo $infoEmail ?>" ><?php echo $infoEmail ?></a></div>
						<div class="foot_live_chat"><a title="<?php echo getLangMsg("lc");?>" class="cursor" onclick="zopimOpenWin()"><i class="fa fa-comment-o"></i><?php echo getLangMsg("lc");?></a></div>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
						<h4><?php echo getLangMsg("info");?></h4>
						<ul class="foot_menu">
							<li><a href="<?php echo site_url('about-us') ?>" title="<?php echo getLangMsg("au");?>"><?php echo getLangMsg("au");?></a></li>
                            <li><a href="<?php echo site_url('draw') ?>" title="<?php echo getLangMsg("dro");?>"><?php echo getLangMsg("dro");?></a></li>
							<li><a href="<?php echo site_url('privacy-policy') ?>" title="<?php echo getLangMsg("pp");?>"><?php echo getLangMsg("pp");?></a></li>
							<li><a href="<?php echo site_url('terms-conditions') ?>" title="<?php echo getLangMsg("tc");?>"><?php echo getLangMsg("tc");?></a></li>
							<li><a href="<?php echo site_url('return-policy') ?>" title="<?php echo getLangMsg("rp");?>"><?php echo getLangMsg("rp");?></a></li>
							<li><a href="<?php echo site_url('faqs') ?>" title="<?php echo getLangMsg("faqs");?>"><?php echo getLangMsg("faqs");?></a></li>
							<li><a href="<?php echo site_url('contact-us') ?>" title="<?php echo getLangMsg("cu");?>"><?php echo getLangMsg("cu");?></a></li>
						</ul>
					</div>
					
					<div class="respond_clear_480"></div>
					
					<div class="col-lg-4 col-md-4 col-sm-6 padbot30">
						<h4><?php echo getLangMsg("as");?></h4>
						<p class="font-12">We Gujcart are presenting an alluring range of Bollywood Replica Suits & Sarees with our esteemed clients. <br />
                        Bollywood can be credited with putting the Indian saree on the global map. Modern day sarees are made in numerous styles and for numerous occasions. <br />
                        Bollywood has, time and again, featured a great collection of sarees in all of its aspects, be it on screen or on the red carpet itself. </p>
					</div>
					
					<div class="respond_clear_768"></div>
					
					<div class="col-lg-4 col-md-4 padbot30">
						<h4><?php echo getLangMsg("nl");?></h4>
						 <form id="subscribe_form" class="newsletter_form clearfix" method="post" onsubmit="return false;">
				            <input name="newsletter_email" size="10" type="text" class="search_newsletter" placeholder="Enter your email"/>
				            <input name="Newsletter" type="submit" value="SUBSCRIBE" class="btn newsletter_btn" id="subscribe_btn" title="Subscribe Newsletter" />
	                        <img id="preloading" class="hide" src="<?php echo asset_url('images/preloader-white.gif') ?>" alt="loader" />
				            <br><span id="subscribe_msg" class="msg_subscribe"></span>
				         </form>
						
						<h4><?php echo getLangMsg("wrsn");?></h4>
						<div class="social">
							<a href="<?php echo getFbPageUrl() ?>" title="<?php echo getLangMsg("fb");?>" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="<?php echo getGooglePageUrl() ?>" title="<?php echo getLangMsg("gplus");?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                            <span><a href="<?php echo getAppleAppUrl();?>" target="_blank"><img class="app-icon" src="<?php echo asset_url('images/apple-app-icon.png');?>" alt="Download on the App Store" onclick="window.location=''"/></a></span>
      						<span><a href="<?php echo getAndroidAppUrl();?>" target="_blank"><img class="app-icon" src="<?php echo asset_url('images/android-app-icon.png');?>" alt="Android app on the Google Play Store"/></a></span>
                            <span class="pull-right"><img src="<?php echo asset_url('images/icon-payu.jpg') ?>" alt="payu" width="65" /></span>
						</div>
					</div>
				</div><!-- //ROW -->
			</div><!-- //CONTAINER -->
			
			<!-- COPYRIGHT -->
			<div class="copyright">
				
				<!-- CONTAINER -->
				<div class="container clearfix">
					<div class="foot_logo"><a href="<?php echo site_url() ?>" ><img src="<?php echo asset_url('images/foot_logo.png')?>" alt="GUJCART" title="Online Vegetables and Fruits Shopping Store" /></a></div>
					
					<div class="copyright_inf">
						<span>GUJCART © <?php echo date('Y') ?>. &nbsp; All rights reserved. </span>
						<span class="hide">Developed by <a href="http://www.hsquaretechnology.com" target="_blank">HSQUARE</a></span> |
						<a class="back_top" href="javascript:void(0);" ><?php echo getLangMsg("back_top");?> <i class="fa fa-angle-up"></i></a>
					</div>
				</div><!-- //CONTAINER -->
			</div><!-- //COPYRIGHT -->
		</footer>
        