<script language="javascript">
$(function() {
	if($( ".k_date" ).size()){
		// DATEPICKER FOR FILTER
		$( ".k_date" ).datepicker({
		  changeMonth: true,
   	  	  dateFormat : 'yy-mm-dd',
		  maxDate: "d",
		  numberOfMonths: 2,
// 		  onClose: function( selectedDate ) {
// 			  if(selectedDate != '')
// 				$( "#to" ).datepicker( "option", "minDate", selectedDate );
// 		  }
		});
	}
});
</script>
<div id="content">
	<?php $this->load->view('admin/elements/breadcrumb');?>
	<div class="box">
		<div class="heading">
			<h1>
				<img alt="<?php echo $this->controller ?>" src="<?php echo getMenuIcon($this->controller)?>" height="22"> <?php echo pgTitle($this->controller);?>
			</h1>
			<div class="buttons">
				<a class="button"  onclick="$('#form').submit();">સાચવો</a>
				<a class="button"  href="<?php echo site_url('admin/'.$this->controller);?>">રદ કરો</a>
			</div>
		</div>
		<div class="content">
			<form id="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/'.$this->controller.'/'.$this->controller.'Form')?>">
				<input type="hidden" name="item_id" value="<?php echo (@$this->cPrimaryId != '') ? _en(@$this->cPrimaryId) : ''; ?>"  />
				<div id="tab-general" style="display: block;">
					<fieldset>
						<legend>કાપણ શામેલ કરો</legend>
						<table class="form">
							<tbody>
								<tr>
									<td><span class="required">*</span> કાપણ નામ: </td>
									<td>
										<input type="text" size="40" name="k_name" id="k_name" value="<?php echo ( @$k_name ) ? $k_name : @$_POST['k_name'];?>" />
										<span class="error_msg"><?php echo (@$error)?form_error('k_name'):''; ?> </span>
									</td>
								</tr>
								<tr>
									<td><span class="required">*</span> ચડાવનારનું નામ: </td>
									<td>
										<input type="text" size="40" name="k_add_name" id="k_add_name" value="<?php echo ( @$k_add_name ) ? $k_add_name : @$_POST['k_add_name'];?>" />
										<span class="error_msg"><?php echo (@$error)?form_error('k_add_name'):''; ?> </span>
									</td>
								</tr>
								<tr>
									<td><span class="required">*</span> કાપણ આખી:</td>
									<td>
										<input type="text" size="5" name="k_total" id="k_total" value="<?php echo ( @$k_total) ? $k_total : @$_POST['k_total'];?>" />
										<span class="error_msg"><?php echo (@$error)?form_error('k_total'):''; ?> </span>
									</td>
								</tr>
								<tr>
									<td><span class="required">*</span> કાપણ વજન:</td>
									<td>
										<input type="text" size="5" name="k_weight" id="k_weight" value="<?php echo ( @$k_weight) ? $k_weight : @$_POST['k_weight'];?>" />
										<span class="error_msg"><?php echo (@$error)?form_error('k_weight'):''; ?> </span>
									</td>
								</tr>
								<tr>
									<td><span class="required">*</span> તારીખ:</td>
									<td>
										<input type="text" class="datepicker k_date" name="k_date" id="k_date" size="40" value="<?php echo ( @$k_date ) ? $k_date : @$_POST['k_date'];?>" />
										<span class="error_msg"><?php echo (@$error)?form_error('k_date'):''; ?> </span>
									</td>
								</tr>
								<tr>
									<td><span class="required">*</span> દિવસ:</td>
									<td>
										<select name="k_day" id="k_day">
											<option value="" selected="selected">પસંદ કરો</option>
											<option value="1" <?php echo ( @$k_day == 1 || @$_POST['k_day'] == 1 )?'selected="selected"':'';?>>સોમવાર</option>
											<option value="2" <?php echo ( @$k_day == 2 || @$_POST['k_day'] == 2 )?'selected="selected"':'';?>>મંગળવાર</option>
											<option value="3" <?php echo ( @$k_day == 3 || @$_POST['k_day'] == 3 )?'selected="selected"':'';?>>બુધવાર</option>
											<option value="4" <?php echo ( @$k_day == 4 || @$_POST['k_day'] == 4 )?'selected="selected"':'';?>>ગુરુવાર</option>
											<option value="5" <?php echo ( @$k_day == 5 || @$_POST['k_day'] == 5 )?'selected="selected"':'';?>>શુક્રવાર</option>
											<option value="6" <?php echo ( @$k_day == 6 || @$_POST['k_day'] == 6 )?'selected="selected"':'';?>>શનિવાર</option>
											<option value="7" <?php echo ( @$k_day == 7 || @$_POST['k_day'] == 7 )?'selected="selected"':'';?>>રવિવાર</option>
										</select>
										<span class="error_msg"><?php echo (@$error)?form_error('k_day'):''; ?> </span>
									</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#k_name").focus();
	});
</script>