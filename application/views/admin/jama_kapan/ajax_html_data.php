<input type="hidden" id="hidden_srt" value="<?php echo $srt; ?>" />
<input type="hidden" id="hidden_field" value="<?php echo $field; ?>" />
<form id="form" enctype="multipart/form-data" method="post" action="">
	<table class="list" style="text-align: center;">
		<thead>
			<tr id="heading_tr" style="cursor:pointer;">
				<?php			    
				$n = get_sort_order($this->input->get('s'),$this->input->get('f'),'kapan_id');
				$a = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_name');
				$b = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_add_name');
				$c = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_total');								
				$d = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_weight');
				$e = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_date');
				$f = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_day');
				?>
				<th class="" f="kapan_id" s="<?php echo @$n;?>">ક્રમ</th>
				<th class="" f="k_name" s="<?php echo @$a;?>">કાપણ નામ</th>
				<th class="" f="k_name" s="<?php echo @$a;?>">ચડાવનારનું નામ</th>
				<th class="" style="width: 70px;" f="k_name" s="<?php echo @$a;?>">આપેલ તારીખ</th>
				<th class="" f="k_add_name" s="<?php echo @$b;?>">રફ</th>
				<th class="" f="k_total" s="<?php echo @$c;?>">આખી કાપણ</th>
				<th class="" f="k_weight" s="<?php echo @$d;?>">કાપણ વજન</th>
				<th class="" f="k_date" s="<?php echo @$e;?>">નંગ</th>
				<th class="" f="k_day" s="<?php echo @$f;?>">ગોળ</th>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">સાઈઝ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ટકાવારી</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">સિંગલ 1</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">સિંગલ 2</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">લોટ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ચોકી 1</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ચોકી 2</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">પલ્સુ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ઉચુ ગોળ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">કલર આઉટ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">આખુ આઉટ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">આઉટ</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ચુર</td>
				<td class="" f="kapan_id" s="<?php echo @$n;?>">ઘટ</td>
				<td class="" style="width: 70px;" f="kapan_id" s="<?php echo @$n;?>">જમા તારીખ</td>
				<td class="">Action</td>
				
			</tr> 
			<tr class="filter">        
				<td class=""></td>
				<td class="">
					<input type="text" size="5" name="k_name" value="<?php echo ( @$_GET['k_name'] );?>" id="k_name">
				</td>
				<td class="">
					<input type="text" size="5" name="k_add_name" value="<?php echo ( @$_GET['k_add_name'] );?>" id="k_name">
				</td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>	
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td class=""></td>
				<td align=""><a class="button"  id="searchFilter">શોધ</a></td>
            </tr>   
               
		</thead>
		<tbody>
			<?php 
			if(count($listArr)):
				$totalKapan = $totalKapanWeight = $totalNund = $totalGol = $totalSingle1 = $totalSingle2 = 0;
				$totalSelection = $totalChoki1 = $totalChoki2 = $totalPalchu = $totalUnchuGol = 0;
				$totalColorOut = $totalAllOut = $totalOut = $totalChur = 0;
				foreach($listArr as $k=>$ar):
				?>
					<tr id="<?php echo $ar[$this->cAutoId];?>">
						<td class=""><?php echo $ar['kapan_id'];?></td>
						<td class="left"><?php echo $ar['k_name'];?></td>
						<td class="left"><?php echo $ar['k_add_name'];?></td>
						<td class=""><?php echo formatDate('d m, Y',$ar['kj_start_date']);?></td>
						<td class=""><?php echo $ar['kj_raph'];?></td>
						<td class=""><?php echo $ar['kj_total_kapan'];?></td>
						<td class=""><?php echo $ar['kj_kapan'];?></td>
						<td class=""><?php echo $ar['kj_nung'];?></td>
						<td class=""><?php echo $ar['kj_gol'];?></td>
						<td class=""><?php echo $ar['kj_saij'];?></td>
						<td class=""><?php echo $ar['kj_takavari'];?></td>
						<td class=""><?php echo $ar['kj_singal_1'];?></td>
						<td class=""><?php echo $ar['kj_singal_2'];?></td>
						<td class=""><?php echo $ar['kj_selection'];?></td>
						<td class=""><?php echo $ar['kj_choki_1'];?></td>
						<td class=""><?php echo $ar['kj_choki_2'];?></td>
						<td class=""><?php echo $ar['kj_palchu'];?></td>
						<td class=""><?php echo $ar['kj_uchu_gol'];?></td>
						<td class=""><?php echo $ar['kj_color_out'];?></td>
						<td class=""><?php echo $ar['kj_total_out'];?></td>
						<td class=""><?php echo $ar['kj_out'];?></td>
						<td class=""><?php echo $ar['kj_chur'];?></td>
						<td class=""><?php echo $ar['kj_ghat'];?></td>
						<td class=""><?php echo formatDate('d m, Y',$ar['kj_end_date']);?></td>
						<td class=""><a href="<?php echo site_url('admin/manage_kapan/manage_kapanForm?edit=true&item_id='._en( $ar['kapan_id'] ) );?>">Edit</a></td>              
					</tr>
					<?php 
					$totalKapan += $ar['kj_total_kapan'];
					$totalKapanWeight += $ar['kj_kapan'];
					$totalNund += $ar['kj_nung'];
					$totalGol += $ar['kj_gol'];
					$totalSingle1 += $ar['kj_singal_1'];
					$totalSingle2 += $ar['kj_singal_2'];
					$totalSelection += $ar['kj_selection'];
					$totalChoki1 += $ar['kj_choki_1'];
					$totalChoki2 += $ar['kj_choki_2'];
					$totalPalchu += $ar['kj_palchu'];
					$totalUnchuGol += $ar['kj_uchu_gol'];
					$totalColorOut += $ar['kj_color_out'];
					$totalAllOut += $ar['kj_total_out'];
					$totalOut += $ar['kj_out'];
					$totalChur += $ar['kj_chur'];
				endforeach;
				?>
				<tr>
						<td class="">કુલ:</td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""><?php echo $totalKapan;?></td>
						<td class=""><?php echo $totalKapanWeight;?></td>
						<td class=""><?php echo $totalNund;?></td>
						<td class=""><?php echo $totalGol;?></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""><?php echo $totalSingle1;?></td>
						<td class=""><?php echo $totalSingle2;?></td>
						<td class=""><?php echo $totalSelection;?></td>
						<td class=""><?php echo $totalChoki1;?></td>
						<td class=""><?php echo $totalChoki2;?></td>
						<td class=""><?php echo $totalPalchu;?></td>
						<td class=""><?php echo $totalUnchuGol;?></td>
						<td class=""><?php echo $totalColorOut;?></td>
						<td class=""><?php echo $totalAllOut?></td>
						<td class=""><?php echo $totalOut;?></td>
						<td class=""><?php echo $totalChur;?></td>
						<td class=""></td>
						<td class=""></td>
						<td class=""><a href="<?php echo site_url('admin/manage_kapan/manage_kapanForm?edit=true&item_id='._en( $ar['kapan_id'] ) );?>">Edit</a></td>              
					</tr>
				<?php
			else:
				echo "<tr><td class='' colspan='25'>No results!</td></tr>";
			endif; 
			?>
		</tbody>
	</table>
	<div class="pagination">
		<?php $this->load->view('admin/elements/table_footer');?>
	</div>
</form>