<div id="content">
	<?php $this->load->view('admin/elements/breadcrumb');?>
	<div class="box">
		<div class="heading">
			<h1>
				<img alt="<?php echo $this->controller ?>" src="<?php echo getMenuIcon($this->controller)?>" height="22"> <?php echo pgTitle($this->controller);?>
			</h1>
			<div class="buttons">
				<form id="form" enctype="multipart/form-data" action="<?php echo site_url('admin/manage_kapan/kapanJamaInvoiceFull' )?>" method="post" >
					<input type="hidden" name="k_name" value="" id="print_k_name">
					<input type="hidden" name="k_add_name" value="" id="print_k_add_name">
					<input type="text" class="datepicker" placeholder="Start Date" name="start_date" id="from" value="<?php echo ( @$_GET['k_date'] );?>" style="width: 65px;"> TO
					<input type="text" class="datepicker" placeholder="End Date" name="end_date" id="to" value="<?php echo ( @$_GET['k_date'] );?>" style="width: 65px;">
					<a class="button" target="_blank" onclick="getFieldValue(); $('#form').submit();" title="Print">Print</a>
				</form>
			</div>
		</div>
		<div class="content">
			<?php $this->load->view('admin/'.$this->controller.'/ajax_html_data'); ?>
		</div>
	</div>
</div>
<script>
function getFieldValue()
{
	$('#print_k_name').val( $('#k_name').val() );
	$('#print_k_add_name').val( $('k_add_name').val() );
}
</script>