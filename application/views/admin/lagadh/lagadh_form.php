<script type="text/javascript" src="<?php echo asset_url('js/admin/diamond.js?v=0.2');?>"></script>
<style>
	.wd20 { width: 20px !important; }
	.burlywood { background-color: burlywood; }
	.lightblue { background-color: lightblue; }
	.end { text-align: end; }
	input { text-align: center; }
	.decoration { text-decoration: none; }
</style>
<div id="content">
	<?php $this->load->view('admin/elements/breadcrumb');?>
	<div class="box">
		<div class="heading">
			<h1>
				<img alt="<?php echo $this->controller ?>" src="<?php echo getMenuIcon($this->controller)?>" height="22"> <?php echo pgTitle($this->controller);?>
			</h1>
			<div class="buttons">
				<a class="button"  onclick="$('#form').submit();">સાચવો</a>
				<a class="button"  href="<?php echo site_url('admin/'.$this->controller);?>">રદ કરો</a>
			</div>
		</div>
		<div class="content">
			<form id="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/'.$this->controller.'/'.$this->controller.'Form')?>">
				<input type="hidden" name="item_id" value="<?php echo (@$this->cPrimaryId != '') ? _en(@$this->cPrimaryId) : ''; ?>"  />
				<div id="tab-general" style="display: block;">
					<fieldset>
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/lagadhInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">લાગઢ શામેલ કરો</a></legend>
							<table class="form">
								<tbody>
									<tr>
										<td class="lightblue" style="width: 60px;">કાપણ નામ:<?php echo $k_name;?></td>
										<td class="burlywood" style="width: 90px;">ચડાવનારનું નામ: <?php echo $k_add_name;?></td>
										<td class="lightblue" style="width: 65px;">કાપણ આખી: <?php echo $k_total;?></td>
										<td class="burlywood" style="width: 65px;">કાપણ વજન: <span id="kWeight"><?php echo $k_weight;?></span></td>
										<td class="lightblue" style="width: 60px;">તારીખ: <?php echo formatDate( 'd-m-Y', $k_date);?></td>
										<td class="burlywood" style="width: 60px;">દિવસ: 
											<?php 
											if( $k_day == 1 )
												echo 'સોમવાર';
											else if( $k_day== 2 )
												echo 'મંગળવાર';
											else if( $k_day== 3 )
												echo 'બુધવાર';
											else if( $k_day== 5 )
												echo 'ગુરુવાર';
											else if( $k_day == 5 )
												echo 'શુક્રવાર';
											else if( $k_day == 6 )
												echo 'શનિવાર';
											else 
												echo 'રવિવાર';
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table style="width: 100%">
								<tr>
									<td>
										<table class="form list">
											<thead>
												<tr id="heading_tr" style="cursor:pointer;">
													<td class="center">નામ</td>
													<td class="center">વજન</td>
									            </tr>
											</thead>
											<tbody>
												<tr>
													<td class="center"> બનાવવાનું: </td>
													<td class="center">
														<input type="text" size="5" class="lagath_count" name="l_bannavvu" id="l_bannavvu" value="<?php echo ( @$l_bannavvu ) ? $l_bannavvu : @$_POST['l_bannavvu'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_bannavvu'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> સિંગલ-1: </td>
													<td class="center">
														<input type="text" size="5" class="lagath_count" name="l_singal_1" id="l_singal_1" value="<?php echo ( @$l_singal_1 ) ? $l_singal_1 : @$_POST['l_singal_1'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_singal_1'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> સિંગલ-2:</td>
													<td class="center">
														<input type="text" size="5" class="lagath_count" name="l_singal_2" id="l_singal_2" value="<?php echo ( @$l_singal_2 ) ? $l_singal_2 : @$_POST['l_singal_2'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_singal_2'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> બોટ:</td>
													<td class="center">
														<input type="text" size="5" class="lagath_count" name="l_bot" id="l_bot" value="<?php echo ( @$l_bot ) ? $l_bot: @$_POST['l_bot'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_bot'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> ચોકી-1:</td>
													<td class="center">
														<input type="text" class="lagath_count" name="l_choki_1" id="l_choki_1" size="5" value="<?php echo ( @$l_choki_1 ) ? $l_choki_1 : @$_POST['l_choki_1'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_choki_1'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> ચોકી-2:</td>
													<td class="center">
														<input type="text" class="lagath_count" name="l_choki_2" id="l_choki_2" size="5" value="<?php echo ( @$l_choki_2 ) ? $l_choki_2 : @$_POST['l_choki_2'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_choki_2'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> પલચું: </td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_palchu" id="l_palchu" value="<?php echo ( @$l_palchu ) ? $l_palchu : @$_POST['l_palchu'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_palchu'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> ઉચું ગોળ: </td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_unchu_gol" id="l_unchu_gol" value="<?php echo ( @$l_unchu_gol) ? $l_unchu_gol: @$_POST['l_unchu_gol'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_unchu_gol'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> કલર આઉટ:</td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_color_out" id="l_color_out" value="<?php echo ( @$l_color_out) ? $l_color_out: @$_POST['l_color_out'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_color_out'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> આખુ આઉટ:</td>
													<td class="center">
														<input type="text" class="lagath_count" class="lagath_count" size="5" name="l_aankhu_out" id="l_aankhu_out" value="<?php echo ( @$l_aankhu_out) ? $l_aankhu_out: @$_POST['l_aankhu_out'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_aankhu_out'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> ચુર:</td>
													<td class="center">
														<input type="text" class="lagath_count" name="l_chur" id="l_chur" size="5" value="<?php echo ( @$l_chur) ? $l_chur: @$_POST['l_chur'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_chur'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> સોઈંગ ઘટ:</td>
													<td class="center">
														<input type="text" class="lagath_count" name="l_soing_ghat" id="l_soing_ghat" size="5" value="<?php echo ( @$l_soing_ghat) ? $l_soing_ghat: @$_POST['l_soing_ghat'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_soing_ghat'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> ચપકા ઘટ: </td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_chapka_ghat" id="l_chapka_ghat" value="<?php echo ( @$l_chapka_ghat) ? $l_chapka_ghat: @$_POST['l_chapka_ghat'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_chapka_ghat'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"> વધારાની ઘટ: </td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_vadharani_ghat" id="l_vadharani_ghat" value="<?php echo ( @$l_vadharani_ghat) ? $l_vadharani_ghat: @$_POST['l_vadharani_ghat'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_vadharani_ghat'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_1" id="l_extra_txt_1" value="<?php echo ( @$l_extra_txt_1) ? $l_extra_txt_1: @$_POST['l_extra_txt_1'];?>" placeholder="extra-1" />:</td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_extra_val_1" id="l_extra_val_1" value="<?php echo ( @$l_extra_val_1) ? $l_extra_val_1: @$_POST['l_extra_val_1'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_1'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_2" id="l_extra_txt_2" value="<?php echo ( @$l_extra_txt_2) ? $l_extra_txt_2: @$_POST['l_extra_txt_2'];?>" placeholder="extra-2" />:</td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_extra_val_2" id="l_extra_val_2" value="<?php echo ( @$l_extra_val_2) ? $l_extra_val_2: @$_POST['l_extra_val_2'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_2'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_3" id="l_extra_txt_3" value="<?php echo ( @$l_extra_txt_3) ? $l_extra_txt_3: @$_POST['l_extra_txt_3'];?>" placeholder="extra-3" />:</td>
													<td class="center">
														<input type="text" class="lagath_count" size="5" name="l_extra_val_3" id="l_extra_val_3" value="<?php echo ( @$l_extra_val_3) ? $l_extra_val_3: @$_POST['l_extra_val_3'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_3'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center" > સરવાળો:</td>
													<td class="center">
														<input type="text" name="l_total" id="lagath_count" size="5" value="<?php echo ( @$l_total) ? $l_total: @$_POST['l_total'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_total'):''; ?> </span>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
									<td style="vertical-align: baseline;">
										<table class="form list">
											<thead>
												<tr id="heading_tr">
													<td class="center" colspan="2">કાચી ટકાવારી</td>
													<td class="center" colspan="2">કાચી સાઈઝ</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="center">કાચુ વજન :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_takavari_1" id="t_kachi_takavari_1" value="<?php echo ( @$t_kachi_takavari_1) ? $t_kachi_takavari_1: @$_POST['t_kachi_takavari_1'];?>">
													</td>
													<td class="center">ચડાવેલ નંગ :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_saij_1" id="t_kachi_saij_1" value="<?php echo ( @$t_kachi_saij_1) ? $t_kachi_saij_1: @$_POST['t_kachi_saij_1'];?>">
													</td>
									            </tr>
									            <tr>
									            	<td class="center">ચડાવેલ વજન :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_takavari_2" id="t_kachi_takavari_2" value="<?php echo ( @$t_kachi_takavari_2) ? $t_kachi_takavari_2: @$_POST['t_kachi_takavari_2'];?>">
													</td>
													<td class="center">ચડાવેલ વજન :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_saij_2" id="t_kachi_saij_2" value="<?php echo ( @$t_kachi_saij_2) ? $t_kachi_saij_2: @$_POST['t_kachi_saij_2'];?>">
													</td>
									            </tr>
									            <tr>
													<td class="center">ટકાવારી :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
													</td>
													<td class="center">ટકાવારી :</td>
													<td class="center">
														<input  type="text" size="5" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
													</td>
												</tr>
												<tr>
													<td class="center" colspan="4"></td>
												</tr>
												<tr>
													<td class="center"> </td>
													<td class="center">આવેલ વજન :</td>
													<td class="center"><input  type="text" size="5" name="l_aavel_vajan" id="l_aavel_vajan" value="<?php echo ( @$l_aavel_vajan) ? $l_aavel_vajan: @$_POST['l_aavel_vajan'];?>"></td>
													<td class="center"></td>
												</tr>
												<tr>
													<td class="center"> </td>
													<td class="center">મંગાવેલ વજન :</td>
													<td class="center"><input  type="text" size="5" name="l_mangel_vajan" id="l_mangel_vajan" value="<?php echo ( @$l_mangel_vajan) ? $l_mangel_vajan: @$_POST['l_mangel_vajan'];?>"></td>
													<td class="center"></td>
												</tr>
												<tr>
													<td class="center"> </td>
													<td class="center">વેરિએશન :</td>
													<td class="center"><input  type="text" size="5" name="l_veriation" id="l_veriation" value="<?php echo ( @$l_veriation) ? $l_veriation: @$_POST['l_veriation'];?>"></td>
													<td class="center"></td>
												</tr>
												<tr>
													<td class="center" colspan="4"></td>
												</tr>
												<tr>
													<td class="center"  colspan="4"><textarea rows="25" cols="70" name="l_note" placeholder="નોંધ શામેલ કરો"><?php echo (@$l_note) ? $l_note : @$_POST['l_note']; ?></textarea></td>
												</tr>
												</tbody>
											</table>
										</td>
										<td style="vertical-align: baseline;">
										<table class="form list">
											<thead>
												<tr id="heading_tr">
													<td class="center">ચારણી</td>
													<td class="center">વજન</td>
													<td class="center">ટકાવારી</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_4" id="l_extra_txt_4" value="<?php echo ( @$l_extra_txt_4) ? $l_extra_txt_1: @$_POST['l_extra_txt_4'];?>" placeholder="extra-4" />:</td>
													<td class="center">
														<input type="text" size="5" name="l_extra_val_4" id="l_extra_val_4" class="l_charni_total" value="<?php echo ( @$l_extra_val_4) ? $l_extra_val_4: @$_POST['l_extra_val_4'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_4'):''; ?> </span>
													</td>
													<td class="center">
														<input type="text" size="5" name="l_t_extra_val_4" id="l_t_extra_val_4" value="<?php echo ( @$l_t_extra_val_4) ? $l_t_extra_val_4: @$_POST['l_t_extra_val_4'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_t_extra_val_4'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_5" id="l_extra_txt_5" value="<?php echo ( @$l_extra_txt_5) ? $l_extra_txt_5: @$_POST['l_extra_txt_5'];?>" placeholder="extra-5" />:</td>
													<td class="center">
														<input type="text" size="5" name="l_extra_val_5" id="l_extra_val_5" class="l_charni_total" value="<?php echo ( @$l_extra_val_5) ? $l_extra_val_5: @$_POST['l_extra_val_5'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_5'):''; ?> </span>
													</td>
													<td class="center">
														<input type="text" size="5" name="l_t_extra_val_5" id="l_t_extra_val_5" value="<?php echo ( @$l_t_extra_val_5) ? $l_t_extra_val_5: @$_POST['l_t_extra_val_5'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_t_extra_val_5'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center"><input type="text" size="5" name="l_extra_txt_6" id="l_extra_txt_6" value="<?php echo ( @$l_extra_txt_6) ? $l_extra_txt_6: @$_POST['l_extra_txt_6'];?>" placeholder="extra-6" />:</td>
													<td class="center">
														<input type="text" size="5" name="l_extra_val_6" id="l_extra_val_6" class="l_charni_total" value="<?php echo ( @$l_extra_val_3) ? $l_extra_val_3: @$_POST['l_extra_val_6'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_3'):''; ?> </span>
													</td>
													<td class="center">
														<input type="text" size="5" name="l_t_extra_val_6" id="l_t_extra_val_6" value="<?php echo ( @$l_t_extra_val_3) ? $l_t_extra_val_3: @$_POST['l_t_extra_val_6'];?>" />
														<span class="error_msg"><?php echo (@$error)?form_error('l_t_extra_val_3'):''; ?> </span>
													</td>
												</tr>
												<tr>
													<td class="center">+11 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_ele" id="l_plus_ele" class="l_charni_total" value="<?php echo ( @$l_plus_ele) ? $l_plus_ele: @$_POST['l_plus_ele'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_ele" id="l_t_plus_ele" value="<?php echo ( @$l_t_plus_ele) ? $l_t_plus_ele: @$_POST['l_t_plus_ele'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">+8-11 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_eight" id="l_plus_eight" class="l_charni_total" value="<?php echo ( @$l_plus_eight) ? $l_plus_eight: @$_POST['l_plus_eight'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_eight" id="l_t_plus_eight" value="<?php echo ( @$l_t_plus_eight) ? $l_t_plus_eight: @$_POST['l_t_plus_eight'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">+6.5-8 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_six" id="l_plus_six" class="l_charni_total" value="<?php echo ( @$l_plus_six) ? $l_plus_six: @$_POST['l_plus_six'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_six" id="l_t_plus_six" value="<?php echo ( @$l_t_plus_six) ? $l_t_plus_six: @$_POST['l_t_plus_six'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">+5-6.5 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_four" id="l_plus_four" class="l_charni_total" value="<?php echo ( @$l_plus_four) ? $l_plus_four: @$_POST['l_plus_four'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_four" id="l_t_plus_four" value="<?php echo ( @$l_t_plus_four) ? $l_t_plus_four: @$_POST['l_t_plus_four'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">+2-5 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_two" id="l_plus_two" class="l_charni_total" value="<?php echo ( @$l_plus_two) ? $l_plus_two: @$_POST['l_plus_two'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_two" id="l_t_plus_two" value="<?php echo ( @$l_t_plus_two) ? $l_t_plus_two: @$_POST['l_t_plus_two'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">+000-2 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_plus_ziro" id="l_plus_ziro" class="l_charni_total" value="<?php echo ( @$l_plus_ziro) ? $l_plus_ziro: @$_POST['l_plus_ziro'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_plus_ziro" id="l_t_plus_ziro" value="<?php echo ( @$l_t_plus_ziro) ? $l_t_plus_ziro: @$_POST['l_t_plus_ziro'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">-000 :</td>
													<td class="center">
														<input  type="text" size="5" name="l_min_ziro" id="l_min_ziro" class="l_charni_total" value="<?php echo ( @$l_min_ziro) ? $l_min_ziro: @$_POST['l_min_ziro'];?>">
													</td>
													<td class="center">
														<input  type="text" size="5" name="l_t_min_ziro" id="l_t_min_ziro" value="<?php echo ( @$l_t_min_ziro) ? $l_t_min_ziro: @$_POST['l_t_min_ziro'];?>">
													</td>
												</tr>
												<tr>
													<td class="center">સરવાળો :</td>
													<td class="center">
														<input  type="text" size="5" name="l_charni_total" id="l_charni_total" value="<?php echo ( @$l_charni_total) ? $l_charni_total: @$_POST['l_charni_total'];?>">
													</td>
													<td class="center"></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>
					</fieldset>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on("change", "#l_mangel_vajan", function() { 
		var a = $("#l_aavel_vajan").val();
		var b = $("#l_mangel_vajan").val();
		$("#l_veriation").val( a - b );
	 });
</script>