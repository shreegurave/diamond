<div id="content">
	<?php $this->load->view('admin/elements/breadcrumb');?>
	<div class="box">
		<div class="heading">
			<h1>
				<img alt="<?php echo $this->controller ?>" src="<?php echo getMenuIcon($this->controller)?>" height="22"> <?php echo pgTitle($this->controller);?>
			</h1>
			<div class="buttons">
				<a class="button"  onclick="$('#form').submit();">સાચવો</a>
				<a class="button"  href="<?php echo site_url('admin/'.$this->controller);?>">રદ કરો</a>
			</div>
		</div>
		<div class="content">
			<form id="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/'.$this->controller.'/'.$this->controller.'Form')?>">
				<input type="hidden" name="item_id" value="<?php echo (@$this->cPrimaryId != '') ? _en(@$this->cPrimaryId) : ''; ?>"  />
				<div id="tab-general" style="display: block;">
					<fieldset>
						<legend>લાગઢ શામેલ કરો</legend>
							<table class="form">
								<tbody>
									<tr>
										<td class="lightblue" style="width: 60px;">કાપણ નામ:<?php echo $k_name;?></td>
										<td class="burlywood" style="width: 90px;">ચડાવનારનું નામ: <?php echo $k_add_name;?></td>
										<td class="lightblue" style="width: 65px;">કાપણ આખી: <?php echo $k_total;?></td>
										<td class="burlywood" style="width: 65px;">કાપણ વજન: <span id="kWeight"><?php echo $k_weight;?></span></td>
										<td class="lightblue" style="width: 60px;">તારીખ: <?php echo formatDate( 'd-m-Y', $k_date);?></td>
										<td class="burlywood" style="width: 60px;">દિવસ: 
											<?php 
											if( $k_day == 1 )
												echo 'સોમવાર';
											else if( $k_day== 2 )
												echo 'મંગળવાર';
											else if( $k_day== 3 )
												echo 'બુધવાર';
											else if( $k_day== 4 )
												echo 'ગુરુવાર';
											else if( $k_day == 5 )
												echo 'શુક્રવાર';
											else if( $k_day == 6 )
												echo 'શનિવાર';
											else 
												echo 'રવિવાર';
											?>
										</td>
									</tr>
								</tbody>
							</table>
							<table class="form list">
								<thead>
									<tr id="heading_tr">
										<td class="center">નામ</td>
										<td class="center">વજન</td>
										<td></td>
										<td class="center" colspan="2">કાચી ટકાવારી</td>
										<td class="center" colspan="2">કાચી સાઈઝ</td>
										
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><span class="required">*</span> બનાવવાનું: </td>
										<td>
											<input type="text" size="5" name="l_bannavvu" id="l_bannavvu" value="<?php echo ( @$l_bannavvu ) ? $l_bannavvu : @$_POST['l_bannavvu'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_bannavvu'):''; ?> </span>
										</td>
										<td style="width: 450px;"></td>
										<td class="wd100px">કાચુ વજન :</td>
										<td>
											<input class="center" type="text" size="4" name="t_kachi_takavari_1" id="t_kachi_takavari_1" value="<?php echo ( @$t_kachi_takavari_1) ? $t_kachi_takavari_1: @$_POST['t_kachi_takavari_1'];?>">
										</td>
										<td>ચડાવેલ નંગ :</td>
										<td>
											<input class="center" type="text" size="4" name="t_kachi_saij_1" id="t_kachi_saij_1" value="<?php echo ( @$t_kachi_saij_1) ? $t_kachi_saij_1: @$_POST['t_kachi_saij_1'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> સિંગલ-1: </td>
										<td>
											<input type="text" size="5" name="l_singal_1" id="l_singal_1" value="<?php echo ( @$l_singal_1 ) ? $l_singal_1 : @$_POST['l_singal_1'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_singal_1'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ચડાવેલ વજન :</td>
										<td>
											<input class="center" type="text" size="4" name="t_kachi_takavari_2" id="t_kachi_takavari_2" value="<?php echo ( @$t_kachi_takavari_2) ? $t_kachi_takavari_2: @$_POST['t_kachi_takavari_2'];?>">
										</td>
										<td>ચડાવેલ વજન :</td>
										<td>
											<input class="center" type="text" size="4" name="t_kachi_saij_2" id="t_kachi_saij_2" value="<?php echo ( @$t_kachi_saij_2) ? $t_kachi_saij_2: @$_POST['t_kachi_saij_2'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> સિંગલ-2:</td>
										<td>
											<input type="text" size="5" name="l_singal_2" id="l_singal_2" value="<?php echo ( @$l_singal_2 ) ? $l_singal_2 : @$_POST['l_singal_2'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_singal_2'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> બોટ:</td>
										<td>
											<input type="text" size="5" name="l_bot" id="l_bot" value="<?php echo ( @$l_bot ) ? $l_bot: @$_POST['l_bot'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_bot'):''; ?> </span>
										</td>
										<td class="center" colspan="5"><b><span style="font-size: 15px;">ચારણી</span></b></td>
									</tr>
									<tr>
										<td><span class="required">*</span> ચોકી-1:</td>
										<td>
											<input type="text" name="l_choki_1" id="l_choki_1" size="5" value="<?php echo ( @$l_choki_1 ) ? $l_choki_1 : @$_POST['l_choki_1'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_choki_1'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> ચોકી-2:</td>
										<td>
											<input type="text" name="l_choki_2" id="l_choki_2" size="5" value="<?php echo ( @$l_choki_2 ) ? $l_choki_2 : @$_POST['l_choki_2'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_choki_2'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> પલચું: </td>
										<td>
											<input type="text" size="5" name="l_palchu" id="l_palchu" value="<?php echo ( @$l_palchu ) ? $l_palchu : @$_POST['l_palchu'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_palchu'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> ઉચું ગોળ: </td>
										<td>
											<input type="text" size="5" name="l_unchu_gol" id="l_unchu_gol" value="<?php echo ( @$l_unchu_gol) ? $l_unchu_gol: @$_POST['l_unchu_gol'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_unchu_gol'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> કલર આઉટ:</td>
										<td>
											<input type="text" size="5" name="l_color_out" id="l_color_out" value="<?php echo ( @$l_color_out) ? $l_color_out: @$_POST['l_color_out'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_color_out'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> આખુ આઉટ:</td>
										<td>
											<input type="text" size="5" name="l_aankhu_out" id="l_aankhu_out" value="<?php echo ( @$l_aankhu_out) ? $l_aankhu_out: @$_POST['l_aankhu_out'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_aankhu_out'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> ચુર:</td>
										<td>
											<input type="text" name="l_chur" id="l_chur" size="5" value="<?php echo ( @$l_chur) ? $l_chur: @$_POST['l_chur'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_chur'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> સોઈંગ ઘટ:</td>
										<td>
											<input type="text" name="l_soing_ghat" id="l_soing_ghat" size="5" value="<?php echo ( @$l_soing_ghat) ? $l_soing_ghat: @$_POST['l_soing_ghat'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_soing_ghat'):''; ?> </span>
										</td>
										<td></td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_takavari_3" id="t_kachi_takavari_3" value="<?php echo ( @$t_kachi_takavari_3) ? $t_kachi_takavari_3: @$_POST['t_kachi_takavari_3'];?>">
										</td>
										<td class="wd100px">ટકાવારી :</td>
										<td class="wd100px">
											<input class="center" type="text" size="4" name="t_kachi_saij_3" id="t_kachi_saij_3" value="<?php echo ( @$t_kachi_saij_3) ? $t_kachi_saij_3: @$_POST['t_kachi_saij_3'];?>">
										</td>
									</tr>
									<tr>
										<td><span class="required">*</span> ચપકા ઘટ: </td>
										<td>
											<input type="text" size="5" name="l_chapka_ghat" id="l_chapka_ghat" value="<?php echo ( @$l_chapka_ghat) ? $l_chapka_ghat: @$_POST['l_chapka_ghat'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_chapka_ghat'):''; ?> </span>
										</td>
										<td colspan="5"></td>
									</tr>
									<tr>
										<td><span class="required">*</span> વધારાની ઘટ: </td>
										<td>
											<input type="text" size="5" name="l_vadharani_ghat" id="l_vadharani_ghat" value="<?php echo ( @$l_vadharani_ghat) ? $l_vadharani_ghat: @$_POST['l_vadharani_ghat'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_vadharani_ghat'):''; ?> </span>
										</td>
										<td colspan="5"></td>
									</tr>
									<tr>
										<td><input type="text" size="5" name="l_extra_txt_1" id="l_extra_txt_1" value="<?php echo ( @$l_extra_txt_1) ? $l_extra_txt_1: @$_POST['l_extra_txt_1'];?>" />:</td>
										<td>
											<input type="text" size="5" name="l_extra_val_1" id="l_extra_val_1" value="<?php echo ( @$l_extra_val_1) ? $l_extra_val_1: @$_POST['l_extra_val_1'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_1'):''; ?> </span>
										</td>
										<td colspan="5"></td>
									</tr>
									<tr>
										<td><input type="text" size="5" name="l_extra_txt_2" id="l_extra_txt_2" value="<?php echo ( @$l_extra_txt_2) ? $l_extra_txt_2: @$_POST['l_extra_txt_2'];?>" />:</td>
										<td>
											<input type="text" size="5" name="l_extra_val_2" id="l_extra_val_2" value="<?php echo ( @$l_extra_val_2) ? $l_extra_val_2: @$_POST['l_extra_val_2'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_2'):''; ?> </span>
										</td>
										<td colspan="5"></td>
									</tr>
									<tr>
										<td><input type="text" size="5" name="l_extra_txt_3" id="l_extra_txt_3" value="<?php echo ( @$l_extra_txt_3) ? $l_extra_txt_3: @$_POST['l_extra_txt_3'];?>" />:</td>
										<td>
											<input type="text" size="5" name="l_extra_val_3" id="l_extra_val_3" value="<?php echo ( @$l_extra_val_3) ? $l_extra_val_3: @$_POST['l_extra_val_3'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_extra_val_3'):''; ?> </span>
										</td>
										<td colspan="5"></td>
									</tr>
									<tr>
										<td><span class="required">*</span> સરવાળો:</td>
										<td>
											<input type="text" name="l_total" id="l_total" size="5" value="<?php echo ( @$l_total) ? $l_total: @$_POST['l_total'];?>" />
											<span class="error_msg"><?php echo (@$error)?form_error('l_total'):''; ?> </span>
										</td>
										<td colspan="5"></td>		
									</tr>
								</tbody>
							</table>
					</fieldset>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#k_name").focus();
	});
</script>