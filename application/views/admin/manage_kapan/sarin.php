<table class="form list" id="sarin_tbl">
	<?php 
	$date = date('m/d/Y h:i:s a', time());
	?>
	<input type="hidden" value="10" name="sarinCount" id="sarinCount"> 
	<thead>
		<tr id="heading_tr">
			<td class="center"></td>
			<td class="center">સાઈઝ</td>
			<td class="center">
				<input type="text" size="5" id="sc_saij" name="sc_saij" value="<?php echo ( @$sc_saij) ? $sc_saij: @$_POST['sc_saij'];?>">
			</td>
			<td class="center">ટકાવારી</td>
			<td class="center">
				<input type="text" size="5" id="sc_takavari" name="sc_takavari" value="<?php echo ( @$sc_takavari) ? $sc_takavari: @$_POST['sc_takavari'];?>">
			</td>
			<td class="center" colspan="4"></td>
		</tr>
		<tr id="heading_tr">
			<td class="center">ક્રમ</td>
			<td class="center">તારીખ</td>
			<td class="center">નંગ</td>
			<td class="center">વજન</td>
			<td class="center">પ્યોરીટી</td>
			<td class="center">ચારણી</td>
			<td class="center hide">ટકાવારી</td>
			<td class="center">સરીન નામ</td>
			<td class="center hide">4P નામ</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$sarinCount = 10;
		$sarinArrCount = count( $sarinArr );
		if( $sarinArrCount >= $sarinCount)
		{
			$sarinCount= $sarinArrCount;
		}
		
		for( $row=0;$row<$sarinCount;$row++ )
		{
			?>
			<tr class="row-table table row-new sarin_tr">
				<td class="center wd20"><?php echo $row+1;?></td>
				<td class="center">
					<input type="text" size="6" class="datepicker k_date" name="s_date[]" value="<?php echo ( ( isset( $sarinArr[$row]['s_date'] ) ) && ( $sarinArr[$row]['s_date'] == "0000-00-00 00:00:00" || $sarinArr[$row]['s_date'] == "2000-01-01 00:00:00"  || $sarinArr[$row]['s_date'] == "1970-01-01 00:00:00" ) ) ?  formatDate( 'd-m-Y', $date ): ( isset( $sarinArr[$row]['s_date'] ) ) ? formatDate( 'd-m-Y', $sarinArr[$row]['s_date'] ) : @$_POST['s_date['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="s_nung[]" class="s_nung" value="<?php echo ( @$sarinArr[$row]['s_nung'] ) ? $sarinArr[$row]['s_nung'] : @$_POST['s_nung['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="s_weight[]" class="s_weight" value="<?php echo ( @$sarinArr[$row]['s_weight'] ) ? $sarinArr[$row]['s_weight'] : @$_POST['s_weight['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="s_pyority[]" value="<?php echo ( @$sarinArr[$row]['s_pyority'] ) ? $sarinArr[$row]['s_pyority'] : @$_POST['s_pyority['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="s_charni[]" value="<?php echo ( @$sarinArr[$row]['s_charni'] ) ? $sarinArr[$row]['s_charni'] : @$_POST['s_charni['.$row.']'];?>">
				</td>
				<td class="center hide">
					<input type="text" size="1" name="s_takavari[]" value="<?php echo ( @$sarinArr[$row]['s_takavari'] ) ? $sarinArr[$row]['s_takavari'] : @$_POST['s_takavari['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="10" name="s_name[]" value="<?php echo ( @$sarinArr[$row]['s_name'] ) ? $sarinArr[$row]['s_name'] : @$_POST['s_name['.$row.']'];?>">
				</td>
				<td class="center hide">
					<input type="text" size="10" name="s_4p[]" value="<?php echo ( @$sarinArr[$row]['s_4p'] ) ? $sarinArr[$row]['s_4p'] : @$_POST['s_4p['.$row.']'];?>">
				</td>
			</tr>
			<?php
		}
		?>
		
		<tr>
			<td colspan="9" class="end">
				<div class="buttons">
					<a class="button" onclick="addMoreSarinRow();">+ ADD</a>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="center wd20">સરવાળો: </td>
			<td class="center"> </td>
			<td class="center">
				<input type="text" size="1" name="sc_nung" id="sc_nung" value="<?php echo ( @$sc_nung) ? $sc_nung: @$_POST['sc_nung'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="sc_weight" id="sc_weight" value="<?php echo ( @$sc_weight) ? $sc_weight: @$_POST['sc_weight'];?>">
			</td>
			<td class="center"> </td>
			<td class="center"> </td>
			<td class="center  hide"> </td>
			<td class="center"> </td>
			<td class="center hide"> </td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	function addMoreSarinRow()
	{
		var rows = 1;
		$('#sarin_tbl tr').each(function()
		{
			if ($(this).hasClass('sarin_tr')) 
			{
				rows++;
			}
		});

		var countSarin = <?php echo (int)$sarinCount;?>;
		var html = '';
		for( var loop=rows; loop<(rows+5);loop++ )
		{
			
			html += '<tr class="row-table table row-new sarin_tr"><td class="center wd20">'+loop+'</td>'; 
			html += '<td class="center"><input type="text" size="6" class="datepicker k_date" name="s_date[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="s_nung[]" class="s_nung" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="s_weight[]" class="s_weight" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="s_pyority[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="s_charni[]" value=""></td>';
			html += '<td class="center hide"><input type="text" size="1" name="s_takavari[]" value=""></td>';
			html += '<td class="center"><input type="text" size="10" name="s_name[]" value=""></td>';
			html += '<td class="center hide"><input type="text" size="10" name="s_4p[]" value=""></td>';
		}

		jQuery( html ).insertAfter( jQuery(".row-table.table.row-new.sarin_tr").last());
		$("#sarinCount").val( (rows + countSarin ) - 6 );
	}
</script>