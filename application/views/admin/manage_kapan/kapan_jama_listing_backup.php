<table class="form list" style="text-align: center; margin-left: -10px;" id="kapan_jama_tbl">
	<thead>
		<tr id="heading_tr">
			<td class="center" style="padding: 0px;">ક્રમ</td>
			<td class="center" style="padding: 0px;">આ. તારીખ</td>
			<td class="center" style="padding: 0px;">રફ</td>
			<td class="center" style="padding: 0px;">આખી કાપણ</td>
			<td class="center" style="padding: 0px;">કાપણ</td>
			<td class="center" style="padding: 0px;">નંગ</td>
			<td class="center" style="padding: 0px;">ગોળ</td>
			<td class="center" style="padding: 0px;">સાઈઝ</td>
			<td class="center" style="padding: 0px;">ટકાવારી</td>
			<td class="center" style="padding: 0px;">સિંગલ 1</td>
			<td class="center" style="padding: 0px;">સિંગલ 2</td>
			<td class="center" style="padding: 0px;">સિલેક્શન</td>
			<td class="center" style="padding: 0px;">ચોકી 1</td>
			<td class="center" style="padding: 0px;">ચોકી 2</td>
			<td class="center" style="padding: 0px;">પલ્સુ</td>
			<td class="center" style="padding: 0px;">ઉચુ ગોળ</td>
			<td class="center" style="padding: 0px;">કલર આઉટ</td>
			<td class="center" style="padding: 0px;">આખુ આઉટ</td>
			<td class="center" style="padding: 0px;">આઉટ</td>
			<td class="center" style="padding: 0px;">ચુર</td>
			<td class="center" style="padding: 0px;">ઘટ</td>
			<td class="center" style="padding: 0px;">જમા તારીખ</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$kapanJamaCount = 30;
		$kapanJamaArrCount = count( $kapanJamaArr );
		if( $kapanJamaArrCount >= $kapanJamaCount)
		{
			$kapanJamaCount= $kapanJamaArrCount;
		}
		
		for( $row=0;$row<$kapanJamaCount;$row++ )
		{
			?>
			<tr class="row-table table row-new kapan_jama_tr">
				<td class="wd20"><?php echo $row+1;?></td>
				<td>
					<input type="text" size="6" class="datepicker k_date kj_start_date" name="kj_start_date[]" value="<?php echo ( @$kapanJamaArr[$row]['kj_start_date'] ) ? $kapanJamaArr[$row]['kj_start_date'] : @$_POST['kj_start_date['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="3" name="kj_raph[]" class="kj_raph" value="<?php echo ( @$kapanJamaArr[$row]['kj_raph'] ) ? $kapanJamaArr[$row]['kj_raph'] : @$_POST['kj_raph['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_total_kapan[]" class="kj_total_kapan" value="<?php echo ( @$kapanJamaArr[$row]['kj_total_kapan'] ) ? $kapanJamaArr[$row]['kj_total_kapan'] : @$_POST['kj_total_kapan['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_kapan[]" class="kj_kapan" value="<?php echo ( @$kapanJamaArr[$row]['kj_kapan'] ) ? $kapanJamaArr[$row]['kj_kapan'] : @$_POST['kj_kapan['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_nung[]" class="kj_nung" value="<?php echo ( @$kapanJamaArr[$row]['kj_nung'] ) ? $kapanJamaArr[$row]['kj_nung'] : @$_POST['kj_nung['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_gol[]" class="kj_gol" value="<?php echo ( @$kapanJamaArr[$row]['kj_gol'] ) ? $kapanJamaArr[$row]['kj_gol'] : @$_POST['kj_gol['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_saij[]" class="kj_saij" value="<?php echo ( @$kapanJamaArr[$row]['kj_saij'] ) ? $kapanJamaArr[$row]['kj_saij'] : @$_POST['kj_saij['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_takavari[]" class="kj_takavari" value="<?php echo ( @$kapanJamaArr[$row]['kj_takavari'] ) ? $kapanJamaArr[$row]['kj_takavari'] : @$_POST['kj_takavari['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_singal_1[]" class="kj_singal_1" value="<?php echo ( @$kapanJamaArr[$row]['kj_singal_1'] ) ? $kapanJamaArr[$row]['kj_singal_1'] : @$_POST['kj_singal_1['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_singal_2[]" class="kj_singal_2" value="<?php echo ( @$kapanJamaArr[$row]['kj_singal_2'] ) ? $kapanJamaArr[$row]['kj_singal_2'] : @$_POST['kj_singal_2['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_selection[]" class="kj_selection" value="<?php echo ( @$kapanJamaArr[$row]['kj_selection'] ) ? $kapanJamaArr[$row]['kj_selection'] : @$_POST['kj_selection['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_choki_1[]" class="kj_choki_1" value="<?php echo ( @$kapanJamaArr[$row]['kj_choki_1'] ) ? $kapanJamaArr[$row]['kj_choki_1'] : @$_POST['kj_choki_1['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_choki_2[]" class="kj_choki_2" value="<?php echo ( @$kapanJamaArr[$row]['kj_choki_2'] ) ? $kapanJamaArr[$row]['kj_choki_2'] : @$_POST['kj_choki_2['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_palchu[]" class="kj_palchu" value="<?php echo ( @$kapanJamaArr[$row]['kj_palchu'] ) ? $kapanJamaArr[$row]['kj_palchu'] : @$_POST['kj_palchu['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_uchu_gol[]" class="kj_uchu_gol" value="<?php echo ( @$kapanJamaArr[$row]['kj_uchu_gol'] ) ? $kapanJamaArr[$row]['kj_uchu_gol'] : @$_POST['kj_uchu_gol['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_color_out[]" class="kj_color_out" value="<?php echo ( @$kapanJamaArr[$row]['kj_color_out'] ) ? $kapanJamaArr[$row]['kj_color_out'] : @$_POST['kj_color_out['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_total_out[]" class="kj_total_out" value="<?php echo ( @$kapanJamaArr[$row]['kj_total_out'] ) ? $kapanJamaArr[$row]['kj_total_out'] : @$_POST['kj_total_out['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_out[]" class="kj_out" value="<?php echo ( @$kapanJamaArr[$row]['kj_out'] ) ? $kapanJamaArr[$row]['kj_out'] : @$_POST['kj_out['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_chur[]" class="kj_chur" value="<?php echo ( @$kapanJamaArr[$row]['kj_chur'] ) ? $kapanJamaArr[$row]['kj_chur'] : @$_POST['kj_chur['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="1" name="kj_ghat[]" class="kj_ghat" value="<?php echo ( @$kapanJamaArr[$row]['kj_ghat'] ) ? $kapanJamaArr[$row]['kj_ghat'] : @$_POST['kj_ghat['.$row.']'];?>">
				</td>
				<td>
					<input type="text" size="6" class="datepicker k_date kj_end_date" name="kj_end_date[]" value="<?php echo ( @$kapanJamaArr[$row]['kj_end_date'] ) ? $kapanJamaArr[$row]['kj_end_date'] : @$_POST['kj_end_date['.$row.']'];?>">
				</td>
			</tr>
			<?php 
		}
		?>
		
		<tr>
			<td colspan="22" class="end">
				<div class="buttons">
					<a class="button" onclick="addMoreRow();">+ ADD</a>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="wd20"><b>સરવાળો:</b></td>
			<td colspan="3"></td>
			<td>
				<input type="text" size="1" name="kjc_kapan" id="kjc_kapan" value="<?php echo ( @$kjc_kapan) ? $kjc_kapan: @$_POST['kjc_kapan'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_nung" id="kjc_nung" value="<?php echo ( @$kjc_nung) ? $kjc_nung: @$_POST['kjc_nung'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_gol" id="kjc_gol" value="<?php echo ( @$kjc_gol) ? $kjc_gol: @$_POST['kjc_gol'];?>">
			</td>
			<td colspan="2"></td>
			<td>
				<input type="text" size="1" name="kjc_singal_1" id="kjc_singal_1" value="<?php echo ( @$kjc_singal_1) ? $kjc_singal_1: @$_POST['kjc_singal_1'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_singal_2" id="kjc_singal_2" value="<?php echo ( @$kjc_singal_2) ? $kjc_singal_2: @$_POST['kjc_singal_2'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_selection" id="kjc_selection" value="<?php echo ( @$kjc_selection) ? $kjc_selection: @$_POST['kjc_selection'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_choki_1" id="kjc_choki_1" value="<?php echo ( @$kjc_choki_1) ? $kjc_choki_1: @$_POST['kjc_choki_1'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_choki_2" id="kjc_choki_2" value="<?php echo ( @$kjc_choki_2) ? $kjc_choki_2: @$_POST['kjc_choki_2'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_palchu" id="kjc_palchu" value="<?php echo ( @$kjc_palchu) ? $kjc_palchu: @$_POST['kjc_palchu'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_uchu_gol" id="kjc_uchu_gol" value="<?php echo ( @$kjc_uchu_gol) ? $kjc_uchu_gol: @$_POST['kjc_uchu_gol'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_color_out" id="kjc_color_out" value="<?php echo ( @$kjc_color_out) ? $kjc_color_out: @$_POST['kjc_color_out'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_total_out" id="kjc_total_out" value="<?php echo ( @$kjc_total_out) ? $kjc_total_out: @$_POST['kjc_total_out'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_out" id="kjc_out" value="<?php echo ( @$kjc_out) ? $kjc_out: @$_POST['kjc_out'];?>">
			</td>
			<td>
				<input type="text" size="1" name="kjc_chur" id="kjc_chur" value="<?php echo ( @$kjc_chur) ? $kjc_chur: @$_POST['kjc_chur'];?>">
			</td>
			<td colspan="2"></td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	function addMoreRow()
	{
		var rows = 1;
		$('#kapan_jama_tbl tr').each(function()
		{
			if ($(this).hasClass('kapan_jama_tr')) 
			{
				rows++;
			}
		});
		
		var html = '';	
		for( var loop=rows; loop<(rows+5);loop++ )
		{
			html += '<tr class="row-table table row-new kapan_jama_tr"><td class="wd20">'+loop+'</td>'; 
			html += '<td><input type="text" size="6" class="datepicker k_date kj_start_date" name="kj_start_date[]" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_raph[]" class="kj_raph" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_total_kapan[]" class="kj_total_kapan" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_kapan[]" class="kj_kapan" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_nung[]" class="kj_nung" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_gol[]" class="kj_gol" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_saij[]" class="kj_saij" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_takavari[]" class="kj_takavari" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_singal_1[]" class="kj_singal_1" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_singal_2[]" class="kj_singal_2" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_selection[]" class="kj_selection" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_choki_1[]" class="kj_choki_1" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_choki_2[]" class="kj_choki_2" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_palchu[]" class="kj_palchu" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_uchu_gol[]" class="kj_uchu_gol" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_color_out[]" class="kj_color_out" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_total_out[]" class="kj_total_out" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_out[]" class="kj_out" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_chur[]" class="kj_chur" value=""></td>';
			html += '<td><input type="text" size="1" name="kj_ghat[]" class="kj_ghat" value=""></td>';
			html += '<td><input type="text" size="6" class="datepicker k_date kj_end_date" name="kj_end_date[]" value=""></td></tr>';
		}
				
		jQuery( html ).insertAfter( jQuery(".row-table.table.row-new.kapan_jama_tr").last());
	}
</script>