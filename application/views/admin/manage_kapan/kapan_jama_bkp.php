<table class="form">
	<tbody>
		<tr>
			<td>આ. તારીખ: </td>
			<td>
				<input type="text" size="6" class="datepicker k_date kj_start_date" name="kj_start_date" value="<?php echo @$kapanJamaArr['kj_start_date'];?>">
			</td>
			<td>ટકાવારી:</td>
			<td>
				<input type="text" size="1" name="kj_takavari" class="kj_takavari" value="<?php echo @$kapanJamaArr['kj_takavari'];?>">
			</td>
			<td>ઉચુ ગોળ: </td>
			<td>
				<input type="text" size="1" name="kj_uchu_gol" class="kj_uchu_gol" value="<?php echo @$kapanJamaArr['kj_uchu_gol'];?>">
			</td>
		</tr>
		<tr>
			<td>રફ: </td>
			<td>
				<input type="text" size="3" name="kj_raph" class="kj_raph" value="<?php echo @$kapanJamaArr['kj_raph'];?>">
			</td>
			<td>સિંગલ 1:</td>
			<td>
				<input type="text" size="1" name="kj_singal_1" class="kj_singal_1" value="<?php echo @$kapanJamaArr['kj_singal_1'];?>">
			</td>
			<td>કલર આઉટ: </td>
			<td>
				<input type="text" size="1" name="kj_color_out" class="kj_color_out" value="<?php echo @$kapanJamaArr['kj_color_out'];?>">
			</td>
		</tr>
		<tr>
			<td>આખી કાપણ:</td>
			<td>
				<input type="text" size="1" name="kj_total_kapan" class="kj_total_kapan" value="<?php echo @$kapanJamaArr['kj_total_kapan'];?>">
			</td>
			<td>સિંગલ 2:</td>
			<td>
				<input type="text" size="1" name="kj_singal_2" class="kj_singal_2" value="<?php echo @$kapanJamaArr['kj_singal_2'];?>">
			</td>
			<td>આખુ આઉટ: </td>
			<td>
				<input type="text" size="1" name="kj_total_out" class="kj_total_out" value="<?php echo @$kapanJamaArr['kj_total_out'];?>">
			</td>
		</tr>
		<tr>
			<td>કાપણ વજન:</td>
			<td>
				<input type="text" size="1" name="kj_kapan" class="kj_kapan" value="<?php echo @$kapanJamaArr['kj_kapan'];?>">
			</td>
			<td>ચોકી 1: </td>
			<td>
				<input type="text" size="1" name="kj_choki_1" class="kj_choki_1" value="<?php echo @$kapanJamaArr['kj_choki_1'];?>">
			</td>
			<td>આઉટ: </td>
			<td>
				<input type="text" size="1" name="kj_out" class="kj_out" value="<?php echo @$kapanJamaArr['kj_out'];?>">
			</td>
		</tr>
		<tr>
			<td>નંગ:</td>
			<td>
				<input type="text" size="1" name="kj_nung" class="kj_nung" value="<?php echo @$kapanJamaArr['kj_nung'];?>">
			</td>
			<td>ચોકી 2: </td>
			<td>
				<input type="text" size="1" name="kj_choki_2" class="kj_choki_2" value="<?php echo @$kapanJamaArr['kj_choki_2'];?>">
			</td>
			<td>ચુર: </td>
			<td>
				<input type="text" size="1" name="kj_chur" class="kj_chur" value="<?php echo @$kapanJamaArr['kj_chur'];?>">
			</td>
		</tr>
		<tr>
			<td>ગોળ: </td>
			<td>
				<input type="text" size="1" name="kj_gol" class="kj_gol" value="<?php echo @$kapanJamaArr['kj_gol'];?>">
			</td>
			<td>પલ્સુ: </td>
			<td>
				<input type="text" size="1" name="kj_palchu" class="kj_palchu" value="<?php echo @$kapanJamaArr['kj_palchu'];?>">
			</td>
			<td>ઘટ: </td>
			<td>
				<input type="text" size="1" name="kj_ghat" class="kj_ghat" value="<?php echo @$kapanJamaArr['kj_ghat'];?>">
			</td>
		</tr>
		<tr>
			<td>લોટ:</td>
			<td>
				<input type="text" size="1" name="kj_selection" class="kj_selection" value="<?php echo @$kapanJamaArr['kj_selection'];?>">
			</td>
			<td>સાઈઝ: </td>
			<td>
				<input type="text" size="1" name="kj_saij" class="kj_saij" value="<?php echo @$kapanJamaArr['kj_saij'];?>">
			</td>
			<td>જમા તારીખ: </td>
			<td>
				<input type="text" size="6" class="datepicker k_date kj_end_date" name="kj_end_date" value="<?php echo @$kapanJamaArr['kj_end_date'];?>">
			</td>
	</tbody>
</table>