<table class="form list" style="width: 33%;">
	<thead>
		<tr id="heading_tr">
			<td class="center" rowspan="2"></td>
			<td class="center" colspan="2">આપેલ</td>
			<td class="center" colspan="2">આવેલ</td>
		</tr>
		<tr id="heading_tr">
			<td class="center">થાણ</td>
			<td class="center">વજન</td>
			<td class="center">થાણ</td>
			<td class="center">વજન</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>ધારખાડ / ગોળાઈ વિક:</td>
			<td>
				<input type="text" size="1" name="pr_dhar_apl_than" class="apl_than" value="<?php echo ( @$pr_dhar_apl_than) ? $pr_dhar_apl_than: @$_POST['pr_dhar_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dhar_apl_weight" class="apl_weight" value="<?php echo ( @$pr_dhar_apl_weight) ? $pr_dhar_apl_weight: @$_POST['pr_dhar_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dhar_avl_than" class="avl_than" value="<?php echo ( @$pr_dhar_avl_than) ? $pr_dhar_avl_than: @$_POST['pr_dhar_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dhar_avl_weight" class="avl_weight" value="<?php echo ( @$pr_dhar_avl_weight) ? $pr_dhar_avl_weight: @$_POST['pr_dhar_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>તળિયે રીપેરિંગ:</td>
			<td>
				<input type="text" size="1" name="pr_tly_apl_than" class="apl_than" value="<?php echo ( @$pr_tly_apl_than) ? $pr_tly_apl_than: @$_POST['pr_tly_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tly_apl_weight" class="apl_weight" value="<?php echo ( @$pr_tly_apl_weight) ? $pr_tly_apl_weight: @$_POST['pr_tly_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tly_avl_than" class="avl_than" value="<?php echo ( @$pr_tly_avl_than) ? $pr_tly_avl_than: @$_POST['pr_tly_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tly_avl_weight" class="avl_weight" value="<?php echo ( @$pr_tly_avl_weight) ? $pr_tly_avl_weight: @$_POST['pr_tly_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>ટેબલ / માથળા રીપેરિંગ:</td>
			<td>
				<input type="text" size="1" name="pr_tbl_apl_than" class="apl_than" value="<?php echo ( @$pr_tbl_apl_than) ? $pr_tbl_apl_than: @$_POST['pr_tbl_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbl_apl_weight" class="apl_weight" value="<?php echo ( @$pr_tbl_apl_weight) ? $pr_tbl_apl_weight: @$_POST['pr_tbl_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbl_avl_than" class="avl_than" value="<?php echo ( @$pr_tbl_avl_than) ? $pr_tbl_avl_than: @$_POST['pr_tbl_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbl_avl_weight" class="avl_weight" value="<?php echo ( @$pr_tbl_avl_weight) ? $pr_tbl_avl_weight: @$_POST['pr_tbl_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>સારી ( તળિયે ):</td>
			<td>
				<input type="text" size="1" name="pr_stly_apl_than" class="apl_than" value="<?php echo ( @$pr_stly_apl_than) ? $pr_stly_apl_than: @$_POST['pr_stly_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_stly_apl_weight" class="apl_weight" value="<?php echo ( @$pr_stly_apl_weight) ? $pr_stly_apl_weight: @$_POST['pr_stly_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_stly_avl_than" class="avl_than" value="<?php echo ( @$pr_stly_avl_than) ? $pr_stly_avl_than: @$_POST['pr_stly_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_stly_avl_weight" class="avl_weight" value="<?php echo ( @$pr_stly_avl_weight) ? $pr_stly_avl_weight: @$_POST['pr_stly_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>સારી ( માથળે ):</td>
			<td>
				<input type="text" size="1" name="pr_m_apl_than" class="apl_than" value="<?php echo ( @$pr_m_apl_than) ? $pr_m_apl_than: @$_POST['pr_m_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_m_apl_weight" class="apl_weight" value="<?php echo ( @$pr_m_apl_weight) ? $pr_m_apl_weight: @$_POST['pr_m_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_m_avl_than" class="avl_than" value="<?php echo ( @$pr_m_avl_than) ? $pr_m_avl_than: @$_POST['pr_m_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_m_avl_weight" class="avl_weight" value="<?php echo ( @$pr_m_avl_weight) ? $pr_m_avl_weight: @$_POST['pr_m_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>આખો હિરો રીપેરિંગ:</td>
			<td>
				<input type="text" size="1" name="pr_hiro_apl_than" class="apl_than" value="<?php echo ( @$pr_hiro_apl_than) ? $pr_hiro_apl_than: @$_POST['pr_hiro_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_hiro_apl_weight" class="apl_weight" value="<?php echo ( @$pr_hiro_apl_weight) ? $pr_hiro_apl_weight: @$_POST['pr_hiro_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_hiro_avl_than" class="avl_than" value="<?php echo ( @$pr_hiro_avl_than) ? $pr_hiro_avl_than: @$_POST['pr_hiro_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_hiro_avl_weight" class="avl_weight" value="<?php echo ( @$pr_hiro_avl_weight) ? $pr_hiro_avl_weight: @$_POST['pr_hiro_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>ટીક પોઇન્ટ / ટીક જિરમ:</td>
			<td>
				<input type="text" size="1" name="pr_tik_apl_than" class="apl_than" value="<?php echo ( @$pr_tik_apl_than) ? $pr_tik_apl_than: @$_POST['pr_tik_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tik_apl_weight" class="apl_weight" value="<?php echo ( @$pr_tik_apl_weight) ? $pr_tik_apl_weight: @$_POST['pr_tik_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tik_avl_than" class="avl_than" value="<?php echo ( @$pr_tik_avl_than) ? $pr_tik_avl_than: @$_POST['pr_tik_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tik_avl_weight" class="avl_weight" value="<?php echo ( @$pr_tik_avl_weight) ? $pr_tik_avl_weight: @$_POST['pr_tik_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td><input type="text" size="1" name="pr_e1_extra_1" class="pr_e1_extra_1" value="<?php echo ( @$pr_e1_extra_1) ? $pr_e1_extra_1: @$_POST['pr_e1_extra_1'];?>">:</td>
			<td>
				<input type="text" size="1" name="pr_e1_apl_than" class="apl_than" value="<?php echo ( @$pr_e1_apl_than) ? $pr_e1_apl_than: @$_POST['pr_e1_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e1_apl_weight" class="apl_weight" value="<?php echo ( @$pr_e1_apl_weight) ? $pr_e1_apl_weight: @$_POST['pr_e1_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e1_avl_than" class="avl_than" value="<?php echo ( @$pr_e1_avl_than) ? $pr_e1_avl_than: @$_POST['pr_e1_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e1_avl_weight" class="avl_weight" value="<?php echo ( @$pr_e1_avl_weight) ? $pr_e1_avl_weight: @$_POST['pr_e1_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td><input type="text" size="1" name="pr_e1_extra_2" class="pr_e1_extra_2" value="<?php echo ( @$pr_e1_extra_2) ? $pr_e1_extra_2: @$_POST['pr_e1_extra_2'];?>">:</td>
			<td>
				<input type="text" size="1" name="pr_e2_apl_than" class="apl_than" value="<?php echo ( @$pr_e2_apl_than) ? $pr_e2_apl_than: @$_POST['pr_e2_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e2_apl_weight" class="apl_weight" value="<?php echo ( @$pr_e2_apl_weight) ? $pr_e2_apl_weight: @$_POST['pr_e2_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e2_avl_than" class="avl_than" value="<?php echo ( @$pr_e2_avl_than) ? $pr_e2_avl_than: @$_POST['pr_e2_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e2_avl_weight" class="avl_weight" value="<?php echo ( @$pr_e2_avl_weight) ? $pr_e2_avl_weight: @$_POST['pr_e2_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td><input type="text" size="1" name="pr_e1_extra_3" class="pr_e1_extra_3" value="<?php echo ( @$pr_e1_extra_3 ) ? $pr_e1_extra_3 : @$_POST['pr_e1_extra_3'];?>">:</td>
			<td>
				<input type="text" size="1" name="pr_e3_apl_than" class="apl_than" value="<?php echo ( @$pr_e3_apl_than) ? $pr_e3_apl_than: @$_POST['pr_e3_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e3_apl_weight" class="apl_weight" value="<?php echo ( @$pr_e3_apl_weight) ? $pr_e3_apl_weight: @$_POST['pr_e3_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e3_avl_than" class="avl_than" value="<?php echo ( @$pr_e3_avl_than) ? $pr_e3_avl_than: @$_POST['pr_e3_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_e3_avl_weight" class="avl_weight" value="<?php echo ( @$pr_e3_avl_weight) ? $pr_e3_avl_weight: @$_POST['pr_e3_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>સરવાળો:</td>
			<td>
				<input type="text" size="1" name="pr_total_apl_than" id="apl_than" value="<?php echo ( @$pr_total_apl_than) ? $pr_total_apl_than: @$_POST['pr_total_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_total_apl_weight" id="apl_weight" value="<?php echo ( @$pr_total_apl_weight) ? $pr_total_apl_weight: @$_POST['pr_total_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_total_avl_than" id="avl_than" value="<?php echo ( @$pr_total_avl_than) ? $pr_total_avl_than: @$_POST['pr_total_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_total_avl_weight" id="avl_weight" value="<?php echo ( @$pr_total_avl_weight) ? $pr_total_avl_weight: @$_POST['pr_total_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>ટેબલ ધુટવાનાં:</td>
			<td>
				<input type="text" size="1" name="pr_tbld_apl_than" value="<?php echo ( @$pr_tbld_apl_than) ? $pr_tbld_apl_than: @$_POST['pr_tbld_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbld_apl_weight" value="<?php echo ( @$pr_tbld_apl_weight) ? $pr_tbld_apl_weight: @$_POST['pr_tbld_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbld_avl_than" value="<?php echo ( @$pr_tbld_avl_than) ? $pr_tbld_avl_than: @$_POST['pr_tbld_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_tbld_avl_weight" value="<?php echo ( @$pr_tbld_avl_weight) ? $pr_tbld_avl_weight: @$_POST['pr_tbld_avl_weight'];?>">
			</td>
		</tr>
		<tr>
			<td>દોરી મારવાના:</td>
			<td>
				<input type="text" size="1" name="pr_dori_apl_than" value="<?php echo ( @$pr_dori_apl_than) ? $pr_dori_apl_than: @$_POST['pr_dori_apl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dori_apl_weight" value="<?php echo ( @$pr_dori_apl_weight) ? $pr_dori_apl_weight: @$_POST['pr_dori_apl_weight'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dori_avl_than" value="<?php echo ( @$pr_dori_avl_than) ? $pr_dori_avl_than: @$_POST['pr_dori_avl_than'];?>">
			</td>
			<td>
				<input type="text" size="1" name="pr_dori_avl_weight" value="<?php echo ( @$pr_dori_avl_weight) ? $pr_dori_avl_weight: @$_POST['pr_dori_avl_weight'];?>">
			</td>
		</tr>
	</tbody>
</table>