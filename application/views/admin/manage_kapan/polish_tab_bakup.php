<style>
	.borderRight { border-right: 2px solid #131212 !important; }
	.borderBottom { border-bottom: 2px solid #131212 !important; }
</style>
<table class="form list" style="border: 2px solid #131212 !important;" id="polish_tbl">
	<thead>
		<tr id="heading_tr" >
		<!-- Main -->
			<td class="center borderRight" rowspan="2" colspan="6">
				<img alt="" src="<?php echo asset_url('images/admin/om_jewels_1.png');?>" style="width: 200px;">
			</td>
		<!-- 4P -->
			<td class="center borderRight" colspan="6">
				<input type="text" size="25" name="pc_4p_name" id="pc_4p_name" value="<?php echo (@$pc_4p_name)?$pc_4p_name:@$_POST['pc_4p_name'];?>" placeholder="મે. નામ">
				<input type="text" size="2" name="pc_4p_takavari" id="pc_4p_takavari" value="<?php echo (@$pc_4p_takavari)?$pc_4p_takavari:@$_POST['pc_4p_takavari'];?>" placeholder="ટકાવારી">
			</td>
		<!-- Table -->
			<td class="center borderRight" colspan="4">
				<input type="text" size="18" name="pc_tbl_name" id="pc_tbl_name" value="<?php echo (@$pc_tbl_name)?$pc_tbl_name:@$_POST['pc_tbl_name'];?>" placeholder="મે. નામ">
				<input type="text" size="2" name="pc_tbl_takavari" id="pc_tbl_takavari" value="<?php echo (@$pc_tbl_takavari)?$pc_tbl_takavari:@$_POST['pc_tbl_takavari'];?>" placeholder="ટકાવારી">
			</td>
		<!-- Taliya --> 
			<td class="center borderRight" colspan="6">
				<input type="text" size="18" name="pc_tly_name" id="pc_tly_name" value="<?php echo (@$pc_tly_name)?$pc_tly_name:@$_POST['pc_tly_name'];?>" placeholder="મે. નામ">
				<input type="text" size="2" name="pc_tly_takavari" id="pc_tly_takavari" value="<?php echo (@$pc_tly_takavari)?$pc_tly_takavari:@$_POST['pc_tly_takavari'];?>" placeholder="ટકાવારી">
			</td>
		<!-- Mathala -->
			<td class="center borderRight" colspan="4">
				<input type="text" size="18" name="pc_m_name" id="pc_m_name" value="<?php echo (@$pc_m_name)?$pc_m_name:@$_POST['pc_m_name'];?>" placeholder="મે. નામ">
				<input type="text" size="2" name="pc_m_takavari" id="pc_m_takavari" value="<?php echo (@$pc_m_takavari)?$pc_m_takavari:@$_POST['pc_m_takavari'];?>" placeholder="ટકાવારી">
			</td>
		<!-- Tarkhuniya -->
			<td class="center borderRight" colspan="4">
				<input type="text" size="18" name="pc_tk_name" id="pc_tk_name" value="<?php echo (@$pc_tk_name)?$pc_tk_name:@$_POST['pc_tk_name'];?>" placeholder="મે. નામ">
				<input type="text" size="2" name="pc_tk_takavari" id="pc_tk_takavari" value="<?php echo (@$pc_tk_takavari)?$pc_tk_takavari:@$_POST['pc_tk_takavari'];?>" placeholder="ટકાવારી">
			</td>
		<!-- Extra -->
			<td class="center borderRight" rowspan="2" colspan="2"></td>
		</tr>
		<tr id="heading_tr">
		<!-- Main -->
			<!-- Row span -->
		<!-- 4P -->
			<td class="center borderRight" colspan="6">:: 4P ::</td>
		<!-- Table -->
			<td class="center borderRight" colspan="4">:: ટેબલ / રશિયન::</td>
		<!-- Taliya --> 
			<td class="center borderRight" colspan="6">:: તળિયા ::</td>
		<!-- Mathala -->
			<td class="center borderRight" colspan="4">:: માથળા ૮-પેલ ::</td>
		<!-- Tarkhuniya -->
			<td class="center borderRight" colspan="4">:: તરખુનિયા ::</td>
		<!-- Extra -->
			<!-- Row span -->
		</tr>
		<tr id="heading_tr">
			<!-- Main -->
			<td class="center borderBottom">ક્રમ</td> <td class="center borderBottom">લોટ ચઢાવ્યા તારીખ</td> <td class="center borderBottom">નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">પ્યોરીટી</td><td class="center borderBottom borderRight">ચારણી</td>
			<!-- 4P -->
			<td class="center borderBottom">ઘાટ નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">ટુ/ખો</td> <td class="center borderBottom">મંગાવેલ ટકાવારી</td> <td class="center borderBottom">આપેલ ટકાવારી</td> <td class="center borderRight borderBottom">ઘાટ વેરિએશન</td>
			<!-- Table -->
			<td class="center borderBottom">ટેબલ નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">ટુ/ખો</td> <td class="center borderRight borderBottom">ટકાવારી</td>
			<!-- Taliya --> 
			<td class="center borderBottom">તળિયા નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">ટુ/ખો</td> <td class="center borderBottom">ટકાવારી</td><td colspan="2" class="center borderRight borderBottom">ચારણી</td>
			<!-- Mathala -->
			<td class="center borderBottom">માથળા નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">ટુ/ખો</td> <td class="center borderRight borderBottom">ટકાવારી</td>
			<!-- Tarkhuniya -->
			<td class="center borderBottom">તારખુનિયા નંગ</td> <td class="center borderBottom">વજન</td> <td class="center borderBottom">ટુ/ખો</td> <td class="center borderRight borderBottom">ટકાવારી</td>
			<!-- Extra -->
			<td class="center borderBottom">લોટ ઉતરિયા તારીખ</td> <td class="center borderBottom borderRight">વેરિએશન</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$polishCount = 30;
		$polishArrCount = count( $polishArr );
		if( $polishArrCount >= $polishCount)
		{
			$polishCount= $polishArrCount;
		}
		
		$main = 0;
		$p = 10000;
		$tbl = 20000;
		$tlia = 30000;
		$mthl = 40000;
		$tk = 50000;
		$ext = 60000;
		
		for( $row=0;$row<$polishCount;$row++ )
		{
			?>
			<tr class="row-table table row-new polish_tr">
			<!-- Main -->
				<td class="center"><?php echo $row+1;?></td>
				<td class="center"> 
					<input type="text" size="6" class="datepicker k_date" name="p_lot_start_date[]" value="<?php echo ( @$polishArr[$row]['p_lot_start_date'] ) ? $polishArr[$row]['p_lot_start_date'] : @$_POST['p_lot_start_date['.$row.']'];?>" tabindex="<?php echo $main += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_nung[]" class="p_nung" value="<?php echo ( @$polishArr[$row]['p_nung'] ) ? $polishArr[$row]['p_nung'] : @$_POST['p_nung['.$row.']'];?>" tabindex="<?php echo $main += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_weight[]" class="p_weight" value="<?php echo ( @$polishArr[$row]['p_weight'] ) ? $polishArr[$row]['p_weight'] : @$_POST['p_weight['.$row.']'];?>" tabindex="<?php echo $main += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_pyority[]" value="<?php echo ( @$polishArr[$row]['p_pyority'] ) ? $polishArr[$row]['p_pyority'] : @$_POST['p_pyority['.$row.']'];?>" tabindex="<?php echo $main += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_charni[]" value="<?php echo ( @$polishArr[$row]['p_charni'] ) ? $polishArr[$row]['p_charni'] : @$_POST['p_charni['.$row.']'];?>" tabindex="<?php echo $main += 1;?>">
				</td>
			<!-- 4P -->
				<td class="center"> 
					<input type="text" size="1" name="p_4p_ghat_nung[]" class="p_4p_ghat_nung" value="<?php echo ( @$polishArr[$row]['p_4p_ghat_nung'] ) ? $polishArr[$row]['p_4p_ghat_nung'] : @$_POST['p_4p_ghat_nung['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_4p_weight[]" class="p_4p_weight" value="<?php echo ( @$polishArr[$row]['p_4p_weight'] ) ? $polishArr[$row]['p_4p_weight'] : @$_POST['p_4p_weight['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_4p_t_k[]" class="p_4p_t_k" value="<?php echo ( @$polishArr[$row]['p_4p_t_k'] ) ? $polishArr[$row]['p_4p_t_k'] : @$_POST['p_4p_t_k['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_4p_takavari[]" value="<?php echo ( @$polishArr[$row]['p_4p_takavari'] ) ? $polishArr[$row]['p_4p_takavari'] : @$_POST['p_4p_takavari['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_4p_avl_takavari[]" value="<?php echo ( @$polishArr[$row]['p_4p_avl_takavari'] ) ? $polishArr[$row]['p_4p_avl_takavari'] : @$_POST['p_4p_avl_takavari['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_4p_ghat_variation[]" class="p_4p_ghat_variation" value="<?php echo ( @$polishArr[$row]['p_4p_ghat_variation'] ) ? $polishArr[$row]['p_4p_ghat_variation'] : @$_POST['p_4p_ghat_variation['.$row.']'];?>" tabindex="<?php echo $p += 1;?>">
				</td>
			<!-- Table -->
				<td class="center"> 
					<input type="text" size="1" name="p_tbl_nung[]" class="p_tbl_nung" value="<?php echo ( @$polishArr[$row]['p_tbl_nung'] ) ? $polishArr[$row]['p_tbl_nung'] : @$_POST['p_tbl_nung['.$row.']'];?>" tabindex="<?php echo $tbl += 1;?>">
				</td><td class="center"> 
					<input type="text" size="1" name="p_tbl_weight[]" class="p_tbl_weight" value="<?php echo ( @$polishArr[$row]['p_tbl_weight'] ) ? $polishArr[$row]['p_tbl_weight'] : @$_POST['p_tbl_weight['.$row.']'];?>" tabindex="<?php echo $tbl += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tbl_t_k[]" value="<?php echo ( @$polishArr[$row]['p_tbl_t_k'] ) ? $polishArr[$row]['p_tbl_t_k'] : @$_POST['p_tbl_t_k['.$row.']'];?>" tabindex="<?php echo $tbl += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_tbl_takavri[]" value="<?php echo ( @$polishArr[$row]['p_tbl_takavri'] ) ? $polishArr[$row]['p_tbl_takavri'] : @$_POST['p_tbl_takavri['.$row.']'];?>" tabindex="<?php echo $tbl += 1;?>">
				</td>
			<!-- Taliya --> 
				<td class="center"> 
					<input type="text" size="1" name="p_tly_nung[]" class="p_tly_nung" value="<?php echo ( @$polishArr[$row]['p_tly_nung'] ) ? $polishArr[$row]['p_tly_nung'] : @$_POST['p_tly_nung['.$row.']'];?>" tabindex="<?php echo $tlia += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tly_weight[]" class="p_tly_weight" value="<?php echo ( @$polishArr[$row]['p_tly_weight'] ) ? $polishArr[$row]['p_tly_weight'] : @$_POST['p_tly_weight['.$row.']'];?>" tabindex="<?php echo $tlia += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tly_t_k[]" value="<?php echo ( @$polishArr[$row]['p_tly_t_k'] ) ? $polishArr[$row]['p_tly_t_k'] : @$_POST['p_tly_t_k['.$row.']'];?>" tabindex="<?php echo $tlia += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tly_takavari[]" value="<?php echo ( @$polishArr[$row]['p_tly_takavari'] ) ? $polishArr[$row]['p_tly_takavari'] : @$_POST['p_tly_takavari['.$row.']'];?>" tabindex="<?php echo $tlia += 1;?>">
				</td>
				<td></td>
				<td class="borderRight"></td>
			<!-- Mathala -->
				<td class="center"> 
					<input type="text" size="1" name="p_m_nung[]" class="p_m_nung" value="<?php echo ( @$polishArr[$row]['p_m_nung'] ) ? $polishArr[$row]['p_m_nung'] : @$_POST['p_m_nung['.$row.']'];?>" tabindex="<?php echo $mthl += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_m_weight[]" class="p_m_weight" value="<?php echo ( @$polishArr[$row]['p_m_weight'] ) ? $polishArr[$row]['p_m_weight'] : @$_POST['p_m_weight['.$row.']'];?>" tabindex="<?php echo $mthl += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_m_t_k[]" value="<?php echo ( @$polishArr[$row]['p_m_t_k'] ) ? $polishArr[$row]['p_m_t_k'] : @$_POST['p_m_t_k['.$row.']'];?>" tabindex="<?php echo $mthl += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_m_takavari[]" value="<?php echo ( @$polishArr[$row]['p_m_takavari'] ) ? $polishArr[$row]['p_m_takavari'] : @$_POST['p_m_takavari['.$row.']'];?>" tabindex="<?php echo $mthl += 1;?>">
				</td>
			<!-- Tarkhuniya -->
				<td class="center"> 
					<input type="text" size="1" name="p_tk_nung[]" class="p_tk_nung" value="<?php echo ( @$polishArr[$row]['p_tk_nung'] ) ? $polishArr[$row]['p_tk_nung'] : @$_POST['p_tk_nung['.$row.']'];?>" tabindex="<?php echo $tk += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tk_weight[]" class="p_tk_weight" value="<?php echo ( @$polishArr[$row]['p_tk_weight'] ) ? $polishArr[$row]['p_tk_weight'] : @$_POST['p_tk_weight['.$row.']'];?>" tabindex="<?php echo $tk += 1;?>">
				</td>
				<td class="center"> 
					<input type="text" size="1" name="p_tk_t_k[]" value="<?php echo ( @$polishArr[$row]['p_tk_t_k'] ) ? $polishArr[$row]['p_tk_t_k'] : @$_POST['p_tk_t_k['.$row.']'];?>" tabindex="<?php echo $tk += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_tk_takavari[]" value="<?php echo ( @$polishArr[$row]['p_tk_takavari'] ) ? $polishArr[$row]['p_tk_takavari'] : @$_POST['p_tk_takavari['.$row.']'];?>" tabindex="<?php echo $tk += 1;?>">
				</td>
			<!-- Extra -->
				<td class="center"> 
					<input type="text" size="6" class="datepicker k_date" name="p_lot_last_date[]" value="<?php echo ( @$polishArr[$row]['p_lot_last_date'] ) ? $polishArr[$row]['p_lot_last_date'] : @$_POST['p_lot_last_date['.$row.']'];?>" tabindex="<?php echo $ext += 1;?>">
				</td>
				<td class="center borderRight"> 
					<input type="text" size="1" name="p_variation[]" value="<?php echo ( @$polishArr[$row]['p_variation'] ) ? $polishArr[$row]['p_variation'] : @$_POST['p_variation['.$row.']'];?>" tabindex="<?php echo $ext += 1;?>">
				</td>
			</tr>
			<?php 
		}
		?>
		
		<tr>
			<td colspan="34" class="end">
				<div class="buttons">
					<a class="button" onclick="addMorePolishRow();">+ ADD</a>
				</div>
			</td>
		</tr>
		
		<tr>
		<!-- Main -->
			<td class="center"> સરવાળો: </td>
			<td class="center"> </td>
			<td class="center"> 
				<input type="text" size="1" name="pc_nung" id="pc_nung" value="<?php echo (@$pc_nung)?$pc_nung:@$_POST['pc_nung'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_weight" id="pc_weight" value="<?php echo (@$pc_weight)?$pc_weight:@$_POST['pc_weight'];?>">
			</td>
			<td colspan="2" class="center borderRight"> </td>
		<!-- 4P -->
			<td class="center"> 
				<input type="text" size="1" name="pc_4p_g_nung" id="pc_4p_g_nung" value="<?php echo (@$pc_4p_g_nung)?$pc_4p_g_nung:@$_POST['pc_4p_g_nung'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_4p_weight" id="pc_4p_weight" value="<?php echo (@$pc_4p_weight)?$pc_4p_weight:@$_POST['pc_4p_weight'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_4p_tk" id="pc_4p_tk" value="<?php echo (@$pc_4p_tk)?$pc_4p_tk:@$_POST['pc_4p_tk'];?>">
			</td>
			<td colspan="2" class="center"> </td>
			<td class="center borderRight">
				<input type="text" size="1" name="pc_4p_variation" id="pc_4p_variation" value="<?php echo (@$pc_4p_variation)?$pc_4p_variation:@$_POST['pc_4p_variation'];?>">
			</td>
			<!-- Table -->
			<td class="center"> 
				<input type="text" size="1" name="pc_tbl_nung" id="pc_tbl_nung" value="<?php echo (@$pc_tbl_nung)?$pc_tbl_nung:@$_POST['pc_tbl_nung'];?>">
			</td><td class="center"> 
				<input type="text" size="1" name="pc_tbl_weight" id="pc_tbl_weight" value="<?php echo (@$pc_tbl_weight)?$pc_tbl_weight:@$_POST['pc_tbl_weight'];?>">
			</td>
			<td class="center borderRight" colspan="2"> </td>
			<!-- Taliya --> 
			<td class="center"> 
				<input type="text" size="1" name="pc_tly_nung" id="pc_tly_nung" value="<?php echo (@$pc_tly_nung)?$pc_tly_nung:@$_POST['pc_tly_nung'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_tly_weight" id="pc_tly_weight" value="<?php echo (@$pc_tly_weight)?$pc_tly_weight:@$_POST['pc_tly_weight'];?>">
			</td>
			<td class="center borderRight" colspan="4"> </td>
			<!-- Mathala -->
			<td class="center"> 
				<input type="text" size="1" name="pc_m_nung" id="pc_m_nung" value="<?php echo (@$pc_m_nung)?$pc_m_nung:@$_POST['pc_m_nung'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_m_weight" id="pc_m_weight" value="<?php echo (@$pc_m_weight)?$pc_m_weight:@$_POST['pc_m_weight'];?>">
			</td>
			<td class="center borderRight" colspan="2"> </td>
			<!-- Tarkhuniya -->
			<td class="center"> 
				<input type="text" size="1" name="pc_tk_nung" id="pc_tk_nung" value="<?php echo (@$pc_tk_nung)?$pc_tk_nung:@$_POST['pc_tk_nung'];?>">
			</td>
			<td class="center"> 
				<input type="text" size="1" name="pc_tk_weight" id="pc_tk_weight" value="<?php echo (@$pc_tk_weight)?$pc_tk_weight:@$_POST['pc_tk_weight'];?>">
			</td>
			<td class="center borderRight" colspan="2"> </td>
			<!-- Extra -->
			<td class="center borderRight" colspan="2"> </td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	function addMorePolishRow()
	{
		var rows = 1;
		$('#polish_tbl tr').each(function()
		{
			if ($(this).hasClass('polish_tr')) 
			{
				rows++;
			}
		});
		
		var html = '';	
		for( var loop=rows; loop<(rows+5);loop++ )
		{
			html += '<tr class="row-table table row-new polish_tr"><td class="center">'+loop+'</td>'; 
			html += '<td class="center"><input type="text" size="6" class="datepicker k_date" name="p_lot_start_date[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_nung[]" class="p_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_weight[]" class="p_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_pyority[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_charni[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_4p_ghat_nung[]" class="p_4p_ghat_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_4p_weight[]" class="p_4p_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_4p_t_k[]" class="p_4p_t_k" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_4p_takavari[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_4p_avl_takavari[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_4p_ghat_variation[]" class="p_4p_ghat_variation" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tbl_nung[]" class="p_tbl_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tbl_weight[]" class="p_tbl_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tbl_t_k[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_tbl_takavri[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tly_nung[]" class="p_tly_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tly_weight[]" class="p_tly_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tly_t_k[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tly_takavari[]" value="" tabindex="1"></td>';
			html += '<td></td><td class="borderRight"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_m_nung[]" class="p_m_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_m_weight[]" class="p_m_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_m_t_k[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_m_takavari[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tk_nung[]" class="p_tk_nung" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tk_weight[]" class="p_tk_weight" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="1" name="p_tk_t_k[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_tk_takavari[]" value="" tabindex="1"></td>';
			html += '<td class="center"><input type="text" size="6" class="datepicker k_date" name="p_lot_last_date[]" value="" tabindex="1"></td>';
			html += '<td class="center borderRight"><input type="text" size="1" name="p_variation[]" value=""v></td>';
		}
		
		jQuery( html ).insertAfter( jQuery(".row-table.table.row-new.polish_tr").last());
	}
</script>