<input type="hidden" value="10" name="sabkaCount" id="sabkaCount">
<?php 
$date = date('m/d/Y h:i:s a', time());
?>
<table class="form list" id="sabka_tbl">
	<thead>
		<tr id="heading_tr">
			<td class="center">ક્રમ</td>
			<td class="center">તારીખ</td>
			<td class="center">નંગ</td>
			<td class="center">વજન</td>
			<td class="center">આવેલ વજન</td>
			<td class="center">ઘટ</td>
			<td class="center">ટકાવારી</td>
			<td class="center">નામ</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$soingCount = 10;
		$soingArrCount = count( $soingArr );
		
		if( $soingArrCount >= $soingCount)
		{
			$soingCount= $soingArrCount;
		}
		
		for( $row=0;$row<$soingCount;$row++ )
		{
			?>
			<tr class="row-table table row-new sabka_tr">
				<td class="center wd20"><?php echo $row+1;?></td>
				<td class="center">
					<input type="text" size="6" class="datepicker k_date" name="sabka_date" value="<?php echo ( @$sabkaArr[$row]['sabka_date'] == "0000-00-00 00:00:00" || @$sabkaArr[$row]['sabka_date'] == "2000-01-01 00:00:00" || @$sabkaArr[$row]['sabka_date'] == "1970-01-01 00:00:00" || !isset( $sabkaArr[$row]['sabka_date'] ) ) ? formatDate( 'd-m-Y', $date ) : formatDate( 'd-m-Y', @$sabkaArr[$row]['sabka_date'] );?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_nung[]" class="sabka_nung" value="<?php echo ( @$sabkaArr[$row]['sabka_nung'] ) ? $sabkaArr[$row]['sabka_nung'] : @$_POST['sabka_nung['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_weight[]" class="sabka_weight" value="<?php echo ( @$sabkaArr[$row]['sabka_weight'] ) ? $sabkaArr[$row]['sabka_weight'] : @$_POST['sabka_weight['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_avl_weight[]" class="sabka_avl_weight" value="<?php echo ( @$sabkaArr[$row]['sabka_avl_weight'] ) ? $sabkaArr[$row]['sabka_avl_weight'] : @$_POST['sabka_avl_weight['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_ghat[]" class="sabka_ghat" value="<?php echo ( @$sabkaArr[$row]['sabka_ghat'] ) ? $sabkaArr[$row]['sabka_ghat'] : @$_POST['sabka_ghat['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_takavari[]" value="<?php echo ( @$sabkaArr[$row]['sabka_takavari'] ) ? $sabkaArr[$row]['sabka_takavari'] : @$_POST['sabka_takavari['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="sabka_name[]" value="<?php echo ( @$sabkaArr[$row]['sabka_name'] ) ? $sabkaArr[$row]['sabka_name'] : @$_POST['sabka_name['.$row.']'];?>">
				</td>
			</tr>
			<?php
		}
		?>
		
		<tr>
			<td colspan="8" class="end">
				<div class="buttons">
					<a class="button" onclick="addMoreSabkaRow();">+ ADD</a>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="center wd20">સરવાળો: </td>
			<td class="center"> </td>
			<td class="center">
				<input type="text" size="1" name="sabka_nung_total" id="sabka_nung" value="<?php echo ( @$sabka_nung_total) ? $sabka_nung_total: @$_POST['sabka_nung_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="sabka_weight_total" id="sabka_weight" value="<?php echo ( @$sabka_weight_total) ? $sabka_weight_total: @$_POST['sabka_weight_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="sabka_avl_weight_total" id="sabka_avl_weight" value="<?php echo ( @$sabka_avl_weight_total) ? $sabka_avl_weight_total: @$_POST['sabka_avl_weight_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="sabka _ghat_total" id="sabka_ghat" value="<?php echo ( @$sabka_ghat_total) ? $sabka_ghat_total: @$_POST['sabka_ghat_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="sabka_takavari" id="sabka_takavari" value="<?php echo ( @$sabka_takavari) ? $sabka_takavari: @$_POST['sabka_takavari'];?>">
			</td>
			<td class="center"> </td>
			<td class="center"> </td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">

	var countSabka = <?php echo (int)$soingCount;?>;
	$("#sabkaCount").val( countSabka );

	function addMoreSabkaRow()
	{
		var rows = 1;
		$('#sabka_tbl tr').each(function()
		{
			if ($(this).hasClass('sabka_tr')) 
			{
				rows++;
			}
		});

		var html = '';
		for( var loop=rows; loop<(rows+5);loop++ )
		{
			
			html += '<tr class="row-table table row-new sabka_tr"><td class="center wd20">'+loop+'</td>'; 
			html += '<td class="center"><input type="text" size="6" class="datepicker k_date" name="sabka_date[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_nung[]" class="sabka_nung" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_weight[]" class="sabka_weight" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_avl_weight[]" class="sabka_avl_weight" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_ghat[]" class="sabka_ghat" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_takavari[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="sabka_name[]" value=""></td>';
		}

		jQuery( html ).insertAfter( jQuery(".row-table.table.row-new.sabka_tr").last());
		$("#sabkaCount").val( (rows + countSabka ) - 6 );
	}

</script>