<script type="text/javascript" src="<?php echo asset_url('js/admin/diamond.js?v=0.8');?>"></script>

<!-- jQuery 2.2.3 -->
<script type="text/javascript" src="<?php //echo asset_url('js/admin/jquery-2.2.3.min.js');?>"></script>

<style>
	.wd20 { width: 20px !important; }
	.burlywood { background-color: burlywood; }
	.lightblue { background-color: lightblue; }
	.end { text-align: end; }
	input { text-align: center; }
</style>

<script language="javascript">
// $(function() {
// 	if($( ".k_date" ).size()){
// 		// DATEPICKER FOR FILTER
// 		$( ".k_date" ).datepicker({
// 		  changeMonth: true,
//    	  	  dateFormat : 'dd-mm-yy',
// 		  maxDate: "d",
// 		  numberOfMonths: 2,
// // 		  onClose: function( selectedDate ) {
// // 			  if(selectedDate != '')
// // 				$( "#to" ).datepicker( "option", "minDate", selectedDate );
// // 		  }
// 		});
// 	}
// });

$('body').on('focus',".k_date", function(){
    $(this).datepicker({
		  changeMonth: true,
   	  	  dateFormat : 'dd-mm-yy',
		  maxDate: "d",
		  numberOfMonths: 2,
// 		  onClose: function( selectedDate ) {
// 			  if(selectedDate != '')
// 				$( "#to" ).datepicker( "option", "minDate", selectedDate );
// 		  }
		});
});
</script>
<div id="content">
	<?php 
		$this->load->view('admin/elements/breadcrumb');
		$date = date('m/d/Y h:i:s a', time());
	?>
	<div class="box">
		<div class="heading">
			<h1>
				<img alt="<?php echo $this->controller ?>" src="<?php echo getMenuIcon($this->controller)?>" height="22"> <?php echo pgTitle($this->controller);?>
			</h1>
			<div class="buttons">
				<a class="button"  onclick="$('#form').submit();">સાચવો</a>
				<a class="button"  href="<?php echo site_url('admin/'.$this->controller);?>">રદ કરો</a>
			</div>
		</div>
  
		<div class="content">
		
			<table class="form">
				<tbody>
					<tr>
						<td class="lightblue" style="width: 60px;">કાપણ નામ:<?php echo $k_name;?></td>
						<td class="burlywood" style="width: 90px;">ચડાવનારનું નામ: <?php echo $k_add_name;?></td>
						<td class="lightblue" style="width: 65px;">કાપણ આખી: <?php echo $k_total;?></td>
						<td class="burlywood" style="width: 65px;">કાપણ વજન: <span id="kWeight"><?php echo $k_weight;?></span></td>
						<td class="lightblue" style="width: 60px;">તારીખ: <?php echo formatDate( 'd-m-Y', $k_date);?></td>
						<td class="burlywood" style="width: 60px;">દિવસ: 
							<?php 
							if( $k_day == 1 )
								echo 'સોમવાર';
							else if( $k_day== 2 )
								echo 'મંગળવાર';
							else if( $k_day== 3 )
								echo 'બુધવાર';
							else if( $k_day== 4 )
								echo 'ગુરુવાર';
							else if( $k_day == 5 )
								echo 'શુક્રવાર';
							else if( $k_day == 6 )
								echo 'શનિવાર';
							else 
								echo 'રવિવાર';
							?>
						</td>
					</tr>
				</tbody>
			</table>
			
			<div class="htabs" id="tabs">
				<a href="#tab-kacho-number" style="display: inline;" class="selected">કાચો નંબર</a>
				<a href="#tab-data-sarin-number" style="display: inline;" id="tabsarinno">સરીન</a>
				<a href="#tab-data-polish" style="display: inline;" id="tabpolish">પોલિશ</a>
				<a href="#tab-data-polish-repair" style="display: inline;" id="tabpolishrepair">પોલિશ રીપેરિંગ</a>
				<a href="#tab-data-takavari" style="display: inline;" id="takavari" onclick="return countTakavari();" >ટકાવારી</a>
				<a href="#tab-data-sabmit-kapan" style="display: inline;" id="tabsubmitkapan">કાપણ જમા</a>
				<a href="#tab-data-soing" style="display: inline;" id="tabsubmitsoing">સોઈંગ</a>
				<a href="#tab-data-sabka" style="display: inline;" id="tabsubmitsabka">સબકા</a>
			</div>
      
			<form id="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/'.$this->controller.'/'.$this->controller.'Form')?>">
				<input type="hidden" name="item_id" value="<?php echo (@$this->cPrimaryId != '') ? _en(@$this->cPrimaryId) : ''; ?>" />
				
				<style>
					.decoration { text-decoration: none; }
				</style>
			<!-- Kacho Number -->
				<div id="tab-kacho-number" style="display: block;">
					<fieldset>
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/kachoNumberInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">કાચો નંબર</a></legend>
						<?php $this->load->view('admin/manage_kapan/kacho_number');?>
					</fieldset>
				</div>
			<!-- // Kacho Number -->
			
			<!-- Sarin Number -->
				<div id="tab-data-sarin-number" style="display: none;">
					<fieldset>
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/sarinInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">સરીન</a></legend>
						<?php $this->load->view('admin/manage_kapan/sarin');?>
					</fieldset>
				</div>
			<!-- // Sarin Number -->
			
			<!-- Polish -->
				<div id="tab-data-polish" style="display: none;">
					<fieldset>
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/polishInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">પોલિશ</a></legend>
						<?php $this->load->view('admin/manage_kapan/polish');?>
					</fieldset>
				</div>
			<!-- // Polish -->
			
			<!-- Polish Repairing -->
				<div id="tab-data-polish-repair" style="display: none;">
					<fieldset>
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/polishRepairingInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">પોલિશ રીપેરિંગ</a></legend>
						<?php $this->load->view('admin/manage_kapan/polish_repairing');?>
					</fieldset>
				</div>
			<!-- // Polish Repairing -->
			
			<!-- Takavari -->
				<div id="tab-data-takavari" style="display: none;">
					<fieldset>
						<legend>ટકાવારી</legend>
						<?php $this->load->view('admin/manage_kapan/takavari');?>
					</fieldset>
				</div>
			<!-- // Takavari -->
			
			<!-- Kapan Jama -->
				<div id="tab-data-sabmit-kapan" style="display: none;">
					<fieldset style="border-radius: 6px 6px 6px 6px; padding: 0px 15px; margin-left: -5px;">
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/kapanJamaInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">કાપણ જમા</a></legend>
						<?php $this->load->view('admin/manage_kapan/kapan_jama');?>
					</fieldset>
				</div>
			<!-- // Kapan Jama -->
			
			<!-- Soing -->
				<div id="tab-data-soing" style="display: none;">
					<fieldset style="border-radius: 6px 6px 6px 6px; padding: 0px 15px; margin-left: -5px;">
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/soingInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">સોઈંગ</a></legend>
						<?php $this->load->view('admin/manage_kapan/soing');?>
					</fieldset>
				</div>
			<!-- // Soing -->
			
			<!-- Sabka -->
				<div id="tab-data-sabka" style="display: none;">
					<fieldset style="border-radius: 6px 6px 6px 6px; padding: 0px 15px; margin-left: -5px;">
						<legend><a href="<?php echo site_url('admin/'.$this->controller.'/sabkaInvoice?id='._en( $this->cPrimaryId ) )?>" class="decoration" target="_blank">સબકા</a></legend>
						<?php $this->load->view('admin/manage_kapan/sabka');?>
					</fieldset>
				</div>
			<!-- // Soing -->
			
		</form>
	</div>
</div>
<script type="text/javascript">
$('#tabs a').tabs();
$('.htabs a').tabs();
$('.vtabs a').tabs();

function countTakavari() 
{
	//
	$("#t_kachi_takavari_1").val( $('#kWeight').text() );
	$("#t_kachi_takavari_2").val( $('#sc_weight').val() );

	var t_kachi_takavari_3 = ( $('#sc_weight').val() / $('#kWeight').text() ) * 100;
    if( t_kachi_takavari_3 == 'Infinity' ) { $("#t_kachi_takavari_3").val( 0 ); }
    else if( !isNaN( t_kachi_takavari_3 ) ) { $("#t_kachi_takavari_3").val( t_kachi_takavari_3.toFixed(2) ); }
    else { $("#t_kachi_takavari_3").val( 0 ); }
	    
	//
	$("#t_kachi_saij_1").val( $('#sc_nung').val() );
	$("#t_kachi_saij_2").val( $('#sc_weight').val() );

	var t_kachi_saij_3 = ( $('#sc_nung').val() / $('#sc_weight').val() );
    if( t_kachi_saij_3 == 'Infinity' ) { $("#t_kachi_saij_3").val( 0 ); }
    else if( !isNaN( t_kachi_saij_3 ) ) { $("#t_kachi_saij_3").val( t_kachi_saij_3.toFixed(2) ); }
    else { $("#t_kachi_saij_3").val( 0 ); }

	//
	$("#t_taiyar_taiytakavari_1").val( $('#sc_weight').val() );
	$("#t_taiyar_taiytakavari_2").val( $('#pc_tk_weight').val() );

	var t_taiyar_taiytakavari_3 = ( $('#pc_tk_weight').val() / $('#sc_weight').val() ) * 100;
    if( t_taiyar_taiytakavari_3 == 'Infinity' ) { $("#t_taiyar_taiytakavari_3").val( 0 ); }
    else if( !isNaN( t_taiyar_taiytakavari_3 ) ) { $("#t_taiyar_taiytakavari_3").val( t_taiyar_taiytakavari_3.toFixed(2) ); }
    else { $("#t_taiyar_taiytakavari_3").val( 0 ); }

	//
	$("#t_taiyar_saij_1").val( $('#pc_tk_nung').val() );
	$("#t_taiyar_saij_2").val( $('#pc_tk_weight').val() );

	var t_taiyar_saij_3 = ( $('#t_taiyar_saij_1').val() / $('#t_taiyar_saij_2').val() );
	if( t_taiyar_saij_3 == 'Infinity' ) { $("#t_taiyar_saij_3").val( 0 ); }
    else if( !isNaN( t_taiyar_saij_3 ) ) { $("#t_taiyar_saij_3").val( t_taiyar_saij_3.toFixed(2) ); }
    else { $("#t_taiyar_saij_3").val( 0 ); }
    
}
</script>
