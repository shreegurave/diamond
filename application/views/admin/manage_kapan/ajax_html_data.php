<input type="hidden" id="hidden_srt" value="<?php echo $srt; ?>" />
<input type="hidden" id="hidden_field" value="<?php echo $field; ?>" />
<form id="form" enctype="multipart/form-data" method="post" action="">
	<table class="list">
		<thead>
			<tr id="heading_tr" style="cursor:pointer;">
				<?php			    
				$n = get_sort_order($this->input->get('s'),$this->input->get('f'),'kapan_id');
				$a = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_name');
				$b = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_add_name');
				$c = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_total');								
				$d = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_weight');
				$e = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_date');
				$f = get_sort_order($this->input->get('s'),$this->input->get('f'),'k_day');
				?>
				<td width="3%" style="text-align: center;">
					<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);">
				</td>
				<th width="1%" class="center" f="kapan_id" s="<?php echo @$n;?>">ક્રમ</th>
				<th width="20%" class="center" f="k_name" s="<?php echo @$a;?>">કાપણ નામ</th>
				<th width="20%" class="center" f="k_add_name" s="<?php echo @$b;?>">ચડાવનારનું નામ</th>
				<th width="5%" class="center" f="k_total" s="<?php echo @$c;?>">કાપણ આખી</th>
				<th width="5%" class="center" f="k_weight" s="<?php echo @$d;?>">કાપણ વજન</th>
				<th width="10%" class="center" f="k_date" s="<?php echo @$e;?>">તારીખ</th>
				<th width="5%" class="center" f="k_day" s="<?php echo @$f;?>">દિવસ</th>
				<td width="1%" class="center">ક્રિયા</td>
			</tr> 
			<tr class="filter">        
				<td class="right"></td>
				<td class="right"></td>
				<td class="center">
					<input type="text" size="40" name="k_name" value="<?php echo ( @$_GET['k_name'] );?>" id="k_name">
				</td>
				<td class="center">
					<input type="text" size="40" name="k_add_name" value="<?php echo ( @$_GET['k_add_name'] );?>" id="k_add_name">
				</td>
				<td class="center">
					<input type="text" size="5" name="k_total" value="<?php echo ( @$_GET['k_total'] );?>" id="k_total">
				</td>
				<td class="center">
					<input type="text" size="5" name="k_weight" value="<?php echo ( @$_GET['k_weight'] );?>" id="k_weight">
				</td>
				<td class="center">
					<input type="text" class="datepicker" name="k_date" id="from" value="<?php echo ( @$_GET['k_date'] );?>"><br />
				</td>
				<td class="center">
					<select name="k_day" id="status_filter">
						<option value="" selected="selected">પસંદ કરો</option>
						<option value="1" <?php echo ( @$_GET['k_day'] == 1 )?'selected="selected"':'';?>>સોમવાર</option>
						<option value="2" <?php echo ( @$_GET['k_day'] == 2 )?'selected="selected"':'';?>>મંગળવાર</option>
						<option value="3" <?php echo ( @$_GET['k_day'] == 3 )?'selected="selected"':'';?>>બુધવાર</option>
						<option value="4" <?php echo ( @$_GET['k_day'] == 4 )?'selected="selected"':'';?>>ગુરુવાર</option>
						<option value="5" <?php echo ( @$_GET['k_day'] == 5 )?'selected="selected"':'';?>>શુક્રવાર</option>
						<option value="6" <?php echo ( @$_GET['k_day'] == 6 )?'selected="selected"':'';?>>શનિવાર</option>
						<option value="7" <?php echo ( @$_GET['k_day'] == 7 )?'selected="selected"':'';?>>રવિવાર</option>
					</select>
				</td>
				<td align="right"><a class="button"  id="searchFilter">શોધ</a></td>
            </tr>   
               
		</thead>
		<tbody>
			<?php 
			if(count($listArr)):
				foreach($listArr as $k=>$ar):
				?>
					<tr id="<?php echo $ar[$this->cAutoId];?>">
						<td style="text-align: center;">
							<input type="checkbox" value="<?php echo $ar[$this->cAutoId]?>" name="selected[]" class="chkbox"> 
						</td>
						<td class="left"><?php echo $ar['kapan_id'];?></td>
						<td class="left"><?php echo $ar['k_name'];?></td>
						<td class="left"><?php echo $ar['k_add_name'];?></td>
						<td class="left"><?php echo $ar['k_total'];?></td>
						<td class="left"><?php echo $ar['k_weight'];?></td>
						<td class="left"><?php echo formatDate('d m, Y <b>h:i a</b>',$ar['k_date']);?></td>
						<td class="left"><?php echo getDayName( $ar['k_day'] );?>
						</td>
						<td class="center"> 
							<?php if($this->per_edit == 0):?> 
								[ <a href="<?php echo site_url('admin/'.$this->controller.'/'.$this->controller.'Form?edit=true&item_id='._en($ar[$this->cAutoId]))?>" title="Edit">Edit</a> ]
							<?php endif;?> 
						</td>              
					</tr>
				<?php 
				endforeach;
			else:
				echo "<tr><td class='center' colspan='9'>No results!</td></tr>";
			endif; 
			?>
		</tbody>
	</table>
	<div class="pagination">
		<?php $this->load->view('admin/elements/table_footer');?>
	</div>
</form>