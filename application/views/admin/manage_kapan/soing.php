<input type="hidden" value="10" name="soingCount" id="soingCount">
<?php 
$date = date('m/d/Y h:i:s a', time());
?>
<table class="form list" id="soing_tbl">
	<thead>
		<tr id="heading_tr">
			<td class="center">ક્રમ</td>
			<td class="center">તારીખ</td>
			<td class="center">નંગ</td>
			<td class="center">વજન</td>
			<td class="center">આવેલ વજન</td>
			<td class="center">ઘટ</td>
			<td class="center">ટકાવારી</td>
			<td class="center">નામ</td>
		</tr>
	</thead>
	<tbody>
		<?php
		$soingCount = 10;
		$soingArrCount = count( $soingArr );
		if( $soingArrCount >= $soingCount)
		{
			$soingCount= $soingArrCount;
		}
		
		for( $row=0;$row<$soingCount;$row++ )
		{
			?>
			<tr class="row-table table row-new soing_tr">
				<td class="center wd20"><?php echo $row+1;?></td>
				<td class="center">
					<input type="text" size="6" class="datepicker k_date" name="soing_date[]" value="<?php echo ( @$soingArr[$row]['soing_date'] == "0000-00-00 00:00:00" || @$soingArr[$row]['soing_date'] == "2000-01-01 00:00:00" || @$soingArr[$row]['soing_date'] == "1970-01-01 00:00:00" || !isset( $sabkaArr[$row]['soing_date'] ) ) ? formatDate( 'd-m-Y', $date ) : formatDate( 'd-m-Y', $soingArr[$row]['soing_date'] );?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_nung[]" class="soing_nung" value="<?php echo ( @$soingArr[$row]['soing_nung'] ) ? $soingArr[$row]['soing_nung'] : @$_POST['soing_nung['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_weight[]" class="soing_weight" value="<?php echo ( @$soingArr[$row]['soing_weight'] ) ? $soingArr[$row]['soing_weight'] : @$_POST['soing_weight['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_avl_weight[]" class="soing_avl_weight" value="<?php echo ( @$soingArr[$row]['soing_avl_weight'] ) ? $soingArr[$row]['soing_avl_weight'] : @$_POST['soing_avl_weight['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_ghat[]" class="soing_ghat" value="<?php echo ( @$soingArr[$row]['soing_ghat'] ) ? $soingArr[$row]['soing_ghat'] : @$_POST['soing_ghat['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_takavari[]" value="<?php echo ( @$soingArr[$row]['soing_takavari'] ) ? $soingArr[$row]['soing_takavari'] : @$_POST['soing_takavari['.$row.']'];?>">
				</td>
				<td class="center">
					<input type="text" size="1" name="soing_name[]" value="<?php echo ( @$soingArr[$row]['soing_name'] ) ? $soingArr[$row]['soing_name'] : @$_POST['soing_name['.$row.']'];?>">
				</td>
			</tr>
			<?php
		}
		?>
		
		<tr>
			<td colspan="8" class="end">
				<div class="buttons">
					<a class="button" onclick="addMoreSoingRow();">+ ADD</a>
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="center wd20">સરવાળો: </td>
			<td class="center"> </td>
			<td class="center">
				<input type="text" size="1" name="soing_nung_total" id="soing_nung" value="<?php echo ( @$soing_nung_total) ? $soing_nung_total: @$_POST['soing_nung_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="soing_weight_total" id="soing_weight" value="<?php echo ( @$soing_weight_total) ? $soing_weight_total: @$_POST['soing_weight_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="soing_avl_weight_total" id="soing_avl_weight" value="<?php echo ( @$soing_avl_weight_total) ? $soing_avl_weight_total: @$_POST['soing_avl_weight_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="soing_ghat_total" id="soing_ghat" value="<?php echo ( @$soing_ghat_total) ? $soing_ghat_total: @$_POST['soing_ghat_total'];?>">
			</td>
			<td class="center">
				<input type="text" size="1" name="soing_takavari_total" id="soing_takavari" value="<?php echo ( @$soing_takavari) ? $soing_takavari: @$_POST['soing_takavari'];?>">
			</td>
			<td class="center"> </td>
			<td class="center"> </td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">

	var countSoing = <?php echo (int)$soingCount;?>;
	$("#soingCount").val( countSoing );

	function addMoreSoingRow()
	{
		var rows = 1;
		$('#soing_tbl tr').each(function()
		{
			if ($(this).hasClass('soing_tr')) 
			{
				rows++;
			}
		});

		var html = '';
		for( var loop=rows; loop<(rows+5);loop++ )
		{
			
			html += '<tr class="row-table table row-new soing_tr"><td class="center wd20">'+loop+'</td>'; 
			html += '<td class="center"><input type="text" size="6" class="datepicker k_date" name="soing_date[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_nung[]" class="soing_nung" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_weight[]" class="soing_weight" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_avl_weight[]" class="soing_avl_weight" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_ghat[]" class="soing_ghat" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_takavari[]" value=""></td>';
			html += '<td class="center"><input type="text" size="1" name="soing_name[]" value=""></td>';
		}

		jQuery( html ).insertAfter( jQuery(".row-table.table.row-new.soing_tr").last());
		$("#soingCount").val( (rows + countSoing ) - 6 );
	}

</script>