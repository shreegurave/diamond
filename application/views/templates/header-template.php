<table style="border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;">
	<thead>
		<tr>
			<td colspan="2" style="border-right: 1px solid #b9b7b7; text-align: center;">
				<img src="<?php echo asset_url('images/Print_header.png'); ?>" style="width: 249px;">
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">
				<hr style="margin-left: 10px; margin-right: 10px; border-color: red;">
				<span style="color:blue; font-size: 10px;"><?php echo getLangMsg("s/g/i")?></span>
				<hr style="margin-left: 10px; margin-right: 10px; border-color: red;">
			</td>
		</tr>
	</thead>
</table>
<table style="border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;" >
	<thead>
		<tr>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ નામ</b> : <?php echo $k_name?></td>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>ચડાવનારનું નામ</b> : <?php echo $k_add_name?></td>
		</tr>
		<tr>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ આખી</b> : <?php echo $k_total?></td>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>તારીખ</b> : <?php echo formatDate( "d-m-Y", $k_date )?></td>
		</tr>
		<tr>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ વજન</b> : <?php echo $k_weight?></td>
			<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>દિવસ</b> : <?php echo getDayName( $k_day )?></td>
		</tr>
	</thead>
</table>