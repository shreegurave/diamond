<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left; width: 1%;">
		<?php 
		$record = exeQuery( "SELECT k.*, kn.* FROM kapan k 
							LEFT JOIN kacho_number kn ON kn.kapan_id = k.kapan_id
							 WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		?>
		<tr>
			<td colspan="2">
				<?php $this->load->view('templates/header-template', $record );?>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">નામ</td>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">નંગ</td>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ચડાવેલ વજન</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">SD :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_sd_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_sd_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">SD 1 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_sd1_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_sd1_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">G-1 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_g1_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_g1_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">G-2 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_g2_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_g2_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">N-1 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u1_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u1_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">N-2 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u2_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u2_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">N-3 :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u3_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_u3_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">પલચું :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_palchu_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_palchu_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">TA :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_ta_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_ta_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">MIX :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_mix_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_mix_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['kn_extra_1'] ) ? $record['kn_extra_1'] : "Extra-1"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra1_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra1_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['kn_extra_2'] ) ? $record['kn_extra_2'] : "Extra-2"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra2_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra2_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['kn_extra_3'] ) ? $record['kn_extra_3'] : "Extra-3"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra3_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['kn_extra3_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">સરવાળો :</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_total_n']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_total_v']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">સાઈઝ :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_saij']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">ટકાવારી :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['kn_takavari']?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
