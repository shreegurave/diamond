<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left; width: 1%;">
		<?php 
		$record = exeQuery( "SELECT k.*, sk.* FROM kapan k
							LEFT JOIN sarin_count sk ON sk.kapan_id = k.kapan_id
							WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		$resultArr = executeQuery( "SELECT * FROM sarin WHERE kapan_id = ".(int)_de( $_GET['id'] ));
		?>
		<tr>
			<td colspan="2">
				<?php $this->load->view('templates/header-template', $record );?>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ક્રમ</td>
											<td style="width: 140px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">તારીખ</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">નંગ</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">વજન</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">પ્યોરીટી</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ચારણી</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center; display: none;">ટકાવારી</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">સરીન નામ</td>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ( $resultArr as $k=>$result )
										{
											?>
											<tr>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $k+1?></td>
												<td style="width: 150px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo ( $result['s_date'] == "2000-01-01 00:00:00" ) ? "" : formatDate( 'd-m-Y', $result['s_date'] );?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['s_nung'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['s_weight'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['s_pyority'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['s_charni'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center; display: none;"><?php echo $result['s_takavari'];?></td>
												<td style="width: 200px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['s_name'];?></td>
											</tr>
											<?php 
										}
										?>
										<tr>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><b>સરવાળો:</b> </td>
											<td style="width: 150px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sc_nung'];?></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sc_weight'];?></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
										</tr>
										<tr>
											<td colspan="7" style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center; padding: 15px;"></td>
										</tr>
										<tr>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 150px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><b>સાઈઝ:</b> </td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sc_saij'];?></td>
											<td colspan="2" style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><b>ટકાવારી:</b> </td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sc_takavari'];?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
