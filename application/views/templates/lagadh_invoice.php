<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left; width: 1%;">
		<?php 
		$record = exeQuery( "SELECT k.*, l.*, t.* FROM kapan k 
							LEFT JOIN lagadh l ON l.kapan_id = k.kapan_id
							LEFT JOIN takavari t ON t.kapan_id = k.kapan_id
							 WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		?>
		<tr>
			<td colspan="2">
				<?php $this->load->view('templates/header-template', $record );?>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 12px; font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">નામ</td>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">વજન</td>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; font-weight: bold; text-align: left; padding: 3px; text-align: center;"></td>
											<td colspan="2" style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">કાચી ટકાવારી</td>
											<td colspan="2" style="width: 211px; font-size: 14px; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">કાચી સાઈઝ</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">બનાવવાનું:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_bannavvu']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">કાચુ વજન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_takavari_1']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચડાવેલ નંગ :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_saij_1']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">સિંગલ-1:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_singal_1']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચડાવેલ વજન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_takavari_2']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચડાવેલ વજન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_saij_2']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">સિંગલ-2:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_singal_2']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ટકાવારી :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_takavari_3']?></td>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ટકાવારી :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['t_kachi_saij_3']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">બોટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_bot']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="4" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચોકી-1:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_choki_1']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="4" style="width: 211px; font-size: 13px; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ચારણી</td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચોકી-2:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_choki_2']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;"><?php echo ( $record['l_extra_txt_4'] ) ? $record['l_extra_txt_4'] : "Extra-1"?> :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_4']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">પલચું:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_palchu']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;"><?php echo ( $record['l_extra_txt_5'] ) ? $record['l_extra_txt_5'] : "Extra-2"?> :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_5']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">ઉચું ગોળ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['l_unchu_gol']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;"><?php echo ( $record['l_extra_txt_6'] ) ? $record['l_extra_txt_6'] : "Extra-3"?> :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_6']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">કલર આઉટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_color_out']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+11 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_ele']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">આખુ આઉટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_aankhu_out']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+8-11 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_eight']?></td>
										</tr>
										
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચુર:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_chur']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+6.5-8 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_six']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">સોઈંગ ઘટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['l_soing_ghat']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+5-6.5 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_four']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ચપકા ઘટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_chapka_ghat']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+2-5 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_two']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">વધારાની ઘટ:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_vadharani_ghat']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">+000-2 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_plus_ziro']?></td>
										</tr>
										
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['l_extra_txt_1'] ) ? $record['l_extra_txt_1'] : "Extra-1"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_1']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">-000 :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_min_ziro']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['l_extra_txt_2'] ) ? $record['l_extra_txt_2'] : "Extra-2"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_2']?></td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; padding: 3px;"></td>
											<td colspan="2" style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px; text-align: right;">સરવાળો :</td>
											<td colspan="2" style="width: 211px; text-align: center; font-size: 13px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_charni_total']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['l_extra_txt_3'] ) ? $record['l_extra_txt_3'] : "Extra-3"?> :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_extra_val_3']?></td>
											<td colspan="5" rowspan="5" style="width: 211px; font-size: 9px; vertical-align: top; border-bottom: 1px solid #b9b7b7; padding: 10px;"><?php echo $record['l_note']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">સરવાળો:</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['l_total']?></td>
										</tr>
										
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">આપેલ વજન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_aavel_vajan']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">આવેલ વજન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['l_mangel_vajan']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">વેરિએશન :</td>
											<td style="width: 211px; text-align: center; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['l_veriation']?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
