<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left;">
		<?php 
		$record = exeQuery( "SELECT k.*, kjc.* FROM kapan k
							LEFT JOIN kapan_jama_count kjc ON kjc.kapan_id = k.kapan_id
							WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		
		$resultArr = executeQuery( "SELECT * FROM kapan_jama WHERE kapan_id = ".(int)_de( $_GET['id'] ));
		?>
		<tr>
			<td colspan="2">
				<?php //$this->load->view('templates/header-template', $record );?>
				<table style="border-collapse: collapse; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;">
					<thead>
						<tr>
							<td style="width: 150px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ નામ</b> : <?php echo $record['k_name']?></td>
							<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>ચડાવનારનું નામ</b> : <?php echo $record['k_add_name']?></td>
							<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ આખી</b> : <?php echo $record['k_total']?></td>
							<td style="width: 211px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ વજન</b> : <?php echo $record['k_weight']?></td>
							<td style="width: 140px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>તારીખ</b> : <?php echo formatDate( "d-m-Y", $record['k_date'] )?></td>
							<td style="width: 111px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>દિવસ</b> : <?php echo getDayName( $record['k_day'] )?></td>
						</tr>
					</thead>
				</table>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse; ">
									<thead>
										<tr>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;">ક્રમ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આ. તારીખ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">રફ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આ. કાપણ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">કાપણ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">નંગ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ગોળ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સાઈઝ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ટકાવારી</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિંગલ 1</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિંગલ 2</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિલેક્શન</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચોકી 1</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચોકી 2</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">પલ્સુ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ઉચુ ગોળ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">કલર આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આખુ આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચુર</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ઘટ</td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-left: 1px solid #b9b7b7; border-top: 1px solid #000;">જમા તારીખ</td>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ( $resultArr as $k=>$result )
										{
											?>
											<tr>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $k+1?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo formatDate( 'd-m-Y', $result['kj_start_date'] );?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_raph'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_total_kapan'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_kapan'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_gol'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_saij'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_takavari'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_singal_1'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_singal_2'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_selection'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_choki_1'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_choki_2'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_palchu'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_uchu_gol'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_color_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_total_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_chur'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_ghat'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo formatDate( 'd-m-Y', $result['kj_end_date'] );?></td>
											</tr>
											<?php 
										}
										?>
										<tr>
											<td style="font-size: 12px; border-top: 1px solid #000; border-right: 1px solid #b9b7b7; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: left; text-align: center;">Total:</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_kapan'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_gol'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_singal_1'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_singal_2'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_selection'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_choki_1'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_choki_2'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_palchu'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_uchu_gol'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_color_out'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_total_out'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_out'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"><?php echo $record['kjc_chur'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: left; text-align: center;"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
