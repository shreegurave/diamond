<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left;">
		<?php 
		$record = exeQuery( "SELECT k.*, pc.* FROM kapan k
							LEFT JOIN polish_count pc ON pc.kapan_id = k.kapan_id
							WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		$resultArr = executeQuery( "SELECT * FROM polish WHERE kapan_id = ".(int)_de( $_GET['id'] ));
		?>
		<tr>
			<td colspan="2">
				<?php //$this->load->view('templates/header-template', $record );?>
				<table style="border-collapse: collapse; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;">
					<thead>
						<tr>
							<td style="width: 50px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">ID: <?php echo $record['kapan_id']?></td>
							<td style="width: 140px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ નામ</b> : <?php echo $record['k_name']?></td>
							<td style="width: 250px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>ચડાવનારનું નામ</b> : <?php echo $record['k_add_name']?></td>
							<td style="width: 150px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ આખી</b> : <?php echo $record['k_total']?></td>
							<td style="width: 150px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ વજન</b> : <?php echo $record['k_weight']?></td>
							<td style="width: 150px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>તારીખ</b> : <?php echo formatDate( "d-m-Y", $record['k_date'] )?></td>
							<td style="width: 110px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>દિવસ</b> : <?php echo getDayName( $record['k_day'] )?></td>
							<td style="width: 100px; font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">ટકાવારી: <?php echo round( ( $record['pc_tk_weight'] / $record['k_total'] ) * 100, 3 ); ?></td>
						</tr>
					</thead>
				</table>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
										<!-- Main -->
											<td rowspan="2" colspan="6" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												<img alt="" src="<?php echo asset_url('images/admin/om_jewels_1.png');?>" style="width: 200px;">
											</td>
										<!-- 4P -->
											<td colspan="7" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												નામ: <?php echo $record['pc_4p_name'];?> <br>
												ટકાવારી: <?php echo $record['pc_4p_takavari'];?>
											</td>
										<!-- Table -->
											<td colspan="4" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												નામ: <?php echo $record['pc_tbl_name'];?> <br>
												ટકાવારી: <?php echo $record['pc_tbl_takavari'];?>
											</td>
										<!-- Taliya --> 
											<td colspan="6" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												નામ: <?php echo $record['pc_tly_name'];?> <br>
												ટકાવારી: <?php echo $record['pc_tly_takavari'];?>
											</td>
										<!-- Mathala -->
											<td colspan="4" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												નામ: <?php echo $record['pc_m_name'];?> <br>
												ટકાવારી: <?php echo $record['pc_m_takavari'];?>
											</td>
										<!-- Tarkhuniya -->
											<td colspan="4" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;">
												નામ: <?php echo $record['pc_tk_name'];?> <br>
												ટકાવારી: <?php echo $record['pc_tk_takavari'];?>
											</td>
										<!-- Extra -->
											<td rowspan="2" colspan="2" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; text-align: left; padding: 3px; color: #222222; text-align: left; border-left: 1px solid #000; border-top: 1px solid #000;"></td>
										</tr>
										<tr>
										<!-- Main -->
											<!-- Row span -->
										<!-- 4P -->
											<td colspan="7" style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000; ">:: 4P ::</td>
										<!-- Table -->
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;" colspan="4" >:: ટેબલ / રશિયન ::</td>
										<!-- Taliya --> 
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;" colspan="6" >:: તળિયા ::</td>
										<!-- Mathala -->
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;" colspan="4" >:: માથળા ૮-પેલ ::</td>
										<!-- Tarkhuniya -->
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;" colspan="4" >:: તરખુનિયા ::</td>
										<!-- Extra -->
											<!-- Row span -->
										</tr>
										<tr>
											<!-- Main -->
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">ક્રમ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">લોટ ચઢાવ્યા તારીખ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">પ્યોરીટી</td>
											<td style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ચારણી</td>
											<!-- 4P -->
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">ઘાટ નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટુ/ખો</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">મંગાવેલ ટકાવારી</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">આપેલ ટકાવારી</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ઘાટ વેરિએશન </td> 
											<td style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">4P નામ</td>
											<!-- Table -->
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">ટેબલ નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટુ/ખો</td> 
											<td style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટકા વારી</td>
											<!-- Taliya --> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">તળિયા નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટુ/ખો</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટકા વારી</td>
											<td colspan="2" style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ચારણી<br>+5&nbsp;-5</td>
											<!-- Mathala -->
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">માથળા નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટુ/ખો</td> 
											<td style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટકા વારી</td>
											<!-- Tarkhuniya -->
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">તારખુનિયા નંગ</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વજન</td> 
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટુ/ખો</td> 
											<td style="font-size: 13px; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">ટકા વારી</td>
											<!-- Extra -->
											<td style="font-size: 13px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center; border-left: 1px solid #000;">લોટ ઉતરિયા તારીખ</td> 
											<td style="font-size: 13px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; font-weight: bold; color: #222222; text-align: center;">વેરિ એશન</td>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ( $resultArr as $k=>$result )
										{
											?>
											<tr>
												<!-- Main -->
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $k+1?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo ( $result['p_lot_start_date'] == "2000-01-01 00:00:00" ) ? "" : formatDate( 'd-m-Y', $result['p_lot_start_date'] );?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_pyority'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_charni'];?></td>
												<!-- 4P -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $result['p_4p_ghat_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_t_k'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_takavari'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_avl_takavari'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_ghat_variation'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_4p_name'];?></td>
												<!-- Table -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $result['p_tbl_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tbl_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tbl_t_k'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tbl_takavri'];?></td>
												<!-- Taliya -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $result['p_tly_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tly_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tly_t_k'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tly_takavari'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tly_sarni_1'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tly_sarni_2'];?>	</td>
												<!-- Mathala -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $result['p_m_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_m_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_m_t_k'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_m_takavari'];?></td>
												<!-- Tarkhuniya -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $result['p_tk_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tk_weight'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tk_t_k'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_tk_takavari'];?></td>
												<!-- extra -->
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo ( $result['p_lot_last_date'] == "2000-01-01 00:00:00" ) ? "" : formatDate( 'd-m-Y', $result['p_lot_last_date'] );?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; color: #222222; text-align: center;"><?php echo $result['p_variation'];?></td>
											</tr>
											<?php 
										}
										?>
										<tr>
											<td style="font-size: 12px; border-top: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000; text-align: center;">Total:</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_4p_g_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_4p_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_4p_tk'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_4p_variation'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tbl_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tbl_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tly_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tly_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_m_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_m_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tk_nung'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"><?php echo $record['pc_tk_weight'];?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; text-align: center;"></td>
										</tr>
										
										<tr>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
