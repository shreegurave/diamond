<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left; width: 1%;">
		<?php 
		$record = exeQuery( "SELECT k.*, sc.* FROM kapan k
							LEFT JOIN sabka_count sc ON sc.kapan_id = k.kapan_id
							WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		$resultArr = executeQuery( "SELECT * FROM sabka WHERE kapan_id = ".(int)_de( $_GET['id'] ));
		?>
		<tr>
			<td colspan="2">
				<?php //$this->load->view('templates/header-template', $record );?>
				<table style="border-collapse: collapse; width: 450px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;">
					<thead>
						<tr>
							<td colspan="2" style="border-right: 1px solid #b9b7b7; text-align: center;">
								<img src="<?php echo asset_url('images/Print_header.png'); ?>" style="width: 249px;">
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">
								<hr style="margin-left: 10px; margin-right: 10px; border-color: red;">
								<span style="color:blue; font-size: 10px;"><?php echo getLangMsg("s/g/i")?></span>
								<hr style="margin-left: 10px; margin-right: 10px; border-color: red;">
							</td>
						</tr>
					</thead>
				</table>
				<table style="border-collapse: collapse; width: 450px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif;" >
					<thead>
						<tr>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ નામ</b> : <?php echo $record['k_name']?></td>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>ચડાવનારનું નામ</b> : <?php echo $record['k_add_name']?></td>
						</tr>
						<tr>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ આખી</b> : <?php echo $record['k_total']?></td>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>તારીખ</b> : <?php echo formatDate( "d-m-Y", $record['k_date'] )?></td>
						</tr>
						<tr>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>કાપણ વજન</b> : <?php echo $record['k_weight']?></td>
							<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><b>દિવસ</b> : <?php echo getDayName( $record['k_day'] )?></td>
						</tr>
					</thead>
				</table>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; width: 450px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ક્રમ</td>
											<td style="width: 140px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">તારીખ</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">નંગ</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">વજન</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">આવેલ વજન</td>
											<td style="width: 25px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ઘટ</td>
											<td style="width: 10px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">ટકાવારી</td>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ( $resultArr as $k=>$result )
										{
											?>
											<tr>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $k+1?></td>
												<td style="width: 150px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo formatDate( 'd-m-Y', $result['sabka_date'] );?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['sabka_nung'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['sabka_weight'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['sabka_avl_weight'];?></td>
												<td style="width: 25px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['sabka_ghat'];?></td>
												<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; padding: 3px; color: #222222; text-align: center;"><?php echo $result['sabka_takavari'];?></td>
											</tr>
											<?php 
										}
										?>
										<tr>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><b>સરવાળો:</b> </td>
											<td style="width: 150px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sabka_nung_total'];?></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sabka_weight_total'];?></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sabka_avl_weight_total'];?></td>
											<td style="width: 25px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"><?php echo $record['sabka_ghat_total'];?></td>
											<td style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center;"></td>
										</tr>
										<tr>
											<td colspan="7" style="width: 10px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; text-align: center; padding: 15px;"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
