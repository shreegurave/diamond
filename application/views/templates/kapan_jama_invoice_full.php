<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left;">
		<?php 
		
		$date = "";
		
		if( !empty( $_POST['start_date'] ) && !empty( $_POST['end_date'] ) )
		{
			$date = "AND k.k_created_date BETWEEN '".formatDate( 'Y-m-d', $_POST['start_date'] )." 00:00:00.000000' AND '".formatDate( 'Y-m-d', $_POST['end_date'] )." 00:00:00.000000'";
		}
		else if( !empty( $_POST['start_date'] )  && empty( $_POST['end_date'] ) ) 
		{
			$date = "AND k.k_created_date BETWEEN '".formatDate( 'Y-m-d', $_POST['start_date'] )." 00:00:00.000000' AND '".date('Y-m-d')." 00:00:00.000000'";
		}
		else if( empty( $_POST['start_date'] )  && !empty( $_POST['end_date'] ) )
		{
			$date = "AND k.k_created_date BETWEEN '".formatDate( 'Y-m-d', $_POST['start_date'] )." 00:00:00.000000' AND '".formatDate( 'Y-m-d', $_POST['end_date'] )." 00:00:00.000000'";
		}
		
		$resultArr= executeQuery( "SELECT k.*, kj.* FROM kapan_jama kj
									LEFT JOIN kapan k ON k.kapan_id = kj.kapan_id
									WHERE k.k_name LIKE '%".$_POST['k_name']."%'
										AND k.k_add_name LIKE '%".$_POST['k_add_name']."%'
										".$date);
		?>
		<tr>
			<td colspan="2">
				<?php //$this->load->view('templates/header-template', $record );?>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse; ">
									<thead>
										<tr>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-left: 1px solid #000; border-top: 1px solid #000;">ક્રમ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">કાપણ નામ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચડાવનારનું નામ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આ. તારીખ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">રફ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આ. કાપણ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">કાપણ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">નંગ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ગોળ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સાઈઝ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ટકાવારી</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિંગલ 1</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિંગલ 2</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">સિલેક્શન</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચોકી 1</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચોકી 2</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">પલ્સુ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ઉચુ ગોળ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">કલર આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આખુ આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">આઉટ</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ચુર</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-top: 1px solid #000;">ઘટ</td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #000; background-color: #efefef; padding: 3px; color: #222222; text-align: center; border-left: 1px solid #b9b7b7; border-top: 1px solid #000;">જમા તારીખ</td>
										</tr>
									</thead>
									<tbody>
										<?php 
										$totalKapan = $totalKapanWeight = $totalNund = $totalGol = $totalSingle1 = $totalSingle2 = 0;
										$totalSelection = $totalChoki1 = $totalChoki2 = $totalPalchu = $totalUnchuGol = 0;
										$totalColorOut = $totalAllOut = $totalOut = $totalChur = 0;
										foreach ( $resultArr as $k=>$result )
										{
											?>
											<tr>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000;"><?php echo $k+1?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['k_name'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['k_add_name'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo formatDate( 'd-m-Y', $result['kj_start_date'] );?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_raph'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_total_kapan'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_kapan'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_nung'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_gol'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_saij'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_takavari'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_singal_1'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_singal_2'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_selection'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_choki_1'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_choki_2'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_palchu'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_uchu_gol'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_color_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_total_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_out'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_chur'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $result['kj_ghat'];?></td>
												<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo formatDate( 'd-m-Y', $result['kj_end_date'] );?></td>
											</tr>
											<?php 
											$totalKapan += $result['kj_total_kapan'];
											$totalKapanWeight += $result['kj_kapan'];
											$totalNund += $result['kj_nung'];
											$totalGol += $result['kj_gol'];
											$totalSingle1 += $result['kj_singal_1'];
											$totalSingle2 += $result['kj_singal_2'];
											$totalSelection += $result['kj_selection'];
											$totalChoki1 += $result['kj_choki_1'];
											$totalChoki2 += $result['kj_choki_2'];
											$totalPalchu += $result['kj_palchu'];
											$totalUnchuGol += $result['kj_uchu_gol'];
											$totalColorOut += $result['kj_color_out'];
											$totalAllOut += $result['kj_total_out'];
											$totalOut += $result['kj_out'];
											$totalChur += $result['kj_chur'];
										}
										?>
										<tr>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center; border-left: 1px solid #000;">કુલ:</td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalKapan;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalKapanWeight;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalNund;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalGol;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalSingle1;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalSingle2;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalSelection;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalChoki1;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalChoki2;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalPalchu;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalUnchuGol;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalColorOut;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalAllOut;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalOut;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"><?php echo $totalChur;?></td>
											<td style="font-size: 12px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
											<td style="font-size: 12px; border-right: 1px solid #000; border-bottom: 1px solid #b9b7b7; text-align: left; color: #222222; text-align: center;"></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
