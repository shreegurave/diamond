<script type="text/javascript" src="<?php echo asset_url('js/admin/jquery/jquery-1.7.1.min.js');?>"></script>
<div class="print">
	<fieldset style="border-radius: 6px 6px 6px 6px; padding: 10px 10px; border: 5px solid #b9b7b7; margin-bottom: 10px; text-align: left; width: 1%;">
		<?php 
		$record = exeQuery( "SELECT k.*, pr.* FROM kapan k 
							LEFT JOIN polish_repairing pr ON pr.kapan_id = k.kapan_id
							 WHERE k.kapan_id = ".(int)_de( $_GET['id'] ) );
		?>
		<tr>
			<td colspan="2">
				<?php $this->load->view('templates/header-template', $record );?>
				<table style="background:url('../../images/print_back_logo.png') center center no-repeat; border-collapse: collapse; width: 640px; border-top: 1px solid #b9b7b7; border-left: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; border-right: 1px solid #b9b7b7; margin-bottom: 20px;font-family:Verdana, Geneva, sans-serif">
					<tbody>
						<tr>
							<td colspan="2">
								<table style="border-collapse: collapse;">
									<thead>
										<tr>
											<td rowspan="2" style="width: 300px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;"></td>
											<td colspan="2" style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">આપેલ</td>
											<td colspan="2" style="width: 211px; font-size: 16px; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">આવેલ</td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">થાણ</td>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">વજન</td>
											<td style="width: 211px; font-size: 16px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">થાણ</td>
											<td style="width: 211px; font-size: 16px; border-bottom: 1px solid #b9b7b7; background-color: #efefef; font-weight: bold; text-align: left; padding: 3px; color: #222222; text-align: center;">વજન</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ધારખાડ / ગોળાઈ વિક:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dhar_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dhar_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dhar_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dhar_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">તળિયે રીપેરિંગ:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tly_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tly_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tly_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tly_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ટેબલ / માથળા રીપેરિંગ:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbl_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbl_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbl_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbl_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">સારી ( તળિયે ):</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_stly_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_stly_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_stly_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_stly_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">સારી ( માથળે ):</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_m_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_m_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_m_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_m_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">આખો હિરો રીપેરિંગ:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_hiro_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_hiro_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_hiro_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_hiro_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;">ટીક પોઇન્ટ / ટીક મુરામ:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tik_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tik_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tik_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tik_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['pr_e1_extra_1'] ) ? $record['pr_e1_extra_1'] : "Extra-1"?>:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e1_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e1_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e1_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e1_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['pr_e1_extra_2'] ) ? $record['pr_e1_extra_2'] : "Extra-2"?>:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e2_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e2_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e2_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e2_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo ( $record['pr_e1_extra_3'] ) ? $record['pr_e1_extra_3'] : "Extra-3"?>:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e3_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_e3_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e3_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_e3_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">સરવાળો:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_total_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_total_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_total_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_total_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">ટેબલ:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_tbld_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_tbld_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbld_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_tbld_avl_weight']?></td>
										</tr>
										<tr>
											<td style="width: 211px; font-size: 14px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;">દોરી મારવાના:</td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_dori_apl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7;"><?php echo $record['pr_dori_apl_weight']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-right: 1px solid #b9b7b7; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dori_avl_than']?></td>
											<td style="width: 211px; text-align: center; font-size: 15px; border-bottom: 1px solid #b9b7b7; padding: 3px;"><?php echo $record['pr_dori_avl_weight']?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" style="font-size:10px; text-align:center"><?php echo getLangMsg("que")?></td>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</fieldset>
</div>

<input type="button" name="Print" value="Print" onclick="printInvoice();" />
<script type="text/javascript">
	function printInvoice() 
    {
		var html = $('.print').html();
        var mywindow = window.open('', '', '');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write(html);

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>
