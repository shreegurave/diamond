-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 26, 2021 at 04:25 PM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u986422130_omj`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE `admin_log` (
  `admin_log_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `admin_class_name` varchar(100) NOT NULL,
  `module_item_name` varchar(255) NOT NULL,
  `module_table_name` varchar(100) NOT NULL,
  `module_table_field` varchar(100) NOT NULL,
  `module_primary_id` int(11) NOT NULL,
  `admin_log_type` varchar(1) NOT NULL,
  `admin_log_ip` varchar(40) NOT NULL,
  `admin_log_created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='generates log  for admin activities';

--
-- Truncate table before insert `admin_log`
--

TRUNCATE TABLE `admin_log`;
--
-- Dumping data for table `admin_log`
--

INSERT INTO `admin_log` (`admin_log_id`, `admin_user_id`, `admin_class_name`, `module_item_name`, `module_table_name`, `module_table_field`, `module_primary_id`, `admin_log_type`, `admin_log_ip`, `admin_log_created_date`) VALUES
(1, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.102.6.140', '2018-01-22 13:31:01'),
(2, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.102.6.140', '2018-01-22 13:31:01'),
(3, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.225.3', '2018-01-23 06:53:51'),
(4, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.108.116', '2018-01-23 06:54:09'),
(5, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.102.6.140', '2018-01-23 06:59:37'),
(6, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 06:59:46'),
(7, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:00:38'),
(8, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:00:39'),
(9, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:00:54'),
(10, 32, 'add_kapan', '6B', 'kapan', 'kapan_id', 1, 'D', '103.251.17.53', '2018-01-23 07:01:14'),
(11, 32, 'add_kapan', '6B', 'kapan', 'kapan_id', 1, 'D', '103.251.17.53', '2018-01-23 07:01:26'),
(12, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:01:33'),
(13, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:01:37'),
(14, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:01:42'),
(15, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:01:55'),
(16, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:02:11'),
(17, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:02:11'),
(18, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:02:18'),
(19, 32, 'manage_kapan', '6B', 'kapan', 'kapan_id', 1, 'D', '103.251.17.53', '2018-01-23 07:02:27'),
(20, 32, 'manage_kapan', '6A', 'kapan', 'kapan_id', 8, 'D', '103.251.17.53', '2018-01-23 07:02:27'),
(21, 32, 'manage_kapan', 'New 25 dec', 'kapan', 'kapan_id', 10, 'D', '103.251.17.53', '2018-01-23 07:02:27'),
(22, 32, 'manage_kapan', '3j18', 'kapan', 'kapan_id', 11, 'D', '103.251.17.53', '2018-01-23 07:02:27'),
(23, 32, 'manage_kapan', '13 B', 'kapan', 'kapan_id', 12, 'D', '103.251.17.53', '2018-01-23 07:02:27'),
(24, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:02:35'),
(25, 32, 'add_kapan', '6B', 'kapan', 'kapan_id', 1, 'D', '103.251.17.53', '2018-01-23 07:02:46'),
(26, 32, 'add_kapan', '6A', 'kapan', 'kapan_id', 8, 'D', '103.251.17.53', '2018-01-23 07:02:46'),
(27, 32, 'add_kapan', 'New 25 dec', 'kapan', 'kapan_id', 10, 'D', '103.251.17.53', '2018-01-23 07:02:46'),
(28, 32, 'add_kapan', '3j18', 'kapan', 'kapan_id', 11, 'D', '103.251.17.53', '2018-01-23 07:02:46'),
(29, 32, 'add_kapan', '13 B', 'kapan', 'kapan_id', 12, 'D', '103.251.17.53', '2018-01-23 07:02:46'),
(30, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:02:49'),
(31, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:02:54'),
(32, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:03:02'),
(33, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:03:03'),
(34, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:03:06'),
(35, 32, 'add_kapan', '97.64', 'kapan', 'kapan_id', 13, 'A', '103.251.17.53', '2018-01-23 07:06:41'),
(36, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:06:41'),
(37, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:06:56'),
(38, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:13:44'),
(39, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:13:51'),
(40, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:13:52'),
(41, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:13:55'),
(42, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:22:03'),
(43, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 07:22:16'),
(44, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 07:22:16'),
(45, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:22:34'),
(46, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 07:24:46'),
(47, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 08:06:09'),
(48, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 08:06:18'),
(49, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.112.118', '2018-01-23 08:06:19'),
(50, 5, 'admin_menu', 'Admin Menu ', 'admin_menu', 'admin_menu_id', 0, 'V', '49.34.112.118', '2018-01-23 08:06:41'),
(51, 5, 'admin_menu', 'lagadh', 'admin_menu', 'admin_menu_id', 174, 'A', '49.34.112.118', '2018-01-23 08:07:22'),
(52, 5, 'admin_menu', 'Admin Menu ', 'admin_menu', 'admin_menu_id', 0, 'V', '49.34.112.118', '2018-01-23 08:07:22'),
(53, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.112.118', '2018-01-23 08:07:29'),
(54, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.112.118', '2018-01-23 08:07:38'),
(55, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.112.118', '2018-01-23 08:07:40'),
(56, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.112.118', '2018-01-23 08:07:59'),
(57, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.112.118', '2018-01-23 08:08:08'),
(58, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 08:08:23'),
(59, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 09:20:08'),
(60, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 09:20:19'),
(61, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 09:20:19'),
(62, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:20:26'),
(63, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:21:32'),
(64, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 09:22:43'),
(65, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 09:22:50'),
(66, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.112.118', '2018-01-23 09:22:51'),
(67, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.112.118', '2018-01-23 09:22:54'),
(68, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:49:40'),
(69, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 09:50:41'),
(70, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 09:50:51'),
(71, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 09:50:52'),
(72, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:50:55'),
(73, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:52:57'),
(74, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:53:21'),
(75, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:53:24'),
(76, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:54:31'),
(77, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:54:36'),
(78, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:55:19'),
(79, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:55:28'),
(80, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:55:46'),
(81, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:55:54'),
(82, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:55:59'),
(83, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:56:02'),
(84, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 09:56:05'),
(85, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.112.118', '2018-01-23 10:03:18'),
(86, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 10:15:55'),
(87, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 10:16:11'),
(88, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 10:16:11'),
(89, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:16:16'),
(90, 32, 'add_kapan', '9', 'kapan', 'kapan_id', 14, 'A', '103.251.17.53', '2018-01-23 10:17:20'),
(91, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:17:20'),
(92, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:17:29'),
(93, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 10:17:40'),
(94, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 10:17:40'),
(95, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:17:45'),
(96, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:20:52'),
(97, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 10:21:28'),
(98, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 10:21:28'),
(99, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 10:21:37'),
(100, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 11:56:01'),
(101, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 11:56:24'),
(102, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 12:12:29'),
(103, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 12:12:31'),
(104, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 12:12:40'),
(105, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 12:14:52'),
(106, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 12:19:26'),
(107, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 13:26:23'),
(108, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 13:26:24'),
(109, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 13:26:57'),
(110, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.239.63', '2018-01-23 14:13:20'),
(111, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.239.63', '2018-01-23 14:13:26'),
(112, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.239.63', '2018-01-23 14:13:26'),
(113, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.239.63', '2018-01-23 14:13:31'),
(114, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.65.232', '2018-01-23 14:13:44'),
(115, 0, 'lgs', 'Login', '', '0', 0, 'V', '64.233.173.33', '2018-01-23 14:16:36'),
(116, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 14:18:55'),
(117, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-23 14:19:04'),
(118, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-23 14:19:04'),
(119, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 14:19:08'),
(120, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-23 14:28:51'),
(121, 0, 'lgs', 'Login', '', '0', 0, 'V', '64.233.173.38', '2018-01-24 02:46:43'),
(122, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.54', '2018-01-24 03:58:07'),
(123, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.54', '2018-01-24 03:58:07'),
(124, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.54', '2018-01-24 03:58:13'),
(125, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.237.54', '2018-01-24 03:58:13'),
(126, 5, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.54', '2018-01-24 03:58:16'),
(127, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.54', '2018-01-24 04:03:21'),
(128, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.54', '2018-01-24 04:24:54'),
(129, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.54', '2018-01-24 04:25:28'),
(130, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.54', '2018-01-24 04:31:28'),
(131, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.30.215', '2018-01-25 03:49:34'),
(132, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.30.215', '2018-01-25 03:49:47'),
(133, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.30.215', '2018-01-25 03:50:12'),
(134, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.30.215', '2018-01-25 03:50:13'),
(135, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.215', '2018-01-25 03:50:19'),
(136, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.30.215', '2018-01-25 03:50:21'),
(137, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.215', '2018-01-25 03:50:25'),
(138, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.215', '2018-01-25 03:58:00'),
(139, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.215', '2018-01-25 03:58:27'),
(140, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.215', '2018-01-25 04:02:09'),
(141, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.7.5', '2018-01-25 04:18:48'),
(142, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.174', '2018-01-25 04:47:28'),
(143, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.174', '2018-01-25 04:48:21'),
(144, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.174', '2018-01-25 04:48:26'),
(145, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:14'),
(146, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:34'),
(147, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:40'),
(148, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:46'),
(149, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:49'),
(150, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:52'),
(151, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 07:30:56'),
(152, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:55:25'),
(153, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:55:31'),
(154, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:55:34'),
(155, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:56:06'),
(156, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:56:31'),
(157, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.60', '2018-01-25 07:56:42'),
(158, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.32.36', '2018-01-25 07:56:53'),
(159, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.15.49', '2018-01-25 08:00:04'),
(160, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.15.49', '2018-01-25 08:00:07'),
(161, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 08:00:10'),
(162, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.15.49', '2018-01-25 08:02:19'),
(163, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.121', '2018-01-25 08:26:25'),
(164, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:27:08'),
(165, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:27:59'),
(166, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:28:06'),
(167, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:28:12'),
(168, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:28:17'),
(169, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:29:58'),
(170, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:30:02'),
(171, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:30:08'),
(172, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:30:12'),
(173, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.177', '2018-01-25 10:32:58'),
(174, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.2.111', '2018-01-25 11:42:53'),
(175, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.180', '2018-01-25 11:45:43'),
(176, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.180', '2018-01-25 11:46:58'),
(177, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.31', '2018-01-26 05:43:40'),
(178, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.31', '2018-01-26 05:43:50'),
(179, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.237.31', '2018-01-26 05:43:50'),
(180, 5, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:43:55'),
(181, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:44:08'),
(182, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:44:13'),
(183, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:44:44'),
(184, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:51:27'),
(185, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:54:21'),
(186, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.31', '2018-01-26 05:54:30'),
(187, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.31', '2018-01-26 05:55:13'),
(188, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.17.219', '2018-01-26 05:57:22'),
(189, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.17.219', '2018-01-26 05:58:26'),
(190, 0, 'lgs', 'Login', '', '0', 0, 'V', '42.106.17.219', '2018-01-26 05:59:13'),
(191, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.17.219', '2018-01-26 05:59:15'),
(192, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.17.219', '2018-01-26 05:59:31'),
(193, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.17.219', '2018-01-26 05:59:37'),
(194, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.17.219', '2018-01-26 06:00:25'),
(195, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.12.229', '2018-01-26 07:22:12'),
(196, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.12.229', '2018-01-26 07:23:47'),
(197, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.12.229', '2018-01-26 07:26:27'),
(198, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:08:25'),
(199, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.30.118', '2018-01-26 08:08:55'),
(200, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.30.118', '2018-01-26 08:08:56'),
(201, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:09:26'),
(202, 32, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.52', '2018-01-26 08:10:20'),
(203, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.52', '2018-01-26 08:10:35'),
(204, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.52', '2018-01-26 08:10:51'),
(205, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.52', '2018-01-26 08:11:25'),
(206, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:11:56'),
(207, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:11:57'),
(208, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:12:22'),
(209, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.30.118', '2018-01-26 08:12:34'),
(210, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:19:01'),
(211, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:25:43'),
(212, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:28:07'),
(213, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.28.101', '2018-01-26 08:30:16'),
(214, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.28.101', '2018-01-26 08:30:19'),
(215, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:30:31'),
(216, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.28.101', '2018-01-26 08:31:02'),
(217, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.28.101', '2018-01-26 08:31:06'),
(218, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:31:17'),
(219, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.28.101', '2018-01-26 08:46:56'),
(220, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.28.101', '2018-01-26 08:47:00'),
(221, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.101', '2018-01-26 08:47:09'),
(222, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.23.86', '2018-01-26 10:05:48'),
(223, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.23.86', '2018-01-26 10:05:52'),
(224, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.23.86', '2018-01-26 10:06:06'),
(225, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.16.32', '2018-01-26 10:19:03'),
(226, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.16.32', '2018-01-26 10:23:36'),
(227, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.16.32', '2018-01-26 10:23:59'),
(228, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.16.32', '2018-01-26 10:24:02'),
(229, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.16.32', '2018-01-26 10:24:14'),
(230, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.16.32', '2018-01-26 10:24:27'),
(231, 32, 'lgs', 'Login', '', '0', 0, 'V', '42.106.16.32', '2018-01-26 10:24:31'),
(232, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '42.106.16.32', '2018-01-26 10:24:31'),
(233, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.16.32', '2018-01-26 10:24:55'),
(234, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.16.32', '2018-01-26 10:25:04'),
(235, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.28.100', '2018-01-26 10:29:35'),
(236, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.88', '2018-01-26 14:23:43'),
(237, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.88', '2018-01-26 14:25:21'),
(238, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.88', '2018-01-26 14:27:34'),
(239, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.2.195', '2018-01-27 03:15:22'),
(240, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.2.195', '2018-01-27 03:18:05'),
(241, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.13.198', '2018-01-27 03:21:26'),
(242, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.180', '2018-01-27 05:40:02'),
(243, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '42.106.22.180', '2018-01-27 05:43:01'),
(244, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 08:59:55'),
(245, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-27 09:00:16'),
(246, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-27 09:00:16'),
(247, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-27 09:00:19'),
(248, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-27 09:00:20'),
(249, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 09:00:25'),
(250, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 09:15:40'),
(251, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-27 13:34:59'),
(252, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-27 13:35:36'),
(253, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-27 13:35:37'),
(254, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:35:44'),
(255, 32, 'add_kapan', '12', 'kapan', 'kapan_id', 15, 'A', '103.251.17.53', '2018-01-27 13:36:58'),
(256, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:36:58'),
(257, 32, 'add_kapan', '12', 'kapan', 'kapan_id', 15, 'E', '103.251.17.53', '2018-01-27 13:37:10'),
(258, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:37:11'),
(259, 32, 'add_kapan', '12', 'kapan', 'kapan_id', 15, 'E', '103.251.17.53', '2018-01-27 13:37:33'),
(260, 32, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:37:33'),
(261, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:37:53'),
(262, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:38:36'),
(263, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:39:31'),
(264, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:52:14'),
(265, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:54:55'),
(266, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 13:58:10'),
(267, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 14:04:13'),
(268, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-27 14:55:52'),
(269, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-27 14:55:53'),
(270, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 14:56:05'),
(271, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-27 14:58:02'),
(272, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 05:45:47'),
(273, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 05:46:38'),
(274, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 05:46:39'),
(275, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 05:46:44'),
(276, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 05:47:43'),
(277, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 06:09:47'),
(278, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 06:27:45'),
(279, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 06:27:56'),
(280, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:06:10'),
(281, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:24:47'),
(282, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:25:01'),
(283, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:01'),
(284, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:06'),
(285, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:25:15'),
(286, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:16'),
(287, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:25:16'),
(288, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:17'),
(289, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:22'),
(290, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:25:35'),
(291, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:25:36'),
(292, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:33:31'),
(293, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:34:50'),
(294, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:34:59'),
(295, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:34:59'),
(296, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-28 07:35:10'),
(297, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-28 07:35:25'),
(298, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-28 07:35:25'),
(299, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 03:03:07'),
(300, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 03:03:42'),
(301, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 03:03:49'),
(302, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 03:03:49'),
(303, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 03:04:43'),
(304, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 03:06:18'),
(305, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 03:07:28'),
(306, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.140.8', '2018-01-29 03:59:48'),
(307, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.140.8', '2018-01-29 04:02:14'),
(308, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.140.8', '2018-01-29 04:02:14'),
(309, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 04:02:23'),
(310, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 04:16:44'),
(311, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 04:18:44'),
(312, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 05:35:24'),
(313, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 05:36:07'),
(314, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 05:36:15'),
(315, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 05:36:25'),
(316, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:44:32'),
(317, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:44:48'),
(318, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:49:13'),
(319, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:49:39'),
(320, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 05:49:40'),
(321, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:49:45'),
(322, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:50:05'),
(323, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 05:50:05'),
(324, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:50:30'),
(325, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:51:11'),
(326, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:51:35'),
(327, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 05:51:36'),
(328, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:51:42'),
(329, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:51:44'),
(330, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 05:51:53'),
(331, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 05:51:53'),
(332, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:51:59'),
(333, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 05:52:41'),
(334, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:01:01'),
(335, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:03:30'),
(336, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:05:09'),
(337, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:05:50'),
(338, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:06:13'),
(339, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:07:00'),
(340, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 06:07:09'),
(341, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 06:07:10'),
(342, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:07:13'),
(343, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:07:19'),
(344, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 06:07:45'),
(345, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 06:07:45'),
(346, 5, 'lgs', 'Login', '', '0', 0, 'V', '49.34.140.8', '2018-01-29 06:08:23'),
(347, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.140.8', '2018-01-29 06:08:24'),
(348, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:08:59'),
(349, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:11:43'),
(350, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:15:03'),
(351, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:17:12'),
(352, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:17:23'),
(353, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:18:03'),
(354, 32, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-29 06:18:15'),
(355, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-29 06:18:16'),
(356, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:18:21'),
(357, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:19:11'),
(358, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:19:16'),
(359, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:19:29'),
(360, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:19:39'),
(361, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:19:58'),
(362, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:20:22'),
(363, 32, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:21:07'),
(364, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:21:09'),
(365, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 06:21:12'),
(366, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.140.8', '2018-01-29 06:29:46'),
(367, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.140.8', '2018-01-29 06:43:07'),
(368, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 09:54:37'),
(369, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.227.155', '2018-01-29 10:04:22'),
(370, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.227.155', '2018-01-29 10:04:29'),
(371, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '123.201.227.155', '2018-01-29 10:04:29'),
(372, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.227.155', '2018-01-29 10:04:32'),
(373, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.227.155', '2018-01-29 10:09:20'),
(374, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.227.155', '2018-01-29 10:09:32'),
(375, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.227.155', '2018-01-29 10:16:53'),
(376, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 10:51:27'),
(377, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 10:52:32'),
(378, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 13:26:30'),
(379, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 13:36:57'),
(380, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 13:48:55'),
(381, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 13:55:42'),
(382, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-29 14:00:39'),
(383, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.238', '2018-01-29 15:53:28'),
(384, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.238', '2018-01-29 15:53:34'),
(385, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.236.238', '2018-01-29 15:53:35'),
(386, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.236.238', '2018-01-29 15:53:38'),
(387, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.236.238', '2018-01-29 16:00:19'),
(388, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.236.238', '2018-01-29 16:02:03'),
(389, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.238', '2018-01-29 16:43:06'),
(390, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-30 03:19:53'),
(391, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-30 03:20:22'),
(392, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-30 03:20:23'),
(393, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 03:20:30'),
(394, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 03:20:50'),
(395, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.238.179', '2018-01-30 03:41:10'),
(396, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.238.179', '2018-01-30 03:41:17'),
(397, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.238.179', '2018-01-30 03:41:17'),
(398, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 03:41:20'),
(399, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 03:41:35'),
(400, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 03:41:53'),
(401, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:00:58'),
(402, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:04:16'),
(403, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:05:11'),
(404, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:06:01'),
(405, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:06:17'),
(406, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:07:05'),
(407, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.238.179', '2018-01-30 04:38:02'),
(408, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.238.179', '2018-01-30 04:45:19'),
(409, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-30 08:52:08'),
(410, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-30 08:53:09'),
(411, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-30 08:53:09'),
(412, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 08:53:17'),
(413, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 08:55:46'),
(414, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.226.174', '2018-01-30 09:11:33'),
(415, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.226.174', '2018-01-30 09:11:33'),
(416, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.226.174', '2018-01-30 09:11:41'),
(417, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '123.201.226.174', '2018-01-30 09:11:41'),
(418, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.226.174', '2018-01-30 09:15:52'),
(419, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.226.174', '2018-01-30 09:16:17'),
(420, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:29:56'),
(421, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:30:04'),
(422, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:37:13'),
(423, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:41:32'),
(424, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:53:34'),
(425, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:55:47'),
(426, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 13:57:33'),
(427, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:10:39'),
(428, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:13:24'),
(429, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:20:03'),
(430, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:30:13'),
(431, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:34:21'),
(432, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:40:30'),
(433, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:47:44'),
(434, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-30 14:50:12'),
(435, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.225.245', '2018-01-30 15:42:15'),
(436, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.225.245', '2018-01-30 15:42:21'),
(437, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '123.201.225.245', '2018-01-30 15:42:21'),
(438, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.225.245', '2018-01-30 15:42:24'),
(439, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '123.201.225.245', '2018-01-30 15:42:34'),
(440, 0, 'lgs', 'Login', '', '0', 0, 'V', '123.201.225.245', '2018-01-30 15:43:00'),
(441, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 05:01:49'),
(442, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 05:01:55'),
(443, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-31 05:01:55'),
(444, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:01:59'),
(445, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:17:09'),
(446, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:17:19'),
(447, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:32:31'),
(448, 5, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 05:32:37'),
(449, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-31 05:32:38'),
(450, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:32:40'),
(451, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:33:17'),
(452, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:33:28'),
(453, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:33:41'),
(454, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:38:06'),
(455, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:38:21'),
(456, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:38:40'),
(457, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:38:41'),
(458, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:39:02'),
(459, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:39:20'),
(460, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:39:34'),
(461, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:40:08'),
(462, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:42:37'),
(463, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:42:39'),
(464, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:43:12'),
(465, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:55:00'),
(466, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 05:55:01'),
(467, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:01:27'),
(468, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:01:27'),
(469, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:10:48'),
(470, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:12:17'),
(471, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:19:23'),
(472, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:27:37'),
(473, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:28:03'),
(474, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:31:32'),
(475, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:31:37'),
(476, 5, 'add_kapan', '33-B', 'kapan', 'kapan_id', 16, 'A', '103.251.17.53', '2018-01-31 06:33:39'),
(477, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:33:39'),
(478, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:33:43'),
(479, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:35:49'),
(480, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:44:13'),
(481, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:44:14'),
(482, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:48:05'),
(483, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:50:46'),
(484, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:50:58');
INSERT INTO `admin_log` (`admin_log_id`, `admin_user_id`, `admin_class_name`, `module_item_name`, `module_table_name`, `module_table_field`, `module_primary_id`, `admin_log_type`, `admin_log_ip`, `admin_log_created_date`) VALUES
(485, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:52:56'),
(486, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:53:08'),
(487, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:53:30'),
(488, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:54:08'),
(489, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 06:57:11'),
(490, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:08:38'),
(491, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 07:09:01'),
(492, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 07:09:59'),
(493, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 07:09:59'),
(494, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-01-31 07:10:05'),
(495, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-01-31 07:10:06'),
(496, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:10:08'),
(497, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:19:11'),
(498, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:19:22'),
(499, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:19:29'),
(500, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-01-31 07:20:28'),
(501, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:23:02'),
(502, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:23:09'),
(503, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:47:33'),
(504, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:58:27'),
(505, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:58:28'),
(506, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:59:30'),
(507, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 09:59:30'),
(508, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:00:29'),
(509, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:00:30'),
(510, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:02:40'),
(511, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:02:46'),
(512, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:04:14'),
(513, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:29:29'),
(514, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:29:29'),
(515, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:29:46'),
(516, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:29:53'),
(517, 5, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.133', '2018-01-31 10:34:45'),
(518, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '219.91.237.133', '2018-01-31 10:34:45'),
(519, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '219.91.237.133', '2018-01-31 10:34:48'),
(520, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.237.133', '2018-01-31 10:35:18'),
(521, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.145', '2018-02-03 04:09:03'),
(522, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.145', '2018-02-03 04:41:19'),
(523, 0, 'lgs', 'Login', '', '0', 0, 'V', '219.91.236.145', '2018-02-03 04:48:35'),
(524, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-02-03 05:13:12'),
(525, 0, 'lgs', 'Login', '', '0', 0, 'V', '103.251.17.53', '2018-02-03 05:13:48'),
(526, 32, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '103.251.17.53', '2018-02-03 05:13:49'),
(527, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-02-03 05:13:55'),
(528, 32, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '103.251.17.53', '2018-02-03 05:17:54'),
(529, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.175.100', '2018-02-04 09:35:04'),
(530, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.124.248', '2018-02-10 12:48:20'),
(531, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.66.157', '2019-05-29 17:49:46'),
(532, 0, 'lgs', 'Forgot Password', '', '0', 0, 'V', '66.249.66.158', '2019-05-31 07:32:33'),
(533, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.65.87', '2019-06-04 03:39:41'),
(534, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.43.117', '2019-06-04 03:39:53'),
(535, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '157.32.43.117', '2019-06-04 03:39:53'),
(536, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '157.32.65.87', '2019-06-04 03:40:10'),
(537, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.65.87', '2019-06-04 03:40:13'),
(538, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.43.117', '2019-06-04 03:40:21'),
(539, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.43.117', '2019-06-04 03:40:30'),
(540, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.43.117', '2019-06-04 03:40:33'),
(541, 5, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.43.117', '2019-06-04 03:40:41'),
(542, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.209.195', '2019-06-04 03:41:22'),
(543, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '157.32.209.195', '2019-06-04 03:41:26'),
(544, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.209.195', '2019-06-04 03:41:31'),
(545, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.209.195', '2019-06-04 03:41:36'),
(546, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '157.32.209.195', '2019-06-04 03:41:37'),
(547, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.65.87', '2019-06-04 03:42:13'),
(548, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.66.203', '2019-06-13 23:54:57'),
(549, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.73.10', '2019-06-14 11:03:52'),
(550, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.29.177', '2019-06-17 17:06:59'),
(551, 0, 'lgs', 'Login', '', '0', 0, 'V', '5.255.250.24', '2019-06-20 13:06:52'),
(552, 0, 'lgs', 'Login', '', '0', 0, 'V', '180.163.220.4', '2019-06-20 20:31:23'),
(553, 0, 'lgs', 'Forgot Password', '', '0', 0, 'V', '5.255.250.24', '2019-06-21 09:39:09'),
(554, 0, 'lgs', 'Forgot Password', '', '0', 0, 'V', '100.43.85.201', '2019-06-22 03:39:07'),
(555, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.220.149.40', '2019-06-30 13:36:21'),
(556, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.36.1.182', '2019-07-02 10:30:41'),
(557, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.36.1.182', '2019-07-02 10:30:46'),
(558, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.36.1.182', '2019-07-02 10:30:47'),
(559, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.1.182', '2019-07-02 10:30:54'),
(560, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.1.182', '2019-07-02 10:31:03'),
(561, 5, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.1.182', '2019-07-02 10:31:10'),
(562, 0, 'lgs', 'Login', '', '0', 0, 'V', '100.43.85.117', '2019-07-03 12:33:21'),
(563, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.64.75', '2019-07-03 18:24:06'),
(564, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.64.75', '2019-07-04 08:57:49'),
(565, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.64.71', '2019-07-04 18:24:06'),
(566, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.75.159', '2019-07-08 16:02:02'),
(567, 0, 'lgs', 'Forgot Password', '', '0', 0, 'V', '5.255.250.24', '2019-07-13 08:46:17'),
(568, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.227.44', '2019-07-17 14:30:51'),
(569, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:16:57'),
(570, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:16:57'),
(571, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:17:08'),
(572, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:17:11'),
(573, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:18:53'),
(574, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:19:02'),
(575, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:19:20'),
(576, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:19:24'),
(577, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.184.129', '2019-07-21 12:19:35'),
(578, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.184.129', '2019-07-21 12:19:54'),
(579, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:20:16'),
(580, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:20:53'),
(581, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:21:21'),
(582, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:22:42'),
(583, 5, 'admin_user', 'Demo', 'admin_user', 'admin_user_id', 33, 'E', '49.34.184.129', '2019-07-21 12:22:57'),
(584, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:22:57'),
(585, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:24:17'),
(586, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:24:17'),
(587, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.184.129', '2019-07-21 12:24:34'),
(588, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:25:03'),
(589, 5, 'admin_user', 'Demo', 'admin_user', 'admin_user_id', 33, 'E', '49.34.184.129', '2019-07-21 12:25:16'),
(590, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:25:16'),
(591, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:25:22'),
(592, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:25:32'),
(593, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:25:43'),
(594, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:25:44'),
(595, 5, 'admin_user', 'Demo', 'admin_user', 'admin_user_id', 33, 'E', '49.34.184.129', '2019-07-21 12:26:01'),
(596, 5, 'admin_user', 'Admin User ', 'admin_user', 'admin_user_id', 0, 'V', '49.34.184.129', '2019-07-21 12:26:01'),
(597, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.184.129', '2019-07-21 12:26:06'),
(598, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:26:26'),
(599, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:26:37'),
(600, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:26:49'),
(601, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:26:50'),
(602, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:27:25'),
(603, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:27:39'),
(604, 33, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.34.184.129', '2019-07-21 12:27:41'),
(605, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:27:53'),
(606, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.184.129', '2019-07-21 12:28:07'),
(607, 5, 'admin_permission', 'Admin Permission ', 'permission', 'permission_id', 0, 'V', '49.34.184.129', '2019-07-21 12:28:30'),
(608, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.184.129', '2019-07-21 12:28:46'),
(609, 5, 'manage_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.34.184.129', '2019-07-21 12:28:58'),
(610, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:30:50'),
(611, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-21 12:34:03'),
(612, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.184.129', '2019-07-21 12:34:40'),
(613, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-22 03:29:07'),
(614, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-22 03:29:07'),
(615, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.141.223', '2019-07-23 02:49:03'),
(616, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.34.141.223', '2019-07-23 02:49:03'),
(617, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-24 02:57:31'),
(618, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-24 02:57:31'),
(619, 0, 'lgs', 'Login', '', '0', 0, 'V', '5.255.250.24', '2019-07-24 10:47:02'),
(620, 0, 'lgs', 'Login', '', '0', 0, 'V', '157.32.161.113', '2019-07-24 16:24:02'),
(621, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-26 03:15:03'),
(622, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-26 03:15:03'),
(623, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-26 15:17:03'),
(624, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-27 03:45:36'),
(625, 0, 'lgs', 'Login', '', '0', 0, 'V', '0.0.0.0', '2019-07-28 05:15:26'),
(626, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.36.15.116', '2019-08-01 12:39:18'),
(627, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.36.15.116', '2019-08-01 12:39:20'),
(628, 0, 'lgs', 'Login', '', '0', 0, 'V', '49.36.15.116', '2019-08-01 12:39:27'),
(629, 5, 'dashboard', 'DashBoard', 'product_categories', 'category_id', 0, 'V', '49.36.15.116', '2019-08-01 12:39:28'),
(630, 5, 'add_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.15.116', '2019-08-01 12:39:36'),
(631, 5, 'lagadh', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.15.116', '2019-08-01 12:39:47'),
(632, 5, 'jama_kapan', 'Admin User ', 'kapan', 'kapan_id', 0, 'V', '49.36.15.116', '2019-08-01 12:40:03'),
(633, 0, 'lgs', 'Login', '', '0', 0, 'V', '141.8.144.41', '2019-08-04 08:17:28'),
(634, 0, 'lgs', 'Login', '', '0', 0, 'V', '66.249.73.8', '2019-08-06 18:25:51'),
(635, 0, 'lgs', 'Forgot Password', '', '0', 0, 'V', '5.255.250.24', '2019-08-13 21:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `admin_menu_id` int(11) NOT NULL,
  `am_class_name` varchar(100) NOT NULL,
  `am_parent_id` int(11) DEFAULT NULL,
  `am_name` varchar(100) NOT NULL,
  `am_icon` mediumtext NOT NULL,
  `image_size_id` int(11) NOT NULL DEFAULT 0,
  `am_sort_order` tinyint(3) NOT NULL DEFAULT 0,
  `am_status` tinyint(1) NOT NULL DEFAULT 0,
  `am_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `am_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='permissions for admin menu';

--
-- Truncate table before insert `admin_menu`
--

TRUNCATE TABLE `admin_menu`;
--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`admin_menu_id`, `am_class_name`, `am_parent_id`, `am_name`, `am_icon`, `image_size_id`, `am_sort_order`, `am_status`, `am_created_date`, `am_modified_date`) VALUES
(1, '', 0, 'ડેશબોર્ડ', 'assets/admin_menu/application_home1699633008_s.png', 3, 1, 0, '2013-07-18 09:44:11', '2017-11-21 04:47:58'),
(2, 'dashboard', 1, 'ડેશબોર્ડ', 'assets/admin_menu/home1608218705_s.png', 4, 1, 0, '2013-07-18 10:06:04', '2017-11-21 04:48:28'),
(3, 'global_configuration', 1, 'Settings', 'assets/admin_menu/setting1688696033_s.png', 0, 2, 1, '2013-07-18 10:08:29', '2013-10-25 05:40:10'),
(4, 'main_front_menu', 1, 'Menu Manager', 'assets/admin_menu/menu11114878812_s.png', 0, 3, 1, '2013-07-19 05:07:24', '2013-08-13 05:04:50'),
(27, '', 0, 'Customers', 'assets/admin_menu/customer2079522939_s.png', 0, 4, 1, '2013-07-20 11:44:18', '2013-07-29 11:18:48'),
(28, 'customer', 27, 'Customers', 'assets/admin_menu/customer608122556_s.png', 0, 1, 0, '2013-07-20 11:45:05', '2013-07-29 11:19:00'),
(31, 'customer_group', 27, 'Customer Groups', 'assets/admin_menu/user-group419461350_s.png', 0, 2, 0, '2013-07-27 05:45:15', '2013-07-29 11:19:22'),
(36, '', 0, 'Localisation', 'assets/admin_menu/localisation2126208169_s.png', 0, 6, 1, '2013-07-27 05:48:10', '2013-07-29 11:28:18'),
(39, 'manufacturer', 36, 'Manufacturer', 'assets/admin_menu/manufacture300774354_s.png', 1, 3, 0, '2013-07-27 06:03:26', '2013-07-29 11:34:29'),
(51, '', 0, 'System', 'assets/admin_menu/hammer_screwdriver248453280_s.png', 0, 7, 1, '2013-07-27 06:10:11', '2013-07-29 11:48:08'),
(52, 'admin_permission', 51, 'Permissions', 'assets/admin_menu/artcle-manage1642685281_s.png', 1, 1, 0, '2013-07-27 06:10:37', '2013-07-29 11:48:23'),
(53, 'admin_menu', 51, 'Admin Menus', 'assets/admin_menu/feed362854865_s.png', 0, 2, 0, '2013-07-27 06:11:02', '2013-07-29 10:55:13'),
(54, 'module_manager', 51, 'Module Manager', 'assets/admin_menu/module704485388_s.png', 0, 3, 1, '2013-07-27 06:11:28', '2013-07-29 11:48:38'),
(55, '', 51, 'Admin Users', 'assets/admin_menu/user_icon1816507526_s.png', 0, 4, 1, '2013-07-27 06:17:08', '2013-07-29 11:49:05'),
(56, 'admin_user', 55, 'Users', 'assets/admin_menu/user_icon483963425_s.png', 0, 1, 1, '2013-07-27 06:19:38', '2013-07-29 11:49:20'),
(57, 'admin_user_group', 55, 'Users Group', 'assets/admin_menu/user-group1769239697_s.png', 0, 2, 1, '2013-07-27 06:20:08', '2013-07-29 11:49:40'),
(61, 'error_code', 51, 'Error Codes', 'assets/admin_menu/error1555240937_s.png', 0, 8, 1, '2013-07-27 06:22:08', '2013-07-29 11:50:41'),
(62, 'configuration', 51, 'Configurations', 'assets/admin_menu/lockscreen846627088_s.png', 1, 9, 0, '2013-07-27 06:22:34', '2013-07-29 11:51:21'),
(64, '', 0, 'Reports', 'assets/admin_menu/report653392728_s.png', 0, 8, 1, '2013-07-27 06:23:35', '2013-07-29 11:51:57'),
(65, 'report_order', 71, 'Orders', 'assets/admin_menu/report127572623_s.png', 0, 1, 0, '2013-07-27 06:23:57', '2013-07-29 11:53:42'),
(66, 'report_order_tracking', 71, 'Order Tracking', 'assets/admin_menu/report899557056_s.png', 0, 2, 1, '2013-07-27 06:24:26', '2013-07-29 11:53:31'),
(67, 'report_tax', 71, 'Tax', 'assets/admin_menu/report1707896058_s.png', 0, 8, 1, '2013-07-27 06:24:50', '2013-09-14 02:46:43'),
(68, 'report_shipping', 71, 'Shipping', 'assets/admin_menu/report1596275677_s.png', 0, 4, 1, '2013-07-27 06:25:11', '2013-07-29 11:55:18'),
(69, 'report_payment', 71, 'Payments', 'assets/admin_menu/report915045446_s.png', 0, 5, 0, '2013-07-27 06:25:45', '2013-07-29 11:55:30'),
(70, 'report_order_return', 71, 'Returns', 'assets/admin_menu/report1295796317_s.png', 0, 6, 0, '2013-07-27 06:26:08', '2013-07-29 11:55:43'),
(71, '', 64, 'Sales', 'assets/admin_menu/order987193613_s.png', 0, 1, 0, '2013-07-27 06:36:48', '2013-07-29 11:56:55'),
(72, 'report_coupon', 64, 'Coupons', 'assets/admin_menu/coupon882003135_s.png', 0, 2, 0, '2013-07-27 06:40:52', '2013-07-29 11:57:10'),
(74, 'report_manufacturer', 64, 'Manufacturer', 'assets/admin_menu/manufacture628286677_s.png', 1, 4, 1, '2013-07-27 06:42:04', '2013-07-29 11:57:30'),
(81, '', 64, 'Reviews', 'assets/admin_menu/review838089458_s.png', 0, 7, 0, '2013-07-27 06:58:15', '2013-07-29 11:59:03'),
(82, 'report_product_review', 81, 'Products', 'assets/admin_menu/report288754493_s.png', 0, 1, 0, '2013-07-27 06:58:51', '2013-07-29 11:59:14'),
(83, 'report_customer_review', 81, 'Customers', 'assets/admin_menu/report1796186893_s.png', 0, 2, 1, '2013-07-27 06:59:22', '2013-07-29 11:59:25'),
(84, 'report_admin_log', 64, 'Admin Logs', 'assets/admin_menu/log1490266929_s.png', 0, 10, 0, '2013-07-27 06:59:53', '2013-12-10 02:49:40'),
(94, '', 0, 'Admin Tools', 'assets/admin_menu/setting1069380680_s.png', 1, 10, 1, '2013-07-27 07:11:45', '2017-11-19 18:30:00'),
(95, 'email_system', 136, 'E-Mail System', 'assets/admin_menu/mail692524744_s.png', 1, 1, 0, '2013-07-27 07:12:24', '2013-12-28 01:46:17'),
(105, 'report_discount', 71, 'Discount', 'assets/admin_menu/1379155333_bag1207971245_s.png', 2, 7, 1, '2013-09-13 23:43:08', '2013-09-14 00:13:13'),
(115, 'mail_templates', 51, 'Mail Templates', 'assets/admin_menu/1380383935_041267807289_s.png', 2, 8, 1, '2013-09-28 05:29:35', '2017-11-18 18:30:00'),
(118, 'lgs', 1, 'Login', 'assets/admin_menu/radiant-cut-diamond748877168_s.jpg', 0, 0, 1, '2013-12-09 04:37:01', '2017-11-18 18:30:00'),
(136, '', 94, 'E-mail', 'assets/admin_menu/category04135499448_s.jpg', 2, 1, 0, '2013-12-28 01:43:44', '2017-11-18 18:30:00'),
(137, 'email_list', 136, 'E-mail List', 'assets/admin_menu/diwali-bg-left-top1645091382_s.jpg', 2, 3, 0, '2013-12-28 01:45:50', '2017-11-18 18:30:00'),
(146, 'languages', 36, 'Languages', 'assets/admin_menu/81394351543_s.jpg', 2, 15, 0, '2014-01-17 02:49:34', '2017-11-11 18:30:00'),
(153, 'ebay', 152, 'eBay', 'assets/admin_menu/001523444055_s.png', 0, 1, 0, '2014-05-02 01:38:22', '2017-11-18 18:30:00'),
(157, 'scraper_pta', 155, 'Scraper PTA', 'assets/admin_menu/8729219343_s.png', 2, 5, 1, '2014-08-02 12:26:58', '2014-08-12 10:34:07'),
(158, 'scraper_pta_editing', 155, 'Scraper PTA Editing', 'assets/admin_menu/white3920391211_s.png', 0, 8, 1, '2014-08-15 09:20:12', '2017-11-11 18:30:00'),
(159, 'inventory_type', 51, 'Manage Inventory', 'assets/admin_menu/Penguins1995040745_s.jpg', 0, 1, 1, '2015-02-25 19:30:11', '2015-02-27 18:44:38'),
(168, 'report_reffer_bonus', 71, 'Reffer Bonus', 'assets/admin_menu/Reffer_Bonus1777290863_s.jpg', 0, 0, 0, '2015-04-28 17:26:14', '2017-11-18 18:30:00'),
(169, 'imrt_content', 152, 'IM_ Content Upload', 'assets/admin_menu/Penguins1274288737_s.jpg', 0, 5, 0, '2015-05-05 23:49:40', '2015-05-05 23:50:00'),
(170, '', 0, 'કાપણ', 'assets/admin_menu/kapan_641880140252_s.PNG', 3, 5, 0, '2017-11-20 10:38:16', '2017-11-21 04:38:54'),
(171, 'add_kapan', 170, 'કાપણ ઉમેરો', 'assets/admin_menu/kapan_641504425534_s.PNG', 3, 1, 0, '2017-11-20 10:40:40', '2017-11-21 04:42:20'),
(172, 'manage_kapan', 170, 'કાપણ ગોઠવણ', 'assets/admin_menu/kapan_641225668910_s.PNG', 3, 5, 0, '2017-11-20 10:41:13', '2017-11-21 04:47:22'),
(173, 'jama_kapan', 170, 'કાપણ જમા', 'assets/admin_menu/kapan_64935461005_s.PNG', 3, 10, 0, '2017-12-01 05:45:33', '2017-12-01 05:45:33'),
(174, 'lagadh', 170, 'લાગઢ', 'assets/admin_menu/cloudwebs535724246_s.png', 3, 20, 0, '2018-01-23 08:07:22', '2018-01-23 08:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE `admin_user` (
  `admin_user_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL DEFAULT 7,
  `admin_user_firstname` varchar(200) DEFAULT NULL,
  `admin_user_lastname` varchar(200) DEFAULT NULL,
  `admin_user_emailid` varchar(200) DEFAULT NULL,
  `admin_xmpp_id` varchar(300) DEFAULT NULL,
  `admin_can_chat` tinyint(1) DEFAULT NULL,
  `admin_chat_priority` int(11) NOT NULL DEFAULT 1,
  `admin_profile_image` mediumtext DEFAULT NULL,
  `admin_user_phone_no` varchar(20) DEFAULT NULL,
  `admin_user_password` varchar(200) DEFAULT NULL,
  `admin_user_salt` varchar(200) DEFAULT NULL,
  `admin_user_group_id` int(11) DEFAULT NULL,
  `admin_user_newslatter` int(11) DEFAULT NULL,
  `admin_user_status` int(11) DEFAULT NULL,
  `admin_user_order_noti_status` tinyint(2) NOT NULL DEFAULT 0,
  `admin_user_customer_noti_status` tinyint(2) NOT NULL,
  `admin_user_message_noti_status` tinyint(2) NOT NULL DEFAULT 0,
  `admin_user_order_last_id` int(11) DEFAULT NULL,
  `admin_user_customer_last_id` int(11) DEFAULT NULL,
  `admin_user_message_last_id` int(11) DEFAULT NULL,
  `admin_user_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `admin_user_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `admin_user`
--

TRUNCATE TABLE `admin_user`;
--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`admin_user_id`, `manufacturer_id`, `admin_user_firstname`, `admin_user_lastname`, `admin_user_emailid`, `admin_xmpp_id`, `admin_can_chat`, `admin_chat_priority`, `admin_profile_image`, `admin_user_phone_no`, `admin_user_password`, `admin_user_salt`, `admin_user_group_id`, `admin_user_newslatter`, `admin_user_status`, `admin_user_order_noti_status`, `admin_user_customer_noti_status`, `admin_user_message_noti_status`, `admin_user_order_last_id`, `admin_user_customer_last_id`, `admin_user_message_last_id`, `admin_user_created_date`, `admin_user_modified_date`) VALUES
(5, 7, 'GAUTAM', 'user', 'gautam@gmail.com', '', 1, 1, 'assets/admin_profile/Lighthouse1879496279_m.jpg', '9428854599', '1741a97777761a76e2b81c24d48307f7', '', 27, 0, 0, 0, 0, 0, 243, 168, 71, '2013-07-16 23:11:25', '2014-04-11 23:24:07'),
(32, 7, 'Jayesh', 'Gopani', 'jayeshgopani8005@gmail.com', '', 1, 0, 'assets/admin_profile/api-integration1134018498_m.png', '9714343870', '1741a97777761a76e2b81c24d48307f7', '', 27, 0, 0, 0, 0, 0, 0, 0, 0, '2014-11-24 07:57:57', '2017-12-08 03:49:28'),
(33, 7, 'Demo', 'User', 'demo_user@gmail.com', '', 0, 0, 'assets/admin_profile/vlcsnap-2017-07-22-22h52m57s7021497686662_m.png', '1234567890', '1741a97777761a76e2b81c24d48307f7', NULL, 27, 0, 0, 0, 0, 0, NULL, NULL, NULL, '2017-08-09 23:37:01', '2019-07-21 12:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_group`
--

CREATE TABLE `admin_user_group` (
  `admin_user_group_id` int(11) NOT NULL,
  `admin_user_group_name` varchar(200) NOT NULL,
  `admin_user_group_key` varchar(200) NOT NULL,
  `admin_user_group_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `admin_user_group_modified_date` timestamp NOT NULL DEFAULT '1970-01-01 05:41:11'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `admin_user_group`
--

TRUNCATE TABLE `admin_user_group`;
--
-- Dumping data for table `admin_user_group`
--

INSERT INTO `admin_user_group` (`admin_user_group_id`, `admin_user_group_name`, `admin_user_group_key`, `admin_user_group_created_date`, `admin_user_group_modified_date`) VALUES
(4, 'administrator', '', '2013-07-16 11:20:12', '2017-11-20 08:47:47'),
(22, 'user', '', '2013-07-17 11:50:53', '2017-11-20 08:47:47'),
(25, 'demo', '', '2013-07-18 08:27:14', '2017-11-20 08:47:47'),
(26, 'Super Admin', 'SUPER_ADMIN', '2013-07-19 11:24:29', '2017-11-20 08:47:47'),
(27, 'Power Admin', 'POWER_ADMIN', '2014-03-31 22:40:56', '2017-11-20 08:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_settings`
--

CREATE TABLE `admin_user_settings` (
  `admin_user_settings_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `aus_key` varchar(20) NOT NULL,
  `aus_id_field` int(11) NOT NULL,
  `aus_value_field` varchar(100) NOT NULL,
  `aus_status` tinyint(1) NOT NULL,
  `aus_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `aus_modified_date` timestamp NOT NULL DEFAULT '1970-01-01 05:41:11'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `admin_user_settings`
--

TRUNCATE TABLE `admin_user_settings`;
-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `config_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL DEFAULT 5,
  `config_key` varchar(50) NOT NULL,
  `config_value` mediumtext NOT NULL,
  `config_display_name` varchar(150) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `configuration`
--

TRUNCATE TABLE `configuration`;
--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`config_id`, `admin_user_id`, `config_key`, `config_value`, `config_display_name`, `created_date`, `modified_date`) VALUES
(1, 5, 'ADMIN_EMAIL', 'cloudwebs17@gmail.com', 'Admin configuration email', '2013-07-29 12:23:31', '2017-11-20 15:14:00'),
(2, 5, 'LABOUR_CHARGE', '0', 'Labour Charge', '2013-07-31 05:30:26', '2017-11-20 15:14:00'),
(3, 5, 'COMPANY_PROFIT', '0', 'Company Profit', '2013-07-31 05:30:22', '2017-11-20 15:14:00'),
(4, 5, 'INFO_EMAIL', 'info@cloudwebs17.com', 'EMAIL_ACCOUNT', '2013-07-29 12:03:18', '2017-11-20 15:14:00'),
(5, 5, 'SUPPORT_EMAIL', 'support@cloudwebs17.com', 'EMAIL_ACCOUNT', '2013-07-18 08:43:52', '2017-11-20 15:14:00'),
(6, 5, 'FILTER_DIFF_PRICE', '5000', 'Filter Price Difference on front side', '2013-07-25 07:23:38', '2017-11-20 15:14:00'),
(7, 5, 'SEO_GENERATOR', 'info@cloudwebs17.com', 'SEO Generator', '2013-08-09 03:11:08', '2017-11-20 15:14:00'),
(14, 5, 'ORDER_ERROR', 'Order Error', 'ORDER_RETURN_REASON', '2013-08-09 05:13:42', '2017-11-20 15:14:00'),
(15, 5, 'ORD_RET_OTHER_REASON', 'Other, please supply details', 'ORDER_RETURN_REASON', '2013-08-09 05:14:50', '2017-11-20 15:14:00'),
(16, 5, 'ORD_RET_REC_WRONG_ITEM', 'Received Wrong Item', 'ORDER_RETURN_REASON', '2013-08-09 05:15:43', '2017-11-20 15:14:00'),
(17, 5, 'ORDER_LAST_NOTIFIED_ID', '0', 'Order LAST NOTIFIED ID', '2013-09-26 22:57:11', '2017-11-20 15:14:00'),
(18, 5, 'CUSTOMER_LAST_NOTIFIED_ID', '0', 'CUSTOMER LAST NOTIFIED ID', '2013-09-26 22:58:49', '2017-11-20 15:14:00'),
(19, 5, 'PRIVATE_MSG_LAST_NOTIFIED_ID', '0', 'PRIVATE MESSAGE LAST NOTIFIED ID', '2013-09-26 22:59:25', '2017-11-20 15:14:00'),
(20, 5, 'SALES_EMAIL', 'sales@cloudwebs17.com', 'Sales email', '2013-10-21 23:45:04', '2017-11-20 15:14:00'),
(21, 5, 'TOLL_FREE_NO', '022 2648 4586', 'Toll Free Number', '2013-10-28 02:30:31', '2017-11-20 15:14:00'),
(22, 5, 'CONTACT_EMAIL', 'info@cloudwebs17.com', 'Contact Email', '2014-01-22 09:56:30', '2017-11-20 15:14:00'),
(23, 5, 'PARTNER_EMAIL', 'partner@cloudwebs17.com', 'EMAIL_ACCOUNT', '2014-04-23 04:30:08', '2017-11-20 15:14:00'),
(63, 33, 'INVOICE_TITLE', 'Bansi Stationery', 'Name of the company', '2017-08-10 05:19:42', '2017-11-20 15:14:00'),
(64, 33, 'ADDRESS', 'Shop No. D-2, Dhanmora Complex,<br>\nDhanmora Char Rasta, Katargam<br>\nSurat-395004, Gujarat.', 'Company Address', '2017-08-10 05:20:42', '2017-11-20 15:14:00'),
(65, 33, 'CONTACT_NO', '9824802874', 'Contact Number', '2017-08-10 05:21:12', '2017-11-20 15:14:00'),
(66, 33, 'EMAIL_ADDRESS', 'ratubhailakum@gmail.com', 'Emali id', '2017-08-10 05:21:35', '2017-11-20 15:14:00'),
(67, 33, 'VAT_TIN', '24221502930', 'VAT - TIN Number', '2017-08-10 05:22:13', '2017-11-20 15:14:00'),
(68, 33, 'PAN_NO', 'ACUPN2794E', 'Pan card number', '2017-08-10 05:22:38', '2017-11-20 15:14:00'),
(69, 33, 'PAYMENT_DETAIL', 'FOR PAYMENT: YES BANK A/C NO.: 03998590000315, MICR CODE: 395532005, IFSC CODE: YESB0000399', 'Paymentn or Bank Details', '2017-08-10 05:24:54', '2017-11-20 15:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `email_campaign`
--

CREATE TABLE `email_campaign` (
  `email_campaign_id` int(11) NOT NULL,
  `ec_subject` varchar(300) NOT NULL,
  `ec_key` varchar(20) NOT NULL,
  `ec_message_body` mediumtext NOT NULL,
  `ec_description` mediumtext NOT NULL,
  `ec_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ec_modified_date` timestamp NOT NULL DEFAULT '1970-01-01 05:41:11'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `email_campaign`
--

TRUNCATE TABLE `email_campaign`;
-- --------------------------------------------------------

--
-- Table structure for table `email_list`
--

CREATE TABLE `email_list` (
  `email_list_id` int(11) NOT NULL,
  `email_id` varchar(200) NOT NULL,
  `el_optout_level` tinyint(1) NOT NULL DEFAULT 1,
  `el_status` varchar(3) NOT NULL DEFAULT 'S',
  `el_reference_source` varchar(255) NOT NULL,
  `el_priority_level` int(11) NOT NULL,
  `el_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `el_modified_date` timestamp NOT NULL DEFAULT '1970-01-01 13:30:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `email_list`
--

TRUNCATE TABLE `email_list`;
-- --------------------------------------------------------

--
-- Table structure for table `email_send_history`
--

CREATE TABLE `email_send_history` (
  `email_send_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `email_campaign_id` int(11) NOT NULL,
  `es_from_emails` varchar(255) NOT NULL,
  `es_to_emails` mediumtext NOT NULL,
  `es_module_primary_id` mediumtext NOT NULL,
  `es_module_name` varchar(100) NOT NULL,
  `es_product_id` mediumtext DEFAULT NULL,
  `es_subject` mediumtext NOT NULL,
  `es_message` mediumtext NOT NULL,
  `es_ip_address` varchar(100) NOT NULL,
  `es_status` int(11) NOT NULL,
  `es_sent_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `email_send_history`
--

TRUNCATE TABLE `email_send_history`;
-- --------------------------------------------------------

--
-- Table structure for table `error_codes`
--

CREATE TABLE `error_codes` (
  `error_id` int(11) NOT NULL,
  `error_code` varchar(5) NOT NULL,
  `error_message` mediumtext NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `error_codes`
--

TRUNCATE TABLE `error_codes`;
--
-- Dumping data for table `error_codes`
--

INSERT INTO `error_codes` (`error_id`, `error_code`, `error_message`, `modified_date`) VALUES
(1, '01001', 'Email address is already exist, please try different email address.', '2013-07-29 23:53:29'),
(2, '01002', 'Your Account has been inactive. Please contact admin OR Please check your mailbox.', '2013-09-28 01:25:10'),
(3, '01003', 'You have entered wrong old password, please correct it.', '2013-07-15 13:00:00'),
(4, '01004', 'Invalid login credentials.', '2013-07-10 23:28:14'),
(5, '01005', 'Warning: Please check the form carefully for errors!', '2013-07-29 23:26:12'),
(6, '01006', 'Warning: Record is deleted', '0000-00-00 00:00:00'),
(7, '01007', 'Sorry! you don\'t have insert permission.', '2013-07-27 06:49:47'),
(8, '01008', 'Sorry! you don\'t have edit permission.', '2013-07-27 06:50:07'),
(9, '01009', 'Sorry! you don\'t have delete permission.', '2013-07-27 06:50:19'),
(10, '01010', 'Sorry! you don\'t have view permission.', '2013-07-27 06:50:37'),
(11, '01011', 'Your email address is subscribed.', '2013-08-16 23:41:02'),
(12, '01012', 'Your account has been created successfully, please check mail to verify your account.', '2013-09-06 23:24:36'),
(13, '01013', 'Invalid email or password combination.', '2013-10-29 01:20:29'),
(14, '01014', 'Password reset information is sent to your email address. Please check your mailbox.', '2013-09-15 14:09:08'),
(15, '01015', 'Sorry! You are not registered.', '2013-09-18 23:48:00'),
(16, '01016', 'You have successfully signed up with facebook. Your password details are sent to your email, please check your mailbox.', '2013-09-15 18:09:30'),
(17, '01017', 'We could not find information about this activation key. Please verify your activation email.', '2013-09-15 13:00:00'),
(18, '01018', 'Your activation link is already approved.', '2013-09-28 04:52:47'),
(19, '01019', 'Your account has been activated, please login to experience the features.', '2013-09-28 04:50:29'),
(20, '01020', 'You are logged in as guest user, please register first.', '2013-09-18 23:39:47'),
(21, '01021', 'Sorry you don\'t have permission on any page of admin, first seek permission from Super Admin', '2013-09-26 01:50:03'),
(22, '01022', 'Sorry you don\'t have any permission on this page.', '2013-09-26 03:37:30'),
(23, '01023', 'Sorry! you haven\'t permission to access this page.', '2013-09-26 05:47:18');

-- --------------------------------------------------------

--
-- Table structure for table `image_size`
--

CREATE TABLE `image_size` (
  `image_size_id` int(11) NOT NULL,
  `image_size_width` varchar(200) NOT NULL,
  `image_size_height` varchar(200) NOT NULL,
  `image_size_status` int(11) NOT NULL,
  `image_size_sort_order` int(11) NOT NULL,
  `image_size_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `image_size_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `image_size`
--

TRUNCATE TABLE `image_size`;
--
-- Dumping data for table `image_size`
--

INSERT INTO `image_size` (`image_size_id`, `image_size_width`, `image_size_height`, `image_size_status`, `image_size_sort_order`, `image_size_created_date`, `image_size_modified_date`) VALUES
(1, '16', '16', 0, 1, '2013-07-16 01:09:32', '2013-08-15 22:10:33'),
(2, '32', '32', 0, 2, '2013-07-16 01:16:52', '2013-07-17 22:19:54'),
(3, '64', '64', 0, 3, '2013-07-16 01:58:05', '2013-08-15 22:10:42'),
(4, '200', '200', 0, 4, '2013-07-16 02:54:18', '2013-08-15 22:10:46'),
(5, '1002', '250', 0, 15, '2013-08-15 22:10:26', '2013-08-22 21:40:55'),
(6, '900', '900', 0, 10, '2015-03-29 23:16:29', '1970-01-01 00:11:11'),
(7, '370', '400', 0, 5, '2015-04-14 00:41:47', '1970-01-01 00:11:11');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_master_specifier`
--

CREATE TABLE `inventory_master_specifier` (
  `inventory_master_specifier_id` int(11) NOT NULL,
  `inventory_type_id` int(11) NOT NULL,
  `ims_input_type` varchar(10) NOT NULL,
  `ims_tab_label` varchar(50) NOT NULL,
  `ims_fieldset_label` varchar(50) NOT NULL,
  `ims_input_label` varchar(50) NOT NULL,
  `ims_default_value` varchar(300) NOT NULL,
  `ims_input_validation` varchar(10) NOT NULL,
  `ims_is_use_in_search_filter` bit(1) NOT NULL,
  `ims_is_use_in_compare` bit(1) NOT NULL,
  `ims_sort_order` int(11) NOT NULL,
  `ims_status` tinyint(1) NOT NULL,
  `ims_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ims_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `inventory_master_specifier`
--

TRUNCATE TABLE `inventory_master_specifier`;
-- --------------------------------------------------------

--
-- Table structure for table `inventory_type`
--

CREATE TABLE `inventory_type` (
  `inventory_type_id` int(11) NOT NULL,
  `it_name` varchar(30) NOT NULL,
  `it_key` varchar(10) NOT NULL,
  `it_status` tinyint(1) NOT NULL,
  `it_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `it_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `inventory_type`
--

TRUNCATE TABLE `inventory_type`;
-- --------------------------------------------------------

--
-- Table structure for table `ip_locations`
--

CREATE TABLE `ip_locations` (
  `ip_locations_id` int(11) NOT NULL,
  `il_ip` varchar(50) NOT NULL,
  `il_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `il_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `ip_locations`
--

TRUNCATE TABLE `ip_locations`;
-- --------------------------------------------------------

--
-- Table structure for table `kacho_number`
--

CREATE TABLE `kacho_number` (
  `kacho_number_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `kn_sd_n` varchar(10) DEFAULT NULL,
  `kn_sd_v` varchar(10) DEFAULT NULL,
  `kn_sd1_n` varchar(10) DEFAULT NULL,
  `kn_sd1_v` varchar(10) DEFAULT NULL,
  `kn_g1_n` varchar(10) DEFAULT NULL,
  `kn_g1_v` varchar(10) DEFAULT NULL,
  `kn_g2_n` varchar(10) DEFAULT NULL,
  `kn_g2_v` varchar(10) DEFAULT NULL,
  `kn_u1_n` varchar(10) DEFAULT NULL,
  `kn_u1_v` varchar(10) DEFAULT NULL,
  `kn_u2_n` varchar(10) DEFAULT NULL,
  `kn_u2_v` varchar(10) DEFAULT NULL,
  `kn_u3_n` varchar(10) DEFAULT NULL,
  `kn_u3_v` varchar(10) DEFAULT NULL,
  `kn_palchu_n` varchar(10) DEFAULT NULL,
  `kn_palchu_v` varchar(10) DEFAULT NULL,
  `kn_ta_n` varchar(10) DEFAULT NULL,
  `kn_ta_v` varchar(10) DEFAULT NULL,
  `kn_mix_n` varchar(10) DEFAULT NULL,
  `kn_mix_v` varchar(10) DEFAULT NULL,
  `kn_extra_1` varchar(10) DEFAULT NULL,
  `kn_extra_2` varchar(10) DEFAULT NULL,
  `kn_extra_3` varchar(10) DEFAULT NULL,
  `kn_extra1_n` varchar(10) DEFAULT NULL,
  `kn_extra1_v` varchar(10) DEFAULT NULL,
  `kn_extra2_n` varchar(10) DEFAULT NULL,
  `kn_extra2_v` varchar(10) DEFAULT NULL,
  `kn_extra3_n` varchar(10) DEFAULT NULL,
  `kn_extra3_v` varchar(10) DEFAULT NULL,
  `kn_total_n` varchar(10) DEFAULT NULL,
  `kn_total_v` varchar(10) DEFAULT NULL,
  `kn_saij` varchar(10) DEFAULT NULL,
  `kn_takavari` varchar(10) DEFAULT NULL,
  `kn_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `kn_modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `kacho_number`
--

TRUNCATE TABLE `kacho_number`;
--
-- Dumping data for table `kacho_number`
--

INSERT INTO `kacho_number` (`kacho_number_id`, `kapan_id`, `kn_sd_n`, `kn_sd_v`, `kn_sd1_n`, `kn_sd1_v`, `kn_g1_n`, `kn_g1_v`, `kn_g2_n`, `kn_g2_v`, `kn_u1_n`, `kn_u1_v`, `kn_u2_n`, `kn_u2_v`, `kn_u3_n`, `kn_u3_v`, `kn_palchu_n`, `kn_palchu_v`, `kn_ta_n`, `kn_ta_v`, `kn_mix_n`, `kn_mix_v`, `kn_extra_1`, `kn_extra_2`, `kn_extra_3`, `kn_extra1_n`, `kn_extra1_v`, `kn_extra2_n`, `kn_extra2_v`, `kn_extra3_n`, `kn_extra3_v`, `kn_total_n`, `kn_total_v`, `kn_saij`, `kn_takavari`, `kn_created_date`, `kn_modified_date`) VALUES
(8, 13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0.00', '2018-01-23 07:13:44', '2018-01-31 10:28:48'),
(9, 14, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0.00', '2018-01-23 10:20:52', '2018-02-03 05:17:53'),
(25, 15, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0.00', '2018-01-27 13:38:36', '2018-01-30 04:06:01'),
(26, 16, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0.00', '2018-01-31 06:35:49', '2018-01-31 07:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `kapan`
--

CREATE TABLE `kapan` (
  `kapan_id` int(11) NOT NULL,
  `k_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `k_add_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `k_total` float DEFAULT NULL,
  `k_weight` float DEFAULT NULL,
  `k_date` timestamp NULL DEFAULT NULL,
  `k_day` tinyint(1) NOT NULL DEFAULT 1,
  `k_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `k_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `kapan`
--

TRUNCATE TABLE `kapan`;
--
-- Dumping data for table `kapan`
--

INSERT INTO `kapan` (`kapan_id`, `k_name`, `k_add_name`, `k_total`, `k_weight`, `k_date`, `k_day`, `k_created_date`, `k_modified_date`) VALUES
(13, '97.64', 'jayesh', 195.38, 97.64, '2018-01-10 00:00:00', 3, '2018-01-23 07:06:41', '2018-01-23 07:06:41'),
(14, '9', 'JAYESH', 155.05, 155.05, '2017-12-12 00:00:00', 2, '2018-01-23 10:17:20', '2018-01-23 10:17:20'),
(15, '12', 'JAYESHBHI', 34.66, 34.66, '2017-12-12 00:00:00', 2, '2018-01-27 13:36:58', '2018-01-27 13:37:33'),
(16, '33-B', '-', 378.18, 63.03, '2017-10-06 00:00:00', 5, '2018-01-31 06:33:39', '2018-01-31 06:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `kapan_jama`
--

CREATE TABLE `kapan_jama` (
  `kapan_jama_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `kj_start_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `kj_raph` varchar(11) DEFAULT NULL,
  `kj_total_kapan` varchar(11) DEFAULT NULL,
  `kj_kapan` varchar(11) DEFAULT NULL,
  `kj_nung` varchar(11) DEFAULT NULL,
  `kj_gol` varchar(11) DEFAULT NULL,
  `kj_saij` varchar(11) DEFAULT NULL,
  `kj_takavari` varchar(11) DEFAULT NULL,
  `kj_singal_1` varchar(11) DEFAULT NULL,
  `kj_singal_2` varchar(11) DEFAULT NULL,
  `kj_selection` varchar(11) DEFAULT NULL,
  `kj_choki_1` varchar(11) DEFAULT NULL,
  `kj_choki_2` varchar(11) DEFAULT NULL,
  `kj_palchu` varchar(11) DEFAULT NULL,
  `kj_uchu_gol` varchar(11) DEFAULT NULL,
  `kj_color_out` varchar(11) DEFAULT NULL,
  `kj_total_out` varchar(11) DEFAULT NULL,
  `kj_out` varchar(11) DEFAULT NULL,
  `kj_chur` varchar(11) DEFAULT NULL,
  `kj_ghat` varchar(11) DEFAULT NULL,
  `kj_end_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `kj_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `kj_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `kapan_jama`
--

TRUNCATE TABLE `kapan_jama`;
--
-- Dumping data for table `kapan_jama`
--

INSERT INTO `kapan_jama` (`kapan_jama_id`, `kapan_id`, `kj_start_date`, `kj_raph`, `kj_total_kapan`, `kj_kapan`, `kj_nung`, `kj_gol`, `kj_saij`, `kj_takavari`, `kj_singal_1`, `kj_singal_2`, `kj_selection`, `kj_choki_1`, `kj_choki_2`, `kj_palchu`, `kj_uchu_gol`, `kj_color_out`, `kj_total_out`, `kj_out`, `kj_chur`, `kj_ghat`, `kj_end_date`, `kj_created_date`, `kj_modified_date`) VALUES
(9, 13, '2018-01-31 07:20:47', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-31 07:20:47', '2018-01-23 07:06:41', '2018-01-31 10:28:48'),
(10, 14, '2018-01-30 04:38:01', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 04:38:01', '2018-01-23 10:17:20', '2018-02-03 05:17:53'),
(11, 15, '2018-01-27 13:36:58', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-27 13:36:58', '2018-01-27 13:36:58', '2018-01-30 04:06:01'),
(12, 16, '2018-01-31 06:33:39', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-31 06:33:39', '2018-01-31 06:33:39', '2018-01-31 07:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `kapan_jama_count`
--

CREATE TABLE `kapan_jama_count` (
  `kapan_jama_count_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `kjc_kapan` varchar(11) DEFAULT NULL,
  `kjc_nung` varchar(11) DEFAULT NULL,
  `kjc_gol` varchar(11) DEFAULT NULL,
  `kjc_singal_1` varchar(11) DEFAULT NULL,
  `kjc_singal_2` varchar(11) DEFAULT NULL,
  `kjc_selection` varchar(11) DEFAULT NULL,
  `kjc_choki_1` varchar(11) DEFAULT NULL,
  `kjc_choki_2` varchar(11) DEFAULT NULL,
  `kjc_palchu` varchar(11) DEFAULT NULL,
  `kjc_uchu_gol` varchar(11) DEFAULT NULL,
  `kjc_color_out` varchar(11) DEFAULT NULL,
  `kjc_total_out` varchar(11) DEFAULT NULL,
  `kjc_out` varchar(11) DEFAULT NULL,
  `kjc_chur` varchar(11) DEFAULT NULL,
  `kjc_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `kjc_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `kapan_jama_count`
--

TRUNCATE TABLE `kapan_jama_count`;
-- --------------------------------------------------------

--
-- Table structure for table `lagadh`
--

CREATE TABLE `lagadh` (
  `lagadh_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `l_bannavvu` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_singal_1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_singal_2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_bot` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_choki_1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_choki_2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_palchu` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_unchu_gol` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_color_out` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_aankhu_out` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_chur` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_soing_ghat` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_chapka_ghat` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_vadharani_ghat` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_1` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_2` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_3` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_total` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_aavel_vajan` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_mangel_vajan` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_veriation` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_5` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_5` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_txt_6` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_extra_val_6` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_ele` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_eight` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_six` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_four` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_two` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_plus_ziro` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_min_ziro` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_extra_val_4` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_extra_val_5` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_extra_val_6` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_ele` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_eight` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_six` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_four` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_two` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_plus_ziro` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_t_min_ziro` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_charni_total` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `l_note` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `l_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `l_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `lagadh`
--

TRUNCATE TABLE `lagadh`;
--
-- Dumping data for table `lagadh`
--

INSERT INTO `lagadh` (`lagadh_id`, `kapan_id`, `l_bannavvu`, `l_singal_1`, `l_singal_2`, `l_bot`, `l_choki_1`, `l_choki_2`, `l_palchu`, `l_unchu_gol`, `l_color_out`, `l_aankhu_out`, `l_chur`, `l_soing_ghat`, `l_chapka_ghat`, `l_vadharani_ghat`, `l_extra_txt_1`, `l_extra_val_1`, `l_extra_txt_2`, `l_extra_val_2`, `l_extra_txt_3`, `l_extra_val_3`, `l_total`, `l_aavel_vajan`, `l_mangel_vajan`, `l_veriation`, `l_extra_txt_4`, `l_extra_val_4`, `l_extra_txt_5`, `l_extra_val_5`, `l_extra_txt_6`, `l_extra_val_6`, `l_plus_ele`, `l_plus_eight`, `l_plus_six`, `l_plus_four`, `l_plus_two`, `l_plus_ziro`, `l_min_ziro`, `l_t_extra_val_4`, `l_t_extra_val_5`, `l_t_extra_val_6`, `l_t_plus_ele`, `l_t_plus_eight`, `l_t_plus_six`, `l_t_plus_four`, `l_t_plus_two`, `l_t_plus_ziro`, `l_t_min_ziro`, `l_charni_total`, `l_note`, `l_created_date`, `l_modified_date`) VALUES
(2, 13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '2018-01-23 08:07:58', '2018-01-23 08:07:58'),
(3, 14, '6', '6', '6', '6', '6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '30', '66', '56', '10', '', '', '8', '88', '', '', '', '', '', '', '', '', '', '78', '77', '', '', '', '', '', '', '', '', '', '', '2018-01-24 04:24:54', '2018-01-24 04:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `languages_id` int(11) NOT NULL,
  `l_name` varchar(100) NOT NULL,
  `l_key` varchar(10) NOT NULL,
  `l_sort_order` int(11) NOT NULL,
  `l_status` tinyint(1) NOT NULL,
  `l_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `l_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `languages`
--

TRUNCATE TABLE `languages`;
--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`languages_id`, `l_name`, `l_key`, `l_sort_order`, `l_status`, `l_created_date`, `l_modified_date`) VALUES
(3, 'aaa', 'AA', 2, 0, '2014-01-17 03:53:14', '2017-11-20 09:54:29'),
(4, 'English', 'EN_US', 1, 0, '2014-01-22 06:53:10', '2017-11-20 09:54:29'),
(5, 'Hindi', 'HI', 4, 0, '2014-01-22 06:53:44', '2017-11-20 09:54:29'),
(6, 'Gujarati', 'GU', 7, 0, '2014-01-22 06:54:17', '2017-11-20 09:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `l_group_key` varchar(50) NOT NULL,
  `l_description` varchar(1000) NOT NULL,
  `l_created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `log`
--

TRUNCATE TABLE `log`;
-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `logins_id` int(11) NOT NULL,
  `cust_admin_user_id` int(11) NOT NULL,
  `sessions_id` int(11) NOT NULL DEFAULT 0,
  `session_id` varchar(128) DEFAULT NULL,
  `l_user_type` varchar(1) NOT NULL,
  `l_user_agent` varchar(1000) NOT NULL,
  `l_user_device` varchar(100) NOT NULL,
  `l_session_status` tinyint(1) NOT NULL DEFAULT 1,
  `l_reason_key` tinyint(4) DEFAULT NULL,
  `l_created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `l_modified_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `logins`
--

TRUNCATE TABLE `logins`;
--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`logins_id`, `cust_admin_user_id`, `sessions_id`, `session_id`, `l_user_type`, `l_user_agent`, `l_user_device`, `l_session_status`, `l_reason_key`, `l_created_time`, `l_modified_time`) VALUES
(1, 32, 6, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 07:00:39', '2018-01-23 07:01:54'),
(2, 32, 7, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 07:02:11', '2018-01-23 07:02:54'),
(3, 32, 8, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 07:03:02', '2018-01-23 07:22:03'),
(4, 32, 9, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 07:22:16', '2018-01-23 09:20:08'),
(5, 5, 3, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 08:06:18', '2018-01-23 08:08:22'),
(6, 32, 11, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 09:20:19', '2018-01-23 09:50:41'),
(7, 5, 10, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 09:22:50', '2018-01-23 10:03:18'),
(8, 32, 12, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 09:50:51', '2018-01-23 10:15:55'),
(9, 32, 14, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 10:16:11', '2018-01-23 10:16:11'),
(10, 5, 15, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-23 14:13:26', '2018-01-23 14:13:31'),
(11, 32, 19, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 14:19:04', '2018-01-23 14:19:04'),
(12, 5, 16, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-24 03:58:13', '2018-01-24 04:31:28'),
(13, 32, 21, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-25 03:50:12', '2018-01-25 03:50:12'),
(14, 5, 22, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-26 05:43:50', '2018-01-26 05:55:13'),
(15, 32, 24, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 0, 1, '2018-01-26 05:59:13', '2018-01-27 13:34:58'),
(16, 32, 25, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-27 13:35:36', '2018-01-27 13:35:36'),
(17, 32, 26, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-28 05:46:38', '2018-01-28 05:46:38'),
(18, 32, 27, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-29 03:03:49', '2018-01-29 03:03:49'),
(19, 5, 23, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-29 04:02:14', '2018-01-29 06:43:07'),
(20, 32, 28, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-29 05:51:35', '2018-01-29 05:51:35'),
(21, 5, 29, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-29 10:04:29', '2018-01-29 10:16:53'),
(22, 5, 30, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-29 15:53:34', '2018-01-29 16:43:06'),
(23, 32, 32, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-30 03:20:22', '2018-01-30 03:20:22'),
(24, 5, 31, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-30 03:41:17', '2018-01-30 04:45:19'),
(25, 32, 34, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-30 08:53:09', '2018-01-30 08:53:09'),
(26, 5, 33, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-30 09:11:41', '2018-01-30 09:16:16'),
(27, 5, 35, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-30 15:42:21', '2018-01-30 15:43:00'),
(28, 5, 36, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-31 05:01:55', '2018-01-31 07:09:00'),
(29, 5, 37, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 0, 1, '2018-01-31 07:10:05', '2018-01-31 10:35:17'),
(30, 32, 39, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-02-03 05:13:48', '2018-02-03 05:13:48'),
(31, 5, 44, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '', 0, 1, '2019-06-04 03:39:53', '2019-06-04 03:41:31'),
(32, 5, 45, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '', 0, 1, '2019-06-04 03:41:36', '2019-06-04 03:42:13'),
(33, 5, 55, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '', 1, NULL, '2019-07-02 10:30:46', '2019-07-02 10:30:46'),
(34, 5, 64, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 0, 1, '2019-07-21 12:17:08', '2019-07-21 12:19:01'),
(35, 5, 65, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 0, 1, '2019-07-21 12:19:20', '2019-07-21 12:30:48'),
(36, 33, 66, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 0, 1, '2019-07-21 12:24:17', '2019-07-21 12:25:31'),
(37, 33, 67, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 0, 1, '2019-07-21 12:25:43', '2019-07-21 12:26:36'),
(38, 33, 68, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:26:49', '2019-07-21 12:26:49'),
(39, 33, 69, NULL, 'A', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', '', 0, 1, '2019-07-21 12:27:39', '2019-07-21 12:27:52'),
(40, 5, 103, NULL, 'A', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', '', 1, NULL, '2019-08-01 12:39:27', '2019-08-01 12:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `login_ip`
--

CREATE TABLE `login_ip` (
  `login_ip_id` int(11) NOT NULL,
  `cust_admin_user_id` int(11) NOT NULL,
  `sessions_id` int(11) NOT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `li_user_type` varchar(1) DEFAULT NULL,
  `li_ip` varchar(50) DEFAULT NULL,
  `li_user_agent` varchar(255) DEFAULT NULL,
  `li_user_device` varchar(100) DEFAULT NULL,
  `li_relevancy_level` tinyint(1) NOT NULL DEFAULT 1,
  `li_original_user_response` tinyint(1) DEFAULT NULL,
  `li_created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `li_modified_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `login_ip`
--

TRUNCATE TABLE `login_ip`;
--
-- Dumping data for table `login_ip`
--

INSERT INTO `login_ip` (`login_ip_id`, `cust_admin_user_id`, `sessions_id`, `session_id`, `li_user_type`, `li_ip`, `li_user_agent`, `li_user_device`, `li_relevancy_level`, `li_original_user_response`, `li_created_time`, `li_modified_time`) VALUES
(1, 32, 6, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 07:00:39', '2018-01-23 07:00:39'),
(2, 32, 7, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 07:02:11', '2018-01-23 07:02:11'),
(3, 32, 8, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 07:03:02', '2018-01-23 07:03:02'),
(4, 32, 9, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 07:22:16', '2018-01-23 07:22:16'),
(5, 5, 3, NULL, 'A', '49.34.112.118', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 08:06:18', '2018-01-23 08:06:18'),
(6, 32, 11, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 09:20:19', '2018-01-23 09:20:19'),
(7, 5, 10, NULL, 'A', '49.34.112.118', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 09:22:50', '2018-01-23 09:22:50'),
(8, 32, 12, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 09:50:51', '2018-01-23 09:50:51'),
(9, 32, 14, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 10:16:11', '2018-01-23 10:16:11'),
(10, 5, 15, NULL, 'A', '219.91.239.63', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 14:13:26', '2018-01-23 14:13:26'),
(11, 32, 19, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-23 14:19:04', '2018-01-23 14:19:04'),
(12, 5, 16, NULL, 'A', '219.91.237.54', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-24 03:58:13', '2018-01-24 03:58:13'),
(13, 32, 21, NULL, 'A', '42.106.30.215', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-25 03:50:12', '2018-01-25 03:50:12'),
(14, 5, 22, NULL, 'A', '219.91.237.31', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-26 05:43:50', '2018-01-26 05:43:50'),
(15, 32, 24, NULL, 'A', '42.106.17.219', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-26 05:59:13', '2018-01-26 05:59:13'),
(16, 32, 25, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-27 13:35:36', '2018-01-27 13:35:36'),
(17, 32, 26, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-28 05:46:38', '2018-01-28 05:46:38'),
(18, 32, 27, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-29 03:03:49', '2018-01-29 03:03:49'),
(19, 5, 23, NULL, 'A', '49.34.140.8', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-29 04:02:14', '2018-01-29 04:02:14'),
(20, 32, 28, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-29 05:51:35', '2018-01-29 05:51:35'),
(21, 5, 29, NULL, 'A', '123.201.227.155', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-29 10:04:29', '2018-01-29 10:04:29'),
(22, 5, 30, NULL, 'A', '219.91.236.238', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-29 15:53:35', '2018-01-29 15:53:35'),
(23, 32, 32, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-30 03:20:22', '2018-01-30 03:20:22'),
(24, 5, 31, NULL, 'A', '219.91.238.179', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-30 03:41:17', '2018-01-30 03:41:17'),
(25, 32, 34, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-01-30 08:53:09', '2018-01-30 08:53:09'),
(26, 5, 33, NULL, 'A', '123.201.226.174', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-30 09:11:41', '2018-01-30 09:11:41'),
(27, 5, 35, NULL, 'A', '123.201.225.245', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-30 15:42:21', '2018-01-30 15:42:21'),
(28, 5, 36, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-31 05:01:55', '2018-01-31 05:01:55'),
(29, 5, 37, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '', 1, NULL, '2018-01-31 07:10:05', '2018-01-31 07:10:05'),
(30, 32, 39, NULL, 'A', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', '', 1, NULL, '2018-02-03 05:13:48', '2018-02-03 05:13:48'),
(31, 5, 44, NULL, 'A', '157.32.43.117', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '', 1, NULL, '2019-06-04 03:39:53', '2019-06-04 03:39:53'),
(32, 5, 45, NULL, 'A', '157.32.209.195', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', '', 1, NULL, '2019-06-04 03:41:36', '2019-06-04 03:41:36'),
(33, 5, 55, NULL, 'A', '49.36.1.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '', 1, NULL, '2019-07-02 10:30:46', '2019-07-02 10:30:46'),
(34, 5, 64, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:17:08', '2019-07-21 12:17:08'),
(35, 5, 65, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:19:20', '2019-07-21 12:19:20'),
(36, 33, 66, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:24:17', '2019-07-21 12:24:17'),
(37, 33, 67, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:25:43', '2019-07-21 12:25:43'),
(38, 33, 68, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', '', 1, NULL, '2019-07-21 12:26:49', '2019-07-21 12:26:49'),
(39, 33, 69, NULL, 'A', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', '', 1, NULL, '2019-07-21 12:27:39', '2019-07-21 12:27:39'),
(40, 5, 103, NULL, 'A', '49.36.15.116', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', '', 1, NULL, '2019-08-01 12:39:27', '2019-08-01 12:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `manufacturer_name` varchar(100) NOT NULL,
  `manufacturer_key` varchar(100) NOT NULL,
  `manufacturer_cctld` varchar(100) NOT NULL,
  `manufacturer_email_id` varchar(255) NOT NULL,
  `manufacturer_xmpp_id` varchar(255) NOT NULL,
  `manufacturer_image` mediumtext DEFAULT NULL,
  `image_size_id` int(11) NOT NULL,
  `manufacturer_status` int(11) NOT NULL,
  `custom_page_title` varchar(200) NOT NULL,
  `meta_keyword` mediumtext DEFAULT NULL,
  `meta_description` mediumtext DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `robots` int(11) DEFAULT NULL,
  `content_rights` varchar(200) DEFAULT NULL,
  `manufacturer_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `manufacturer_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `manufacturer`
--

TRUNCATE TABLE `manufacturer`;
--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `manufacturer_name`, `manufacturer_key`, `manufacturer_cctld`, `manufacturer_email_id`, `manufacturer_xmpp_id`, `manufacturer_image`, `image_size_id`, `manufacturer_status`, `custom_page_title`, `meta_keyword`, `meta_description`, `author`, `robots`, `content_rights`, `manufacturer_created_date`, `manufacturer_modified_date`) VALUES
(7, 'English', 'EN_US', '', '', '', 'assets/manufacturer/login_logo1965707144_m.png', 0, 0, 'HSquare', '', '', '', 2, '', '2013-09-29 18:15:17', '2014-11-24 07:54:37'),
(8, 'Hindi', 'HI', '', 'test@gmail.com', '', 'assets/manufacturer/login_logo1965707144_m.png', 0, 0, '', NULL, NULL, NULL, NULL, NULL, '2015-01-02 13:06:03', '1970-01-01 05:41:11'),
(9, 'Gujarati', 'GU', '', '', '', 'assets/manufacturer/login_logo1965707144_m.png', 0, 0, 'en-US', 'en-US', 'en-US', '', 2, '', '2013-09-29 18:15:17', '2015-03-26 08:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `module_manager`
--

CREATE TABLE `module_manager` (
  `module_manager_id` int(11) NOT NULL,
  `module_manager_title` varchar(255) NOT NULL,
  `module_manager_table_name` varchar(100) NOT NULL,
  `module_manager_field_name` varchar(100) NOT NULL,
  `module_manager_primary_id` varchar(100) NOT NULL,
  `position_id` int(11) NOT NULL,
  `module_manager_title_show_hide` int(11) NOT NULL,
  `module_manager_serialize_menu` mediumtext NOT NULL,
  `module_manager_description` mediumtext NOT NULL,
  `module_manager_css` mediumtext NOT NULL,
  `module_manager_sort_order` int(11) NOT NULL,
  `module_manager_status` tinyint(1) NOT NULL,
  `module_manager_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `module_manager_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `module_manager`
--

TRUNCATE TABLE `module_manager`;
-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `admin_menu_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `permission_add` tinyint(1) NOT NULL DEFAULT 1,
  `permission_edit` tinyint(1) NOT NULL DEFAULT 1,
  `permission_delete` tinyint(1) NOT NULL DEFAULT 1,
  `permission_view` tinyint(1) NOT NULL DEFAULT 1,
  `permission_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `permission_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `permission`
--

TRUNCATE TABLE `permission`;
--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `admin_menu_id`, `admin_user_id`, `permission_add`, `permission_edit`, `permission_delete`, `permission_view`, `permission_created_date`, `permission_modified_date`) VALUES
(832, 2, 5, 0, 0, 0, 0, '2013-07-26 20:44:36', '2014-07-24 05:19:22'),
(837, 3, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-07-24 05:19:22'),
(838, 4, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-07-24 05:19:22'),
(839, 6, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-05-21 20:25:01'),
(840, 7, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-05-21 20:25:01'),
(844, 24, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2015-04-23 04:58:04'),
(845, 29, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-05-21 20:25:02'),
(847, 28, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-05-21 20:25:02'),
(848, 31, 5, 0, 0, 0, 0, '2013-07-26 22:47:39', '2014-05-21 20:25:02'),
(854, 39, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-05-21 20:25:02'),
(855, 40, 5, 1, 1, 1, 1, '2013-07-26 22:47:40', '2017-07-18 10:19:38'),
(856, 41, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-05-21 20:25:02'),
(857, 42, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-05-21 20:25:02'),
(858, 43, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-05-21 20:25:02'),
(866, 52, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-07-24 05:19:22'),
(867, 53, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-07-24 05:19:22'),
(868, 54, 5, 0, 0, 0, 0, '2013-07-26 22:47:40', '2014-07-24 05:19:22'),
(869, 55, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:22'),
(870, 56, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:22'),
(871, 57, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:22'),
(872, 58, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2017-07-18 10:20:22'),
(873, 59, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:13'),
(874, 60, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:17'),
(875, 61, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:22'),
(876, 62, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:23'),
(877, 63, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:26'),
(878, 71, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:23'),
(879, 65, 5, 0, 0, 0, 0, '2013-07-26 22:47:41', '2014-07-24 05:19:23'),
(880, 66, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:36'),
(881, 67, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:39'),
(882, 68, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:41'),
(883, 69, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:42'),
(884, 70, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:43'),
(885, 72, 5, 1, 1, 1, 1, '2013-07-26 22:47:41', '2017-07-18 10:20:46'),
(887, 74, 5, 1, 1, 1, 1, '2013-07-26 22:47:42', '2017-07-18 10:20:49'),
(894, 81, 5, 0, 0, 0, 0, '2013-07-26 22:47:42', '2017-07-18 10:21:18'),
(895, 82, 5, 1, 1, 1, 1, '2013-07-26 22:47:42', '2017-07-18 10:21:08'),
(896, 83, 5, 1, 1, 1, 1, '2013-07-26 22:47:42', '2017-07-18 10:21:09'),
(897, 84, 5, 0, 0, 0, 0, '2013-07-26 22:47:42', '2017-07-18 10:21:16'),
(908, 95, 5, 0, 0, 0, 0, '2013-07-26 22:47:43', '2014-05-21 20:25:04'),
(1443, 105, 5, 1, 1, 1, 1, '2013-09-13 12:43:18', '2017-07-18 10:20:44'),
(1444, 107, 5, 0, 0, 0, 0, '2013-09-18 16:34:13', '2014-05-21 20:25:02'),
(1445, 106, 5, 0, 0, 0, 0, '2013-09-18 16:34:21', '2014-05-21 20:25:02'),
(2168, 114, 5, 1, 1, 1, 1, '2013-09-26 13:34:22', '2017-07-18 10:20:11'),
(2169, 115, 5, 0, 0, 0, 0, '2013-09-27 18:30:00', '2014-07-24 05:19:23'),
(2228, 117, 5, 1, 1, 1, 1, '2013-12-04 14:09:25', '2017-07-18 10:20:12'),
(2233, 118, 5, 0, 0, 0, 0, '2013-12-09 13:18:37', '2014-07-24 05:19:22'),
(2254, 137, 5, 0, 0, 0, 0, '2013-12-27 14:48:21', '2014-05-21 20:25:04'),
(2270, 146, 5, 0, 0, 0, 0, '2014-01-16 15:50:05', '2014-05-21 20:25:02'),
(2298, 153, 5, 0, 0, 0, 0, '2014-05-01 14:39:35', '2014-05-21 20:25:05'),
(2406, 136, 5, 0, 0, 0, 0, '2014-05-21 20:24:29', '2017-07-18 10:22:06'),
(3424, 157, 5, 0, 0, 0, 0, '2014-08-02 06:57:16', '1970-01-01 05:41:11'),
(3453, 159, 5, 0, 0, 0, 0, '2015-02-25 08:31:08', '1970-01-01 05:41:11'),
(3454, 160, 5, 0, 0, 0, 0, '2015-02-27 07:44:53', '1970-01-01 05:41:11'),
(3465, 168, 5, 1, 1, 1, 1, '2015-04-28 06:27:11', '2017-07-18 10:20:45'),
(3466, 169, 5, 0, 0, 0, 0, '2015-05-05 12:52:55', '1970-01-01 05:41:11'),
(3467, 2, 33, 0, 0, 0, 0, '2017-08-09 18:09:47', '1970-01-01 05:41:11'),
(3468, 118, 33, 0, 0, 0, 0, '2017-08-09 18:09:51', '1970-01-01 05:41:11'),
(3469, 7, 33, 0, 0, 0, 0, '2017-08-09 18:09:57', '1970-01-01 05:41:11'),
(3470, 24, 33, 0, 0, 0, 0, '2017-08-09 18:09:59', '1970-01-01 05:41:11'),
(3471, 62, 33, 0, 0, 0, 0, '2017-08-09 18:10:17', '1970-01-01 05:41:11'),
(3472, 171, 5, 0, 0, 0, 0, '2017-11-20 10:44:07', '1970-01-01 05:41:11'),
(3473, 172, 5, 0, 0, 0, 0, '2017-11-20 10:44:08', '1970-01-01 05:41:11'),
(3474, 173, 5, 0, 0, 0, 0, '2017-12-01 05:45:47', '1970-01-01 05:41:11'),
(3475, 2, 32, 0, 0, 0, 0, '2017-12-08 03:51:16', '1970-01-01 05:41:11'),
(3476, 118, 32, 0, 0, 0, 0, '2017-12-08 03:51:19', '1970-01-01 05:41:11'),
(3477, 171, 32, 0, 0, 0, 0, '2017-12-08 03:51:21', '1970-01-01 05:41:11'),
(3478, 172, 32, 0, 0, 0, 0, '2017-12-08 03:51:23', '1970-01-01 05:41:11'),
(3479, 173, 32, 0, 0, 0, 0, '2017-12-08 03:51:24', '1970-01-01 05:41:11'),
(3480, 171, 33, 0, 0, 0, 0, '2017-12-25 09:19:37', '1970-01-01 05:41:11'),
(3481, 172, 33, 0, 0, 0, 0, '2017-12-25 09:19:38', '1970-01-01 05:41:11'),
(3482, 173, 33, 0, 0, 0, 0, '2017-12-25 09:19:40', '1970-01-01 05:41:11'),
(3483, 171, 33, 1, 1, 1, 1, '2017-12-25 09:20:22', '1970-01-01 05:41:11'),
(3484, 172, 33, 1, 1, 1, 1, '2017-12-25 09:20:23', '1970-01-01 05:41:11'),
(3485, 173, 33, 1, 1, 1, 1, '2017-12-25 09:20:25', '1970-01-01 05:41:11'),
(3486, 174, 32, 0, 0, 0, 0, '2018-01-23 08:07:33', '2018-01-23 08:07:33'),
(3487, 174, 5, 0, 0, 0, 0, '2018-01-23 08:07:36', '2018-01-23 08:07:36'),
(3488, 174, 33, 0, 0, 0, 0, '2019-07-21 12:24:42', '2019-07-21 12:24:42'),
(3489, 2, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3490, 3, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3491, 4, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3492, 118, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3493, 171, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3494, 172, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3495, 173, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3496, 174, 33, 1, 1, 0, 1, '2019-07-21 12:26:12', '2019-07-21 12:26:12'),
(3497, 2, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3498, 3, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3499, 4, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3500, 118, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3501, 171, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3502, 172, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3503, 173, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3504, 174, 33, 1, 1, 1, 1, '2019-07-21 12:26:14', '2019-07-21 12:26:14'),
(3505, 2, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3506, 3, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3507, 4, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3508, 118, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3509, 171, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3510, 172, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3511, 173, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3512, 174, 32, 1, 1, 0, 1, '2019-07-21 12:28:00', '2019-07-21 12:28:00'),
(3513, 2, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3514, 3, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3515, 4, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3516, 118, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3517, 171, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3518, 172, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3519, 173, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3520, 174, 32, 1, 1, 1, 1, '2019-07-21 12:28:01', '2019-07-21 12:28:01'),
(3521, 2, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3522, 3, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3523, 4, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3524, 118, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3525, 171, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3526, 172, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3527, 173, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3528, 174, 32, 1, 1, 0, 1, '2019-07-21 12:28:21', '2019-07-21 12:28:21'),
(3529, 2, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3530, 3, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3531, 4, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3532, 118, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3533, 171, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3534, 172, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3535, 173, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24'),
(3536, 174, 32, 1, 1, 1, 1, '2019-07-21 12:28:24', '2019-07-21 12:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `polish`
--

CREATE TABLE `polish` (
  `polish_id` int(11) NOT NULL,
  `p_no` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `p_lot_start_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `p_nung` varchar(11) DEFAULT NULL,
  `p_weight` varchar(10) DEFAULT NULL,
  `p_pyority` varchar(11) DEFAULT NULL,
  `p_charni` varchar(10) DEFAULT NULL,
  `p_4p_ghat_nung` varchar(11) DEFAULT NULL,
  `p_4p_weight` varchar(11) DEFAULT NULL,
  `p_4p_t_k` varchar(11) DEFAULT NULL,
  `p_4p_takavari` varchar(10) DEFAULT NULL,
  `p_4p_avl_takavari` varchar(10) DEFAULT NULL,
  `p_4p_ghat_variation` varchar(11) DEFAULT NULL,
  `p_4p_name` varchar(25) DEFAULT NULL,
  `p_tbl_nung` varchar(11) DEFAULT NULL,
  `p_tbl_weight` varchar(10) DEFAULT NULL,
  `p_tbl_t_k` varchar(11) DEFAULT NULL,
  `p_tbl_takavri` varchar(10) DEFAULT NULL,
  `p_tly_nung` varchar(11) DEFAULT NULL,
  `p_tly_weight` varchar(10) DEFAULT NULL,
  `p_tly_t_k` varchar(11) DEFAULT NULL,
  `p_tly_takavari` varchar(10) DEFAULT NULL,
  `p_tly_sarni_1` varchar(10) DEFAULT NULL,
  `p_tly_sarni_2` varchar(10) DEFAULT NULL,
  `p_m_nung` varchar(11) DEFAULT NULL,
  `p_m_weight` varchar(10) DEFAULT NULL,
  `p_m_t_k` varchar(11) DEFAULT NULL,
  `p_m_takavari` varchar(10) DEFAULT NULL,
  `p_tk_nung` varchar(11) DEFAULT NULL,
  `p_tk_weight` varchar(10) DEFAULT NULL,
  `p_tk_t_k` varchar(11) DEFAULT NULL,
  `p_tk_takavari` varchar(10) DEFAULT NULL,
  `p_lot_last_date` datetime DEFAULT NULL,
  `p_variation` varchar(11) DEFAULT NULL,
  `pc_tbl_tk` varchar(10) DEFAULT NULL,
  `pc_tly_tk` varchar(10) DEFAULT NULL,
  `pc_m_tk` varchar(10) DEFAULT NULL,
  `pc_tk_tk` varchar(10) DEFAULT NULL,
  `p_created_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `polish`
--

TRUNCATE TABLE `polish`;
--
-- Dumping data for table `polish`
--

INSERT INTO `polish` (`polish_id`, `p_no`, `kapan_id`, `p_lot_start_date`, `p_nung`, `p_weight`, `p_pyority`, `p_charni`, `p_4p_ghat_nung`, `p_4p_weight`, `p_4p_t_k`, `p_4p_takavari`, `p_4p_avl_takavari`, `p_4p_ghat_variation`, `p_4p_name`, `p_tbl_nung`, `p_tbl_weight`, `p_tbl_t_k`, `p_tbl_takavri`, `p_tly_nung`, `p_tly_weight`, `p_tly_t_k`, `p_tly_takavari`, `p_tly_sarni_1`, `p_tly_sarni_2`, `p_m_nung`, `p_m_weight`, `p_m_t_k`, `p_m_takavari`, `p_tk_nung`, `p_tk_weight`, `p_tk_t_k`, `p_tk_takavari`, `p_lot_last_date`, `p_variation`, `pc_tbl_tk`, `pc_tly_tk`, `pc_m_tk`, `pc_tk_tk`, `p_created_date`) VALUES
(1094, 1, 15, '2018-01-31 05:10:56', '5', '7', '8', '7', '6', '5', '4', '5', '6', '87', '8', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1095, 2, 15, '2018-01-31 05:11:01', '655', '5', '4', '3', '3', '4', '6', '7', '8', '8', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1096, 3, 15, '2018-01-31 05:11:05', '9', '8', '7', '4', '3', '4', '6', '8', '9', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1097, 4, 15, '2018-01-31 05:11:08', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1098, 5, 15, '2018-01-31 05:11:11', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1099, 6, 15, '2018-01-31 05:11:14', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1100, 7, 15, '2018-01-31 05:11:17', '7', '6', '4', '2', '2', '4', '6', '8', '98', '8', '6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1101, 8, 15, '2018-01-31 05:11:19', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1102, 9, 15, '2018-01-31 05:11:23', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1103, 10, 15, '2018-01-31 05:11:30', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 04:06:01'),
(1114, 1, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1115, 2, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1116, 3, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1117, 4, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1118, 5, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1119, 6, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1120, 7, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1121, 8, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1122, 9, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1123, 10, 14, '2018-01-31 00:00:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 08:55:46'),
(1579, 1, 13, '2018-01-30 18:30:00', '58', '1.06', 'N1', '+3-5', '58', '0.56', '', '36.70', '52.54', '15.84', 'S', '58', '0.53', '', '95.00', '58', '0.41', '', '77.92', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1580, 2, 13, '2018-01-30 18:30:00', '20', '0.79', 'SD2', '+7', '20', '0.45', '', '40.20', '56.32', '16.12', 'S', '20', '0.43', '', '95.00', '20', '0.33', '', '77.46', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1581, 3, 13, '2018-01-30 18:30:00', '37', '1.54', 'N1', '+7', '37', '0.84', '', '38.90', '54.67', '15.77', 'S', '37', '0.81', '', '96.00', '37', '0.65', '', '0.80', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1582, 4, 13, '2018-01-30 18:30:00', '50', '1.22', 'SD1', '+5-6', '50', '0.65', '', '37.50', '53.03', '15.53', 'S', '50', '0.62', '', '95.98', '50', '0.49', '', '79.22', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1583, 5, 13, '2018-01-30 18:30:00', '36', '0.61', 'SD1', '+3-5', '33', '0.29', '', '35.70', '50.34', '14.64', 'S', '33', '0.27', '', '93.15', '33', '0.21', '', '77.57', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1584, 6, 13, '2018-01-30 18:30:00', '44', '1.25', 'SD2', '+5-7', '44', '0.68', '', '39.00', '54.64', '15.64', 'S', '44', '0.65', '', '95.60', '44', '0.51', '', '78.10', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1585, 7, 13, '2018-01-30 18:30:00', '39', '0.92', 'N1', '+5-6', '39', '0.50', '', '37.90', '54.78', '16.08', 'S', '39', '0.48', '', '94.44', '39', '0.40', '', '78.99', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1586, 8, 13, '2018-01-30 18:30:00', '62', '1.79', 'N1', '+6-7', '62', '0.99', '', '39.40', '55.58', '16.18', 'S', '61', '0.95', '1', '94.96', '61', '0.76', '', '80.42', '', '', '', '', '', '', '', '', '', '', '2018-01-30 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1587, 9, 13, '2018-01-30 18:30:00', '76', '1.35', 'SD2', '+3-5', '76', '0.71', '', '36.00', '52.81', '16.81', 'S', '76', '0.68', '', '94.49', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1588, 10, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1589, 11, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1590, 12, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1591, 13, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1592, 15, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1593, 16, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1594, 16, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1595, 17, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1596, 18, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1597, 19, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1598, 20, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1599, 21, 13, '2018-01-30 18:30:00', '58', '1.69', 'N23', '+6-7', '58', '0.94', '', '39.20', '55.38', '16.18', 'B', '58', '0.89', '', '95.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1600, 22, 13, '2018-01-30 18:30:00', '85', '1.45', 'N1', '+3-5', '82', '0.74', '', '36.10', '51.97', '15.87', 'S', '80', '0.68', '2', '92.95', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1601, 23, 13, '2018-01-30 18:30:00', '33', '1.03', 'PAL', '+5', '31', '0.32', '', '20.60', '32.22', '11.62', 'S', '31', '0.31', '', '95.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1602, 24, 13, '2018-01-30 18:30:00', '49', '1.78', 'INC', '+6', '49', '0.90', '', '36.10', '50.33', '14.23', 'S', '49', '0.86', '', '80.51', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1603, 25, 13, '2018-01-30 18:30:00', '54', '1.87', 'SD1', '+6', '49', '0.90', '', '36.10', '50.33', '14.23', 'S', '54', '0.48', '', '94.23', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1604, 26, 13, '2018-01-30 18:30:00', '55', '0.95', 'n23', '+3-4', '55', '0.50', '', '36.40', '53.15', '16.75', 'S', '62', '0.35', '1', '90.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1605, 27, 13, '2018-01-30 18:30:00', '83', '0.93', 'SD1', '-3', '63', '0.38', '', '31.80', '52.19', '20.39', 'S', '47', '0.43', '1', '93.62', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1606, 28, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1607, 29, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1608, 30, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1609, 31, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1610, 32, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1611, 33, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1612, 34, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1613, 35, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1614, 36, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1615, 37, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1616, 38, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1617, 39, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1618, 40, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1619, 41, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1620, 42, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1621, 43, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1622, 44, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1623, 45, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-30 14:50:11'),
(1624, 14, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-31 09:38:35'),
(1625, 46, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-31 09:45:06'),
(1626, 47, 13, '2018-01-30 18:30:00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2000-01-01 00:00:00', '', NULL, NULL, NULL, NULL, '2018-01-31 09:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `polish_count`
--

CREATE TABLE `polish_count` (
  `polish_count_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `pc_4p_name` varchar(20) DEFAULT NULL,
  `pc_4p_takavari` varchar(11) DEFAULT NULL,
  `pc_tbl_name` varchar(20) DEFAULT NULL,
  `pc_tbl_takavari` varchar(20) DEFAULT NULL,
  `pc_tly_name` varchar(20) DEFAULT NULL,
  `pc_tly_takavari` varchar(20) DEFAULT NULL,
  `pc_m_name` varchar(20) DEFAULT NULL,
  `pc_m_takavari` varchar(20) DEFAULT NULL,
  `pc_tk_name` varchar(20) DEFAULT NULL,
  `pc_tk_takavari` varchar(20) DEFAULT NULL,
  `pc_nung` varchar(11) DEFAULT NULL,
  `pc_weight` varchar(20) DEFAULT NULL,
  `pc_4p_g_nung` varchar(11) DEFAULT NULL,
  `pc_4p_weight` varchar(20) DEFAULT NULL,
  `pc_4p_tk` varchar(11) DEFAULT NULL,
  `pc_4p_variation` varchar(11) DEFAULT NULL,
  `pc_tbl_nung` varchar(11) DEFAULT NULL,
  `pc_tbl_weight` varchar(20) DEFAULT NULL,
  `pc_tly_nung` varchar(11) DEFAULT NULL,
  `pc_tly_weight` varchar(20) DEFAULT NULL,
  `pc_m_nung` varchar(11) DEFAULT NULL,
  `pc_m_weight` varchar(20) DEFAULT NULL,
  `pc_tk_nung` varchar(11) DEFAULT NULL,
  `pc_tk_weight` varchar(20) DEFAULT NULL,
  `pc_created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `pc_modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `polish_count`
--

TRUNCATE TABLE `polish_count`;
--
-- Dumping data for table `polish_count`
--

INSERT INTO `polish_count` (`polish_count_id`, `kapan_id`, `pc_4p_name`, `pc_4p_takavari`, `pc_tbl_name`, `pc_tbl_takavari`, `pc_tly_name`, `pc_tly_takavari`, `pc_m_name`, `pc_m_takavari`, `pc_tk_name`, `pc_tk_takavari`, `pc_nung`, `pc_weight`, `pc_4p_g_nung`, `pc_4p_weight`, `pc_4p_tk`, `pc_4p_variation`, `pc_tbl_nung`, `pc_tbl_weight`, `pc_tly_nung`, `pc_tly_weight`, `pc_m_nung`, `pc_m_weight`, `pc_tk_nung`, `pc_tk_weight`, `pc_created_date`, `pc_modified_date`) VALUES
(7, 13, '', '51.16', '', '91.01', '', '39.92', NULL, '0.00', NULL, '0', '839', '20.229999999999997', '806', '10.350000000000001', '0', '251.88', '799', '9.419999999999998', '342', '3.7600000000000007', '0', '0', '0', '0', '2018-01-23 07:13:44', '2018-01-31 15:15:32'),
(8, 14, '', '0', '', '0', '', '0', NULL, '0', NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2018-01-23 10:20:52', '2018-02-03 05:17:53'),
(9, 15, '', '', '', '', '', '', NULL, '', NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-27 13:38:36', '2018-01-30 04:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `polish_repairing`
--

CREATE TABLE `polish_repairing` (
  `polish_repairing_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `pr_dhar_apl_than` varchar(10) DEFAULT NULL,
  `pr_dhar_apl_weight` varchar(10) DEFAULT NULL,
  `pr_dhar_avl_than` varchar(10) DEFAULT NULL,
  `pr_dhar_avl_weight` varchar(10) DEFAULT NULL,
  `pr_tly_apl_than` varchar(10) DEFAULT NULL,
  `pr_tly_apl_weight` varchar(10) DEFAULT NULL,
  `pr_tly_avl_than` varchar(10) DEFAULT NULL,
  `pr_tly_avl_weight` varchar(10) DEFAULT NULL,
  `pr_tbl_apl_than` varchar(10) DEFAULT NULL,
  `pr_tbl_apl_weight` varchar(10) DEFAULT NULL,
  `pr_tbl_avl_than` varchar(10) DEFAULT NULL,
  `pr_tbl_avl_weight` varchar(10) DEFAULT NULL,
  `pr_stly_apl_than` varchar(10) DEFAULT NULL,
  `pr_stly_apl_weight` varchar(10) DEFAULT NULL,
  `pr_stly_avl_than` varchar(10) DEFAULT NULL,
  `pr_stly_avl_weight` varchar(10) DEFAULT NULL,
  `pr_m_apl_than` varchar(10) DEFAULT NULL,
  `pr_m_apl_weight` varchar(10) DEFAULT NULL,
  `pr_m_avl_than` varchar(10) DEFAULT NULL,
  `pr_m_avl_weight` varchar(10) DEFAULT NULL,
  `pr_hiro_apl_than` varchar(10) DEFAULT NULL,
  `pr_hiro_apl_weight` varchar(10) DEFAULT NULL,
  `pr_hiro_avl_than` varchar(10) DEFAULT NULL,
  `pr_hiro_avl_weight` varchar(10) DEFAULT NULL,
  `pr_tik_apl_than` varchar(10) DEFAULT NULL,
  `pr_tik_apl_weight` varchar(10) DEFAULT NULL,
  `pr_tik_avl_than` varchar(10) DEFAULT NULL,
  `pr_tik_avl_weight` varchar(10) DEFAULT NULL,
  `pr_e1_apl_than` varchar(10) DEFAULT NULL,
  `pr_e1_apl_weight` varchar(10) DEFAULT NULL,
  `pr_e1_avl_than` varchar(10) DEFAULT NULL,
  `pr_e1_avl_weight` varchar(10) DEFAULT NULL,
  `pr_e2_apl_than` varchar(10) DEFAULT NULL,
  `pr_e2_apl_weight` varchar(10) DEFAULT NULL,
  `pr_e2_avl_than` varchar(10) DEFAULT NULL,
  `pr_e2_avl_weight` varchar(10) DEFAULT NULL,
  `pr_e3_apl_than` varchar(10) DEFAULT NULL,
  `pr_e3_apl_weight` varchar(10) DEFAULT NULL,
  `pr_e3_avl_than` varchar(10) DEFAULT NULL,
  `pr_e3_avl_weight` varchar(10) DEFAULT NULL,
  `pr_total_apl_than` varchar(10) DEFAULT NULL,
  `pr_total_apl_weight` varchar(10) DEFAULT NULL,
  `pr_total_avl_than` varchar(10) DEFAULT NULL,
  `pr_total_avl_weight` varchar(10) DEFAULT NULL,
  `pr_tbld_apl_than` varchar(10) DEFAULT NULL,
  `pr_tbld_apl_weight` varchar(10) DEFAULT NULL,
  `pr_tbld_avl_than` varchar(10) DEFAULT NULL,
  `pr_tbld_avl_weight` varchar(10) DEFAULT NULL,
  `pr_dori_apl_than` varchar(10) DEFAULT NULL,
  `pr_dori_apl_weight` varchar(10) DEFAULT NULL,
  `pr_dori_avl_than` varchar(10) DEFAULT NULL,
  `pr_dori_avl_weight` varchar(10) DEFAULT NULL,
  `pr_e1_extra_1` varchar(11) DEFAULT NULL,
  `pr_e1_extra_2` varchar(11) DEFAULT NULL,
  `pr_e1_extra_3` varchar(11) DEFAULT NULL,
  `pr_created_date` timestamp NULL DEFAULT current_timestamp(),
  `pr_modified_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `polish_repairing`
--

TRUNCATE TABLE `polish_repairing`;
--
-- Dumping data for table `polish_repairing`
--

INSERT INTO `polish_repairing` (`polish_repairing_id`, `kapan_id`, `pr_dhar_apl_than`, `pr_dhar_apl_weight`, `pr_dhar_avl_than`, `pr_dhar_avl_weight`, `pr_tly_apl_than`, `pr_tly_apl_weight`, `pr_tly_avl_than`, `pr_tly_avl_weight`, `pr_tbl_apl_than`, `pr_tbl_apl_weight`, `pr_tbl_avl_than`, `pr_tbl_avl_weight`, `pr_stly_apl_than`, `pr_stly_apl_weight`, `pr_stly_avl_than`, `pr_stly_avl_weight`, `pr_m_apl_than`, `pr_m_apl_weight`, `pr_m_avl_than`, `pr_m_avl_weight`, `pr_hiro_apl_than`, `pr_hiro_apl_weight`, `pr_hiro_avl_than`, `pr_hiro_avl_weight`, `pr_tik_apl_than`, `pr_tik_apl_weight`, `pr_tik_avl_than`, `pr_tik_avl_weight`, `pr_e1_apl_than`, `pr_e1_apl_weight`, `pr_e1_avl_than`, `pr_e1_avl_weight`, `pr_e2_apl_than`, `pr_e2_apl_weight`, `pr_e2_avl_than`, `pr_e2_avl_weight`, `pr_e3_apl_than`, `pr_e3_apl_weight`, `pr_e3_avl_than`, `pr_e3_avl_weight`, `pr_total_apl_than`, `pr_total_apl_weight`, `pr_total_avl_than`, `pr_total_avl_weight`, `pr_tbld_apl_than`, `pr_tbld_apl_weight`, `pr_tbld_avl_than`, `pr_tbld_avl_weight`, `pr_dori_apl_than`, `pr_dori_apl_weight`, `pr_dori_avl_than`, `pr_dori_avl_weight`, `pr_e1_extra_1`, `pr_e1_extra_2`, `pr_e1_extra_3`, `pr_created_date`, `pr_modified_date`) VALUES
(7, 13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '2018-01-23 07:13:44', '2018-01-31 10:28:48'),
(8, 14, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '2018-01-23 10:20:52', '2018-02-03 05:17:53'),
(9, 15, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-27 13:38:36', '2018-01-30 04:06:01'),
(10, 16, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '', '', '2018-01-31 06:35:49', '2018-01-31 07:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `sabka`
--

CREATE TABLE `sabka` (
  `sabka_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `sabka_date` timestamp NULL DEFAULT NULL,
  `sabka_nung` varchar(11) DEFAULT NULL,
  `sabka_weight` varchar(11) DEFAULT NULL,
  `sabka_avl_weight` varchar(11) DEFAULT NULL,
  `sabka_ghat` varchar(11) DEFAULT NULL,
  `sabka_takavari` varchar(11) DEFAULT NULL,
  `sabka_name` varchar(30) DEFAULT NULL,
  `sabka_created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sabka`
--

TRUNCATE TABLE `sabka`;
--
-- Dumping data for table `sabka`
--

INSERT INTO `sabka` (`sabka_id`, `kapan_id`, `sabka_date`, `sabka_nung`, `sabka_weight`, `sabka_avl_weight`, `sabka_ghat`, `sabka_takavari`, `sabka_name`, `sabka_created_date`) VALUES
(51, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(52, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(53, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(54, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(55, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(56, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(57, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(58, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(59, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(60, 14, '0000-00-00 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `sabka_count`
--

CREATE TABLE `sabka_count` (
  `sabka_count_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `sabka_nung_total` varchar(11) DEFAULT NULL,
  `sabka_weight_total` varchar(11) DEFAULT NULL,
  `sabka_avl_weight_total` varchar(11) DEFAULT NULL,
  `sabka_ghat_total` varchar(11) DEFAULT NULL,
  `sabka_takavari` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `sabka_count`
--

TRUNCATE TABLE `sabka_count`;
--
-- Dumping data for table `sabka_count`
--

INSERT INTO `sabka_count` (`sabka_count_id`, `kapan_id`, `sabka_nung_total`, `sabka_weight_total`, `sabka_avl_weight_total`, `sabka_ghat_total`, `sabka_takavari`) VALUES
(1, 14, '', '', '', NULL, NULL),
(2, 15, '', '', '', NULL, NULL),
(3, 13, NULL, NULL, NULL, NULL, NULL),
(4, 16, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sarin`
--

CREATE TABLE `sarin` (
  `sarin_id` int(11) NOT NULL,
  `s_no` int(11) DEFAULT NULL,
  `kapan_id` int(11) NOT NULL,
  `s_date` timestamp NULL DEFAULT NULL,
  `s_nung` varchar(11) DEFAULT NULL,
  `s_weight` varchar(11) DEFAULT NULL,
  `s_pyority` varchar(11) DEFAULT NULL,
  `s_charni` varchar(11) DEFAULT NULL,
  `s_takavari` varchar(11) DEFAULT NULL,
  `s_name` varchar(50) DEFAULT NULL,
  `s_4p` varchar(50) DEFAULT NULL,
  `s_created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sarin`
--

TRUNCATE TABLE `sarin`;
--
-- Dumping data for table `sarin`
--

INSERT INTO `sarin` (`sarin_id`, `s_no`, `kapan_id`, `s_date`, `s_nung`, `s_weight`, `s_pyority`, `s_charni`, `s_takavari`, `s_name`, `s_4p`, `s_created_date`) VALUES
(2327, 1, 15, '2018-01-27 00:00:00', '61', '0.72', 'G1', '-3', '', 'A', '', '2018-01-30 04:06:01'),
(2328, 2, 15, '2018-01-27 00:00:00', '75', '2.02', 'G1', '+6-7', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2329, 3, 15, '2018-01-27 00:00:00', '65', '1.43', 'G1', '+5-6', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2330, 4, 15, '2018-01-27 00:00:00', '83', '1.33', 'G1', '+3-5', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2331, 5, 15, '2018-01-27 00:00:00', '85', '1.4', 'G1', '+3-5', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2332, 6, 15, '2018-01-27 00:00:00', '72', '1.24', 'SD1', '+3-5', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2333, 7, 15, '2018-01-27 00:00:00', '78', '0.87', 'SD1', '-3', '', 'A', '', '2018-01-30 04:06:01'),
(2334, 8, 15, '2018-01-27 00:00:00', '32', '0.71', 'SD1', '+5-6', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2335, 9, 15, '2018-01-27 00:00:00', '21', '0.89', 'G1', '+7', '', 'A', '', '2018-01-30 04:06:01'),
(2336, 10, 15, '2018-01-27 00:00:00', '65', '0.68', 'G1', '-3', '', 'A', '', '2018-01-30 04:06:01'),
(2337, 11, 15, '2018-01-27 00:00:00', '39', '0.43', 'N1', '-3', '', 'A', '', '2018-01-30 04:06:01'),
(2338, 12, 15, '2018-01-27 00:00:00', '32', '0.86', 'SD1', '+6', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2339, 13, 15, '2018-01-27 00:00:00', '45', '1.37', 'N1', '+6', '', 'A', '', '2018-01-30 04:06:01'),
(2340, 14, 15, '2018-01-27 00:00:00', '46', '0.71', 'N1', '+3-4', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2341, 15, 15, '2018-01-27 00:00:00', '49', '0.99', 'N1', '+4-6', '', 'BHARAT', '', '2018-01-30 04:06:01'),
(2342, 16, 15, '2018-01-29 00:00:00', '15', '0.28', 'n1', 'mix', '', 'A', '', '2018-01-30 04:06:01'),
(2343, 17, 15, '2018-01-29 00:00:00', '56', '1.06', 'G2', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2344, 18, 15, '2018-01-29 00:00:00', '37', '0.6', 'INC', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2345, 18, 15, '2018-01-29 00:00:00', '43', '0.66', 'G1', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2346, 20, 15, '2018-01-29 00:00:00', '22', '0.8', 'INC', '+5', '', 'A', '', '2018-01-30 04:06:01'),
(2347, 21, 15, '2018-01-29 00:00:00', '23', '0.5', 'FIX', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2348, 22, 15, '2018-01-29 00:00:00', '40', '0.54', 'TA', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2349, 23, 15, '2018-01-29 00:00:00', '31', '0.75', 'TA', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2350, 24, 15, '2018-01-29 00:00:00', '34', '0.7', 'N23', 'MIX', '', 'A', '', '2018-01-30 04:06:01'),
(2351, 25, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2352, 26, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2353, 27, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2354, 28, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2355, 29, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2356, 30, 15, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(2367, 1, 14, '2018-01-23 00:00:00', '46', '1.17', 'SD1', '+6-7', '', 'A', '', '2018-01-30 08:55:45'),
(2368, 2, 14, '2018-01-23 00:00:00', '43', '1.24', 'SD1', '+6-7', '', 'A', '', '2018-01-30 08:55:45'),
(2369, 3, 14, '2018-01-23 00:00:00', '49', '0.76', 'SD1', '-3', '', 'A', '', '2018-01-30 08:55:45'),
(2370, 4, 14, '2018-01-23 00:00:00', '54', '2.03', 'SD1', '+7-8', '', 'A', '', '2018-01-30 08:55:45'),
(2371, 5, 14, '2018-01-25 00:00:00', '78', '1.58', 'SD1', '+3-5', '', 'A', '', '2018-01-30 08:55:45'),
(2372, 6, 14, '2018-01-25 00:00:00', '66', '0.99', 'SD1', '-3', '', 'A', '', '2018-01-30 08:55:45'),
(2373, 7, 14, '2018-01-25 00:00:00', '41', '1.27', 'SD1', '+6-8', '', 'BHARAT', '', '2018-01-30 08:55:45'),
(2374, 8, 14, '2018-01-25 00:00:00', '37', '2.33', 'G1', '+9-10', '', 'A', '', '2018-01-30 08:55:45'),
(2375, 9, 14, '2018-01-25 00:00:00', '39', '1.97', 'G1', '+8-9', '', 'A', '', '2018-01-30 08:55:45'),
(2376, 10, 14, '2018-01-25 00:00:00', '43', '2.16', 'G1', '+8-9', '', 'A', '', '2018-01-30 08:55:46'),
(2607, 1, 13, '2018-01-31 00:00:00', '58', '1.06', 'N1', '+3-5', '', 'G', '', '2018-01-30 14:50:11'),
(2608, 2, 13, '2018-01-31 00:00:00', '20', '0.79', 'SD2', '+7', '', 'G', '', '2018-01-30 14:50:11'),
(2609, 3, 13, '2018-01-31 00:00:00', '37', '1.54', 'N1', '+7', '', 'G', '', '2018-01-30 14:50:11'),
(2610, 4, 13, '2018-01-31 00:00:00', '50', '1,22', 'SD1', '+5-6', '', 'G', '', '2018-01-30 14:50:11'),
(2611, 5, 13, '2018-01-31 00:00:00', '36', '0.61', 'SD1', '+3-5', '', 'G', '', '2018-01-30 14:50:11'),
(2612, 6, 13, '2018-01-31 00:00:00', '44', '1.25', 'SD1', '+3-5', '', 'G', '', '2018-01-30 14:50:11'),
(2613, 7, 13, '2018-01-31 00:00:00', '39', '0.92', 'N1', '+5-6', '', 'G', '', '2018-01-30 14:50:11'),
(2614, 8, 13, '2018-01-31 00:00:00', '62', '1.72', 'N1', '+6-7', '', 'G', '', '2018-01-30 14:50:11'),
(2615, 9, 13, '2018-01-31 00:00:00', '76', '1.35', 'SD2', '+3-5', '', 'G', '', '2018-01-30 14:50:11'),
(2616, 10, 13, '2018-01-31 00:00:00', '65', '1.18', 'G1', '+3-5', '', 'G', '', '2018-01-30 14:50:11'),
(2617, 11, 13, '2018-01-31 00:00:00', '74', '2.31', 'N1', '+6-7', '', 'G', '', '2018-01-31 05:29:45'),
(2618, 12, 13, '2018-01-31 00:00:00', '76', '1.39', 'N1', '+3-5', '', 'G', '', '2018-01-31 05:29:45'),
(2619, 13, 13, '2018-01-31 00:00:00', '108', '1.25', 'G1', '-3', '', 'G', '', '2018-01-31 05:29:45'),
(2620, 14, 13, '2018-01-31 00:00:00', '66', '2.97', 'N1', '+7', '', 'G', '', '2018-01-31 05:29:45'),
(2621, 15, 13, '2018-01-31 00:00:00', '57', '1.33', 'N1', '+5-6', '', 'G', '', '2018-01-31 05:29:45'),
(2622, 16, 13, '2018-01-31 00:00:00', '64', '1.17', 'G1', '+3+6', '', 'G', '', '2018-01-31 05:29:45'),
(2623, 17, 13, '2018-01-31 00:00:00', '59', '1.82', 'G1', '+6', '', 'G', '', '2018-01-31 05:29:45'),
(2624, 18, 13, '2018-01-31 00:00:00', '84', '1.59', 'SD1', '+3-6', '', 'G', '', '2018-01-31 05:29:45'),
(2625, 19, 13, '2018-01-31 00:00:00', '40', '1.80', 'N23', '+7', '', 'G', '', '2018-01-31 05:29:45'),
(2626, 20, 13, '2018-01-31 00:00:00', '57', '1.27', 'N23', '+4-6', '', 'G', '', '2018-01-31 05:29:45'),
(2627, 21, 13, '2018-01-31 00:00:00', '58', '1.69', 'N23', '+6-7', '', 'G', '', '2018-01-31 05:42:32'),
(2628, 22, 13, '2018-01-31 00:00:00', '85', '1.45', 'N1', '+3-5', '', 'G', '', '2018-01-31 05:42:32'),
(2629, 23, 13, '2018-01-31 00:00:00', '33', '1.03', 'P', '+5', '', 'G', '', '2018-01-31 05:42:32'),
(2630, 24, 13, '2018-01-31 00:00:00', '49', '1.78', 'IN1', '+6', '', 'G', '', '2018-01-31 05:42:32'),
(2631, 25, 13, '2018-01-31 00:00:00', '54', '1.87', 'SD1', '+6', '', 'G', '', '2018-01-31 05:42:32'),
(2642, 26, 13, '2018-01-31 00:00:00', '55', '0.95', 'N23', '+3-4', '', 'G', '', '2018-01-31 05:54:57'),
(2643, 27, 13, '2018-01-31 00:00:00', '83', '0.93', 'SD1', '-3', '', 'G', '', '2018-01-31 05:54:57'),
(2644, 28, 13, '2018-01-31 00:00:00', '50', '0.97', 'IN1', '-6', '', 'G', '', '2018-01-31 05:54:57'),
(2645, 29, 13, '2018-01-31 00:00:00', '33', '1.02', 'P', '+5', '', 'G', '', '2018-01-31 05:54:57'),
(2646, 30, 13, '2018-01-31 00:00:00', '66', '1.02', 'N1', '+3-4', '', 'G', '', '2018-01-31 05:54:57'),
(2647, 31, 13, '2018-01-31 00:00:00', '61', '2.09', 'G1', '+6', '', 'G', '', '2018-01-31 05:54:57'),
(2648, 32, 13, '2018-01-31 00:00:00', '71', '0.89', 'SD1', '-5', '', 'G', '', '2018-01-31 05:54:57'),
(2649, 33, 13, '2018-01-31 00:00:00', '50', '1.01', 'N1', '+4-6', '', 'G', '', '2018-01-31 05:54:57'),
(2650, 34, 13, '2018-01-31 00:00:00', '34', '1.05', 'SD', '+6', '', 'G', '', '2018-01-31 05:54:57'),
(2651, 35, 13, '2018-01-31 00:00:00', '80', '0.95', 'N1', '-3', '', 'G', '', '2018-01-31 05:54:57'),
(2652, 36, 13, '2018-01-31 00:00:00', '67', '2.17', 'N1', '+6', '', 'G', '', '2018-01-31 06:01:23'),
(2653, 37, 13, '2018-01-31 00:00:00', '62', '0.72', 'G1', '-3', '', 'G', '', '2018-01-31 06:01:23'),
(2657, 38, 13, '2018-01-31 00:00:00', '82', '1.27', 'SD', '-6', '', 'G', '', '2018-01-31 06:12:17'),
(2658, 39, 13, '2018-01-31 00:00:00', '48', '1.22', 'SD1', '+5', '', 'G', '', '2018-01-31 06:12:17'),
(2659, 40, 13, '2018-01-31 00:00:00', '67', '0.77', 'N1', '-3', '', 'G', '', '2018-01-31 06:12:17'),
(2662, 41, 13, '2018-01-31 00:00:00', '89', '1.67', 'G1', '+3-6', '', 'G', '', '2018-01-31 06:19:22'),
(2663, 42, 13, '2018-01-31 00:00:00', '66', '0.87', 'N23', '-5', '', 'G', '', '2018-01-31 06:19:22'),
(2664, 43, 13, '2018-01-31 00:00:00', '62', '0.98', 'TA', '-5', '', 'G', '', '2018-01-31 06:19:22'),
(2665, 44, 13, '2018-01-31 00:00:00', '39', '0.54', 'G2', '-5', '', 'G', '', '2018-01-31 06:19:22'),
(2666, 45, 13, '2018-01-31 00:00:00', '28', '0.71', 'FIX-TABLE', 'MIX', '', 'G', '', '2018-01-31 06:19:22'),
(2667, 46, 13, '2018-01-31 00:00:00', '48', '1.28', 'TA', '+5', '', 'G', '', '2018-01-31 06:19:22'),
(2668, 47, 13, '2018-01-31 00:00:00', '17', '0.40', 'N1', 'MIX', '', 'G', '', '2018-01-31 06:19:22'),
(2669, 48, 13, '2018-01-31 00:00:00', '28', '0.78', 'G2', '+5', '', 'G', '', '2018-01-31 06:27:37'),
(2670, 49, 13, '2018-01-31 00:00:00', '65', '1.47', 'MIX', '+4-7', '', 'G', '', '2018-01-31 06:27:37'),
(2671, 50, 13, '2018-01-31 00:00:00', '38', '1.12', 'IM', 'MIX', '', 'G', '', '2018-01-31 06:27:37'),
(2672, 51, 13, '2018-01-31 00:00:00', '45', '1.40', 'N23', '+5', '', 'G', '', '2018-01-31 06:27:37'),
(2673, 52, 13, '2018-01-31 00:00:00', '47', '0.98', 'G1', 'MIX', '', 'G', '', '2018-01-31 06:27:37'),
(2674, 53, 13, '2018-01-31 00:00:00', '15', '0.28', 'P', 'MIX', '', 'G', '', '2018-01-31 06:27:37'),
(2675, 54, 13, '2018-01-31 00:00:00', '28', '1.21', 'MIX', '+7', '', 'G', '', '2018-01-31 06:27:37'),
(2676, 55, 13, '2018-01-31 00:00:00', '57', '0.60', 'MIX', '-3', '', 'G', '', '2018-01-31 06:27:37'),
(2677, 56, 13, '2018-01-31 00:00:00', '48', '0.78', 'MIX', '+3-4', '', 'G', '', '2018-01-31 06:27:37'),
(2678, 57, 13, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-31 06:27:37'),
(2679, 1, 16, '2018-01-31 00:00:00', '61', '1.90', 'SD', '+6-7', '', 'G', '', '2018-01-31 06:35:49'),
(2680, 2, 16, '2018-01-31 00:00:00', '37', '1.52', 'SD', '+7-8', '', 'G', '', '2018-01-31 06:35:49'),
(2681, 3, 16, '2018-01-31 00:00:00', '41', '1.70', 'SD', '+7-8', '', 'G', '', '2018-01-31 06:35:49'),
(2682, 4, 16, '2018-01-31 00:00:00', '47', '1.06', 'SD', '+5-6', '', 'G', '', '2018-01-31 06:35:49'),
(2683, 5, 16, '2018-01-31 00:00:00', '18', '0.67', 'SD1', 'MIX', '', 'G', '', '2018-01-31 06:35:49'),
(2684, 6, 16, '2018-01-31 00:00:00', '28', '0.86', 'SD1', '+6-7', '', 'G', '', '2018-01-31 06:35:49'),
(2685, 7, 16, '2018-01-31 00:00:00', '33', '0.96', 'SD1', '+6-7', '', 'G', '', '2018-01-31 06:35:49'),
(2686, 8, 16, '2018-01-31 00:00:00', '45', '1.55', 'SD1', '+6-8', '', 'G', '', '2018-01-31 06:35:49'),
(2687, 9, 16, '2018-01-31 00:00:00', '44', '0.67', 'SD1', '+3-6', '', 'G', '', '2018-01-31 06:35:49'),
(2688, 10, 16, '2018-01-31 00:00:00', '49', '0.97', 'SD1', '+3-6', '', 'G', '', '2018-01-31 06:35:49'),
(2689, 11, 16, '2018-01-31 00:00:00', '33', '1.07', 'SD', '+6', '', 'G', '', '2018-01-31 06:48:04'),
(2690, 12, 16, '2018-01-31 00:00:00', '45', '0.94', 'SD', '+4-6', '', 'G', '', '2018-01-31 06:48:04'),
(2691, 13, 16, '2018-01-31 00:00:00', '34', '0.57', 'SD', '+3-4', '', 'G', '', '2018-01-31 06:48:04'),
(2692, 14, 16, '2018-01-31 00:00:00', '34', '0.56', 'SD', '+3-4', '', 'G', '', '2018-01-31 06:48:04'),
(2693, 15, 16, '2018-01-31 00:00:00', '52', '0.59', 'SD1', '-3', '', 'G', '', '2018-01-31 06:48:04'),
(2694, 16, 16, '2018-01-31 00:00:00', '56', '0.67', 'SD', 'MIX', '', 'G', '', '2018-01-31 06:50:46'),
(2695, 17, 16, '2018-01-31 00:00:00', '51', '0.57', 'SD1', '-3', '', 'G', '', '2018-01-31 06:50:46'),
(2696, 18, 16, '2018-01-31 00:00:00', '47', '0.51', 'SD1', '-3', '', 'G', '', '2018-01-31 06:50:46'),
(2697, 19, 16, '2018-01-31 00:00:00', '41', '1.28', 'IN', '+6', '', 'G', '', '2018-01-31 06:50:46'),
(2698, 20, 16, '2018-01-31 00:00:00', '43', '0.83', 'IN', '-6', '', 'G', '', '2018-01-31 06:50:46'),
(2699, 21, 16, '2018-01-31 00:00:00', '35', '0.67', 'P', '+4-6', '', 'G', '', '2018-01-31 07:08:38'),
(2700, 22, 16, '2018-01-31 00:00:00', '27', '0.78', 'SD2', '+6', '', 'G', '', '2018-01-31 07:08:38'),
(2701, 23, 16, '2018-01-31 00:00:00', '30', '0.71', 'P', '+6-7', '', 'G', '', '2018-01-31 07:08:38'),
(2702, 24, 16, '2018-01-31 00:00:00', '35', '1.24', 'N1', '+6', '', 'G', '', '2018-01-31 07:08:38'),
(2703, 25, 16, '2018-01-31 00:00:00', '42', '0.51', 'SD2', '-3', '', 'G', '', '2018-01-31 07:08:38'),
(2704, 26, 16, '2018-01-31 00:00:00', '35', '0.92', 'TOPS', 'MIX', '', 'G', '', '2018-01-31 07:08:38'),
(2705, 27, 16, '2018-01-31 00:00:00', '41', '1.16', 'P', '+7', '', 'G', '', '2018-01-31 07:08:38'),
(2706, 28, 16, '2018-01-31 00:00:00', '27', '0.63', 'P', '+6-7', '', 'G', '', '2018-01-31 07:08:38'),
(2707, 29, 16, '2018-01-31 00:00:00', '36', '0.67', 'P', '+4-6', '', 'G', '', '2018-01-31 07:08:38'),
(2708, 30, 16, '2018-01-31 00:00:00', '86', '1.44', 'SD', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2709, 31, 16, '2018-01-31 00:00:00', '36', '1.03', 'P', '+7', '', 'G', '', '2018-01-31 07:08:38'),
(2710, 32, 16, '2018-01-31 00:00:00', '63', '0.97', 'SD2', '+3-4', '', 'G', '', '2018-01-31 07:08:38'),
(2711, 33, 16, '2018-01-31 00:00:00', '24', '0.29', 'N1', '-3', '', 'G', '', '2018-01-31 07:08:38'),
(2712, 34, 16, '2018-01-31 00:00:00', '47', '0.89', 'N1', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2713, 35, 16, '2018-01-31 00:00:00', '40', '0.46', 'SD2', '-3', '', 'G', '', '2018-01-31 07:08:38'),
(2714, 36, 16, '2018-01-31 00:00:00', '23', '0.71', 'N23', '+6', '', 'G', '', '2018-01-31 07:08:38'),
(2715, 37, 16, '2018-01-31 00:00:00', '31', '0.59', 'N23', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2716, 38, 16, '2018-01-31 00:00:00', '31', '0.52', 'N23', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2717, 39, 16, '2018-01-31 00:00:00', '50', '0.65', 'N23', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2718, 40, 16, '2018-01-31 00:00:00', '27', '0.31', 'N23', '-3', '', 'G', '', '2018-01-31 07:08:38'),
(2719, 41, 16, '2018-01-31 00:00:00', '45', '1.31', 'COLOR', 'MIX', '', 'G', '', '2018-01-31 07:08:38'),
(2720, 42, 16, '2018-01-31 00:00:00', '41', '0.83', 'SD2', '+4-6', '', 'G', '', '2018-01-31 07:08:38'),
(2721, 43, 16, '2018-01-31 00:00:00', '20', '0.34', 'TA', 'MIX', '', 'G', '', '2018-01-31 07:08:38'),
(2722, 44, 16, '2018-01-31 00:00:00', '56', '0.69', 'MIX', '+7-3', '', 'G', '', '2018-01-31 07:08:38'),
(2723, 45, 16, '2018-01-31 00:00:00', '26', '1.02', 'MIX', '+7', '', 'G', '', '2018-01-31 07:08:38'),
(2724, 46, 16, '2018-01-31 00:00:00', '36', '1.02', 'MIX', '+6-7', '', 'G', '', '2018-01-31 07:08:38'),
(2725, 47, 16, '2018-01-31 00:00:00', '44', '0.79', 'MIX', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2726, 48, 16, '2018-01-31 00:00:00', '39', '0.70', 'MIX', '+3-6', '', 'G', '', '2018-01-31 07:08:38'),
(2727, 49, 16, '2018-01-31 00:00:00', '56', '0.63', 'NI', 'MIX', '', 'G', '', '2018-01-31 07:08:38'),
(2728, 50, 16, '0000-00-00 00:00:00', '', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(2729, 11, 14, '2018-02-03 00:00:00', '67', '2.59', 'G1', '+6-9', '', '', '', '2018-02-03 05:17:53'),
(2730, 12, 14, '2018-02-03 00:00:00', '67', '2.41', 'G1', '+7-8', '', '', '', '2018-02-03 05:17:53'),
(2731, 13, 14, '2018-02-03 00:00:00', '64', '1.22', 'G1', '+5-6', '', '', '', '2018-02-03 05:17:53'),
(2732, 14, 14, '2018-02-03 00:00:00', '52', '1.14', 'G1', '+5-6', '', '', '', '2018-02-03 05:17:53'),
(2733, 15, 14, '2018-02-03 00:00:00', '53', '1.09', 'G1', '+5-6', '', '', '', '2018-02-03 05:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `sarin_count`
--

CREATE TABLE `sarin_count` (
  `sarin_count_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `sc_saij` int(11) DEFAULT NULL,
  `sc_takavari` float DEFAULT NULL,
  `sc_nung` int(11) DEFAULT NULL,
  `sc_weight` float DEFAULT NULL,
  `sc_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `sc_modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `sarin_count`
--

TRUNCATE TABLE `sarin_count`;
--
-- Dumping data for table `sarin_count`
--

INSERT INTO `sarin_count` (`sarin_count_id`, `kapan_id`, `sc_saij`, `sc_takavari`, `sc_nung`, `sc_weight`, `sc_created_date`, `sc_modified_date`) VALUES
(7, 13, 0, 0, 3080, 0, '2018-01-23 07:13:44', '2018-01-31 10:28:48'),
(8, 14, 33, 15.45, 799, 23.95, '2018-01-23 10:20:52', '2018-02-03 05:17:53'),
(9, 15, 53, 62.15, 1149, 21.54, '2018-01-27 13:38:36', '2018-01-30 04:06:01'),
(10, 16, 47, 66.52, 1968, 41.93, '2018-01-31 06:35:49', '2018-01-31 07:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `sessions_id` int(11) NOT NULL,
  `session_id` varchar(128) NOT NULL,
  `s_ip` varchar(50) NOT NULL,
  `s_user_agent` varchar(1000) DEFAULT NULL,
  `s_user_device` varchar(255) NOT NULL,
  `s_created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `s_modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;
--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`sessions_id`, `session_id`, `s_ip`, `s_user_agent`, `s_user_device`, `s_created_time`, `s_modified_date`) VALUES
(1, 'b3b982aa0e0354e1155279b09b94b389', '66.102.6.140', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2018-01-22 13:31:01', '2018-01-22 13:31:01'),
(2, 'fcb77446fb5f08c9b3b1fd5e32872a9f', '66.102.6.140', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36', 'PC: Chrome', '2018-01-22 13:31:01', '2018-01-22 13:31:01'),
(3, '79e99f2d88072d0ba0a41670f6b2899b', '123.201.225.3', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 06:53:50', '2018-01-23 06:53:50'),
(4, 'd5004e8886fd60dd2eedd1f360e5ab27', '49.34.108.116', 'WhatsApp/2.17.351 A', 'PC: ', '2018-01-23 06:54:09', '2018-01-23 06:54:09'),
(5, '315a97e949a160f8bd7853f2fbf4b959', '66.102.6.140', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2018-01-23 06:59:37', '2018-01-23 06:59:37'),
(6, '9a047481964fe4f78ef8bb6c92138a99', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 06:59:46', '2018-01-23 06:59:46'),
(7, '1dd70c7358f7e73950a7da8a6bd6dcca', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 07:01:55', '2018-01-23 07:01:55'),
(8, '785641a350a24fb1f550a18e177f2a93', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 07:02:54', '2018-01-23 07:02:54'),
(9, '715330472372942d4f0e77e83a0d1c98', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 07:22:03', '2018-01-23 07:22:03'),
(10, 'e478117bcb5378604b5ad8f337582ba8', '49.34.112.118', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 08:08:23', '2018-01-23 08:08:23'),
(11, '4765e5d2a4320d9db72acc495e1784f0', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 09:20:08', '2018-01-23 09:20:08'),
(12, 'aadea351c0eb30d7bda1b516d9672017', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 09:50:41', '2018-01-23 09:50:41'),
(13, 'e8674620a8b69484c1c5ba3f6bb664f1', '49.34.112.118', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 10:03:18', '2018-01-23 10:03:18'),
(14, '1188b32b15daabe3c128e13ddec8bb6e', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 10:15:55', '2018-01-23 10:15:55'),
(15, 'ac4f8a4314a8beb31c66888c59b3ab04', '219.91.239.63', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 14:13:20', '2018-01-23 14:13:20'),
(16, 'f46be5f71e81f53832e2c0c6e06463a1', '219.91.239.63', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 14:13:31', '2018-01-23 14:13:31'),
(17, 'c315e903c18b4d7e57974c1be89ecb1e', '49.34.65.232', 'WhatsApp/2.17.351 A', 'PC: ', '2018-01-23 14:13:44', '2018-01-23 14:13:44'),
(18, 'f20fbfa523a34ca68db0c9171f460bd6', '64.233.173.33', 'Mozilla/5.0 (Linux; Android 7.1.2; Mi A1 Build/N2G47H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.111 Mobile Safari/537.36', 'Mobile: Chrome: AndroidOS', '2018-01-23 14:16:36', '2018-01-23 14:16:36'),
(19, '56f531d3313adb3740ae96c9cebaa292', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-23 14:18:55', '2018-01-23 14:18:55'),
(20, 'e2c67a27169fb56b9966816800116991', '219.91.237.54', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-24 04:31:28', '2018-01-24 04:31:28'),
(21, '61b9099cf6993cfe8c296e2e292c4b4c', '42.106.30.215', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-25 03:49:33', '2018-01-25 03:49:33'),
(22, '091a10442ce9b3bb79ed245698618f2d', '219.91.237.31', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-26 05:43:40', '2018-01-26 05:43:40'),
(23, 'b19512ec9856d30959a9c5105ceac350', '219.91.237.31', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-26 05:55:13', '2018-01-26 05:55:13'),
(24, '9606da127af4cebac408976449f0d11e', '42.106.17.219', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-26 05:57:22', '2018-01-26 05:57:22'),
(25, '68c79375dd2b4583f33079b3d96df085', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-27 13:34:58', '2018-01-27 13:34:58'),
(26, '9cd172a43bef2b072935e897eb646a7c', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-28 05:45:47', '2018-01-28 05:45:47'),
(27, '737f5443c21df233476ee27394a2cf1b', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-29 03:03:07', '2018-01-29 03:03:07'),
(28, '84eba8e0571da64701921804ccc6f247', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-29 05:50:30', '2018-01-29 05:50:30'),
(29, '885cb4fd9f296e696d9aca0d0ad786c2', '49.34.140.8', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-29 06:43:07', '2018-01-29 06:43:07'),
(30, '30c77e124472fd3c7924b326f0ec159a', '123.201.227.155', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-29 10:16:53', '2018-01-29 10:16:53'),
(31, 'f0a7d190a1bda7cca6107e0f7aea2494', '219.91.236.238', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-29 16:43:06', '2018-01-29 16:43:06'),
(32, '61f176d802746694649dea408e3158f0', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-30 03:19:53', '2018-01-30 03:19:53'),
(33, 'cfce2257762b733f58de287f799c2eb4', '219.91.238.179', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-30 04:45:19', '2018-01-30 04:45:19'),
(34, '651d7e164ac456a97370dbf90ed2db10', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-01-30 08:52:08', '2018-01-30 08:52:08'),
(35, '5c98bc583213e2b88af295078f2e8b8b', '123.201.226.174', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-30 09:16:17', '2018-01-30 09:16:17'),
(36, 'ce57763a309b49eb909394be98b6bec5', '123.201.225.245', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-30 15:43:00', '2018-01-30 15:43:00'),
(37, '2b13ba322fec50ac7f9ed7ea6364bd0e', '103.251.17.53', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-31 07:09:01', '2018-01-31 07:09:01'),
(38, '5326880489a12ae4988519980ca550f0', '219.91.237.133', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 'PC: Chrome', '2018-01-31 10:35:18', '2018-01-31 10:35:18'),
(39, 'c21f153ea8333e6aaa47700179a684e5', '103.251.17.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063', 'PC: Chrome', '2018-02-03 05:13:12', '2018-02-03 05:13:12'),
(40, 'b935e7ecf0e782c4a66fbe1562b6417e', '157.32.32.187', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:59.0) Gecko/20100101 Firefox/59.0', 'PC: Firefox', '2018-02-10 12:48:20', '2018-02-10 12:48:20'),
(41, '3dbfe758dc60db102f8d26648b690427', '157.32.124.248', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:59.0) Gecko/20100101 Firefox/59.0', 'PC: Firefox', '2018-02-10 12:48:20', '2018-02-10 12:48:20'),
(42, 'a46233e520a78c299b2e59122ee07163', '66.249.66.157', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-05-29 17:49:46', '2019-05-29 17:49:46'),
(43, 'de36e0da1ffd12e051efed0aadeac55a', '66.249.66.158', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-05-31 07:32:33', '2019-05-31 07:32:33'),
(44, '339f94cb480440f4ef0a627a02667b78', '157.32.65.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'PC: Chrome', '2019-06-04 03:39:40', '2019-06-04 03:39:40'),
(45, 'c2abbb64acb5f5050f98b695d1cc56e8', '157.32.209.195', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'PC: Chrome', '2019-06-04 03:41:31', '2019-06-04 03:41:31'),
(46, '12fdb8ba4894552e5a0482f48161f2e3', '157.32.65.87', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'PC: Chrome', '2019-06-04 03:42:13', '2019-06-04 03:42:13'),
(47, 'b2a75263bd78dd3b39f0e0faeb2be529', '66.249.66.203', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-06-13 23:54:57', '2019-06-13 23:54:57'),
(48, '064063163f1d0e7a8b8391b7fdbac309', '66.249.73.10', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-06-14 11:03:52', '2019-06-14 11:03:52'),
(49, '9d63689708764d974548a6f999315bfd', '157.32.29.177', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'PC: Chrome', '2019-06-17 17:06:59', '2019-06-17 17:06:59'),
(50, 'cc33e52302d1402fa23cd3f434f90e27', '5.255.250.24', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-06-20 13:06:52', '2019-06-20 13:06:52'),
(51, 'a88dc2c92e22595d0b406480d5c86e36', '180.163.220.4', 'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 baidu.sogo.uc.UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN', 'Mobile: Chrome: AndroidOS', '2019-06-20 20:31:23', '2019-06-20 20:31:23'),
(52, '071e1f31d1d7e079dccfef9e7474228c', '5.255.250.24', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-06-21 09:39:09', '2019-06-21 09:39:09'),
(53, 'acd521626d3e391256e3065a2405418d', '100.43.85.201', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-06-22 03:39:07', '2019-06-22 03:39:07'),
(54, '75f45de54f85ddd051ce4b58f75d2a7e', '66.220.149.40', 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)', 'PC: ', '2019-06-30 13:36:21', '2019-06-30 13:36:21'),
(55, '017e95459d1251ff5dbee66ff440f0b4', '49.36.1.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'PC: Chrome', '2019-07-02 10:30:40', '2019-07-02 10:30:40'),
(56, 'b382fb2f4319728b048f11dea48161b4', '100.43.85.117', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-07-03 12:33:21', '2019-07-03 12:33:21'),
(57, '3ab85654f3664e54c2fe78eb34f99796', '66.249.64.75', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-07-03 18:24:06', '2019-07-03 18:24:06'),
(58, '32c399f470759a0b7e2b05ead3c9552a', '66.249.64.75', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-07-04 08:57:49', '2019-07-04 08:57:49'),
(59, '65fef4b626a1b34abe0431b20eea60fc', '66.249.64.71', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-07-04 18:24:06', '2019-07-04 18:24:06'),
(60, '5a7597b2edec4f26d73531439edc51e1', '66.249.75.159', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-07-08 16:02:02', '2019-07-08 16:02:02'),
(61, '9e51dd43a0cb9ffd334956adcb98704d', '5.255.250.24', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-07-13 08:46:16', '2019-07-13 08:46:16'),
(62, '4a33bfd8acc02ed82975500d8785bf57', '157.32.227.44', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'PC: Chrome', '2019-07-17 14:30:51', '2019-07-17 14:30:51'),
(63, 'e19fc358ddacd4ad1cc430d8b01bfc1c', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:16:57', '2019-07-21 12:16:57'),
(64, '3e4715d0020568e14cc351e14d0d8fad', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:16:57', '2019-07-21 12:16:57'),
(65, '18532014ff7d10dcda342627785af64e', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:19:02', '2019-07-21 12:19:02'),
(66, 'd34f37303efbc0614628f65aab85177d', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:20:53', '2019-07-21 12:20:53'),
(67, '917394d514a100b52f615f9c2c834cfa', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:25:32', '2019-07-21 12:25:32'),
(68, 'c037be125bd176d12ed490ef88e7a358', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:26:37', '2019-07-21 12:26:37'),
(69, 'b2fe5a730230de00c92d8e7bd676b85d', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 'PC: Firefox', '2019-07-21 12:27:24', '2019-07-21 12:27:24'),
(70, 'd0a5acf9b4856f2fa10eb2aa356b7e63', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 'PC: Firefox', '2019-07-21 12:27:53', '2019-07-21 12:27:53'),
(71, '0d12af30ec9d7dc3a313a03d97654fff', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'PC: Chrome', '2019-07-21 12:30:50', '2019-07-21 12:30:50'),
(72, 'ac074aa360c11194f6f6b45646ec2d60', '[2405:204:840a:4dbf:318a:5428:307a:8d42]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-21 12:34:03', '2019-07-21 12:34:03'),
(73, 'f66cabd891dfd0d4fa4f963f536ee8b2', '[2405:204:840a:4dbf:318a:5428:307a:8d42]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-21 12:34:03', '2019-07-21 12:34:03'),
(74, '0407cf04f00a44ccc0aa57e26137f2f0', '[2405:204:840a:4dbf:318a:5428:307a:8d42]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-21 12:34:05', '2019-07-21 12:34:05'),
(75, '9fd6050edcae57d472fc7f13413b43a0', '49.34.184.129', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 'PC: Firefox', '2019-07-21 12:34:40', '2019-07-21 12:34:40'),
(76, '07d1ec8b8b8663905d19c2d0191e1b69', '[2405:204:840a:4dbf:b92c:99ae:5f31:9326]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-22 03:29:06', '2019-07-22 03:29:06'),
(77, 'c60393c85d504892e848b2e3a17b2ae2', '[2405:204:840a:4dbf:b92c:99ae:5f31:9326]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-22 03:29:06', '2019-07-22 03:29:06'),
(78, 'c0317008241a1924ec76206462afe7f4', '[2405:204:840a:4dbf:b92c:99ae:5f31:9326]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-22 03:29:07', '2019-07-22 03:29:07'),
(79, 'c59a041991b5eadee3778a6ea390364c', '[2405:204:840a:4dbf:b92c:99ae:5f31:9326]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-22 03:29:07', '2019-07-22 03:29:07'),
(80, 'e73ec5fd508609c64bad15adad61d9fe', '49.34.141.223', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-23 02:49:03', '2019-07-23 02:49:03'),
(81, 'f670565e6093362787c16202c64ab7d6', '49.34.141.223', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-23 02:49:03', '2019-07-23 02:49:03'),
(82, '34014b90b51a7543bd311e6145c6df63', '49.34.141.223', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-23 02:49:03', '2019-07-23 02:49:03'),
(83, 'cd54f8300d76ad38b7f0f5e8c2fb3c95', '49.34.141.223', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-23 02:49:03', '2019-07-23 02:49:03'),
(84, 'df53ce49c9f36e0c74e82eade85dbc0d', '[2405:205:c861:ab37:bda5:1845:bc27:8e2a]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 02:57:31', '2019-07-24 02:57:31'),
(85, '54b420feaa4fbb896be8fafa97553bcc', '[2405:205:c861:ab37:bda5:1845:bc27:8e2a]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 02:57:31', '2019-07-24 02:57:31'),
(86, '6d8fe7da865d5c4057e6983824a18b84', '[2405:205:c861:ab37:bda5:1845:bc27:8e2a]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 02:57:31', '2019-07-24 02:57:31'),
(87, '00c8f8ef12b0ff395921dd78e7da00f1', '[2405:205:c861:ab37:bda5:1845:bc27:8e2a]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 02:57:31', '2019-07-24 02:57:31'),
(88, '8c0e1ece9585bcab329f3e19f2b1e22b', '5.255.250.24', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-07-24 10:47:02', '2019-07-24 10:47:02'),
(89, '1eca5186a2386c7b7c8fee57e45cd30b', '157.32.161.113', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 16:24:01', '2019-07-24 16:24:01'),
(90, '30a5cecb2e2f18b8f1dd64f3e6108053', '157.32.161.113', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 16:24:02', '2019-07-24 16:24:02'),
(91, 'bf884bfaf2e40065e60df1c770e7b25d', '157.32.161.113', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-24 16:24:05', '2019-07-24 16:24:05'),
(92, 'bf6e802dc59ec2297cf15fd48d9b5b27', '[2405:205:c861:ab37:5c09:c3e3:40a3:ffad]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 03:15:03', '2019-07-26 03:15:03'),
(93, '4e5d043d7b93683e1d377872de77edef', '[2405:205:c861:ab37:5c09:c3e3:40a3:ffad]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 03:15:03', '2019-07-26 03:15:03'),
(94, 'a57f8f0522d3e8051b359253adb4c821', '[2405:205:c861:ab37:5c09:c3e3:40a3:ffad]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 03:15:03', '2019-07-26 03:15:03'),
(95, '2af31ca0160fe3934e671891d9bd0fdf', '[2405:205:c861:ab37:5c09:c3e3:40a3:ffad]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 03:15:03', '2019-07-26 03:15:03'),
(96, 'a4380a62d48d3bee0d39c8a80d67dede', '[2405:205:c861:ab37:f5f0:f2af:c2bf:17a1]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 15:17:03', '2019-07-26 15:17:03'),
(97, 'bd66b954a4f74f9008b2e6ad0b09e924', '[2405:205:c861:ab37:f5f0:f2af:c2bf:17a1]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 15:17:03', '2019-07-26 15:17:03'),
(98, '5e2f798b3ab8db1eade71dcce14ee439', '[2405:205:c861:ab37:f5f0:f2af:c2bf:17a1]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-26 15:17:03', '2019-07-26 15:17:03'),
(99, '626c180c882573b966014e71d080a81d', '[2405:205:c861:ab37:818:5967:aded:67b2]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-27 03:45:36', '2019-07-27 03:45:36'),
(100, '075167033b13b21474520181f3538614', '[2405:205:c861:ab37:818:5967:aded:67b2]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-27 03:45:36', '2019-07-27 03:45:36'),
(101, 'ce178eb512ffcd94b78258d79a129f65', '[2405:204:8003:f380:a83e:51f:6c90:7e6f]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-28 05:15:26', '2019-07-28 05:15:26'),
(102, '8f22e94f7ae0cba83e70290234a5f1da', '[2405:204:8003:f380:a83e:51f:6c90:7e6f]', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', 'PC: Chrome', '2019-07-28 05:15:26', '2019-07-28 05:15:26'),
(103, '93ca2a589be242402085b9f599588e45', '49.36.15.116', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36', 'PC: Chrome', '2019-08-01 12:39:18', '2019-08-01 12:39:18'),
(104, 'b1230c77c6301a4aee00236149583848', '141.8.144.41', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-08-04 08:17:28', '2019-08-04 08:17:28'),
(105, 'bec13977224e9a6bc0b726a929c7ed06', '66.249.73.8', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Mobile: Chrome: AndroidOS', '2019-08-06 18:25:51', '2019-08-06 18:25:51'),
(106, '626f9468fd46172eb579a0356e798e6a', '5.255.250.24', 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)', 'PC: Bot', '2019-08-13 21:44:35', '2019-08-13 21:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `site_config`
--

CREATE TABLE `site_config` (
  `site_config_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `site_offline` tinyint(1) NOT NULL,
  `offline_msg` tinyint(1) NOT NULL,
  `custom_message` text NOT NULL,
  `offline_image` text NOT NULL,
  `noti_new_order` tinyint(1) NOT NULL,
  `noti_new_customer` tinyint(1) NOT NULL,
  `noti_new_message` tinyint(1) NOT NULL,
  `custom_page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `robots` int(11) NOT NULL,
  `author` varchar(255) NOT NULL,
  `content_rights` text NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `site_config`
--

TRUNCATE TABLE `site_config`;
-- --------------------------------------------------------

--
-- Table structure for table `soing`
--

CREATE TABLE `soing` (
  `soing_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `soing_date` datetime DEFAULT NULL,
  `soing_nung` varchar(11) DEFAULT NULL,
  `soing_weight` varchar(11) DEFAULT NULL,
  `soing_avl_weight` varchar(11) DEFAULT NULL,
  `soing_ghat` varchar(11) DEFAULT NULL,
  `soing_takavari` varchar(11) DEFAULT NULL,
  `soing_name` varchar(30) DEFAULT NULL,
  `soing_created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `soing`
--

TRUNCATE TABLE `soing`;
--
-- Dumping data for table `soing`
--

INSERT INTO `soing` (`soing_id`, `kapan_id`, `soing_date`, `soing_nung`, `soing_weight`, `soing_avl_weight`, `soing_ghat`, `soing_takavari`, `soing_name`, `soing_created_date`) VALUES
(31, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(32, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(33, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(34, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(35, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(36, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(37, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(38, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(39, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(40, 15, '1970-01-01 00:00:00', '', '', '', '', '', '', '2018-01-30 04:06:01'),
(91, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(92, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(93, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(94, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(95, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(96, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(97, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(98, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(99, 16, '2018-01-31 00:00:00', '', '', '', '', '', '', '2018-01-31 07:08:38'),
(100, 16, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 07:08:38'),
(121, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(122, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(123, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(124, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(125, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(126, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(127, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(128, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(129, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(130, 13, '1970-01-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-31 10:28:48'),
(131, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(132, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(133, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(134, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(135, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(136, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(137, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(138, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(139, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53'),
(140, 14, '2018-02-03 00:00:00', '', '', '', '', '', '', '2018-02-03 05:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `soing_count`
--

CREATE TABLE `soing_count` (
  `soing_count_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `soing_nung_total` varchar(11) DEFAULT NULL,
  `soing_weight_total` varchar(11) DEFAULT NULL,
  `soing_avl_weight_total` varchar(11) DEFAULT NULL,
  `soing_ghat_total` varchar(11) DEFAULT NULL,
  `soing_takavari` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `soing_count`
--

TRUNCATE TABLE `soing_count`;
--
-- Dumping data for table `soing_count`
--

INSERT INTO `soing_count` (`soing_count_id`, `kapan_id`, `soing_nung_total`, `soing_weight_total`, `soing_avl_weight_total`, `soing_ghat_total`, `soing_takavari`) VALUES
(1, 14, '', '', '', '', ''),
(2, 15, '', '', '', '', ''),
(3, 13, NULL, NULL, NULL, NULL, NULL),
(4, 16, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `takavari`
--

CREATE TABLE `takavari` (
  `takavari_id` int(11) NOT NULL,
  `kapan_id` int(11) NOT NULL,
  `t_kachi_takavari_1` varchar(11) DEFAULT NULL,
  `t_kachi_takavari_2` varchar(11) DEFAULT NULL,
  `t_kachi_takavari_3` varchar(11) DEFAULT NULL,
  `t_kachi_saij_1` varchar(11) DEFAULT NULL,
  `t_kachi_saij_2` varchar(11) DEFAULT NULL,
  `t_kachi_saij_3` varchar(11) DEFAULT NULL,
  `t_taiyar_taiytakavari_1` varchar(11) DEFAULT NULL,
  `t_taiyar_taiytakavari_2` varchar(11) DEFAULT NULL,
  `t_taiyar_taiytakavari_3` varchar(11) DEFAULT NULL,
  `t_taiyar_saij_1` varchar(11) DEFAULT NULL,
  `t_taiyar_saij_2` varchar(11) DEFAULT NULL,
  `t_taiyar_saij_3` varchar(11) DEFAULT NULL,
  `t_created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `t_modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `takavari`
--

TRUNCATE TABLE `takavari`;
--
-- Dumping data for table `takavari`
--

INSERT INTO `takavari` (`takavari_id`, `kapan_id`, `t_kachi_takavari_1`, `t_kachi_takavari_2`, `t_kachi_takavari_3`, `t_kachi_saij_1`, `t_kachi_saij_2`, `t_kachi_saij_3`, `t_taiyar_taiytakavari_1`, `t_taiyar_taiytakavari_2`, `t_taiyar_taiytakavari_3`, `t_taiyar_saij_1`, `t_taiyar_saij_2`, `t_taiyar_saij_3`, `t_created_date`, `t_modified_date`) VALUES
(7, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-23 07:13:44', '2018-01-30 14:50:11'),
(8, 14, '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-23 10:20:52', '2018-01-30 08:55:46'),
(9, 15, '', '', '', '', '', '', '', '', '', '', '', '', '2018-01-27 13:38:36', '2018-01-30 04:06:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`admin_log_id`),
  ADD KEY `FK_al_admin_user_id` (`admin_user_id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`admin_menu_id`);

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`admin_user_id`);

--
-- Indexes for table `admin_user_group`
--
ALTER TABLE `admin_user_group`
  ADD PRIMARY KEY (`admin_user_group_id`);

--
-- Indexes for table `admin_user_settings`
--
ALTER TABLE `admin_user_settings`
  ADD PRIMARY KEY (`admin_user_settings_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `email_campaign`
--
ALTER TABLE `email_campaign`
  ADD PRIMARY KEY (`email_campaign_id`);

--
-- Indexes for table `email_list`
--
ALTER TABLE `email_list`
  ADD PRIMARY KEY (`email_list_id`);

--
-- Indexes for table `email_send_history`
--
ALTER TABLE `email_send_history`
  ADD PRIMARY KEY (`email_send_id`);

--
-- Indexes for table `error_codes`
--
ALTER TABLE `error_codes`
  ADD PRIMARY KEY (`error_id`);

--
-- Indexes for table `image_size`
--
ALTER TABLE `image_size`
  ADD PRIMARY KEY (`image_size_id`);

--
-- Indexes for table `inventory_master_specifier`
--
ALTER TABLE `inventory_master_specifier`
  ADD PRIMARY KEY (`inventory_master_specifier_id`);

--
-- Indexes for table `inventory_type`
--
ALTER TABLE `inventory_type`
  ADD PRIMARY KEY (`inventory_type_id`);

--
-- Indexes for table `ip_locations`
--
ALTER TABLE `ip_locations`
  ADD PRIMARY KEY (`ip_locations_id`);

--
-- Indexes for table `kacho_number`
--
ALTER TABLE `kacho_number`
  ADD PRIMARY KEY (`kacho_number_id`),
  ADD KEY `kn_kapan_id` (`kapan_id`);

--
-- Indexes for table `kapan`
--
ALTER TABLE `kapan`
  ADD PRIMARY KEY (`kapan_id`);

--
-- Indexes for table `kapan_jama`
--
ALTER TABLE `kapan_jama`
  ADD PRIMARY KEY (`kapan_jama_id`),
  ADD KEY `FK_kj_kapan_id` (`kapan_id`);

--
-- Indexes for table `kapan_jama_count`
--
ALTER TABLE `kapan_jama_count`
  ADD PRIMARY KEY (`kapan_jama_count_id`),
  ADD KEY `FK_kjc_kapan_id` (`kapan_id`);

--
-- Indexes for table `lagadh`
--
ALTER TABLE `lagadh`
  ADD PRIMARY KEY (`lagadh_id`),
  ADD KEY `l_kapan_id` (`kapan_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`languages_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`logins_id`);

--
-- Indexes for table `login_ip`
--
ALTER TABLE `login_ip`
  ADD PRIMARY KEY (`login_ip_id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `module_manager`
--
ALTER TABLE `module_manager`
  ADD PRIMARY KEY (`module_manager_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `polish`
--
ALTER TABLE `polish`
  ADD PRIMARY KEY (`polish_id`),
  ADD KEY `p_kapan_id` (`kapan_id`);

--
-- Indexes for table `polish_count`
--
ALTER TABLE `polish_count`
  ADD PRIMARY KEY (`polish_count_id`),
  ADD KEY `fk_pc_kapan_id` (`kapan_id`);

--
-- Indexes for table `polish_repairing`
--
ALTER TABLE `polish_repairing`
  ADD PRIMARY KEY (`polish_repairing_id`),
  ADD KEY `fk_pr_kapan_id` (`kapan_id`);

--
-- Indexes for table `sabka`
--
ALTER TABLE `sabka`
  ADD PRIMARY KEY (`sabka_id`),
  ADD KEY `s_kapan_id` (`kapan_id`);

--
-- Indexes for table `sabka_count`
--
ALTER TABLE `sabka_count`
  ADD PRIMARY KEY (`sabka_count_id`),
  ADD KEY `FK_sc_k_id` (`kapan_id`);

--
-- Indexes for table `sarin`
--
ALTER TABLE `sarin`
  ADD PRIMARY KEY (`sarin_id`),
  ADD KEY `s_kapan_id` (`kapan_id`);

--
-- Indexes for table `sarin_count`
--
ALTER TABLE `sarin_count`
  ADD PRIMARY KEY (`sarin_count_id`),
  ADD KEY `sc_kapan_id` (`kapan_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sessions_id`);

--
-- Indexes for table `site_config`
--
ALTER TABLE `site_config`
  ADD PRIMARY KEY (`site_config_id`);

--
-- Indexes for table `soing`
--
ALTER TABLE `soing`
  ADD PRIMARY KEY (`soing_id`),
  ADD KEY `s_kapan_id` (`kapan_id`);

--
-- Indexes for table `soing_count`
--
ALTER TABLE `soing_count`
  ADD PRIMARY KEY (`soing_count_id`),
  ADD KEY `FK_sc_k_id` (`kapan_id`);

--
-- Indexes for table `takavari`
--
ALTER TABLE `takavari`
  ADD PRIMARY KEY (`takavari_id`),
  ADD KEY `FK_t_kapan_id` (`kapan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `admin_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=636;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `admin_menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `admin_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `admin_user_group`
--
ALTER TABLE `admin_user_group`
  MODIFY `admin_user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `admin_user_settings`
--
ALTER TABLE `admin_user_settings`
  MODIFY `admin_user_settings_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `email_campaign`
--
ALTER TABLE `email_campaign`
  MODIFY `email_campaign_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_list`
--
ALTER TABLE `email_list`
  MODIFY `email_list_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `error_codes`
--
ALTER TABLE `error_codes`
  MODIFY `error_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `image_size`
--
ALTER TABLE `image_size`
  MODIFY `image_size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `inventory_master_specifier`
--
ALTER TABLE `inventory_master_specifier`
  MODIFY `inventory_master_specifier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory_type`
--
ALTER TABLE `inventory_type`
  MODIFY `inventory_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_locations`
--
ALTER TABLE `ip_locations`
  MODIFY `ip_locations_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kacho_number`
--
ALTER TABLE `kacho_number`
  MODIFY `kacho_number_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `kapan`
--
ALTER TABLE `kapan`
  MODIFY `kapan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `kapan_jama`
--
ALTER TABLE `kapan_jama`
  MODIFY `kapan_jama_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `kapan_jama_count`
--
ALTER TABLE `kapan_jama_count`
  MODIFY `kapan_jama_count_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lagadh`
--
ALTER TABLE `lagadh`
  MODIFY `lagadh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `languages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `logins_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `login_ip`
--
ALTER TABLE `login_ip`
  MODIFY `login_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `module_manager`
--
ALTER TABLE `module_manager`
  MODIFY `module_manager_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3537;

--
-- AUTO_INCREMENT for table `polish`
--
ALTER TABLE `polish`
  MODIFY `polish_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1627;

--
-- AUTO_INCREMENT for table `polish_count`
--
ALTER TABLE `polish_count`
  MODIFY `polish_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `polish_repairing`
--
ALTER TABLE `polish_repairing`
  MODIFY `polish_repairing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sabka`
--
ALTER TABLE `sabka`
  MODIFY `sabka_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `sabka_count`
--
ALTER TABLE `sabka_count`
  MODIFY `sabka_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sarin`
--
ALTER TABLE `sarin`
  MODIFY `sarin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2734;

--
-- AUTO_INCREMENT for table `sarin_count`
--
ALTER TABLE `sarin_count`
  MODIFY `sarin_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `sessions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `site_config`
--
ALTER TABLE `site_config`
  MODIFY `site_config_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `soing`
--
ALTER TABLE `soing`
  MODIFY `soing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `soing_count`
--
ALTER TABLE `soing_count`
  MODIFY `soing_count_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `takavari`
--
ALTER TABLE `takavari`
  MODIFY `takavari_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kacho_number`
--
ALTER TABLE `kacho_number`
  ADD CONSTRAINT `FK_kn_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `kapan_jama`
--
ALTER TABLE `kapan_jama`
  ADD CONSTRAINT `FK_kj_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `lagadh`
--
ALTER TABLE `lagadh`
  ADD CONSTRAINT `FK_l_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `polish`
--
ALTER TABLE `polish`
  ADD CONSTRAINT `FK_p_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `polish_count`
--
ALTER TABLE `polish_count`
  ADD CONSTRAINT `FK_pc_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `polish_repairing`
--
ALTER TABLE `polish_repairing`
  ADD CONSTRAINT `FK_pr_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `sarin`
--
ALTER TABLE `sarin`
  ADD CONSTRAINT `FK_s_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);

--
-- Constraints for table `sarin_count`
--
ALTER TABLE `sarin_count`
  ADD CONSTRAINT `FK_sc_kapan_id` FOREIGN KEY (`kapan_id`) REFERENCES `kapan` (`kapan_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
